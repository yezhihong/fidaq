########################################################################################################################
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
########################################################################################################################

import numpy as np


class CCData:
    def __init__(self, num_sweep_dim_indices=1, numBins=13, numViews=1, numRows=24, numCols=36):
        """
        data object for CC collection data
        TODO: params should be pulled from experiment params
        """
        #####################################################################################
        #   create array for collecting coincidence counts from data buffer following capture
        cc_data = np.zeros((num_sweep_dim_indices, numBins, numViews, numRows, numCols), dtype='int')
        time_data = np.zeros(numViews, dtype=float)

        self.info = {}
        self.params = {}
        self.data = {'cc_data': cc_data, 'time': time_data}
