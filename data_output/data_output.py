########################################################################################################################
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
########################################################################################################################

import os
import json
import logging
import numpy as np
from scipy import io as sio

import globals

from DDC0864.dmFunctions import readAsicTemp, getHpf, getLftMode, getModeBinAlign, maskAsicList, getAcsMode, \
    readDmTempMon
from DDC0864.misc_chain import multiSensorChainsToVals

from cc_data_struct import CCData


logger = logging.getLogger(__name__)


def prepare_output_data(baseLine, experiment, activeAsicNums, spectral_sweep=False):
    """ instantiate data structure objects for each active asic
    initiate 'params' and 'info' from baseline
    """
    if spectral_sweep:
        num_sweep_dim_indices = len(range(baseLine.experimentDict[experiment]['startThresh'],
                                  baseLine.experimentDict[experiment]['endThresh'])) + 1
    else:
        num_sweep_dim_indices = len(baseLine.experimentDict[experiment]['tubeCurrentList'])

    test_type = baseLine.testType
    numViews = int(
        baseLine.experimentDict[experiment]['totTime'] / baseLine.experimentDict[experiment]['timeResolution'])
		
    asic_data_structs = []
    for active_asic in activeAsicNums:
        cc_data = CCData(num_sweep_dim_indices=num_sweep_dim_indices, numBins=13, numViews=numViews)
        cc_data.params = baseLine.experimentDict[experiment]
        cc_data.params['source'] = baseLine.experiment['SOURCE']
        if test_type == globals.testTypeDict['Sensor']: # Needed to get the CZT ID for sensor screening. Might need to pass in as argument in future
            cc_data.info['sensor'] = baseLine.sensor['CZTID'][active_asic]
        else:
            cc_data.info['sensor'] = baseLine.sensor['IDLIST'][active_asic]
        cc_data.info['site'] = baseLine.site['NAME']
        passiveAsicNums = baseLine.sensor['PASSIVE_ASIC_NUMS']
        [tempCodeTmp, asicTempTmp, cztTempTmp] = readAsicTemp(True, False)
        logger.info('While saving data, Temperature Reading for ASIC-%s: tempCode=%s, ASIC_Temp=%s, CZT_Temp=%s '%(baseLine.sensor['IDLIST'][active_asic],tempCodeTmp, asicTempTmp, cztTempTmp))
        cc_data.info['tempCode'] = tempCodeTmp
        cc_data.info['ASIC_Temp'] = asicTempTmp
        cc_data.info['CZT_Temp'] = cztTempTmp
        # get bar temperature
        cc_data.info['monTemp1'], cc_data.info['monTemp2'] = readDmTempMon(['MON_TEMP_1', 'MON_TEMP_2'])	
        cc_data.info['Purpose'] = globals.textModeSettings['Purpose']
        
        asic_data_structs.append(cc_data)
    return asic_data_structs


def prepare_output_paths(baseLine, experiment, activeAsicNums, device_ID, spectral_sweep=False, extra_append=''):
    """ create output folders and file names for each active asic """
    test_type = baseLine.testType
    output_file_names = []
    for active_asic in activeAsicNums:
        ############################################
        # check/make directory for current test type
        root_output_dir = os.path.join(globals.ldir, baseLine.site['NAME'], globals.DATA_OUTPUT_DIR,
                                       device_ID)
        dest_output_dir = os.path.join(globals.tdir, baseLine.site['NAME'], globals.DATA_OUTPUT_DIR)
        current_output_dir = os.path.join(root_output_dir,
                                          baseLine.experimentDict[experiment]['test_type'])
        # if not os.path.exists(dest_output_dir):
        #     os.makedirs(dest_output_dir)
        if not os.path.exists(current_output_dir):
            os.makedirs(current_output_dir)

        ############################################
        #   set file name per asic
        if spectral_sweep:
            output_file_name = os.path.join(baseLine.experimentDict[experiment]['test_type'],
                                            '{}_{}_{}_{}{}_{}'.format(baseLine.experiment['SOURCE'],
                                                                      experiment,
                                                                      baseLine.experimentDict[experiment]['hvList'][0],
                                                                      baseLine.sensor['IDLIST'][active_asic],
                                                                      extra_append,
                                                                      baseLine.timestamp))

        else:
            if test_type == globals.testTypeDict['Sensor']:
                output_file_name = os.path.join(baseLine.experimentDict[experiment]['test_type'],
                                                '{}_{}{}_{}'.format(experiment,   # change from ['test_type']
                                                                    baseLine.sensor['CZTID'][active_asic],
                                                                    extra_append,
                                                                    baseLine.timestamp))
            else:
                output_file_name = os.path.join(baseLine.experimentDict[experiment]['test_type'],
                                                '{}_{}{}_{}'.format(experiment,   # change from ['test_type']
                                                                    baseLine.sensor['IDLIST'][active_asic],
                                                                    extra_append,
                                                                    baseLine.timestamp))
        output_file_names.append(output_file_name)
    return output_file_names, root_output_dir  # dest_output_dir


def save_output_data(activeAsicNums, asic_data_structs, output_file_names, root_output_dir):
    """ save test data in *.mat format """
    for asic in range(len(activeAsicNums)):
        current_file_path = os.path.join(root_output_dir, output_file_names[asic])
        struct_wrapper = {'cc_struct': asic_data_structs[asic]}
        sio.savemat(current_file_path, struct_wrapper, appendmat=True, format='5')


def prepare_output_summary(test_suite_file_summary, baseLine, experiment, mat_file_names):
    test_suite_file_summary['test_type'].append(baseLine.experimentDict[experiment]['test_type'])
    test_suite_file_summary['output_file_list'].append(mat_file_names)


def save_output_summary(test_suite_file_summary, root_output_dir, baseLine):
    summary_file_name = os.path.join(root_output_dir,
                                     'TEST_SUITE_SUMMARY_{}.json'.format(baseLine.timestamp))
    with open(summary_file_name, 'w') as out_file:
        json.dump(test_suite_file_summary, out_file)


def bin2array(asic_data_structs, sweep_dim_index, data_bufList, timeResolution, accumData=1, Ncounters=13):
    """
    Converts buffer output into bytes separated into four arrays (2 AICSs x 2 lanes)
    Data is inserted into asic_data_structs array
    """
    num_views = np.size(asic_data_structs[0].data['cc_data'], axis=2)

    for idx, module in enumerate(globals.detector.module_serials):
        if module is None or module == '':
            continue
        if data_bufList[idx] == '[]' or data_bufList[idx] == []:
            continue

        data_array = globals.buffer.parse_views(data_bufList[idx], num_views, 1, Ncounters, accumData)

        asic_data_structs[0].data['cc_data'][sweep_dim_index] = data_array[:, 0, ...].transpose((3, 0, 1, 2))
        asic_data_structs[1].data['cc_data'][sweep_dim_index] = data_array[:, 1, ...].transpose((3, 0, 1, 2))

        # asic_data_structs[0].data['time'][viewCount - 1] = elapsedTime
        # asic_data_structs[1].data['time'][viewCount - 1] = elapsedTime


def get_misc_params(activeAsicNums, passiveAsicNums, maxNumAsics, VER_ID, tdir):
    """
    procedure to collect miscelaneaous data previously collected iteratively during data capture routines
    TODO: INCOMPLETE
    TODO: remove and have values populated at creation time as members of experiment object
    """
    misc_params = {}
    # get bar temperature
    misc_params['monTemp1'], misc_params['monTemp2'] = readDmTempMon(['MON_TEMP_1', 'MON_TEMP_2'])

    # get ASIC temperature
    [tempCode, asicTemp, cztTemp] = [[[]] * maxNumAsics, [[]] * maxNumAsics, [[]] * maxNumAsics]
    for asic in activeAsicNums:
        maskAsicList(activeAsicNums, passiveAsicNums, maskAllOthersOff=1, asicSingle=asic)
        [tempCodeTmp, asicTempTmp, cztTempTmp] = readAsicTemp(True, False)
        [tempCode[asic], asicTemp[asic], cztTemp[asic]] = [tempCodeTmp, asicTempTmp, cztTempTmp]
    maskAsicList(activeAsicNums, passiveAsicNums, disableTestPattern=1)

    misc_params['HPF_Mhz'] = getHpf(VER_ID)
    misc_params['acs_mode'] = getAcsMode()
    misc_params['lft_mode'] = getLftMode()
    misc_params['mode_bin_align'] = getModeBinAlign()

    [addCapList, td_trim_feList, td_trim_beList, deadTimeList, adcOffsetList, threshList, tL0, tL1, tL2,
     tL3, tL4, tL5] = multiSensorChainsToVals(tdir, VER_ID, activeAsicNums, passiveAsicNums)

    return misc_params
