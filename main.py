########################################################################################################################
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
########################################################################################################################

import os
import sys
import Queue
import shutil
import logging
import threading
from time import strftime, time
from Tkinter import Tk, Message, StringVar
from colorama import Fore, Style
from easygui import ynbox, ccbox
import datetime # bsb 2019.07.25 for MM acquisition t_0

import globals

from DDC0864.cal_lib import checkForEnergyCal, loadElectricalCal
from DDC0864.dmFunctions import maskAsicList, readAsicGlobals, readAsicTemp, selDataOutputByMM, updateMessage, storeRawData, saveLog
from DDC0864.dmInitDDC0864 import initDDC0864
from DDC0864.misc_chain import readPerPixCal
from DDC0864.testProcedures import genSweep, getUserInput, thresholdSweep, check_isotope, \
    energy_cal_filename
from DDC0864.thresh_chain import readPerPixThresh
from DDC0864.verifications.global_registers.global_reg_verification import verify_asic_global_registers

import dmBuffer.DMBufferBringUp as DMBB
from dmBuffer import DM

from instrumentation.hvSuppDynam import getHV
from instrumentation.initInstruments import initInst, closeInst

from data_analysis.pcct_analysis import pcct_analysis
from data_output.data_output import prepare_output_data, prepare_output_paths, prepare_output_summary, \
    save_output_data, save_output_summary

from config_interface.config_constants import TEST_TYPE_TITLES

import pcct
from Load_Input import Load_Input ##new input function added by Z. Ye on 07/18/2019

try:
    from DDC0864.calibrations.electrical_cal import chkElectricalCal
    electrical_cal_enabled = True
except ImportError:
    electrical_cal_enabled = False

try:
    from DDC0864.calibrations.energy_cal import energyCalibration
    energy_cal_enabled = True
except ImportError:
    energy_cal_enabled = False


logger = logging.getLogger(__name__)


def main(cm, pa, fw, slQueue, shQueue, w, messageText):

    # shQueue.put('open');

    # *** INITIALIZATION ***
    ##This new section was added by Z. Ye on 07/18/2019
    if len(sys.argv)>1:
        inputfile_name = sys.argv[1]
        if os.path.exists(inputfile_name):
            globals.enableTestSuiteGui = False
            globals.enableTextMode = True
            Load_Input(inputfile_name)
        else:
            raise RuntimeError('*** The input file does NOT exist! *** ')            
    else:
        globals.enableTextMode = False
        globals.enableTestSuiteGui = True
    ##End new section

    baseLine, experimentList = getUserInput(pcct.detector_types())

    if not experimentList:
        raise RuntimeError('There must be at least one experiment in the experiment list')

    baseLine.experiment['EXPERIMENT_LIST'] = experimentList

    VER_ID = baseLine.sensor['ASICREVISION']
    bLine = baseLine.params['EXPERIMENT']['BASELINE']
    activeAsicNums = baseLine.sensor['ACTIVE_ASIC_NUMS']
    passiveAsicNums = baseLine.sensor['PASSIVE_ASIC_NUMS']

    # Setup Directory Paths
    tdir = '{}\{}'.format(globals.tdir, baseLine.site['NAME'])  # server directory
    ldir = '{}\{}'.format(globals.ldir, baseLine.site['NAME'])  # local directory

    # Remove any files in multiFileAppend directory
    fList = os.listdir(os.path.join(ldir, globals.scratchDir))
    if fList:
        for f in fList:
            filePath = os.path.join(ldir, globals.scratchDir, f)
            try:
                os.remove(filePath)
            except (OSError, IOError) as e:
                logger.error('Unable to remove {}\n{}'.format(filePath, e.message))

    # Before the experiment the logger writes to the the default location, this function moves it to the experiment
    # folder by changing the file handler and moving the log file.
    log_file_path = pcct.move_log_file(os.path.join(ldir, globals.scratchDir))

    ###########################################################
    #   add timestamp to baseline marking entire test suite
    #                                               AD 2018-11-13
    baseLine.timestamp = strftime('%Y_%m_%d__%H_%M_%S')
    #
    ###########################################################

    slQueue.put(['red', 'flashing'])
    slQueue.put(['green', 'off'])


    ## Added by Z. Ye on 07/22/2019, to give a feature of changing IP address from the input files,
    ### instead of hard-codeing in dmBuffer/DMBufferStartUp.py
    if globals.enableTextMode:
        buffer = DMBB.startBuffer(15, globals.textModeSettings['IPAddress'], globals.textModeSettings['IPPort'])
    else:
        buffer = DMBB.startBuffer(15)


    detector_cls = baseLine.dm_info['DM_TYPE']
    detector_serial = baseLine.dm_info['SERIAL_NUM']
    module_serials = [module or None for module in baseLine.dm_info['MODULES']]

    detector = detector_cls(buffer, detector_serial, module_serials, baseLine.sensor['PASSIVE_ASIC_NUMS'])

    # Assign global class instances
    globals.buffer = buffer
    globals.detector = detector
    baseLine.dm = detector

    hv = getHV(detector)

    detector.reset()

    initDDC0864(baseLine.baseLineDict[bLine]['modeList'][0], baseLine.baseLineDict[bLine]['hpfList'][0], activeAsicNums,
                baseLine.experiment['GAIN'], site=baseLine.params['SITE']['NAME'])
    detector.setup()

    experiment_start_time = time()
    t0 = time()
    tEcal = time()
    # *** TEST AREA ***

    # # Check for electrical calibrations
    asicsPerMM = baseLine.dm.N_ASICS_PER_MODULE

    if electrical_cal_enabled:
        for asic in activeAsicNums:
            selDataOutputByMM(asic // asicsPerMM, asicsPerMM)
            logger.info('performing electrical calibrations for {}'.format(baseLine.sensor['IDLIST'][asic]))
            maskAsicList(activeAsicNums, passiveAsicNums, maskAllOthersOff=1, asicSingle=asic)
            chkElectricalCal(ldir, baseLine, asic, msgWindow, messageText)

        # Re-initialize after electrical calibration - TI recommendation
        initDDC0864(baseLine.baseLineDict[bLine]['modeList'][0], baseLine.baseLineDict[bLine]['hpfList'][0],
                    activeAsicNums, baseLine.experiment['GAIN'], site=baseLine.params['SITE']['NAME'])

    # flag indicate that all ASICs pass register verification
    passed_global_reg_verification = True

    # path to store dumped asic register files, better in env variable or settings
    dump_dir = globals.DUMP_DIR
    for asic in activeAsicNums:
        # To iterate ASICs' register, comparing to TI reference
        maskAsicList(activeAsicNums, passiveAsicNums, maskAllOthersOff=1, asicSingle=asic)
        if globals.DO_GLOBAL_REGISTER_VERIFICATION:
            # if any ASIC failed the verification, mark the flag to false
            if not verify_asic_global_registers(asic, dump_dir):
                passed_global_reg_verification = False

        updateMessage(messageText, msgWindow, text='Loading Electrical Calibrations for {}'.format(baseLine.sensor['IDLIST'][asic]))
        # Todo: Make into a function that can find calibrations by parameter
        calibration_fn = 'cal_vals_%s_Act2TC_%dns_DT_%dns_QT_%d_B.csv' % (baseLine.sensor['IDLIST'][asic],
                                                                          int(baseLine.baseLineDict[bLine]['active2TC'] / 1e-9),
                                                                          int(baseLine.baseLineDict[bLine]['deadTimeExpList'][0] / 1e-9),
                                                                          baseLine.baseLineDict[bLine]['qtrigExpList'][0])
        calibration_dir = os.path.join(ldir, globals.calsDir, baseLine.sensor['IDLIST'][asic])
        maskAsicList(activeAsicNums, passiveAsicNums, maskAllOthersOff=1, asicSingle=asic)
        loadElectricalCal(ldir, baseLine.sensor['IDLIST'][asic],
                          calFile=os.path.join(calibration_dir, calibration_fn))
        readPerPixCal(ldir, int(baseLine.sensor['ASICREVISION']),
                      miscReadValsFile="calibration_read_vals_{}.csv".format(baseLine.sensor['IDLIST'][asic]))
    
    for asic in activeAsicNums:
        # To iterate ASICs' register, comparing to TI reference
        ## Redlen suggested me to mask the asic by using this line, if it gives trouble, comment out., ZYE -08/23/2019
        maskAsicList(activeAsicNums, passiveAsicNums, maskAllOthersOff=1, asicSingle=asic)
         ## Get ASIC temperature, added by Z. Ye on 07/17/2019
        [tempCodeTmp, asicTempTmp, cztTempTmp] = readAsicTemp(True, True)
        logger.info('First Temperature Reading for ASIC-%s: tempCode=%s, ASIC_Temp=%s, CZT_Temp=%s '%(baseLine.sensor['IDLIST'][asic],tempCodeTmp, asicTempTmp, cztTempTmp))
        ## End temperature section

    logger.info('Electrical Calibrations Completed in %0.1fs' % (time() - tEcal))
    maskAsicList(activeAsicNums, passiveAsicNums, disableTestPattern=1)     # Enable all active ASICs
    testFolder = None

    current_isotope = None
    current_isotope_mm = None

    # if failed verification, warn the user and indicate the path to check for details
    if not passed_global_reg_verification:
        logger.error("ASICs' register values are NOT identical to TI reference,please check {} for detail results."
                     .format(dump_dir))

    try:
    
        ##Add a button to conform before acquring data, Z. Ye 07/29/2019
        Ready2GO= ccbox(title='Ready to GO? ',
                msg='Please click Continue to start data capture',
                choices=['C[o]ntinue', 'C[a]ncel'], default_choice='Continue', cancel_choice='Cancel')
        if not Ready2GO:
            raise Exception('User terminated the test')
    
        ##Modified to make sure it only check at the very beginning of the experiment, Z. Ye 07/29/2019
        XRayNotChecked=True
        for mm in [moduleNum for moduleNum, moduleName in enumerate(baseLine.dm_info['MODULES']) if moduleName]:
            backupAsicActiveNums = baseLine.sensor['ACTIVE_ASIC_NUMS']      # Saves the single ASIC for sensor screener
            baseLine.sensor['ACTIVE_ASIC_NUMS'] = [mm * asicsPerMM + i for i in range(asicsPerMM)]  # Activates the full MM for sensor screener since data_output is done in pairs
            activeAsicNums = baseLine.sensor['ACTIVE_ASIC_NUMS']

            expFiles = []

            ########################################################################################
            #   create dictionary of lists to collect summary of array data file/locations
            #                                                                           AD-2018-11-03
            if baseLine.testType == globals.testTypeDict['Sensor']:
                device_ID = baseLine.sensor['CZTID'][detector.YELLOW_BOX_ACTIVE_ASICS[0]]
            else:
                device_ID = baseLine.dm_info['MODULES'][mm] # from user prompt dialog
            test_suite_file_summary = {'test_type': [],
                                       'output_file_list': [],
                                       'device': device_ID,
                                       'device_manufacturer': baseLine.dm_info['MM_TYPE']}
            #
            ########################################################################################
            output_file_names, root_output_dir = None, None

            for experiment_name in experimentList:

                experiment = baseLine.experimentDict.get(experiment_name)
                if experiment is None:
                    raise ValueError('Invalid experiment: {}'.format(experiment_name))

                if 'xray' not in experiment['photon_source']:
                    isotope = experiment['photon_source']

                    if globals.check_isotope and (current_isotope != isotope or current_isotope_mm != mm):
                        slQueue.put(['yellow', 'flashing'])
                        hv.setHvState('off')
                        check_isotope(ldir, baseLine, isotope, mm)
                        slQueue.put(['yellow', 'off'])

                        current_isotope = isotope
                        current_isotope_mm = mm
                else:
                    if globals.check_isotope and current_isotope is not None:
                        slQueue.put(['yellow', 'flashing'])
                        hv.setHvState('off')
                        check_isotope(ldir, baseLine, None, mm)
                        slQueue.put(['yellow', 'off'])

                        current_isotope = None
                        current_isotope_mm = None

                runs = experiment.get('runs', 1) or 1

                # get time of acquisition - bsb 2019.07.25
                now = datetime.datetime.now()
                t = now.strftime("%Y-%m-%d %H:%M:%S")
                f = open(globals.textModeSettings['DataDir']+"log.txt", "a+")
                f.write("T0_%s_MM%s: %s\n" % (globals.textModeSettings['DMID'], mm, t))
                f.close()
                
                for run in range(1, runs + 1):
                    

                    message = '{} on ASIC {} in progress'.format(TEST_TYPE_TITLES.get(experiment['test_type'], 'Experiment'),  baseLine.sensor['ACTIVE_ASIC_NUMS'])
                    message += ' (Run {} of {})'.format(run, runs) if runs > 1 else ''
                    file_name_append = '_Run{:03}'.format(run) if runs > 1 else ''

                    updateMessage(messageText, w, text=message)

                    if experiment['test_type'] in ['DYNAMIC', 'STABILITY', 'UNIFORMITY']:
                        fw.filter = (experiment['filter_type'], experiment['filter_thickness'])

                        # Open shutter for all tests except dynamic response (which requires PWM control)
                        shQueue.put('open' if experiment['test_type'] != 'DYNAMIC' else 'close')

                    elif experiment['test_type'] == 'ENERGY_CAL':
                        if energy_cal_enabled:
                            hvList = baseLine.get_unique_hv_list(experimentList)
                            for bias in hvList:
                                experiment['hvList'] = [bias]
                                ecal_filename = energy_cal_filename(experiment)
                                energyCalExists = checkForEnergyCal(ldir, baseLine, ecal_filename)
                                useExistingEnergyCal = False
                                if energyCalExists:
                                    useExistingEnergyCal = ynbox(msg='Do you want to use the\nexisting energy calibration for {}V bias?'.format(bias),
                                                                 title='Existing Energy Calibration', choices=['Yes', 'No'],
                                                                 default_choice='No')
                                if not useExistingEnergyCal:
                                    updateMessage(messageText, w, text='{}V bias K-edge energy calibration starting'.format(bias))
                                    logger.info('{}V bias K-edge energy calibration starting'.format(bias))
                                    shQueue.put('open')
                                    root_output_dir = energyCalibration(cm, hv, fw, ldir, test_suite_file_summary,
                                                                        baseLine, experiment_name, file_name_append)
                                    save_output_summary(test_suite_file_summary, root_output_dir, baseLine)
                            continue
                        else:
                            raise RuntimeError('Cannot find energy calibration module')

                    elif experiment['test_type'] == 'SPECTRUM':
                        shQueue.put('open')
                        sweepType = 'closed-bin'

                        if 'xray' not in experiment['photon_source']:
                            #pass
                            fw.home()
                        else:
                            #pass
                            fw.filter = (experiment['filter_type'], experiment['filter_thickness'])

                        root_output_dir = thresholdSweep(cm, hv, fw, shQueue, ldir, test_suite_file_summary,
                                                             VER_ID, baseLine,experiment_name, sweepType, file_name_append)
                        save_output_summary(test_suite_file_summary, root_output_dir, baseLine)

                        continue

                    else:
                        raise ValueError('Invalid experiment test type: {}'.format(experiment['test_type']))

                    baseLine.experiment['NAME'] = experiment_name
                    baseLine.experiment['SOURCE'] = 'xray'
                    energyCalibrated = baseLine.experimentDict[experiment_name]['energy_cal']
                    ##Modified to make sure it only check at the very beginning of the experiment
                    ##It could be a old bug in the code that always ask to check even it has been arm, Z. Ye 07/29/2019
                    #if 'xray' in baseLine.experiment['SOURCE'].lower():
                    if 'xray' in baseLine.experiment['SOURCE'].lower() and XRayNotChecked:
                        cm.checkXrayArm()
                        XRayNotChecked=False

                    ## Get ASIC temperature, added by Z. Ye on 07/17/2019
                    for asic in activeAsicNums:
                        ## Redlen suggested me to mask the asic by using this line, if it gives trouble, comment out., ZYE -08/23/2019
                        maskAsicList(activeAsicNums, passiveAsicNums, maskAllOthersOff=1, asicSingle=asic)
                        [tempCodeTmp, asicTempTmp, cztTempTmp] = readAsicTemp(True, False)
                        logger.info('Before Saving Data for Experiment <%s>, Temperature Reading for ASIC-%s: tempCode=%s, ASIC_Temp=%s, CZT_Temp=%s '%(experiment['test_type'], baseLine.sensor['IDLIST'][asic],tempCodeTmp, asicTempTmp, cztTempTmp))
                    
                    ## Redlen suggested me to mask the asic by using this line, if it gives trouble, comment out., ZYE -08/23/2019
                    maskAsicList(activeAsicNums, passiveAsicNums, disableTestPattern=1)     # Enable all active ASICs
                    ##End temp. section
     
                    ##########################################################################
                    #   insert data array structure functionality parallel to csv creation
                    #                                                           AD 2018-10-31
                    asic_data_structs = prepare_output_data(baseLine, experiment_name, activeAsicNums)
                    if baseLine.testType == globals.testTypeDict['Sensor']:         # Hack to get the sensor screener working
                        baseLine.sensor['ACTIVE_ASIC_NUMS'] = backupAsicActiveNums
                        activeAsicNums = backupAsicActiveNums

                    output_file_names, root_output_dir = prepare_output_paths(baseLine, experiment_name,
                                                                              activeAsicNums,
                                                                              device_ID, extra_append=file_name_append)
                    #
                    ##########################################################################

                    sweepFiles = genSweep(asic_data_structs, cm, hv, pa, shQueue, ldir, baseLine, experiment_name, energyCalibrated=energyCalibrated)
                    expFiles.append(sweepFiles)
                    ##########################################################################
                    #   save data array structures after data capture
                    #                                                           AD 2018-10-31
                    logger.info('Saving array data')
                    if baseLine.testType == globals.testTypeDict['Sensor']:  # Hack to get the sensor screener working
                        asic_data_structs.pop(passiveAsicNums[0] % asicsPerMM)
                    save_output_data(activeAsicNums, asic_data_structs, output_file_names, root_output_dir)

                    # Save summary file of test types and data locations
                    prepare_output_summary(test_suite_file_summary, baseLine, experiment_name, output_file_names)
                    save_output_summary(test_suite_file_summary, root_output_dir, baseLine)
                    #
                    ##########################################################################

                logger.info('Experiment complete in {} s'.format(time() - t0))

            ########################################################################
            #   start data_analysis
            #                                                          AD 2018-11-07
            if globals.run_analysis:
                logger.info('Running in-line analysis')

                try:
                    pcct_analysis(root_dir=root_output_dir, 
                    auto_load=True, summary_dict=test_suite_file_summary)
                except:
                    logger.warn('In-line analysis failed: try running as stand-alone')
            #
            ########################################################################

            ## Get ASIC temperature, added by Z. Ye on 07/17/2019
            for asic in activeAsicNums:
                ## Redlen suggested me to mask the asic by using this line, if it gives trouble, comment out., ZYE -08/23/2019
                maskAsicList(activeAsicNums, passiveAsicNums, maskAllOthersOff=1, asicSingle=asic)
                [tempCodeTmp, asicTempTmp, cztTempTmp] = readAsicTemp(True, False)
                logger.info('Last Temperature Reading for ASIC-%s: tempCode=%s, ASIC_Temp=%s, CZT_Temp=%s '%(baseLine.sensor['IDLIST'][asic],tempCodeTmp, asicTempTmp, cztTempTmp))
            ## Redlen suggested me to mask the asic by using this line, if it gives trouble, comment out., ZYE -08/23/2019
            maskAsicList(activeAsicNums, passiveAsicNums, disableTestPattern=1)     # Enable all active ASICs
            ##End temp. section
 
            logger.info('Shutting off X-ray')
            cm.shutterControl('off')

            updateMessage(messageText, w, text='Testing completed. Transferring files')
            additionalFiles = []
            for asic in activeAsicNums:
                maskAsicList(activeAsicNums, passiveAsicNums, maskAllOthersOff=1, asicSingle=asic)
                regDumpFile = readAsicGlobals(ldir,'S%s_%s' % (baseLine.sensor['IDLIST'][asic],
                                                               strftime('%Y_%m_%d__%H_%M_%S')))
                additionalFiles.append(regDumpFile)
                shutil.copy(regDumpFile, os.path.join(ldir, globals.scratchDir, os.path.basename(regDumpFile)))
                miscFile = readPerPixCal(ldir, VER_ID,
                              miscReadValsFile=os.path.join(globals.chainToScratch, r'misc_chain_read_vals.csv'),
                              sensorId=baseLine.sensor['IDLIST'][asic])
                additionalFiles.append(miscFile)
                threshfile = readPerPixThresh(ldir,
                                 threshReadValsFile=os.path.join(globals.chainToScratch, r'thresh_chain_read_vals.csv'),
                                 sensorId=baseLine.sensor['IDLIST'][asic])
                additionalFiles.append(threshfile)
           
            maskAsicList(activeAsicNums, passiveAsicNums, disableTestPattern=1)  # Enable all active ASICs

            testFolder = storeRawData(ldir, root_output_dir, baseLine, mm, additionalFiles, testFolder)
    except Exception:
        raise
    finally:
        hv.setHvState('off')
        hv.setHvVoltage(0)
        hv.closeHv()

    testTime = time() - experiment_start_time
    maskAsicList(activeAsicNums, passiveAsicNums, disableTestPattern=1)  # Enable all active ASICs
    logger.info('Experiment Finished, total elapsed time = %d Hours, %d Mins' %
                (int(testTime//3600), int(round((testTime % 3600)/60.0))))

    # *** Experiment Clean-up ***
    logger.info('Closing Connections')
    logger.debug('Returning ASICs to reset and closing DM Buffer connection')
    DM.SPI_write('0x0000f', '0xFFFF')
    globals.buffer.stop()
    if testFolder:
        saveLog(ldir, testFolder, log_file_path, baseLine, None)


if __name__ == '__main__':
    pcct.config_logging()
    pcct.load_packages()

    master = Tk()
    master.wm_attributes("-topmost", 1)
    master.title(globals.versionString)
    messageText = StringVar()
    messageText.set('Initializing Instruments')
    msgWindow = Message(master, textvariable=messageText, width=800, font=("Helvetica", "24", "bold"))
    msgWindow.pack()
    msgWindow.update()

    cm, pa, sh, fw = initInst(xRayStabTime=60)
    slQueue = Queue.Queue(0)
    slCtrl = threading.Thread(name='stackLights', target=sh.stackLights, args=(slQueue,))
    slCtrl.start()
    slQueue.put(['green', 'on'])
    shQueue = Queue.Queue(0)
    shCtrl = threading.Thread(name='shutter', target=sh.shutter.shCtrl, args=(shQueue,))
    shCtrl.start()
    shQueue.put('open')\


    try:
        main(cm, pa, fw, slQueue, shQueue, msgWindow, messageText)
    except Exception:
        slQueue.put(['yellow', 'flashing'])
        exc_type, exc_value, exc_traceback = sys.exc_info()
        logger.critical('Unhandled exception', exc_info=(exc_type, exc_value, exc_traceback))
        logger.warn('program has terminated abnormally')
        cm.setTubeCurrent(0)
        cm.shutterControl('off')
        logger.warn('high voltage bias shut off')
        raw_input(Fore.RED + 'Press enter to continue to shut down\n' + Style.RESET_ALL)
        slQueue.put(['stop', 'stop'])
        slCtrl.join()
        shQueue.put('end')
        shCtrl.join()
        closeInst(cm, pa, sh, fw)
        sys.exit(1)
    slQueue.put(['stop', 'stop'])
    #slQueue.join()
    slCtrl.join()
    shQueue.put('close')
    shQueue.put('end')
    #shQueue.join()
    shCtrl.join()
    logger.info('Shutter control thread closed successfully')
    fw.home()
    closeInst(cm, pa, sh, fw)
    sys.exit(0)
