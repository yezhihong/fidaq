########################################################################################################################
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
########################################################################################################################

import socket
import struct
import logging
import numpy as np
from time import time, sleep

from pcct.ddc0864 import N_PIXELS, N_ROWS, N_COLUMNS
from pcct.utils import assert_width, chunk_str, pack_u16_le, unpack_u16_le

from .errors import BufferTimeoutError
from .protobuf import buffer_pb2 as pb


logger = logging.getLogger(__name__)


N_ASICS = 2

SPI_ADDRESS_WIDTH = 20
SPI_REGISTER_WIDTH = 16


class ZynqBuffer(object):

    def __init__(self, address='192.168.1.25', port=4, timeout=10):
        self.address = address
        self.port = port
        self.timeout = timeout
        self.socket = None

        # Identity
        self.version = None
        self.pl_version = None
        self.jumper = None
        self.mac_address = None

        # Capture configuration
        self._n_views = 0
        self._n_accum_views = 0
        self._n_counters = 0
        self._view_period = 0
        self._capture_delay = 0

        # Dump status
        self._dump_bytes = 0
        self._dump_captures = 0
        self._dump_views = 0

    def start(self):
        """Attempt to connect to the buffer and read its identity information."""
        try:
            logger.info("Connecting to buffer at {} port {}".format(self.address, self.port))
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.socket.setsockopt(socket.SOL_TCP, socket.TCP_NODELAY, 1)
            self.socket.settimeout(self.timeout)
            self.socket.connect((self.address, self.port))

            self.identify()
            logger.info("Connected to buffer {} jumper {} (ver {} {:08x})".format(
                self.mac_address, self.jumper, self.version, self.pl_version))

        except Exception as e:
            logger.error("Failed to connect to buffer: {}".format(e))
            raise

    def stop(self):
        """Disconnect from the buffer."""
        self.socket.close()
        self.socket = None

        logger.info("Disconnected from buffer")

    def reset(self):
        """Reset the buffer FPGA logic."""
        logger.info("Resetting buffer FPGA logic")
        self._send_cmd(pb.cmd(buffer_reset=pb.empty()))

    def identify(self):
        """Read the buffer's identity (version numbers, jumper setting, and MAC address)."""
        identity = self._send_cmd(pb.cmd(buffer_identify=pb.empty())).buffer_identify

        self.version = identity.version
        self.pl_version = identity.pl_version
        self.jumper = identity.jumper
        self.mac_address = ':'.join(('{:02X}'.format(ord(mac_byte)) for mac_byte in identity.mac_addr))

        return identity

    def debug(self, debug=True):
        """Enable or disable debug mode."""
        self._send_cmd(pb.cmd(buffer_debug=debug))

    def reset_detector(self, power_cycle=True, busy_wait=15):
        """
        Reset or power cycle the detector hardware and wait for it to de-assert the busy bit.

        Parameters
        ----------
        power_cycle : bool, optional
            Whether to power cycle the DM via its PMIC reset line or just reset the FPGA logic. Defaults to True.

        busy_wait : int or float, optional
            How long to wait for DM to de-assert its busy bit (defaults to 15 seconds). If 0, do not wait at all.
        """
        if power_cycle:
            logger.info("Power cycling detector hardware")
            self._send_cmd(pb.cmd(detector_power_cycle=pb.empty()))
        else:
            logger.info("Resetting detector hardware")
            self._send_cmd(pb.cmd(detector_reset=pb.empty()))

        if busy_wait > 0:
            logger.info("Waiting up to {} s for detector to be ready".format(busy_wait))
            timeout_time = time() + busy_wait

            while True:
                is_ready = self._send_cmd(pb.cmd(detector_is_ready=pb.empty())).detector_is_ready

                if is_ready:
                    return

                if time() >= timeout_time:
                    raise BufferTimeoutError("Detector did not clear busy bit within {} s".format(busy_wait))

                sleep(0.1)

    ####################################################################################################################
    # ASIC Register Manipulation
    ####################################################################################################################

    def read_asic_reg(self, address):
        """
        Read from an ASIC register through the detector.

        Parameters
        ----------
        address : int
            Register address to read from.

        Returns
        -------
        int
            Read register value.
        """
        cmd = pb.cmd(read_reg=pb.reg_addr(target=pb.ASIC, addr=address))
        resp = self._send_cmd(cmd)

        return resp.read_reg.val

    def write_asic_reg(self, address, value):
        """

        """
        cmd = pb.cmd(write_reg=pb.reg_val(target=pb.ASIC, addr=address, val=value))
        self._send_cmd(cmd)

    def modify_asic_reg(self, address, value, mask=None, msb=None, lsb=None):
        """

        """
        if mask is None:
            if msb is None and lsb is None:
                raise ValueError()

            # Calculate mask for specified range of bits
            width = msb - lsb + 1
            mask = (2**width - 1) << lsb

            value = (value << lsb) & mask

        cmd = pb.cmd(modify_reg=pb.reg_mask_val(target=pb.ASIC, addr=address, mask=mask, val=value))
        self._send_cmd(cmd)

    def toggle_asic_reg_bit(self, address, bit, active_low=False):
        """

        """
        if not active_low:
            self.modify_asic_reg(address, 1, msb=bit, lsb=bit)
            self.modify_asic_reg(address, 0, msb=bit, lsb=bit)
        else:
            self.modify_asic_reg(address, 0, msb=bit, lsb=bit)
            self.modify_asic_reg(address, 1, msb=bit, lsb=bit)

    ####################################################################################################################
    # Detector Register Manipulation
    ####################################################################################################################

    def read_detector_reg(self, address):
        """
        Read from a detector register over SPI.

        Parameters
        ----------
        address : int
            Register address to read from.

        Returns
        -------
        int
            Read register value.

        Raises
        ------
        ValueError
            If the address is out of range.
        """
        assert_width(address, SPI_ADDRESS_WIDTH)

        cmd = pb.cmd(read_reg=pb.reg_addr(target=pb.DETECTOR, addr=address))
        resp = self._send_cmd(cmd)

        return resp.read_reg.val

    def write_detector_reg(self, address, value):
        """
        Write to a detector register over SPI.

        Parameters
        ----------
        address : int
            Register address to write to.

        value : int
            Register value to write.

        Raises
        ------
        ValueError
            If the address or value are out of range.
        """
        assert_width(address, SPI_ADDRESS_WIDTH)
        assert_width(value, SPI_REGISTER_WIDTH)

        cmd = pb.cmd(write_reg=pb.reg_val(target=pb.DETECTOR, addr=address, val=value))
        self._send_cmd(cmd)

    def modify_detector_reg(self, address, value, mask=None, msb=None, lsb=None):
        """
        Modify a detector register over SPI. This performs a read-modify-write within the buffer using a mask:

            new_value = (old_value & ~mask) | (value & mask)

        A mask can be provided with the `mask` parameter, or generated for a specified range of bits given by the `msb`
        and `lsb` parameters. In the case of a generated mask, the provided `value` is shifted left by `lsb`.

        Parameters
        ----------
        address : int
            Register address to modify.

        value : int
            Register value to write.

        mask : int, optional

        msb : int, optional
        lsb : int, optional

        Returns
        -------
        int
            Modified register value.
        """
        if mask is None:
            if msb is None and lsb is None:
                raise ValueError()

            # Calculate mask for specified range of bits
            width = msb - lsb + 1
            mask = (2**width - 1) << lsb

            value = (value << lsb) & mask
        else:
            if msb is not None or lsb is not None:
                raise ValueError()

        cmd = pb.cmd(modify_reg=pb.reg_mask_val(target=pb.DETECTOR, addr=address, mask=mask, val=value))
        resp = self._send_cmd(cmd)

        return resp.modify_reg.val

    def read_detector_reg_range(self, start_address, n_reads):
        """
        Read a range of detector registers over SPI.

        Parameters
        ----------
        start_address : int
            Register address to start reading from.

        n_reads: int
            Number of registers to read.

        Returns
        -------
        list of int
            Read register values as list of integers.
        """
        assert_width(start_address + n_reads - 1, SPI_ADDRESS_WIDTH)

        values = chunk_str(self.read_chain(start_address, n_reads), chunk_len=2)
        return [unpack_u16_le(value) for value in values]

    def write_detector_reg_range(self, start_address, values, n_writes=None):
        """
        Write a range of detector registers over SPI.

        Parameters
        ----------
        start_address : int
            Register address to start writing to.

        values: list of int or int
            Register values to write as list of integers or single integer.

        n_writes: int, optional
            If provided, writes a single value this many times. `values` must be a single integer. If None, the number
            of writes is determined by the length of `values`.
        """

        if n_writes is not None:
            values = pack_u16_le(values)
            data = ''.join((values for _ in range(n_writes)))
        else:
            data = ''.join((pack_u16_le(value) for value in values))

        self.write_chain(start_address, data)

    ####################################################################################################################
    # Detector Chain RAM Manipulation
    ####################################################################################################################

    def read_chain(self, address, n_words):
        """
        Read from detector chain RAM over SPI.

        Parameters
        ----------
        address : int
            Start address to start reading from.

        n_words: int
            Number of words to read.

        Returns
        -------
        str
            Read string of bytes.
        """
        cmd = pb.cmd(read_chain=pb.chain_addr(addr=address, n_words=n_words))
        resp = self._send_cmd(cmd)

        return resp.read_chain.words

    def write_chain(self, address, words):
        """
        Write to detector chain RAM over SPI.

        Parameters
        ----------
        address : int
            Address to start writing to.

        words : str
            String of bytes to write.
        """
        cmd = pb.cmd(write_chain=pb.chain_val(addr=address, words=words))
        resp = self._send_cmd(cmd)

    ####################################################################################################################
    # Detector Capture
    ####################################################################################################################

    def config_capture(self, n_views=1, view_period=1000, azero_period=16, n_accum_views=0, n_counters=13, reset=True):
        """
        Set the buffer capture configuration, and reset any previous captures.

        Parameters
        ----------
        n_views : int, optional
            Number of views to capture. Defaults to 1.

        view_period : int, optional
            View period in microseconds. Defaults to 1 millisecond.

        azero_period : int, optional
            ASIC auto-zero period in microseconds. Added to view period to determine conversion clock period. Defaults
            to 16 microseconds.

        n_accum_views : int, optional
            Number of views to accumulate in the buffer during each captured "view". If this is greater than 1, the
            actual number of views captured will be n_views * n_accum_views, but only n_views will be returned. Defaults
            to 0 (no accumulation).

        n_counters : int, optional
            Number of counters to be captured. This must match the ASIC output or the capture will fail. Defaults to 13.

        reset : bool, optional
            Whether to reset the capture buffer (clear existing data) or keep accumulating data. Defaults to True.
        """
        vtrig_period = view_period + azero_period

        config = pb.capture_config()
        config.view_period = vtrig_period
        config.n_views = n_views
        config.n_accum_views = n_accum_views
        config.n_counters = n_counters
        config.reset_capture = reset

        cmd = pb.cmd(buffer_config_capture=config)
        resp = self._send_cmd(cmd)

        # Store capture configuration
        self._n_views = n_views
        self._n_accum_views = n_accum_views
        self._n_counters = n_counters
        self._view_period = view_period
        self._capture_delay = resp.buffer_capture_delay

        if reset:
            # Reset dump status
            self._dump_bytes = 0
            self._dump_captures = 0
            self._dump_views = 0

    def capture(self):
        """
        Perform a capture using the previously set configuration. This may be repeated as many times as required using
        the same configuration. Each capture will be separated out when the data is dumped.
        """
        cmd = pb.cmd(buffer_start_capture=pb.empty())
        self._send_cmd(cmd)

        # Sleep if the required delay is larger than 15 ms (Windows can't sleep for less than this)
        delay = self._capture_delay / 1e6
        extra_delay = max(delay * 1.05, 1)
        end_time = time() + extra_delay

        if delay > 0.015 or self._view_period < 1000:
            sleep(max(delay, 0.015))

        while True:
            cmd = pb.cmd(buffer_capture_is_done=pb.empty())
            resp = self._send_cmd(cmd)

            if resp.buffer_capture_status.capture_is_done:
                self._dump_bytes = resp.buffer_capture_status.captured_bytes
                self._dump_captures += 1
                self._dump_views += self._n_views
                return
            else:
                if time() > end_time:
                    raise BufferTimeoutError("Capture did not complete within {} s".format(delay + extra_delay))

    def dump(self, parse=True, merge_captures=False):
        """
        Dump the captured data, either as a NumPy array or a raw byte string. The NumPy array will have the dimensions:

            (n_captures, n_views, N_ASICS (2), N_ROWS (24), N_COLUMNS (36), n_counters)

        If `n_captures` is 1, the first dimension will be pre-squeezed out, and the output array will only have the five
        remaining dimensions. Note that the views and counters dimensions will always exist, even if they are 1.

        Parameters
        ----------
        parse : bool, optional
            Whether the data should be parsed into a NumPy array or returned as a raw byte string. Defaults to True.

        merge_captures : bool, optional
            Whether multiple captures should be merged into a single capture. Defaults to False.

        Returns
        -------
        ndarray or str
            The data parsed into a NumPy array or as a raw byte string, depending on the `parse` parameter.
        """
        bytes_captured = 0

        buf = bytearray(self._dump_bytes)
        view = memoryview(buf)

        cmd = pb.cmd(buffer_start_dump=pb.empty())
        self._send_cmd(cmd, wait_for_resp=False)

        while True:
            try:
                n_bytes = self.socket.recv_into(view)
                bytes_captured += n_bytes

                if bytes_captured >= self._dump_bytes:
                    break

                view = view[n_bytes:]

            except socket.timeout:
                raise BufferTimeoutError()

        if parse:
            if merge_captures:
                captures = 1
            else:
                captures = self._dump_captures

            return self.parse_views(buf, self._dump_views, captures, self._n_counters, self._n_accum_views > 1)
        else:
            return buf

    @staticmethod
    def parse_views(raw_data, n_views, n_captures=1, n_counters=13, accum=False):
        """Parse raw data into a view array."""
        views_shape = (n_views, N_ASICS, N_ROWS, N_COLUMNS, n_counters)

        if n_captures > 1:
            views_shape = (n_captures,) + views_shape

        if accum:
            views_dtype = np.uint32
            view_number_len = 16
            preamble_len = 16
        else:
            views_dtype = np.uint16
            view_number_len = 4
            preamble_len = 8

        views = np.zeros(views_shape, dtype=views_dtype)

        header_len = view_number_len + preamble_len
        view_len = N_ASICS * N_PIXELS * n_counters * views_dtype().nbytes

        capture_idx = 0
        view_idx = 0

        data = memoryview(raw_data)

        while True:
            data = data[header_len:]

            view_data = np.asarray(data[0:view_len], dtype=np.uint8).astype(views_dtype)

            if n_captures > 1:
                current_view = views[capture_idx, view_idx]
            else:
                current_view = views[view_idx]

            for asic_idx in range(N_ASICS):
                for lane_idx in range(2):
                    lane = view_data[lane_idx + asic_idx * 2::N_ASICS * 2]

                    if accum:
                        lane = (lane[0::4] << 24) + (lane[1::4] << 16) + (lane[2::4] << 8) + lane[3::4]
                    else:
                        lane = (lane[0::2] << 8) + lane[1::2]

                    lane = np.transpose(lane.reshape((-1, n_counters, N_ROWS / 2)), axes=(2, 0, 1))

                    if lane_idx == 0:
                        current_view[asic_idx, 0:N_ROWS / 2, :, :] = lane
                    else:
                        current_view[asic_idx, N_ROWS / 2:N_ROWS, :, :] = np.flip(lane, axis=0)

            data = data[view_len:]

            view_idx += 1

            if view_idx == n_views:
                view_idx = 0
                capture_idx += 1

            if capture_idx == n_captures:
                break

        # Reverse the order of the counters
        return np.flip(views, axis=-1)

    @staticmethod
    def view_size(n_counters=13, accum=False):
        """Calculate the view size for the specified number of counters and accumulation mode."""
        total_counters = 2 * N_PIXELS * n_counters
        if accum:
            return total_counters * 4 + 32
        else:
            return total_counters * 2 + 12

    ####################################################################################################################
    # Internal Buffer Register Manipulation
    ####################################################################################################################

    def _read_reg(self, address):
        """Read from internal buffer register at `address`."""
        cmd = pb.cmd(read_reg=pb.reg_addr(target=pb.BUFFER, addr=address))
        resp = self._send_cmd(cmd)

        return resp.read_reg.val

    def _write_reg(self, address, value):
        """Write `value` to internal buffer register at `address`."""
        cmd = pb.cmd(write_reg=pb.reg_val(target=pb.BUFFER, addr=address, val=value))
        self._send_cmd(cmd)

    ####################################################################################################################
    # Command Handling
    ####################################################################################################################

    def _send_cmd(self, cmd, wait_for_resp=True):
        """Send a command and wait for a response if required."""
        cmd_frame = struct.pack('<I', cmd.ByteSize() + 4) + cmd.SerializeToString()
        self.socket.send(cmd_frame)

        if wait_for_resp:
            try:
                resp_frame = self.socket.recv(2048)
                resp_frame_len = struct.unpack('<I', resp_frame[0:4])

                resp = pb.resp()
                resp.MergeFromString(resp_frame[4:])

                if resp.status != pb.OK:
                    raise BufferError()

                return resp
            except socket.timeout:
                raise BufferTimeoutError("Socket timeout while waiting for {} response".format(cmd.WhichOneof('type')))
