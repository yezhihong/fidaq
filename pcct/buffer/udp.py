########################################################################################################################
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
########################################################################################################################

from six.moves import StringIO

import sys
import socket
import logging
from time import sleep, time

from .zynq import ZynqBuffer
from pcct.utils import chunk_str, modify_bits, pack_u16_be, unpack_u16_be

from dmBuffer import DM
from DDC0864.dmFunctions import asic2ram, ram2asic


logger = logging.getLogger(__name__)


class UdpBuffer(object):

    def __init__(self, address='192.168.1.25', port=4, timeout=5):
        self.address = address
        self.port = port
        self.timeout = timeout
        self.socket = None

        self.parse_views = ZynqBuffer.parse_views

    def start(self):
        try:
            logger.info("Connecting to buffer at {} port {}".format(self.address, self.port))
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            self.socket.connect((self.address, self.port))
            self.socket.settimeout(self.timeout)

        except Exception as e:
            logger.error("Failed to connect to buffer: {}".format(e))
            raise

    def stop(self):
        self.socket.close()
        self.socket = None

        logger.info("Disconnected from buffer")

    def reset(self):
        logger.info("Resetting buffer FPGA logic")
        DM.set_SYS_reset(1)
        DM.set_SYS_reset(0)
        DM.set_SYS_DMadapter(1)

    def reset_detector(self, power_cycle=True, busy_wait=15):
        logger.info("Power cycling detector hardware")
        DM.set_DM_PKGRSTN(0)
        DM.set_DM_PKGRSTN(1)

        if busy_wait > 0:
            logger.info("Waiting up to {} s for detector to be ready".format(busy_wait))
            timeout_time = time() + busy_wait

            while True:
                is_ready = DM.get_DM_busy() == '0'

                if is_ready:
                    return

                if time() >= timeout_time:
                    raise Exception("Detector did not clear busy bit within {} s".format(busy_wait))

                sleep(1)

    ####################################################################################################################
    # ASIC Register Manipulation
    ####################################################################################################################

    def read_asic_reg(self, address):
        asic2ram(hex(address), hex(address))
        return self.read_detector_reg(address + 0x2000)

    def write_asic_reg(self, address, value):
        self.write_detector_reg(address + 0x2000, value)
        ram2asic(hex(address), hex(address))

    def modify_asic_reg(self, address, value, mask=None, msb=None, lsb=None):
        current_value = self.read_asic_reg(address)
        new_value = modify_bits(current_value, value, 16, msb, lsb)
        self.write_asic_reg(address, new_value)

    def toggle_asic_reg_bit(self, address, bit, active_low=False):
        if not active_low:
            self.modify_asic_reg(address, 1, msb=bit, lsb=bit)
            self.modify_asic_reg(address, 0, msb=bit, lsb=bit)
        else:
            self.modify_asic_reg(address, 0, msb=bit, lsb=bit)
            self.modify_asic_reg(address, 1, msb=bit, lsb=bit)

    ####################################################################################################################
    # Detector Register Manipulation
    ####################################################################################################################

    def read_detector_reg(self, address):
        if (address > 0xfffff):
            logger.warn('SPI_read: Greater than 20 bits. MSBs will be ignored by DM Buffer.')
        read_bit = '1'
        spi_cmd_addr = DM.get_msg_bits(DM.padhex8(hex(address).strip('L')))
        spi_cmd_addr = DM.update_field(spi_cmd_addr, read_bit, 28)  ## Update read bit
        spi_cmd_addr = DM.bits2hex(DM.reverse_str(spi_cmd_addr))

        # Write to SPI_spi_cmd_addr register triggers SPI output
        addr_str = hex(DM.SPI_BASE_ADDR + 0x00000008)  ## SPI_spi_cmd_addr register
        status = DM.write_reg(addr_str, spi_cmd_addr)
        status = DM.get_msg_bits(status)
        if (status[9] == '1'):
            logger.warn('SPI_read: Parity error reported by DM SPI slave for previous frame.')

        addr_str = hex(DM.SPI_BASE_ADDR + 0x00000010)  ## SPI_read_data register
        rdata = DM.read_reg(addr_str)
        if (status[9] == '1'):
            logger.warn('SPI_read: Parity error reported by DM SPI slave for previous frame.')
        if (status[8] == '1'):
            logger.warn('SPI_read: Parity error reported by SPI Master for read data.')
        return int(rdata, 16)

    def write_detector_reg(self, address, value):
        addr_str = hex(DM.SPI_BASE_ADDR + 0x0000000C)  ## SPI_write_data register
        status = DM.write_reg(addr_str, hex(value).strip('L'))
        if (status == 0):
            logger.error('SPI_write to SPI_write_data register: failed.')
            return (0)
            # Write to SPI_spi_cmd_addr register triggers SPI output
        addr_str = hex(DM.SPI_BASE_ADDR + 0x00000008)  ## SPI_spi_cmd_addr register
        spi_cmd_addr = DM.padhex8(hex(address).strip('L'))  ## Read control bit is 0 for write
        status = DM.write_reg(addr_str, spi_cmd_addr)
        if (status == 0):
            logger.error('SPI_write to SPI_spi_cmd_addr register: failed.')
            return (0)
        status = DM.get_msg_bits(status)
        if (status[9] == '1'):
            logger.warn('SPI_write: Parity error reported by DM SPI slave for previous frame.')
        return (1)

    def modify_detector_reg(self, address, value, mask=None, msb=None, lsb=None):
        current_value = self.read_detector_reg(address)
        new_value = modify_bits(current_value, value, 16, msb, lsb)
        self.write_detector_reg(address, new_value)

    def read_detector_reg_range(self, start_address, n_reads):
        raise NotImplementedError()

    def write_detector_reg_range(self, start_address, values, n_writes=None):
        if n_writes:
            values = [values for _ in range(n_writes)]

        address = start_address
        for value in values:
            self.write_detector_reg(address, value)
            address += 1

    ####################################################################################################################
    # Detector Chain RAM Manipulation
    ####################################################################################################################

    def read_chain(self, address, n_words):
        words = StringIO()

        for _ in range(n_words):
            words.write(pack_u16_be(self.read_detector_reg(address)))
            address += 1

        return words.getvalue()

    def write_chain(self, start_address, words):
        address = start_address
        words = chunk_str(words, 2)
        for word in words:
            self.write_detector_reg(address, unpack_u16_be(word))
            address += 1

    ####################################################################################################################
    # Internal Buffer Register Manipulation
    ####################################################################################################################

    def _read_reg(self, address):
        """Read from internal buffer register at `address`."""
        cmd = 'R ' + hex(address).strip('L')
        self.socket.send(cmd)
        rdata = self.socket.recv(DM.MAX_MSG_SIZE)
        rdata = DM.get_msg_hex(rdata)
        if (rdata):
            return int(rdata, 16)
        else:
            sys.exit(1)

    def _write_reg(self, address, value):
        """Write `value` to internal buffer register at `address`."""
        cmd = 'W ' + hex(address).strip('L') + ' ' + DM.padhex8(hex(value).strip('L'))
        self.socket.send(cmd)
        try:
            timeStart = time()
            status = self.socket.recv(DM.MAX_MSG_SIZE)  ## Every UDP command requires a handshaking response
            status = DM.get_msg_hex(status)
            if (status):
                return (status)
            else:
                sys.exit(1)
        except socket.timeout:
            logger.critical('SOCKET TIMEOUT CAUGHT in write_reg!')
            logger.critical('Timeout duration = %2.5fs' % (time() - timeStart))
            # logger.critical('Status = %s' % status) # This will cause undefined behaviour I believe
            return self._write_reg(address, value)  # Use with caution. You could end up in an infinite loop
