########################################################################################################################
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
########################################################################################################################

import logging
import numpy as np

from pcct.ddc0864 import N_COLUMNS, N_ROWS


logger = logging.getLogger(__name__)


class BaseModule(object):
    MODULE_NAME = 'BASE MODULE'
    DEFAULT_PIXEL_AREA = 0.9

    # Lag/Dynamic Analysis Constants
    NCP_RANGE = [-2.5, 2.5]

    # Uniformity Analysis Constants
    OCR_COLOR_RANGE = [0, 4e6]
    FORCE_CALC_FLUX_VALS = True
    OPEN_BIN_INTEGRATION_IND = 1   # 30keV and above
    CURRENTS_OF_INTEREST_IND = [0, 4, 6, 11]
    OCR_ICR_NCP_TO_AMALGAMATE_THRESH_IND = [1, 2, 3, 4]
    OCR_ICR_NCP_TO_AMALGAMATE_CURRENT_IND = [1, 2, 3]

    @staticmethod
    def area_correction_voxel():
        area = BaseModule.DEFAULT_PIXEL_AREA

        logger.warning('No area correction for this module, using default pixel area value (%.3f mm^2)', area)

        correction = np.ones((N_ROWS, N_COLUMNS))
        pixel_area = np.full((N_ROWS, N_COLUMNS), area)
        pixel_shape = np.full((N_ROWS, N_COLUMNS, 2), np.sqrt(area))

        return correction, pixel_area, pixel_shape
