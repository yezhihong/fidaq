########################################################################################################################
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
########################################################################################################################

import logging
from time import time, sleep
import numpy as np

from pcct.ddc0864 import N_REGISTERS
from pcct.utils import select_bits

# Legacy test code
from DDC0864.dmFunctions import writeReg, readReg, is_set, asicDMMPatternEnable, chkCrcError
from DDC0864.cal_lib import genVtrigs
from dmBuffer import DM

logger = logging.getLogger(__name__)


# Version 1.8 (2019) modifies the chain register spaces
V1_8_ASIC_THRESH_CHAIN_BASE = 0x20200
V1_8_ASIC_SET18_CHAIN_BASE = 0x20700


class DetectorError(Exception):
    pass


class DetectorTimeoutError(DetectorError):
    pass


class BaseDetector(object):
    MODULE_TYPE = None
    N_MODULES = None
    N_ASICS_PER_MODULE = 2

    REGISTER_WIDTH = 16
    ASIC_REGISTER_BASE = 0x2000
    ASIC_MISC_CHAIN_BASE = 0x20000
    ASIC_THRESH_CHAIN_BASE = 0x10000
    ASIC_SET18_CHAIN_BASE = 0x10100

    V_DFT_DAC_MAX = 0.4

    def __init__(self, buffer, serial, module_serials, passive_asics=None):
        self.buffer = buffer
        self.version = None
        self.date = None
        self.serial = serial
        self.module_serials = [module or None for module in module_serials]

        self.fitted_asics = []
        for idx, module in enumerate(self.module_serials):
            if module:
                self.fitted_asics += self.map_module_to_asics(idx)

        self.passive_asics = passive_asics or []
        self.active_asics = [asic for asic in self.fitted_asics if asic not in self.passive_asics]

        self.selected_modules = []
        self.selected_asics = []

    def reset(self):
        """
        Reset the detector.
        """
        # Reset selected ASICs and modules
        self.selected_modules = []
        self.selected_asics = []

        # Place all ASICs in hardware reset
        self.write_reg(0x0f, 0xffff)

        # Read version and date code
        version = self.read_reg(0x00)
        self.version = {
            'pcb_work':     select_bits(version, self.REGISTER_WIDTH, msb=16, lsb=12),
            'pcb_rev':      select_bits(version, self.REGISTER_WIDTH, msb=11, lsb=8),
            'fpga_rev':     select_bits(version, self.REGISTER_WIDTH, msb=7,  lsb=4),
            'fpga_sub_rev': select_bits(version, self.REGISTER_WIDTH, msb=3,  lsb=0),
        }

        date_code = self.read_reg(0x01)
        self.date = {
            'year':  select_bits(date_code, self.REGISTER_WIDTH, msb=15, lsb=9),
            'month': select_bits(date_code, self.REGISTER_WIDTH, msb=8,  lsb=5),
            'day':   select_bits(date_code, self.REGISTER_WIDTH, msb=4,  lsb=0),
        }

        logger.info('Detector FPGA date 20%02d-%02d-%02d (0x%04X rev 0x%X%X) PCB work %d rev %d',
            self.date['year'], self.date['month'], self.date['day'], date_code, self.version['fpga_rev'],
            self.version['fpga_sub_rev'], self.version['pcb_work'], self.version['pcb_rev'])

        # Set correct chain address spaces based on date code
        if self.date['year'] >= 19:
            self.ASIC_SET18_CHAIN_BASE = V1_8_ASIC_SET18_CHAIN_BASE
            self.ASIC_THRESH_CHAIN_BASE = V1_8_ASIC_THRESH_CHAIN_BASE

        # Reset ASIC register and chain RAM
        self.buffer.write_detector_reg_range(self.ASIC_REGISTER_BASE, 0, n_writes=256)
        self.buffer.write_detector_reg_range(self.ASIC_MISC_CHAIN_BASE, 0, n_writes=512)
        self.buffer.write_detector_reg_range(self.ASIC_THRESH_CHAIN_BASE, 0, n_writes=256)
        self.buffer.write_detector_reg_range(self.ASIC_SET18_CHAIN_BASE, 0, n_writes=256)

    def setup(self):
        """
        Perform any additional setup prior to operating the detector.
        """
        pass

    ####################################################################################################################
    # Register Manipulation
    ####################################################################################################################

    def read_reg(self, address, msb=None, lsb=None, bit=None):
        """
        Read from a register.

        Parameters
        ----------
        address : int
            Address in detector register space to read from.

        msb, lsb, bit: int, optional
            Range of bits or single bit to return. Register value will be masked and shifted based on the provided
            parameters before being returned.

        Returns
        -------
        int
            Read register value.
        """
        return select_bits(self.buffer.read_detector_reg(address), self.REGISTER_WIDTH, msb, lsb, bit)

    def write_reg(self, address, value):
        """
        Write to a register.

        Parameters
        ----------
        address : int
            Address in detector register space to write to.

        value : int
            Register value to write.
        """
        self.buffer.write_detector_reg(address, value)

    def modify_reg(self, address, value, msb=None, lsb=None, bit=None):
        """
        Read-modify-write a register.

        Parameters
        ----------
        address : int
            Address in detector register space to modify.

        value : int
            Register value to write.

        msb, lsb, bit: int
            Range of bits or single bit to modify. The register will be read and masked and the new value will be
            written based on these parameters.

        Raises
        ------
        ValueError
            If the address, value, or bits are out of range.
        """
        if bit is not None:
            msb = lsb = bit

        self.buffer.modify_detector_reg(address, value, msb=msb, lsb=lsb)

    def wait_for_bit_in_reg(self, address, bit, should_be_set=True, timeout=5, interval=0):
        """
        Wait for a register bit to be set or cleared.

        Parameters
        ----------
        address : int
            Address in detector register space to read.

        bit : int
            Bit within register to check.

        should_be_set : bool, optional
            Whether to wait for the specified `bit` to be set or cleared. True by default.

        timeout : float, optional
            Number of seconds to wait before timing out. Defaults to 5.

        interval : float, optional
            Number of seconds to wait between reads. Defaults to 0 (no sleep).

        Raises
        ------
        DetectorTimeoutError
            If `bit` is not set or cleared within `timeout` seconds.
        """
        timeout_time = time() + timeout

        while True:
            is_set = (self.read_reg(address) & (1 << bit)) > 0

            if is_set == should_be_set:
                return

            if time() >= timeout_time:
                condition = "set" if should_be_set else "cleared"
                raise DetectorTimeoutError("Bit {} in register {:#x} not {} within {} s".format(
                    bit, address, condition, timeout))

            if interval > 0:
                sleep(interval)

    ####################################################################################################################
    # ASIC Register Manipulation
    ####################################################################################################################

    def read_asic_reg(self, address, msb=None, lsb=None, bit=None, single_asic=False):
        """
        Read from an ASIC register.

        Parameters
        ----------
        address : int
            Address in ASIC register space to read from.

        msb, lsb, bit: int, optional
            Range of bits or single bit to return. Register value will be masked and shifted based on the provided
            parameters before being returned.

        single_asic : bool, optional
            Read from a single ASIC (do not modify mask or return an array). Defaults to False.

        Returns
        -------
        ndarray or int
            Read register value(s).

        Raises
        ------
        ValueError
            If the address or bits are out of range.
        """
        if single_asic:
            value = self.buffer.read_asic_reg(address)

        else:
            value = np.zeros(len(self.selected_asics), dtype=np.uint16)

            for idx, asic in enumerate(self.selected_asics):
                self.buffer.write_detector_reg(0x0c, 1 << asic)
                value[idx] = self.buffer.read_asic_reg(address)

            return value

        return select_bits(value, self.REGISTER_WIDTH, msb, lsb, bit)

    def write_asic_reg(self, address, value, single_asic=False):
        """
        Write to an ASIC register.

        Parameters
        ----------
        address : int
            Address in ASIC register space to write to.

        value : array_like or int
            Register value(s) to write.

        single_asic : bool, optional
            Write to a single ASIC (do not modify mask). Defaults to False.

        Raises
        ------
        ValueError
            If the address or value are out of range.
        """
        if single_asic:
            self.buffer.write_asic_reg(address, value)

        else:
            if type(value) == int:
                mask = 0
                for asic in self.selected_asics:
                    mask |= 1 << asic
                self.buffer.write_detector_reg(0x0c, mask)
                self.buffer.write_asic_reg(address, value)
            else:
                for idx, asic in enumerate(self.selected_asics):
                    self.buffer.write_detector_reg(0x0c, 1 << asic)
                    self.buffer.write_asic_reg(address, value[idx])

    def modify_asic_reg(self, address, value, msb=None, lsb=None, bit=None, single_asic=False):
        """
        Read-modify-write an ASIC register.

        Parameters
        ----------
        address : int
            Address in ASIC register space to modify.

        value : array_like or int
            Register value to write.

        msb, lsb, bit: int
            Range of bits or single bit to modify. The register will be read and masked and the new value will be
            written based on these parameters.

        single_asic : bool, optional
            Read-modify-write a single ASIC (do not modify mask). Defaults to False.

        Raises
        ------
        ValueError
            If the address, value, or bits are out of range.
        """
        if bit is not None:
            msb = lsb = bit

        width = msb - lsb + 1
        mask = (2 ** width - 1) << lsb

        old_val = self.read_asic_reg(address, single_asic)
        new_val = (old_val & ~mask) | (value << lsb)

        self.write_asic_reg(address, new_val, single_asic)

    def _asic_reg_address(self, address):
        """Map an address in the ASIC register space into the detector register space."""
        if address < 0 or address >= N_REGISTERS:
            raise ValueError()

        return self.ASIC_REGISTER_BASE + address

    def _asic_reg_transfer(self, start, end, write=False):
        """Transfer a range of registers between the ASIC and the detector's RAM."""
        if start < 0 or start >= N_REGISTERS or end < 0 or end >= N_REGISTERS or start > end:
            raise ValueError()

        # Pack the 8-bit start and end addresses into a single 16-bit register
        addresses = ((start << 8) & 0xFF00) | (end & 0x00FF)
        self.write_reg(0x0B, addresses)

        command_bit, done_bit = (3, 1) if write else (2, 0)
        self._asic_read_write_control(command_bit, done_bit)

    def _asic_read_write_control(self, command_bit, done_bit):
        """
        Execute an ASIC read or write by setting `command_bit` and waiting for `done_bit` in the ASIC Register
        Read/Write Execution Status register
        """
        self.write_reg(0x0E, 1 << command_bit)
        self.write_reg(0x0E, 0)
        self.wait_for_bit_in_reg(0x0E, done_bit)

    ####################################################################################################################
    # Data Capture
    ####################################################################################################################

    def config_capture(self, n_views, view_period=1000, azero_period=16, n_accum_views=0, n_counters=13):
        """Set the buffer's capture configuration."""
        self.buffer.config_capture(n_views, view_period, azero_period, n_accum_views, n_counters)

    def capture(self):
        """Perform a capture for each selected module."""
        for module in self.selected_modules:
            self.write_reg(0x15, 3 << module * 2)
            self.buffer.capture()

    def dump(self, parse=True):
        """Dump the captured data and re-organize it such that the captures and ASICs dimensions are correct."""
        dump = self.buffer.dump(parse)

        if len(self.selected_modules) == 1 or not parse:
            return dump
        else:
            # Determine correct output shape
            dump_shape = dump.shape

            n_modules = len(self.selected_modules)
            n_captures = dump_shape[0] // n_modules
            n_views = dump_shape[1]
            n_asics = n_modules * 2

            out = np.zeros((n_captures, n_views, n_asics) + dump_shape[-3:])

            # De-interleave module selection from multiple captures
            for idx in range(n_captures):
                out[idx, ...] = dump[idx * n_modules: (idx + 1) * n_modules, ...].transpose((1, 0, 2, 3, 4, 5)).reshape(
                    (n_views, 2 * n_modules) + dump_shape[-3:])

            return out

    ####################################################################################################################
    # Module/ASIC Selection and Mapping
    ####################################################################################################################

    @classmethod
    def map_module_to_asics(cls, module):
        """
        Map a module index to its ASIC indices.

        Parameters
        ----------
        module : int
            Module index.

        Returns
        -------
        list of int
            ASIC indices.
        """
        return [module * 2, module * 2 + 1]

    @classmethod
    def map_asic_to_module(cls, asic):
        """
        Map an ASIC index to its module index.

        Parameters
        ----------
        asic : int
            ASIC index.

        Returns
        -------
        int
            Module index.
        """
        return (asic + 1) // 2


class DmAdapter(BaseDetector):
    V_DFT_DAC_MAX = 2.5

    def setup(self):
        self.calibrate_lvds()

    def calibrate_lvds(self):
        logger.info('Performing LVDS calibration')
        writeReg('0x1e', 15, 0, '0x8001', 1, 1)
        writeReg('0x2b', 15, 0, '0x3d3d', 1, 1)
        asicDMMPatternEnable(self.active_asics, self.passive_asics)  # enable preamble on unused ASIC slot
        DM.set_DATA_preamble("0xFEDC")
        chkCrcError()  # Check and clear CRC errors
        for asic in self.active_asics:
            writeReg('0x00a05', asic, asic, '0x0000')  # clear LVDS cal mask for each ASIC
        for asic in self.active_asics:
            writeReg('0x00a05', asic, asic, '0x0001')  # set LVDS cal mask for each ASIC
        genVtrigs(NumTriggers=200)
        chkCrcError()  # Check and clear CRC errors
        statusBits = int(readReg('0x00a06'), 0)  # ASIC Receive LVDS Calibration Done Status

        ddrCalPass = True
        for asic in self.active_asics:
            if not is_set(statusBits, asic):
                logger.error('LVDS Calibration Status bits did not get set for ASIC%s' % asic)
                ddrCalPass = False

        if ddrCalPass:
            logger.info('DDR calibration for DMM completed successfully')
        else:
            logger.error('DDR calibration for DMM failed')

        writeReg('0x2b', 15, 0, '0x0000', 1, 1)
        writeReg('0x1e', 15, 0, '0x0001', 1, 1)

        return ddrCalPass


class KintexDmAdapter(BaseDetector):
    def setup(self):
        self.enable_adc_vref()

    def enable_adc_vref(self):
        writeReg('0x00014', 15, 0, '0x8045', 0, 1)
