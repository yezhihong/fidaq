########################################################################################################################
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
########################################################################################################################

# Layout

N_PIXELS = 864

N_ROWS = 24
N_COLUMNS = 36

N_COLUMN_GROUPS = 6
N_COLUMNS_IN_COLUMN_GROUP = N_COLUMNS // N_COLUMN_GROUPS
N_PIXELS_IN_COLUMN_GROUP = N_ROWS * N_COLUMNS_IN_COLUMN_GROUP

# Counters

N_COUNTERS = 13
COUNTER_BYTES = 2

# Registers

N_REGISTERS = 255
REGISTER_WIDTH = 16
REGISTER_BYTES = REGISTER_WIDTH // 8
EXCLUDE_REGISTERS = [0x40, 0x41, 0x43]

# Misc Chains

N_MISC_CHAINS = 12
N_MISC_CHAIN_PIXELS = N_PIXELS // N_MISC_CHAINS

MISC_PIXEL_BYTES = 10
MISC_CHAIN_BYTES = N_MISC_CHAIN_PIXELS * MISC_PIXEL_BYTES
N_MISC_CHAIN_TRANSFERS = MISC_CHAIN_BYTES // REGISTER_BYTES

# Thresh Chains

N_THRESH_CHAINS = 12
N_THRESH_CHAIN_PIXELS = N_PIXELS // N_THRESH_CHAINS

THRESH_PIXEL_BYTES = 6
THRESH_CHAIN_BYTES = N_THRESH_CHAIN_PIXELS * THRESH_PIXEL_BYTES
N_THRESH_CHAIN_TRANSFERS = THRESH_CHAIN_BYTES // REGISTER_BYTES

N_THRESHOLDS = 6
MIN_THRESHOLD = 0
MAX_THRESHOLD = 255

# SET18 Chains

N_SET18_CHAINS = 48
N_SET18_CHAIN_PIXELS = N_PIXELS // N_SET18_CHAINS

SET18_CHAIN_BYTES = 6
N_SET18_CHAIN_TRANSFERS = SET18_CHAIN_BYTES // REGISTER_BYTES

# Data Structure

PREAMBLE = 0xFEDC
CRC_LEN = 2

N_LANES = 2
N_LANE_ROWS = N_ROWS // N_LANES
