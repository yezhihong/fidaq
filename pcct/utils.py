########################################################################################################################
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
########################################################################################################################

from struct import Struct


_UINT16_LE = Struct('<H')
_UINT16_BE = Struct('>H')
_UINT32_LE = Struct('<I')

pack_u16_le = _UINT16_LE.pack
pack_u16_be = _UINT16_BE.pack
pack_u32_le = _UINT32_LE.pack


def unpack_u16_le(val):
    return _UINT16_LE.unpack(val)[0]


def unpack_u16_be(val):
    return _UINT16_BE.unpack(val)[0]


def unpack_u32_le(val):
    return _UINT32_LE.unpack(val)[0]


def chunk_str(string, chunk_len=None, n_chunks=None):
    if chunk_len is None:
        chunk_len = len(string) // n_chunks
    elif n_chunks is None:
        n_chunks = len(string) // chunk_len
    elif n_chunks * chunk_len != len(string):
        raise ValueError()

    return [string[chunk * chunk_len: (chunk + 1) * chunk_len] for chunk in range(n_chunks)]


def assert_width(value, width):
    if value.bit_length() > width:
        raise ValueError()


def modify_bits(current_value, value, width, msb=None, lsb=None, bit=None):
    if bit is not None and bit <= width:
        msb = lsb = bit

    if msb is None or lsb is None:
        raise ValueError()

    if lsb < 0 or msb >= width or msb < lsb:
        raise ValueError()

    mask = (2**(msb - lsb + 1) - 1) << lsb

    return (current_value & ~mask) | ((value << lsb) & mask)


def select_bits(value, width, msb=None, lsb=None, bit=None):
    if bit is None and lsb is None and bit is None:
        return value

    elif bit is not None and bit <= width:
        return (value & (1 << bit)) >> bit

    elif msb is not None and lsb is not None and msb >= lsb:
        mask = (2 ** (msb - lsb + 1) - 1) << lsb
        return (value & mask) >> lsb

    else:
        raise ValueError()
