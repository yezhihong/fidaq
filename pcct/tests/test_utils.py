########################################################################################################################
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
########################################################################################################################

import pytest

import pcct.utils as utils


def test_assert_width():
    utils.assert_width(0xffff, 16)

    with pytest.raises(ValueError):
        utils.assert_width(0x10000, 16)


def test_chunk_str():
    str = 'abcdefghijkl'
    str_2 = ['ab', 'cd', 'ef', 'gh', 'ij', 'kl']
    str_3 = ['abc', 'def', 'ghi', 'jkl']

    assert utils.chunk_str(str, n_chunks=6) == str_2
    assert utils.chunk_str(str, n_chunks=4) == str_3
    assert utils.chunk_str(str, chunk_len=2) == str_2
    assert utils.chunk_str(str, chunk_len=3) == str_3

    with pytest.raises(ValueError):
        utils.chunk_str(str, chunk_len=3, n_chunks=6)


def test_modify_bits():
    assert utils.modify_bits(0xff00, 1, width=16, bit=4) == 0xff10
    assert utils.modify_bits(0xff00, 0, width=16, bit=8) == 0xfe00
    assert utils.modify_bits(0xf000, 0xff, width=16, msb=7, lsb=0) == 0xf0ff
    assert utils.modify_bits(0xf0ff, 0, width=16, msb=15, lsb=12) == 0x00ff


def test_select_bits():
    assert utils.select_bits(0xff00, width=16, msb=15, lsb=8) == 0xff
    assert utils.select_bits(0xff0f, width=16, msb=7, lsb=0) == 0x0f
    assert utils.select_bits(0xff00, width=16, bit=7) == 0
    assert utils.select_bits(0xff00, width=16, bit=8) == 1


def test_packing_and_unpacking():
    UINT16 = 0xabcd
    UINT16_PACKED_BE = '\xab\xcd'
    UINT16_PACKED_LE = '\xcd\xab'

    UINT32 = 0x89abcdef
    UINT32_PACKED_LE = '\xef\xcd\xab\x89'

    assert utils.pack_u16_be(UINT16) == UINT16_PACKED_BE
    assert utils.unpack_u16_be(UINT16_PACKED_BE) == UINT16
    assert utils.pack_u16_le(UINT16) == UINT16_PACKED_LE
    assert utils.unpack_u16_le(UINT16_PACKED_LE) == UINT16
    assert utils.pack_u32_le(UINT32) == UINT32_PACKED_LE
    assert utils.unpack_u32_le(UINT32_PACKED_LE) == UINT32
