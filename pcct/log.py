########################################################################################################################
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
########################################################################################################################

import os
import sys
import logging
from datetime import datetime
from shutil import copy

from colorama import Style, Back, Fore


root_logger = logging.getLogger()
logger = logging.getLogger(__name__)


DEFAULT_FILE_PATH = os.path.join(os.path.expanduser('~'), 'CTData', 'Logs')
DEFAULT_FILE_NAME = datetime.now().strftime('%Y-%m-%d_%H-%M-%S.log')
DEFAULT_FILE_HANDLER_NAME = 'pcct_log_file_handler'

LOG_FORMAT = '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
LOG_DATE_FORMAT = '%Y-%m-%d %H:%M:%S'

LOG_COLORS = {
    'CRITICAL': Fore.RED,
    'ERROR': Back.RED + Fore.BLACK,
    'WARNING': Fore.LIGHTYELLOW_EX,
    'DEBUG': Fore.BLUE,
}


class ColoredFormatter(logging.Formatter):
    def format(self, record):
        msg = super(ColoredFormatter, self).format(record)
        color = LOG_COLORS.get(record.levelname, '')

        return color + msg + Style.RESET_ALL


def config_logging(console_level=logging.INFO, file_level=logging.DEBUG, file_dir=DEFAULT_FILE_PATH):
    """Configure basic logging including coloured console output and file output."""
    root_logger.setLevel(logging.DEBUG)

    if len(root_logger.handlers) == 0:
        # Attach console handler with colored output
        console_handler = logging.StreamHandler(sys.stdout)
        console_handler.setFormatter(ColoredFormatter(LOG_FORMAT, datefmt=LOG_DATE_FORMAT))
        console_handler.setLevel(console_level)

        root_logger.addHandler(console_handler)

        if file_level is not None:
            file_name = os.path.join(file_dir, DEFAULT_FILE_NAME)
            logger.info('Logging to %s', file_name)

            # Attach file handler
            if not os.path.exists(file_dir):
                os.makedirs(file_dir)
            file_handler = logging.FileHandler(file_name)
            file_handler.name = DEFAULT_FILE_HANDLER_NAME
            file_handler.setFormatter(logging.Formatter(LOG_FORMAT, datefmt=LOG_DATE_FORMAT))
            file_handler.setLevel(file_level)

            root_logger.addHandler(file_handler)

        sys.excepthook = log_exception


def log_exception(exc_type, exc_value, exc_traceback):
    """Log an exception to root logger without handling it."""
    root_logger.critical("Unhandled exception", exc_info=(exc_type, exc_value, exc_traceback))


def move_log_file(new_path):
    """Move the current log file to a new path."""
    for handler in root_logger.handlers:
        if handler.name == DEFAULT_FILE_HANDLER_NAME:
            old_file_path = handler.baseFilename
            formatter = handler.formatter
            handler.close()
            root_logger.removeHandler(handler)

            if old_file_path is not None:
                if not os.path.exists(new_path):
                    os.makedirs(new_path)

                new_file_path = os.path.join(new_path, os.path.basename(old_file_path))

                copy(old_file_path, new_file_path)
                os.remove(old_file_path)

                handler = logging.FileHandler(new_file_path)
                handler.setFormatter(formatter)
                root_logger.addHandler(handler)

                logger.info('Moving log to %s', new_file_path)
                return new_file_path
