########################################################################################################################
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
########################################################################################################################

import logging
import pkgutil
import importlib

from .log import config_logging, log_exception, move_log_file


logger = logging.getLogger(__name__)


_DETECTOR_TYPES = {}
_MODULE_TYPES = {}


def load_packages():
    pcct_packages = (name for (_, name, _) in pkgutil.iter_modules() if name.startswith('pcct') and name != 'pcct')

    for package_name in pcct_packages:
        package = importlib.import_module(package_name)

        try:
            for name, cls in package.PCCT_DETECTORS.items():
                _DETECTOR_TYPES[name] = cls
                logger.info("Found %s detector in %s package", name, package_name)
        except AttributeError:
            pass

        try:
            for name, cls in package.PCCT_MODULES.items():
                _MODULE_TYPES[name] = cls
                logger.info("Found %s module in %s package", name, package_name)
        except AttributeError:
            pass


def detector_types():
    return dict(_DETECTOR_TYPES)


def module_types():
    return dict(_MODULE_TYPES)
