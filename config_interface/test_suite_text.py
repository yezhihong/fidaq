########################################################################################################################
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
########################################################################################################################

import pickle
import os
from config_constants import *
import globals

class TestSuiteText(object):
    """Text manager for test suite items"""
    def __init__(self, stand_alone=False):
        self.file_extension = SUITE_FILE_EXTENSION
        self.default_dir = os.path.join(DEFAULT_DIR, TEST_SUITE_DIR, '')
        self.test_items = []    # list of experiment dictionaries
        self.test_entries = []  # list of widget objects

    def suite_load(self):
        """load test suite from file"""
        file_name_in = globals.textModeSettings['Directory'] + globals.textModeSettings['SuiteName'];
        if not file_name_in:
            return
        fname_in = file_name_in
        with open(fname_in, 'rb') as pickle_in_handle:
            self.test_items = pickle.load(pickle_in_handle)

    def format_suite_for_baseline(self):
        """format experiment dictionaries to match baseline experimentDict keys / elements
        and populate experiment list"""
        formatted_dict = {}
        experiment_list = []
        for test_item in self.test_items:
            experiment = test_item['test_ID']
            experiment_list.append(experiment)  # add experiment id to list - required in main()
            formatted_dict[experiment] = test_item
        return formatted_dict, experiment_list

if __name__ == '__main__':
    TestSuiteText(stand_alone=True)
