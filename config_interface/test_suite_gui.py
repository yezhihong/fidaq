########################################################################################################################
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
########################################################################################################################

from tkinter import *
import pickle
import easygui as eg
from test_item_gui import TestItemGUI
import os
from config_constants import *


class TestSuiteGUI(object):
    """GUI manager for test suite items"""
    def __init__(self, stand_alone=False):
        if stand_alone:
            self.root = Tk()
        else:
            self.root = Toplevel()

        self.root.title('Test Suite Manager')
        self.file_extension = SUITE_FILE_EXTENSION
        self.cancelled = True
        self.default_dir = os.path.join(DEFAULT_DIR, TEST_SUITE_DIR, '')
        control_frame = Frame(self.root, width=500)
        control_frame.pack(side=TOP, fill=X)
        run_frame = Frame(self.root)
        run_frame.pack(side=BOTTOM, fill=X)
        add_item_button = Button(control_frame, text='Add test item',
                                 command=(lambda: self.item_open(ind=None)))
        add_item_button.pack(side=LEFT, padx=5, pady=5, anchor=NW)
        load_suite_button = Button(control_frame, text='Load test suite',
                                   command=(lambda: self.suite_load()))
        load_suite_button.pack(side=LEFT, padx=5, pady=5, anchor=NW)
        save_suite_button = Button(control_frame, text='Save test suite',
                                   command=(lambda: self.suite_save()))
        save_suite_button.pack(side=LEFT, padx=5, pady=5, anchor=NW)
        cancel_button = Button(run_frame, text='Cancel', width=10,
                               command=(lambda: self.cancel()))
        cancel_button.pack(side=LEFT, padx=5, pady=5, anchor=NW)
        run_suite_button = Button(run_frame, text='Run test suite',
                                  command=(lambda: self.suite_run()))
        run_suite_button.pack(side=LEFT, padx=5, pady=5, anchor=NW)
        self.test_items = []    # list of experiment dictionaries
        self.test_entries = []  # list of widget objects

        if stand_alone:
            self.root.mainloop()
            #   handle closing of window
            self.root.protocol("WM_DELETE_WINDOW", self.root.quit)
        else:
            self.root.wait_window()

    def cancel(self):
        self.cancelled = True
        self.root.destroy()

    def suite_run(self):
        """close normally and return control to main"""
        self.cancelled = False
        self.root.destroy()

    def suite_load(self):
        """load test suite from file"""
        file_name_in = eg.fileopenbox(msg='select test suite to open',
                                      title='Load Test Suite',
                                      default=self.default_dir,
                                      filetypes=['*.'+self.file_extension])
        if not file_name_in:
            return
        fname_in = file_name_in
        with open(fname_in, 'rb') as pickle_in_handle:
            self.test_items = pickle.load(pickle_in_handle)
        self.entries_populate()

    def suite_save(self):
        """save test suite to disk"""
        file_name_out = eg.filesavebox(msg='select tst suite save location',
                                       default=self.default_dir,
                                       filetypes=['*.'+self.file_extension])
        if not file_name_out:
            return
        if self.file_extension in file_name_out[-4:]:
            fname_out = file_name_out
        else:
            fname_out = "{}.{}".format(file_name_out, self.file_extension)
        with open(fname_out, 'wb') as pickle_out_handle:
            pickle.dump(self.test_items, pickle_out_handle, protocol=pickle.HIGHEST_PROTOCOL)

    def format_suite_for_baseline(self):
        """format experiment dictionaries to match baseline experimentDict keys / elements
        and populate experiment list"""
        formatted_dict = {}
        experiment_list = []
        for test_item in self.test_items:
            experiment = test_item['test_ID']
            experiment_list.append(experiment)  # add experiment id to list - required in main()
            formatted_dict[experiment] = test_item
        return formatted_dict, experiment_list

    def item_open(self, ind=None):
        """add / open / view test item in Form2Dict tkinter window and commit changes"""
        if ind is not None:
            item = self.test_items[ind]
        else:
            item = None
        exp_gui = TestItemGUI(test_item=item)
        if exp_gui.cancelled:
            print('test item cancelled')
        else:
            print('test item committed')
            test_item = exp_gui.current_dict
            valid = exp_gui.current_valid
            if valid:
                if ind is not None:  # remove current entry
                    self.test_items.pop(ind)
                    self.test_items.insert(ind, test_item)
                else:
                    self.test_items.append(test_item)
                self.entries_populate()
            # exp_gui.form_root.destroy()

    def item_up(self, ind):
        """shift test item up one index in suite list"""
        if ind > 0:
            self.entries_clear()
            item = self.test_items.pop(ind)
            self.test_items.insert(ind-1, item)
            self.entries_populate()

    def item_down(self, ind):
        """shift test item down one index in suite list"""
        last = len(self.test_items) - 1
        if ind < last:
            self.entries_clear()
            item = self.test_items.pop(ind)
            self.test_items.insert(ind+1, item)
            self.entries_populate()

    def item_top(self, ind):
        """shift test item up to top index in suite list"""
        if ind > 0:
            self.entries_clear()
            item = self.test_items.pop(ind)
            self.test_items.insert(0, item)
            self.entries_populate()

    def item_bottom(self, ind):
        """shift test item down to bottom index in suite list"""
        last = len(self.test_items) - 1
        if ind < last:
            self.entries_clear()
            item = self.test_items.pop(ind)
            self.test_items.insert(last, item)
            self.entries_populate()

    def item_remove(self, ind):
        """remove test item from suite list"""
        self.entries_clear()
        self.test_items.pop(ind)
        # self.test_entries.pop(ind)
        self.entries_populate()

    def entries_clear(self):
        for entry in self.test_entries:
            for widget in entry:
                widget.destroy()

    def entries_populate(self):
        """populate / refresh test item entries by clearing and recreating all widgets"""
        self.entries_clear()
        for ind, test_item in enumerate(self.test_items):
            row = Frame(self.root)
            ent = Entry(row, width=60)
            ent.insert(0, test_item['test_ID'])
            view_but = Button(row, text='view', width=4, command=(lambda i=ind: self.item_open(i)))
            view_but.pack(side=RIGHT)
            remove_but = Button(row, text='remove', width=6, command=(lambda i=ind: self.item_remove(i)))
            remove_but.pack(side=RIGHT)
            top_but = Button(row, text=u'\u2912', width=1, command=(lambda i=ind: self.item_top(i)))
            top_but.pack(side=RIGHT)
            up_but = Button(row, text=u'\u2bc5', width=2, command=(lambda i=ind: self.item_up(i)))
            up_but.pack(side=RIGHT)
            down_but = Button(row, text=u'\u2bc6', width=2, command=(lambda i=ind: self.item_down(i)))
            down_but.pack(side=RIGHT)
            bottom_but = Button(row, text=u'\u2913', width=1, command=(lambda i=ind: self.item_bottom(i)))
            bottom_but.pack(side=RIGHT)
            row.pack(side=TOP, fill=X, padx=5, pady=0)
            ent.pack(side=RIGHT, expand=YES, fill=X)

            self.test_entries.append((row, ent, view_but, remove_but))


if __name__ == '__main__':
    TestSuiteGUI(stand_alone=True)
