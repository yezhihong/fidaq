########################################################################################################################
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
########################################################################################################################

from tkinter import *
import pickle
import re
import easygui as eg
import os
from config_constants import *


class TestItemGUI(object):
    """GUI form for creating, loading, viewing, changing test item dictionaries"""
    def __init__(self, test_item=None):
        # self.form_root = Tk()
        self.form_root = Toplevel()
        self.current_valid = False
        self.cancelled = True
        ##############################################################
        #   set up TK variables
        self.type_list_var = StringVar(self.form_root)
        self.select_type_str = 'select type...'
        self.type_list = [self.select_type_str]
        self.type_list.extend(TEST_TYPE_LIST)
        self.type_list_var.set(self.type_list[0])
        self.type_list_var.get()

        self.source_list_var = StringVar(self.form_root)
        self.select_source_str = 'select source...'
        self.source_list = [self.select_source_str]
        self.source_list.extend(PHOTON_SOURCE_LIST)
        self.source_list_var.set(self.source_list[0])
        self.source_list_var.get()

        self.filter_list_var = StringVar(self.form_root)
        self.select_filter_str = 'select filter...'
        self.filter_list = [self.select_filter_str]
        self.filter_list.extend(FILTER_TYPE_LIST)
        self.filter_list_var.set(self.filter_list[0])
        self.filter_list_var.get()

        self.ecal_var = IntVar(self.form_root)
        self.entries = self.make_form()
        ###############################################################
        #   set up experiment variables
        self.current_dict = {}
        self.default_dir = os.path.join(DEFAULT_DIR, EXPERIMENT_DIR, '')
        self.file_extension = EXPERIMENT_FILE_EXTENSION
        cancel_button = Button(self.form_root, text='Cancel', width=10, command=lambda: self.cancel())
        cancel_button.pack(side=LEFT, padx=5, pady=5)
        if test_item is not None:
            #   set up buttons for viewing / changing existing test item
            self.entries_populate(test_item)
            commit_button = Button(self.form_root, text='Commit', width=10, command=lambda: self.dict_add_commit())
            commit_button.pack(side=LEFT, padx=5, pady=5)
        else:
            #   set up buttons for adding / loading new test item
            new_button = Button(self.form_root, text='New', width=10, command=(lambda: self.entries_clear()))
            new_button.pack(side=LEFT, padx=5, pady=5)
            load_button = Button(self.form_root, text='Load', width=10, command=(lambda: self.dict_load()))
            load_button.pack(side=LEFT, padx=5, pady=5)
            save_button = Button(self.form_root, text='Save', width=10, command=(lambda: self.dict_save()))
            save_button.pack(side=LEFT, padx=5, pady=5)
            add_button = Button(self.form_root, text='Add', width=10, command=lambda: self.dict_add_commit())
            add_button.pack(side=LEFT, padx=5, pady=5)
        self.form_root.wait_window()
        # self.form_root.protocol("WM_DELETE_WINDOW", self.cancel())

    def cancel(self):
        self.cancelled = True
        self.form_root.destroy()

    def make_form(self):
        """create form widgets from """
        fields = PARAMETER_FIELD_LIST
        optional_fields = OPTIONAL_FIELDS
        entries = []
        for ind, [field, vartype] in enumerate(fields):
            row = Frame(self.form_root)
            lab = Label(row, width=15, text=field, anchor='w')
            row.pack(side=TOP, fill=X, padx=5, pady=1)
            lab.pack(side=LEFT)
            if field == 'test_type':
                ent = OptionMenu(row, self.type_list_var, *self.type_list)
                # ent.configure(width=12)
                ent.pack(side=LEFT)
            elif field == 'photon_source':
                ent = OptionMenu(row, self.source_list_var, *self.source_list)
                # ent.configure(width=18)
                ent.pack(side=LEFT)
            elif field == 'filter_type':
                ent = OptionMenu(row, self.filter_list_var, *self.filter_list)
                # ent.configure(width=18)
                ent.pack(side=LEFT)
            elif vartype == bool:
                ent = Checkbutton(row, width=10, onvalue=1, offvalue=0, variable=self.ecal_var)
                ent.pack(side=LEFT)
            else:
                ent = Entry(row, width=50)
                ent.pack(side=LEFT, expand=YES, fill=X)

                if field in FIELD_DEFAULTS:
                    ent.insert(0, str(FIELD_DEFAULTS[field]))

            if field in optional_fields:
                but = Button(row, text='disable', width=6)
                but.configure(command=(lambda i=ind: self.entry_change_state(i)))
                but.pack(side=LEFT, padx=5, pady=0)
            else:
                but = None
            entries.append((field, vartype, but, ind, ent))

        return entries

    def dict_add_commit(self):
        self.current_dict, self.current_valid = self.dict_get()
        if self.current_valid:
            self.cancelled = False
            self.form_root.destroy()
        else:
            print('item invalid')

    def dict_load(self):
        """load experiment dictionary from file"""
        file_name_in = eg.fileopenbox('select experiment file to open',
                                      default=self.default_dir,
                                      filetypes=['*.' + self.file_extension])
        if not file_name_in:
            return
        fname_in = file_name_in
        with open(fname_in, 'rb') as pickle_in_handle:
            exp_dict = pickle.load(pickle_in_handle)

        self.entries_populate(exp_dict)

    def dict_save(self):
        """save dictionary of form values to disk"""

        dict_out, valid = self.dict_get()
        #   check that dictionary is valid
        if not valid:
            return

        default_location = os.path.join(self.default_dir, dict_out['test_ID'])
        file_name_out = eg.filesavebox(msg='select experiment save location',
                                       title='Save Experiment',
                                       default=default_location,
                                       filetypes=['*.' + self.file_extension])
        if not file_name_out:
            return
        # check for extension in overwritten file - avoid duplicating extension
        if self.file_extension in file_name_out[-4:]:
            fname_out = file_name_out
        else:
            fname_out = "{}.{}".format(file_name_out, self.file_extension)
        with open(fname_out, 'wb') as pickle_out_handle:
            pickle.dump(dict_out, pickle_out_handle, protocol=pickle.HIGHEST_PROTOCOL)

    def dict_get(self):
        """return form entry values as dictionary"""
        dict_out = {}
        valid = True
        for entry in self.entries:
            [field, vartype, _, _, ent] = entry
            if field == 'test_type':
                if self.type_list_var.get() == self.select_type_str:
                    eg.msgbox('select test type', 'Test Type Error')
                    valid = False
                    return _, valid
                else:
                    dict_out[field] = self.type_list_var.get()
            elif field == 'photon_source':
                if self.source_list_var.get() == self.select_source_str:
                    eg.msgbox('select photon source', 'Photon Source Error')
                    valid = False
                    return _, valid
                else:
                    dict_out[field] = self.source_list_var.get()
            elif field == 'filter_type':
                if self.filter_list_var.get() == self.select_filter_str:
                    eg.msgbox('select filter type', 'Filter Type Error')
                    valid = False
                    return _, valid
                else:
                    dict_out[field] = self.filter_list_var.get()
            elif field == 'energy_cal':
                dict_out[field] = self.ecal_var.get()
            else:
                if ent.get() == '':
                    eg.msgbox('all fields must be either populated, or deactivated', 'Field Value Error')
                    valid = False
                    return _, valid
                if ent.get() == 'N/A':
                    vartype = str
                if vartype is str:
                    dict_out[field] = ent.get()
                elif vartype in [float, int]:
                    dict_out[field] = self.entry_to_num(ent.get(), vartype=vartype)

        #   enforce divisibility of timeResolution and max_view_period (compare rounded float/int division values)
        check_div_float = round(dict_out['timeResolution'] / (dict_out['max_view_period'] / 1e6), 8)
        check_div_int = int(round(dict_out['timeResolution'] / (dict_out['max_view_period'] / 1e6), 8))
        if not check_div_float == check_div_int:
            eg.msgbox('timeResolution must be evenly divisible by max_view_period', 'Time Division Error')
            valid = False
            return _, valid

        return dict_out, valid

    def entry_change_state(self, ind):
        """toggle state of entry"""
        _, _, but, index, ent = self.entries[ind]
        if ent['state'] == 'normal':
            self.entry_disable(but, ent)
        elif ent['state'] == 'disabled':
            self.entry_enable(but, ent)

    def entry_disable(self, but, ent):
        """disable given entry"""
        ent.mem_val = ent.get()
        ent.delete(0, END)
        ent.insert(0, 'N/A')
        ent.configure(state='disabled')
        but.configure(text='enable')

    def entry_enable(self, but, ent):
        """enable geven entry"""
        ent.configure(state='normal')
        but.configure(text='disable')
        ent.delete(0, END)
        ent.insert(0, ent.mem_val)

    def entry_to_num(self, val_string, vartype=int):
        """convert entry text to numerical values, either singular or list
        based on numtype and list identifier characters"""
        list_chars = [',', '[', ']', '{', '}', '(', ')']
        if any(char in val_string for char in list_chars):
            # remove list bracketing characters
            val_replaced = re.sub('" "|\[|\]|\{|\}|\(|\)', '', val_string)
            val_list = val_replaced.split(',')
            if vartype == int:
                val_stripped = [val.split('.', 1)[0] for val in val_list]
                val_numeric = [vartype(val) for val in val_stripped]
            else:
                val_numeric = [vartype(val) for val in val_list]
        else:
            if vartype == int:
                val_stripped = val_string.split('.', 1)[0]  # handle decimal entry for integers
                val_numeric = vartype(val_stripped)
            else:
                val_numeric = vartype(val_string)

        return val_numeric

    def entries_clear(self):
        """clear all entry values"""
        for _, _, _, _, entry in self.entries:
            if entry.winfo_class() == "Entry":
                entry.configure(state='normal')
                entry.delete(0, END)
        self.type_list_var.set(self.type_list[0])  # return value to 'select...'
        self.source_list_var.set(self.source_list[0])  # return value to 'select...'
        self.filter_list_var.set(self.filter_list[0])  # return value to 'select...'
        self.ecal_var.set(0)  # set energy calibration to False

    def entries_populate(self, exp_dict):
        """populate widgets with dictionary members"""
        self.entries_clear()
        for ind, [field, vartype, but, _, entry] in enumerate(self.entries):
            ent_field = field
            for dict_field in exp_dict:
                if dict_field == ent_field:
                    if dict_field == 'test_type':
                        self.type_list_var.set(exp_dict[dict_field])
                    elif dict_field == 'photon_source':
                        self.source_list_var.set(exp_dict[dict_field])
                    elif dict_field == 'filter_type':
                        self.filter_list_var.set(exp_dict[dict_field])
                    elif vartype == bool:
                        if exp_dict[dict_field]:
                            entry.select()
                        else:
                            entry.deselect()
                    else:
                        entry.delete(0, END)
                        entry.insert(0, exp_dict[dict_field])
                    if exp_dict[dict_field] == 'N/A' or exp_dict[dict_field] == '':
                        self.entry_disable(but, entry)


if __name__ == '__main__':
    TestItemGUI()
