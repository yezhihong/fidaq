########################################################################################################################
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
########################################################################################################################

DEFAULT_DIR = r'\\server1\X\DM\Test Suites'
EXPERIMENT_DIR = 'Experiments'
TEST_SUITE_DIR = 'Suites'
EXPERIMENT_FILE_EXTENSION = 'exp'   # (EXP)erminet
SUITE_FILE_EXTENSION = 'xts'        # e(X)periment (T)est (S)uite

# list of test types
TEST_TYPE_LIST = ['SPECTRUM',
                  'UNIFORMITY',
                  'STABILITY',
                  'DYNAMIC',
                  'ENERGY_CAL']

TEST_TYPE_TITLES = {
    'SPECTRUM': 'Spectral scan',
    'UNIFORMITY': 'Uniformity test',
    'STABILITY': 'Short term stability test',
    'DYNAMIC': 'Dynamic response test',
    'ENERGY_CAL': 'K-edge energy calibration',
}

# list of test parameters and data types
PARAMETER_FIELD_LIST = [('test_ID', str),
                        ('test_type', str),
                        ('energy_cal', bool),
                        ('photon_source', str),
                        ('filter_type', str),
                        ('filter_thickness', float),
                        ('hvList', int),
                        ('max_view_period', int),
                        ('timeResolution', float),
                        ('totTime', float),
                        ('shutter_off', float),
                        ('shutter_on', float),
                        ('tubeVoltageList', int),
                        ('tubeCurrentList', float),
                        ('hvStabTime', int),
                        ('xRayStabTime', int),
                        ('distance', int),
                        ('thresholds', int),
                        ('threshStep', int),
                        ('startThresh', int),
                        ('endThresh', int),
                        ('runs', int)]

# list of fields which can be deactiveated with 'N/A' value
OPTIONAL_FIELDS = ['shutter_off',
                   'shutter_on',
                   'tubeVoltageList',
                   'tubeCurrentList',
                   'filter_thickness',
                   'thresholds',
                   'threshStep',
                   'startThresh',
                   'endThresh']

FIELD_DEFAULTS = {
    'runs': 1,
}

# list of photon sources
PHOTON_SOURCE_LIST = ['xray',
                      'am241',
                      'co57']

FILTER_TYPE_LIST = ['None',
                    'Cu',
                    'Pb',
                    'CeO2',
                    'Al']
