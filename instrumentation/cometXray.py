########################################################################################################################
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
########################################################################################################################

import serial
import logging
from time import sleep

from easygui import ccbox


logger = logging.getLogger(__name__)

# todo: when manually setting shutter on check interlock


class comet:
    numBits = 1000

    def __init__(self, stabTime=60):
        logger.info('Initializing COMET X-RAY Tube Controller')
        self.stabTime = stabTime
        self.tubeVoltage = 0
        self.comet = serial.Serial(
            port='COM1',
            baudrate=9600,
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS,
            xonxoff=1,
            timeout=0.2)
        self.comet.write('\x02')      # Send CTRL+B for STX
        # Todo: Put a check here to if comet is in mode 400 else make a popup
        self.comet.write('M400\r')  # Set High current mode
        sleep(5)
        self.comet.write('OF\r')
        self.comet.write('NI6\r')   # Switch to MO938 comms mode
        self.comet.write('T9999\r')   # set exposure time to infinite
        # zStatus = self.comet.readline()
        # if not zStatus.find('Z') > -1:
        #     logger.info('Unable to communicate with Comet X-ray controller')
        #     raise serial.SerialException
        garbage = self.comet.read(comet.numBits)

    def closeComet(self):
        self.comet.close()

    def readyToXray(self):
        self.comet.write('Z\r')
        ans = self.comet.read(comet.numBits)
        #ans = ans.replace(" ", "").replace('\r', "")
        return True if 'Z8' in ans else False

    def setTubeVoltage(self, voltage):
        if voltage != self.tubeVoltage:
            if 7.5 <= voltage <= 160:
                self.comet.write("u%s\r" % int(voltage * 10))
                logger.info('Setting X-Ray Tube Voltage to %3.1fkV' % voltage)
                self.tubeVoltage = voltage
                #sleep(self.stabTime)
            else:
                logger.error('Invalid X-Ray Tube Voltage!')
        else:
            logger.info('X-Ray Tube Voltage already at %3.1fkV' % voltage)
        return self.comet.read(comet.numBits)

    def setTubeCurrent(self, current, stabTime=60):
        if current == 0:
            logger.info('Setting X-Ray Tube Current to %2.2fmA' % current)
            self.shutterControl('off')
        elif 0 < current <= 6:
            logger.info('Setting X-Ray Tube Current to %2.2fmA' % current)
            newMode = 'M401'
            oldMode = self.checkMode()
            if newMode != oldMode:
                self.shutterControl('off')
                self.comet.write('%s\r' % newMode)
                sleep(5)
                self.comet.write("i%s\r" % int(current * 100))
                self.shutterControl('on')
            else:
                self.comet.write("i%s\r" % int(current * 100))
                self.shutterControl('on')
            logger.info('Stabilizing tube current for %d seconds' % self.stabTime)
            sleep(self.stabTime)
            logger.info('Stabilization period done')
        elif 6 < current <= 25:
            logger.info('Setting X-Ray Tube Current to %2.2fmA' % current)
            newMode = 'M400'
            oldMode = self.checkMode()
            if newMode != oldMode:
                self.shutterControl('off')
                self.comet.write('%s\r' % newMode)
                sleep(5)
                self.comet.write("i%s\r" % int(current * 100))
                self.shutterControl('on')
            else:
                self.comet.write("i%s\r" % int(current * 100))
                self.shutterControl('on')
            logger.info('Stabilizing tube current for %d seconds' % self.stabTime)
            sleep(self.stabTime)
            logger.info('Stabilization period done')
        else:
            logger.error('Invalid X-Ray Tube Current!')
        return self.comet.read(comet.numBits)

    def shutterControl(self, state='off'):
        shCtrl = {'on': 'ON\r', 'off': 'OF\r'}
        if state in ['on', 'off']:
            self.comet.write(shCtrl[state])
            #sleep(self.stabTime)
        else:
            self.comet.write(shCtrl['off'])
            logger.error('Invalid shutter command')
        return self.comet.read(comet.numBits)

    def checkMode(self):
        garbage = self.comet.read(comet.numBits)
        self.comet.write('M\r')
        mode = self.comet.read(comet.numBits)
        return mode.replace(" ", "").replace("\r", "")

    def checkXrayArm(self):
        while not self.readyToXray():
            logger.warn('X-ray tube is not armed!')
            waitForArm = ccbox(title='X-ray tube is not armed!',
                               msg='Please arm the X-ray tube and press Continue or Cancel',
                               choices=['C[o]ntinue', 'C[a]ncel'], default_choice='Continue', cancel_choice='Cancel')
            if not waitForArm:
                raise Exception('User terminated the test')

    def checkInterlock(self):
        garbage = self.comet.read(self.numBits)
        self.comet.write("C\r")
        mode = self.comet.read(self.numBits)
        return mode.replace(" ", "").replace("\r", "")

    def tubeConditioning(self):
        garbage = self.comet.read(self.numBits)
        self.comet.write("W1400\r")
        garbage = self.comet.read(self.numBits)
        self.comet.write("M501\r")
        garbage = self.comet.read(self.numBits)
        return

    def checkStatus(self):
        garbage = self.comet.read(self.numBits)
        self.comet.write("Z\r")
        status = self.comet.read(self.numBits)
        print status
