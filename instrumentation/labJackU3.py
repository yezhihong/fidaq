########################################################################################################################
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
########################################################################################################################

import u3
import logging
from time import sleep

logger = logging.getLogger(__name__)

# DAC Configs
DAC_0_ADDR = 5000
DAC_1_ADDR = 5002
DAX_MIN = 0.04
DAX_MAX = 4.95

# ADC/AIN Channels
GND_REF_CHANNEL = 31

# PWM
PWM_16BIT_TOTAL_TICK = 65536
PWM_16BIT_MODE = 0
PWM_MAX_DIVISOR = 256
PWM_CLOCK_ARRAY = [0, 0, 0, 1e6, 4e6, 12e6, 48e6]
PWM_1MHZ_CLOCK = 3
PWM_4MHZ_CLOCK = 4
PWM_12MHZ_CLOCK = 5
PWM_48MHZ_CLOCK = 6

# Pin Number Mapping
F0 = 0
F1 = 1
F2 = 2
F3 = 3
F4 = 4
F5 = 5
F6 = 6
F7 = 7
E0 = 8
E1 = 9
E2 = 10
E3 = 11
E4 = 12
E5 = 13
E6 = 14
E7 = 15
greenStackLight = F1
yellowStackLight = F0
redStackLight = F2


class LabJack:
    def __init__(self):
        self.dev = u3.U3()
        self.slgreenState = 'off'
        self.slyellowState = 'off'
        self.slredState = 'off'
        # Init Instruments connected to LabJack
        # Init Instruments connected to LabJack
        self.shutter = shutter(labjack=self)
        # set all F pins to digital outputs
        self.dev.configU3(FIOAnalog=0x00, FIODirection=0x00, EIOAnalog=0x00, EIODirection=0x00)

    def close(self):
        self.dev.close()

    def setDAC0(self, output_voltage):
        if output_voltage >= DAX_MIN and output_voltage <= DAX_MAX:
            self.dev.writeRegister(DAC_0_ADDR, output_voltage)

    def setDAC1(self, output_voltage):
        if output_voltage >= DAX_MIN and output_voltage <= DAX_MAX:
            self.dev.writeRegister(DAC_1_ADDR, output_voltage)

    def setFIOAsInput(self, fioNum):
        self.dev.setFIOState(fioNum, 0)

    def setFIOAsOutput(self, fioNum):
        self.dev.setFIOState(fioNum, 1)

    def setFIOHigh(self, fioNum):
        self.dev.setDOState(fioNum, 1)

    def setFIOLow(self, fioNum):
        self.dev.setDOState(fioNum, 0)

    def getFIOState(self, fioNum):
        self.dev.getDIState(fioNum)

    def setAIN(self, fioNum):
        self.dev.configAnalog(fioNum)

    def getAIN(self, fioNum):
        self.dev.getAIN(fioNum, GND_REF_CHANNEL)

    def _findBestTimerClockAndDivisor(self, frequency):
        """figures out the best combo of clock and divisor to get closest to your frequency"""

        # Start of by assuming we have a very small frequency, in which case we use the smallest clock
        baseClock = PWM_1MHZ_CLOCK
        # Some clocks wont be able to generate larger frequencies so need to keep going till we find the right one
        if frequency > 15.26:
            baseClock = PWM_4MHZ_CLOCK
        if frequency > 61.04:
            baseClock = PWM_12MHZ_CLOCK
        if frequency > 183.11:
            baseClock = PWM_48MHZ_CLOCK
        if frequency > 732.42:
            # 732Hz is the largest frequency possible so we need to use this
            return (baseClock, 1)

        divisor = int(round(PWM_CLOCK_ARRAY[baseClock] / (PWM_16BIT_TOTAL_TICK * frequency)))
        # In case too large a divisor is required use the largest possible
        if divisor > 256:
            divisor = 256
        elif divisor == 0:  # I think it's mathematically impossible to get here
            divisor = 1
        return (baseClock, divisor)

    def setPWM0(self, fioNum, duty_cycle, frequency):
        baseClock, divisor = self._findBestTimerClockAndDivisor(frequency)
        self.dev.getFeedback(self.dev.configTimerClock(TimerClockBase=baseClock, TimerClockDivisor=divisor))
        self.dev.getFeedback(self.dev.configIO(TimerCounterPinOffset=fioNum, NumberOfTimersEnabled=2))
        # calculate number of ticks/65536 where the output will be low
        duty_cycle_ticks = int((1-duty_cycle) * PWM_16BIT_TOTAL_TICK)
        self.dev.getFeedback(u3.Timer0Config(0, Value=duty_cycle_ticks))

    def setPWM1(self, fioNum, duty_cycle, frequency):
        self.dev.getFeedback(self.dev.configIO(TimerCounterPinOffset=fioNum))
        baseClock, divisor = self._findBestTimerClockAndDivisor(frequency)
        self.dev.getFeedback(self.dev.configTimerClock(TimerClockBase=baseClock, TimerClockDivisor=divisor))
        self.dev.getFeedback(self.dev.configIO(NumberOfTimersEnabled=2))
        # calculate number of ticks/65536 where the output will be low
        duty_cycle_ticks = int((1 - duty_cycle) * PWM_16BIT_TOTAL_TICK)
        u3.Timer1Config(0, Value=duty_cycle_ticks)

    def disablePWM0(self,shutterPin):
        self.dev.configIO(NumberOfTimersEnabled=0)
        self.dev.setDIOState(shutterPin,1)
        self.dev.setDOState(shutterPin,0)
        #u3.Timer0(Value=0, UpdateReset=True)

    def disablePWM1(self):
        self.dev.configIO(NumberOfTimersEnabled=0)
        #u3.Timer1(Value=0, UpdateReset=True)

    def stackLights(self, slQueue):
        loop = True
        redflashCount = 0
        yellowflashCount = 0
        while loop:
            if slQueue.empty():
                if self.slgreenState == 'off':
                    self.setFIOLow(greenStackLight)
                elif self.slgreenState == 'on':
                    self.setFIOHigh(greenStackLight)
                if self.slyellowState == 'off':
                    self.setFIOLow(yellowStackLight)
                elif self.slyellowState == 'on':
                    self.setFIOHigh(yellowStackLight)
                elif self.slyellowState == 'flashing':
                    self.setFIOLow(yellowStackLight) if yellowflashCount // 5 % 2 else self.setFIOHigh(yellowStackLight)
                    yellowflashCount += 1
                if self.slredState == 'off':
                    self.setFIOLow(redStackLight)
                elif self.slredState == 'on':
                    self.setFIOHigh(redStackLight)
                elif self.slredState == 'flashing':
                    self.setFIOLow(redStackLight) if redflashCount//5 % 2 else self.setFIOHigh(redStackLight)
                    redflashCount += 1
                sleep(0.095)
            else:
                newState = slQueue.get()
                # logger.info('new state = %s, %s' % (newState[0],newState[1]))
                if newState[0] == 'stop':
                    self.setFIOHigh(greenStackLight)
                    self.setFIOLow(yellowStackLight)
                    self.setFIOLow(redStackLight)
                    loop = False
                elif newState[0].lower() == 'green':
                    self.slgreenState = newState[1]
                elif newState[0].lower() == 'yellow':
                    self.slyellowState = newState[1]
                    yellowflashCount = 0
                elif newState[0].lower() == 'red':
                    self.slredState = newState[1]
                    redflashCount = 0
                slQueue.task_done()

class shutter:
    def __init__(self, labjack, shutterPin=E0):
        self.shutterPin = shutterPin
        self.labjack = labjack
        self.labjack.setFIOAsOutput(self.shutterPin)
        #self.labjack.setFIOLow(self.shutterPin)

    def open(self):
        self.labjack.setFIOHigh(self.shutterPin)

    def close(self):
        self.labjack.setFIOLow(self.shutterPin)

    def pwm(self, onTime=1, offTime=1, duration=10):
        # onTime and OffTime given in seconds
        if onTime < 0.1 or offTime < 0.1:
            logger.error('Desired shutter open/close speed is too fast!')
            return
        duty_cycle = float(onTime)/float(onTime + offTime)
        frequency = 1.0/float(onTime + offTime)
        # self.labjack.setPWM0(self.shutterPin, duty_cycle, frequency)
        # self.close()
        sleep(offTime)
        for i in range(duration):
            self.open()
            sleep(onTime)
            self.close()
            sleep(offTime)

    def stopPWM(self):
        self.labjack.disablePWM0(self.shutterPin)

    def shCtrl(self, shQueue):
        loop = True
        while loop:
            if shQueue.empty():
                sleep(0.1)
            else:
                task = shQueue.get()
                if task == 'open':      # open shutter
                    self.open()
                elif task == 'close':   # close shutter
                    self.close()
                elif 'pwm' in task:   # pulse shutter
                    # pwm 1 1 10 is ontime=1, offtime=1 and duration = 10 cycles
                    onTime, offTime, duration = [float(x) for x in task.split()[1:]]
                    self.pwm(onTime, offTime, int(duration))
                elif task == 'stop':    # stop pulsing shutter
                    pass                # not used
                    # self.stopPWM()
                elif task == 'end':     # end shutter control loop
                    loop = False
                shQueue.task_done()
