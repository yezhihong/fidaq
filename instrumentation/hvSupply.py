########################################################################################################################
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
########################################################################################################################

import serial
import logging
from time import sleep
from Phidget22.Devices.VoltageOutput import *
from Phidget22.Devices.VoltageInput import *


logger = logging.getLogger(__name__)


class hvSup():
    def __init__(self, stabTime=2):
        logger.info('Initializing SR PS325 HV Supply')
        self.stabTime = stabTime
        self.hVoltage = 0
        self.hv = serial.Serial(
            port='COM6',
            baudrate=115200,
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS,
            xonxoff=1,
            timeout=1)
        self.hv.write('*IDN?\r')
        self.hv.readline()
        self.hv.write('*CLS\r')
        self.hv.write('VLIM-%.2E\r' % 1250)

    def closeHv(self):
        self.hv.close()

    def setHvVoltage(self, voltage):
        if self.hVoltage != voltage:
            if voltage <= 1200:
                logger.info('Setting HV Supply Voltage to {}V'.format(-voltage))
                self.hv.write('VSET-%.2E\r' % voltage)
                self.hVoltage = voltage
            else:
                logger.error('Invalid HV Voltage or voltage out of range' % voltage)
        else:
            logger.info('HV Supply Voltage already at {}V'.format(-voltage))

    def setHvState(self, state='off'):
        if state == 'on':
            self.hv.write("HVON\r")
            logger.info('Waiting {}s for HV to stabilize'.format(self.stabTime))
            sleep(self.stabTime)
        elif state == 'off':
            self.hv.write("HVOF\r")
        else:
            raise ValueError('Invalid HV state: {}'.format(state))


class matsuhvSup():

    def __init__(self, stabTime=2):
        logger.info('Initializing Matsusada HV Supply')
        # dev info
        self.hvPort = 0
        self.vPort = 1
        self.iPort = 2
        self.stabTime = stabTime

        # State Variables
        self.hvState = 'off'
        self.voltageSetting = 0

        # scale values
        self.vScale = 150.0
        self.vmonScale = 0.00328946
        self.vmonOffset = 0.0098
        self.imonScale = 0.00327912
        self.imonOffset = 0.0142

        # open high voltage set port and set output to zero V
        self.hv = VoltageOutput()
        self.hv.setHubPort(self.hvPort)
        self.hv.openWaitForAttachment(500)
        self.hv.setVoltage(0)

        # open current monitor port
        self.monI = VoltageInput()
        self.monI.setHubPort(self.iPort)
        self.monI.setIsHubPortDevice(1)
        self.monI.open()

        # open voltage monitor port
        self.monV = VoltageInput()
        self.monV.setHubPort(self.vPort)
        self.monV.setIsHubPortDevice(1)
        self.monV.open()

    def closeHv(self):
        self.monI.close()
        self.monV.close()
        self.hv.close()

    def setHvVoltage(self, voltage):
        if 0 <= voltage <= 1200:
            self.voltageSetting = voltage
            logger.info('Setting HV Supply Voltage to {}V'.format(-voltage))
            if self.hvState == 'on':
                self.hv.setVoltage(voltage / self.vScale)
                logger.info('Waiting {}s for HV to stabilize'.format(self.stabTime))
                sleep(self.stabTime)
        else:
            logger.error('Invalid HV Voltage or voltage out of range: {}'.format(voltage))

    def setHvState(self, state='off'):
        if state == 'on':
            self.hvState = state
            self.hv.setVoltage(self.voltageSetting / self.vScale)
            logger.info('Waiting {}s for HV to stabilize'.format(self.stabTime))
            sleep(self.stabTime)
        elif state == 'off':
            self.hvState = state
            self.hv.setVoltage(0)
        else:
            raise ValueError('Invalid HV state: {}'.format(state))

    def readMonitors(self):
        monV = self.monV.getVoltage()
        monI = self.monI.getVoltage()
        return (monV + self.vmonOffset) / self.vmonScale, (monI + self.imonOffset) / self.imonScale
