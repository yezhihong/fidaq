########################################################################################################################
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
########################################################################################################################

import re
import time
import serial
import logging
from collections import namedtuple


logger = logging.getLogger(__name__)


POSITION_REGEX = re.compile('^Position (\d+):')


def parse_position_str(position_str):
    return int(POSITION_REGEX.match(position_str).group(1))


class Filter(namedtuple('Filter', ['material', 'thickness'])):
    def __str__(self):
        return '{} mm {}'.format(self.thickness, self.material)


class FilterWheel(object):
    def __init__(self, response_timeout=5):
        logger.info('Initialize Filter wheel')
        self.serial = serial.Serial(
            port='COM5',
            baudrate=9600,
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS,
            xonxoff=1,
            timeout=0.2)

        self.response_timeout = response_timeout

        self.filters = {
            Filter('None', 0): 1,
            Filter('Cu', 1): 2,
            Filter('Cu', 2): 3,
            Filter('Pb', 1.9): 4,
            Filter('CeO2', 1): 5,
            Filter('Al', 2): 6,
        }

        self.home()

    def home(self):
        logger.info("Set filter to home position")
        self.position = 1

    def close(self):
        self.serial.close()

    @property
    def filter(self):
        try:
            current_position = self.position
            return (filter for filter, position in self.filters if position == current_position).next()
        except StopIteration:
            return None

    @filter.setter
    def filter(self, filter_params):
        filter = Filter(*filter_params)
        try:
            logger.info("Set filter to {}".format(filter))
            self.position = self.filters[filter]
        except KeyError:
            raise KeyError('{} filter not loaded'.format(filter))

    @property
    def position(self):
        self.serial.reset_input_buffer()
        self.serial.write('7')

        return parse_position_str(self._wait_for_response())

    @position.setter
    def position(self, position):
        position = int(position)

        if self.position == position:
            return

        self.serial.write(str(position))

        if parse_position_str(self._wait_for_response()) != position:
            raise Exception()

    def _wait_for_response(self):
        timeout = time.time() + self.response_timeout

        while True:
            response = self.serial.readline()
            if len(response) > 0:
                return response
            if time.time() > timeout:
                raise serial.SerialException('Timeout while waiting for response from filter wheel')
