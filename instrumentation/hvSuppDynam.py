########################################################################################################################
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
########################################################################################################################

import globals
from instrumentation.dummy import DummyHv
from instrumentation.hvSupply import hvSup


def getHV(DMType):
    if globals.USE_DUMMY_HV:
        return DummyHv()
    if hasattr(DMType, 'setHvState') and hasattr(DMType, 'setHvVoltage') and hasattr(DMType, 'closeHv'):
        return DMType
    else:
        return hvSup()
