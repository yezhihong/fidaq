########################################################################################################################
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
########################################################################################################################

import logging
from time import sleep
from serial import SerialException
from LabJackPython import LabJackException

import globals
from instrumentation.dummy import DummyFilterWheel, DummyLabJack, DummyXray
from instrumentation.cometXray import comet
from instrumentation.picoAmmeter import PicoAmmeter
from instrumentation.labJackU3 import LabJack
from instrumentation.filter import FilterWheel


logger = logging.getLogger(__name__)


def initInst(xRayStabTime=60):
    if globals.USE_DUMMY_XRAY:
        cm = DummyXray()
    else:
        try:
            cm = comet(stabTime=xRayStabTime)
            cm.shutterControl('off')
        except SerialException as e:
            logger.error(e.message)
            raise

    try:
        pa = PicoAmmeter()
    except SerialException or ValueError:
        pa = None
        logger.info("Unable to connect to Picoammeter")

    if globals.USE_DUMMY_FILTER_WHEEL:
        fw = DummyFilterWheel()
    else:
        try:
            fw = FilterWheel()
        except SerialException or ValueError:
            fw = None
            logger.info("Unable to connect to filter wheel")

    if globals.USE_DUMMY_LABJACK:
        sh = DummyLabJack()
    else:
        try:
            sh = LabJack()
            sleep(2)
        except LabJackException:
            sh = None
            logger.info("Unable to connect to LabJack and shutter")

    return cm, pa, sh, fw


def closeInst(cm, pa, sh, fw):
    if cm is not None:
        cm.shutterControl('off')
        cm.closeComet()

    if pa is not None:
        pa.closePicoAmmeter()

    if sh is not None:
        sh.shutter.close()
        sh.close()

    if fw is not None:
        fw.close()
