########################################################################################################################
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
########################################################################################################################

import serial
import logging
from time import time, sleep

# Picoammeter Serial Command Configurations
OUTPUT_MAX_BYTE_SIZE = 1000
MESSAGE_PREFIX = "&"
END_OF_LINE = serial.CR + serial.LF  # \r\n
PORT_FILE_DESCRIPTOR = 'COM100' # Can be found my typing `mode` in cmd or through Device Manager

# Tuple Definitions/Lookup tables
unit_prefixes = {'mA':1e-3, 'uA':1e-6, 'nA':1e-9}
currentRangeTuple = {'auto':'&R0', '2nA':'&R1', '20nA':'&R2', '200nA':'&R3', '20uA':'&R5', '200uA':'&R6', '2mA':'&R7'}

# Logger Object
logger = logging.getLogger(__name__)

def serialWriteRead(picoamm, data):
    """Friend Function which writes then reads the response to a serial device. Needs to be declared outside class"""
    serial_out = data + END_OF_LINE
    """Logger prints out the ANSI Escape character at the end in the .txt file; attempted to get rid of it, no luck"""
    logger.debug("Picoammeter command sent: %s" % data) # using data so the extra \r\n doesn't get printed
    picoamm.write(serial_out)
    sleep(0.01)
    pico_output_list = picoamm.read(OUTPUT_MAX_BYTE_SIZE).split(END_OF_LINE)  # everything returned in plain ascii (1 line per result)
    picoamm.flushInput()
    return pico_output_list[:-1]  # An extra \r\n is sent so that is removed here



class PicoAmmeter:

    def __init__(self):
        logger.info('Initializing 9103 Picoammeter')
        self._picoamm = serial.Serial(
            port=PORT_FILE_DESCRIPTOR,
            baudrate=57600,
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS,
            xonxoff=False,
            timeout=1)
        # block till open. Shoudn't take a noticeable amount of time
        while not self._picoamm.isOpen():
            logger.info('Picoammeter on COM port %s not open. Sleeping for 10ms' % PORT_FILE_DESCRIPTOR)
            sleep(0.01)
            pass
        # The 9103 starts off in intervaled sampling which makes it difficult to command, so turn it off initially
        logger.debug('Disabling Interval sampling')
        # The only way (AFAIK) to ensure it's always disabled is to just spam the disable command till it stops
        # There's probably a better way to do this
        # We're checking if the very last thing sent back is a response to the &I command
        # I have never actually looked at how well this works, but the whole init process never takes more than a second
        while "&I" not in serialWriteRead(self._picoamm, "&I0000")[-1]:
            sleep(0.05)
        self._picoamm.flushInput() # In case we asked too many times


    def closePicoAmmeter(self):
        self._picoamm.close()


    def changeDeviceID(self, device_name):
        """Change Device Name to new 10 Character name"""
        if not len(device_name) == 10:
            logger.warn("Invalid device name %s. Name must be 10 characters" % device_name)
            return
        serialWriteRead(self._picoamm, "&P" + device_name)


    def setCurrentRange(self, data):
        rangeCommand = '&R0' # Default to auto
        try:
            rangeCommand = currentRangeTuple[data]
        except:
            logger.warn("Invalid Current range %s" % data)
            return
        serialWriteRead(self._picoamm, rangeCommand)


    def parseSingleCurrentValue(self, single_reading_string):
        """
        Parses the list format that the Picoammeter returns for a single current reading
        and returns a tuple of the value and magnitude e.g (1.25, 1e-6) 
        """
        currentSample = single_reading_string.split(",")
        base = float(currentSample[-2])
        magnitude = unit_prefixes[currentSample[-1]] # Dictionary is defined at the top of this file
        logger.debug("Value: %f * %s" % (base, str(magnitude)))
        return (base*magnitude)


    def getCurrentReading(self):
        """Function assumes that interval reading is off"""
        currentReadingArray = serialWriteRead(self._picoamm, "&S")
        return self.parseSingleCurrentValue(currentReadingArray[0])


    def getIntervalCurrentReading(self, interval, duration):
        """
        Returns an array of readings taken at some interval (in ms) for duration seconds long
        You will always get more readings than you asked for (due to timing issues)
        Some of these readings may be duplicated
        """
        if interval < 50 or interval > 9999:
            logger.warn("A value between 50 and 9999 ms is required. Given %d" % interval)
            return
        # TODO: Find a less messy way of doing this
        interval_arg_zeroed = "0000"
        num_zeroes = len(interval_arg_zeroed) - len(str(interval))
        interval_arg = interval_arg_zeroed[0:num_zeroes] + str(interval)
        serialWriteRead(self._picoamm, "&I" + interval_arg)
        self._picoamm.flushInput() # Just incase theres already data there
        start = time()
        readings = []
        while (time() - start) <= duration:
            readings.extend(serialWriteRead(self._picoamm, "&S"))
        while "&I" not in serialWriteRead(self._picoamm, "&I0000")[-1]:
            sleep(0.05)
        self._picoamm.flushInput() # In case we asked too many times
        return_list = []
        for reading in readings:
            return_list.append(self.parseSingleCurrentValue(reading))
        return return_list


    def enableOrDisableBias(self, enable = False):
        if enable:
            serialWriteRead(self._picoamm, "&B1")
        else:
            serialWriteRead(self._picoamm, "&B0")
            # was this before...looks incorrect: serialWriteRead("B0")  -- V.L.


    """
    enableOrDisableGrounding() might not be working properly
    May need to do some further troubleshooting with some test equipment hooked up to the picoAmmeter
    """
    #def enableOrDisableGrounding(self, picoamm, enable = False):
    def enableOrDisableGrounding(self, enable = False):
        if enable:
            serialWriteRead(self._picoamm, "&G1")
        else:
            serialWriteRead(self._picoamm, "&G0")
        pass


    #def setCurrentOffset(self, picoamm, enableOffset = False, current_range = "auto"):
    def setCurrentOffset(self, enableOffset = False, current_range = "auto"):
        """
        Will subtract the current reading from all future readings 
        until it is disabled or there is a power cycle
        """

        serialWriteRead(self._picoamm, current_range)
            # The above line of code was corrected it was originally
            # serialWriteRead(current_range)
            # -- V.L.

        if enableOffset and current_range is not "auto":
            serialWriteRead(self._picoamm, "&N")


    def setFilter(self, filterValue):
        serial_out = "&F0"
        # The point of this check is to figure out how to format the message. It needs to be &F + 3 digits
        if filterValue in [2, 4, 8]:
        # Initially, code the missing 8; corrected -- V.L.
            serial_out += "0"
        elif filterValue in [16, 32, 64]:
            # The &F0 + 2 digits will make 3 digits so dont add anything
            pass
        else:
            logger.warn("Invalid filter value %d" % filterValue)
            return
        serial_out += str(filterValue)
        serialWriteRead(self._picoamm, serial_out)


    def pwprint(self, data):
        """
            Program which can help during debugging
            HOW TO USE THIS?    Enter in the &## command into data
            e.g.  device.pwprint('&Q')
            -- V.L.
        """
        pico_output_list = serialWriteRead(self._picoamm, data)
        print("Data recieved: ")
        for index, value in enumerate(pico_output_list):
            print("%i: %s" % (index, value))
            
if __name__ == '__main__':
    pa = PicoAmmeter()
    while True:
        print(pa.getCurrentReading())
        sleep(0.5)

