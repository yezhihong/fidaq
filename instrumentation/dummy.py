########################################################################################################################
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
########################################################################################################################

import logging
from instrumentation.filter import Filter


logger = logging.getLogger(__name__)


class DummyFilterWheel(object):
    def __init__(self):
        self._filter = None

    def home(self):
        self._filter = None
        logger.info('Set dummy filter to home position')

    @property
    def filter(self):
        return self._filter

    @filter.setter
    def filter(self, filter_params):
        self._filter = Filter(*filter_params)
        logger.info("Set dummy filter to {}".format(self._filter))


class DummyHv(object):
    def __init__(self):
        logger.info('Dummy HV started')

    def closeHv(self):
        logger.info('Dummy HV stopped')

    def setHvVoltage(self, v):
        logger.info('Dummy HV set to %d V', -v)

    def setHvState(self, state):
        if state == 'on':
            logger.info('Dummy HV on')
        else:
            logger.info('Dummy HV off')


class DummyLabJack(object):
    def __init__(self):
        self.shutter = self.Shutter()

    def stackLights(self, slQueue):
        pass

    class Shutter(object):
        def open(self):
            logger.info('Dummy shutter open')

        def close(self):
            logger.info('Dummy shutter close')

        def shCtrl(self, shQueue):
            pass

class DummyXray(object):
    def __init__(self):
        logger.info('Dummy X-ray started')

    def closeComet(self):
        logger.info('Dummy X-ray stopped')

    def readyToXray(self):
        return True

    def setTubeVoltage(self, v):
        logger.info('Dummy X-ray tube voltage set to %d kV', v)

    def setTubeCurrent(self, i):
        logger.info('Dummy X-ray tube current set to %f mA', i)

    def shutterControl(self, state='off'):
        if state == 'on':
            logger.info('Dummy X-ray on')
        else:
            logger.info('Dummy X-ray off')

    def checkXrayArm(self):
        return True
