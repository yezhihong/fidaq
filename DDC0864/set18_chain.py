########################################################################################################################
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
########################################################################################################################

from six.moves import StringIO

import os
import logging

import globals

from DDC0864.devInit import a2d
from DDC0864.dmFunctions import writeReg, waitForBit

from pcct.utils import chunk_str, pack_u16_be, unpack_u16_be


logger = logging.getLogger(__name__)


def basic_set18_streams(args):
    '''Generates 3 different types of basic streams which will be used by set18_chain_prog'''
    [typeA_set18_list, typeB_set18_list, tm1, tm2, tm3, tm4, tm5, tm6, tm7, tm8, tm9, tm10, tm11, tm12, tm13, tm14,
     tm15, tm16, tm17, tm18, tm19, tm20, tm21] = args
    for k in range(3):
        set18_stream = a2d(tm1[k], 5) + a2d(tm2[k], 1) + a2d(tm3[k], 1) + a2d(tm4[k], 1) + a2d(tm5[k], 1) +\
                       a2d(tm6[k], 2) + a2d(tm7[k], 2) + a2d(tm8[k], 1) + a2d(tm9[k], 2) + a2d(tm10[k], 2) +\
                       a2d(tm11[k], 1) + a2d(tm12[k], 1) + '000' + a2d(tm13[k], 1) + a2d(tm14[k], 1) +\
                       a2d(tm15[k], 2) + a2d(tm16[k], 6) + a2d(tm17[k], 6) + a2d(tm18[k], 3) + a2d(tm19[k], 1) +\
                       a2d(tm20[k], 3) + a2d(tm21[k], 2)
        if k == 1:
            typeA_set18_stream = set18_stream
        elif k == 2:
            typeB_set18_stream = set18_stream
        else:
            def_set18_stream = set18_stream

    return ([typeA_set18_stream, typeB_set18_stream, def_set18_stream])


def broadcast_set18_seq(stream):    # Broadcast data to device
    """Broadcasts 'stream' of bits into set18_chain of device"""
    no_of_reg_writes = int(len(stream)/16)
    writeReg('0x3c', 8, 8, '0x0001')  # Enable Set18 Chain
    for words in range(no_of_reg_writes):
        b = '0x{:04x}'.format( int(stream[words*16:(words+1)*16], base=2) )
        print b
        writeReg('0x43', 15, 0, b)
    writeReg('0x3c', 8, 8, '0x0000')  # Disable Set18 Chain
    return


def sendset18Chain(stream):
    enableSet18Chain(True)

    writes = StringIO()
    for word in reversed(chunk_str(stream, chunk_len=16)):
        writes.write(pack_u16_be(int(word, base=2)))

    globals.buffer.write_chain(globals.detector.ASIC_SET18_CHAIN_BASE, writes.getvalue())

    writeSet18Chain()
    enableSet18Chain(False)


def receiveSet18Chain():
    stream = ''
    num_pixels = 48
    num_reads = num_pixels * 3
    enableSet18Chain(True)
    readSet18Chain()

    reads = globals.buffer.read_chain(globals.detector.ASIC_SET18_CHAIN_BASE, num_reads)

    for word in reversed(chunk_str(reads, chunk_len=2)):
        stream += a2d(unpack_u16_be(word), 16)

    enableSet18Chain(False)
    return stream


def gen_set18_stream(args):
    '''Generates string of bit stream based on args in the correct order'''
    [typeA_set18_stream, typeB_set18_stream, default_set18_stream] = basic_set18_streams(args)
    [typeA_set18_list, typeB_set18_list, tm1, tm2, tm3, tm4, tm5, tm6, tm7, tm8, tm9, tm10, tm11, tm12, tm13, tm14,
     tm15, tm16, tm17, tm18, tm19, tm20, tm21] = args
    data_stream = ""
    set18_order = range(47, -1, -1)
    for set18_num in set18_order:
        if set18_num in typeA_set18_list:
            data_stream = data_stream + typeA_set18_stream
        elif set18_num in typeB_set18_list:
            data_stream = data_stream + typeB_set18_stream
        else:
            data_stream = data_stream + default_set18_stream
        data_stream = data_stream
    return data_stream


def set18_to_csv(fh, stream):
    '''Writes stream data into fh in appropriate location '''
    no_of_set18_writes = int(len(stream) / 48)
    writeStr = ""
    for chunk48 in reversed(range(no_of_set18_writes)):
        b = stream[chunk48 * 48:(chunk48 + 1) * 48]
        set18 = 47 - chunk48
        writeStr = '%d,' % set18
        writeStr = writeStr + '%d,' % int(b[0:5], base=2)  # tm1
        writeStr = writeStr + '%d,' % int(b[5], base=2)  # tm2
        writeStr = writeStr + '%d,' % int(b[6], base=2)  # tm3
        writeStr = writeStr + '%d,' % int(b[7], base=2)  # tm4
        writeStr = writeStr + '%d,' % int(b[8], base=2)  # tm5
        writeStr = writeStr + '%d,' % int(b[9:11], base=2)  # tm6
        writeStr = writeStr + '%d,' % int(b[11:13], base=2)  # tm7
        writeStr = writeStr + '%d,' % int(b[13], base=2)  # tm8
        writeStr = writeStr + '%d,' % int(b[14:16], base=2)  # tm9
        writeStr = writeStr + '%d,' % int(b[16:18], base=2)  # tm10
        writeStr = writeStr + '%d,' % int(b[18], base=2)  # tm11
        writeStr = writeStr + '%d,' % int(b[19], base=2)  # tm12
        writeStr = writeStr + '%d,' % int(b[23], base=2)  # tm13
        writeStr = writeStr + '%d,' % int(b[24], base=2)  # tm14
        writeStr = writeStr + '%d,' % int(b[25:27], base=2)  # tm15
        writeStr = writeStr + '%d,' % int(b[27:33], base=2)  # tm16
        writeStr = writeStr + '%d,' % int(b[33:39], base=2)  # tm17
        writeStr = writeStr + '%d,' % int(b[39:42], base=2)  # tm18
        writeStr = writeStr + '%d,' % int(b[42], base=2)  # tm19
        writeStr = writeStr + '%d,' % int(b[43:46], base=2)  # tm20
        writeStr = writeStr + '%d\n' % int(b[46:48], base=2)  # tm21

        fh.write(writeStr)
    return


def set18_from_csv(tdir, progFile=r"set18_chain_prog_vals_B.csv"):
    """Generates from csv file a data_stream of bits which shall later be broadcast into device"""
    data_stream = ""
    progFile = os.path.join(tdir, 'docs', progFile)
    try:
        with open(progFile, 'r') as fh:
            content = fh.readlines()
    except IOError:
        logger.error('File %s was not found.' % progFile)
        return
    else:
        numSet18s = 48
        for chunk in [numSet18s - i for i in range(numSet18s)]:
            a = content[chunk].replace('\n', '').split(',')
            new_stream = a2d(a[1], 5) + a2d(a[2], 1) + a2d(a[3], 1) + a2d(a[4], 1) + a2d(a[5], 1) + a2d(a[6], 2) +\
                         a2d(a[7], 2) + a2d(a[8], 1) + a2d(a[9], 2) + a2d(a[10], 2) + a2d(a[11], 1) + a2d(a[12], 1) +\
                         '000' + a2d(a[13], 1) + a2d(a[14], 1) + a2d(a[15], 2) + a2d(a[16], 6) + a2d(a[17], 6) +\
                         a2d(a[18], 3) + a2d(a[19], 1) + a2d(a[20], 3) + a2d(a[21], 2)
            data_stream = data_stream + new_stream
    return data_stream


def set18_chain_prog(tdir, tm1=0, tm2=0, tm3=0, tm4=0, tm5=0, tm6=0,tm7=0, tm8=0, tm9=0, tm10=0, tm11=0, tm12=0, tm13=0,
                     tm14=0, tm15=0, tm16=31, tm17=15, tm18=0, tm19=0, tm20=0, tm21=0,
                     writeValsFile=r"set18_chain_write_vals_B.csv", progFile="set18_chain_prog_vals_B.csv",
                     from_csv=1, rst_ctrlr=1):
    """Programs set18_chain of device using one of various modes"""
    progFile = os.path.join(tdir, globals.docDir, progFile)
    writeValsFile = os.path.join(tdir, globals.docDir, writeValsFile)
    if rst_ctrlr:  # !=0
        writeReg('0x3c', 0, 0, '0x0001', 1, 1)  # reset chain controller
        writeReg('0x3c', 0, 0, '0x0000', 1, 1)  # release chain controller
    if from_csv == 0:
        logger.info('Set18 Chain Data will be written into file: %s\n' % writeValsFile)
        with open(writeValsFile, 'w') as fh:
            fh.write(
                'Set18,tm1,tm2,tm3,tm4,tm5,tm6,tm7,tm8,tm9,tm10,tm11,tm12,tm13,tm14,tm15,tm16,tm17,tm18,tm19,tm20,tm21\n')
            args = [tm1, tm2, tm3, tm4, tm5, tm6, tm7, tm8, tm9, tm10, tm11, tm12, tm13,
                    tm14, tm15, tm16, tm17, tm18, tm19, tm20, tm21]

            newstream = gen_set18_stream(args)
            set18_to_csv(fh, newstream)
    else:
        logger.info('Set18 Chain Data from %s will be programmed into the device.' % progFile)
        newstream = set18_from_csv(tdir)
        sendset18Chain(newstream)
    return


def enableSet18Chain(On):
    """ set en_set18_chain to 1 (True) or 0 (False)"""
    if On:
        writeReg('0x3c', 8, 8, '0x0001', 1, 1)  # Enable Set18 Chain
    else:
        writeReg('0x3c', 8, 8, '0x0000', 1, 1)  # Disable Set18 Chain


def writeSet18Chain():
    """ force set to 1 of write set18 bit to write set18 chain and wait for write done """
    writeReg('0x0000e', 15, 15, '0x0000')
    writeReg('0x0000e', 15, 15, '0x0001')
    waitForBit('0x0000e', 13, True)


def readSet18Chain():
    """ force set to 1 of read set18 bit to read set18 chain and wait for read done """
    writeReg('0x0000e', 14, 14, '0x0000')
    writeReg('0x0000e', 14, 14, '0x0001')
    waitForBit('0x0000e', 12, True)
