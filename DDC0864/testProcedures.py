########################################################################################################################
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
########################################################################################################################

import os
import logging
from shutil import copy
from itertools import product
from easygui import msgbox, multenterbox, choicebox, enterbox, EgStore
import globals

from DDC0864.baseline import baseline
from DDC0864.cal_lib import collect_data, loadElectricalCal, loadEnergyCalFromFile
from DDC0864.calcFlux import calcFlux
from DDC0864.dmFunctions import setLeakCompMode, setHpf, setAcsWindow, setLftMode, maskAsicList, setAcsMode, \
    setModeBinAlign, setACSWindowRevB, getFbC, selDataOutputByMM
from DDC0864.dmInitDDC0864 import initDDC0864
from DDC0864.set18_chain import set18_chain_prog
from DDC0864.thresh_chain import perPixThresh, create_thresh_chain_prog

import dmBuffer.DMBufferBringUp as DMBB

import data_output.data_output as data_output

from config_interface.test_suite_gui import TestSuiteGUI
from config_interface.test_suite_text import TestSuiteText


logger = logging.getLogger(__name__)


def sweepThresh(asic_data_structs, tdir, baseLine, experiment, bLine, sweepType, hv, cm, autoAppend=0, pa=None):
    VER_ID = baseLine.sensor['ASICREVISION']
    setModeBinAlign('off')
    setLftMode('nonlft')
    hv.setHvVoltage(baseLine.experimentDict[experiment]['hvList'][0])
    hv.setHvState('on')
    setLeakCompMode(baseLine.baseLineDict[bLine]['modeList'][0], VER_ID)
    setHpf(baseLine.baseLineDict[bLine]['hpfList'][0], VER_ID)
    for acs1, acs2 in baseLine.baseLineDict[bLine]['acsWindowExpList']:
        if VER_ID >= 5:
            setACSWindowRevB(acs1, VER_ID)
        else:
            setAcsWindow(acs1, acs2, VER_ID)
        for acsMode in baseLine.baseLineDict[bLine]['acsModeList']:
            setAcsMode(acsMode)
    if 'xray' in baseLine.experiment['SOURCE'].lower():
        for tubeVoltage in baseLine.experimentDict[experiment]['tubeVoltageList']:
            cm.setTubeVoltage(tubeVoltage)
            for tubeCurrent in baseLine.experimentDict[experiment]['tubeCurrentList']:
                cm.setTubeCurrent(tubeCurrent)
                if tubeCurrent != 0:
                    cm.shutterControl('on')
                sweepThreshVals(asic_data_structs, tdir, baseLine, experiment, sweepType)
    else:

        sweepThreshVals(asic_data_structs, tdir, baseLine, experiment, sweepType)


def genOpenBinThresholds(thresh, lowThresh, highThresh, scanBin, numBins):
    thresholds = [[] for _ in range(numBins)]
    for i in range(numBins):
        if i < scanBin:
            thresholds[i] = lowThresh
        elif i == scanBin:
            thresholds[i] = thresh
        else:
            thresholds[i] = highThresh
    return thresholds


def genClosedBinThresholds(thresh, stepSize, maxThreshold, numBins):
    thresholds = [[] for _ in range(numBins)]
    for i in range(numBins):
        thresholds[i] = thresh if thresh <= maxThreshold else maxThreshold
        thresh += stepSize
    return thresholds


def chkCollectionTimeParams(viewPeriod, timeResolution, totalTime):
    viewPeriodInSec = viewPeriod / 1e6
    warningThreshold = 0.01

    if viewPeriodInSec <= timeResolution:
        numAccumulations = int(round(timeResolution / viewPeriodInSec))
        timeResolution, oldTimeResolution = numAccumulations * viewPeriodInSec, timeResolution

        if timeResolution > (oldTimeResolution * (1 + warningThreshold)):
            logger.warn("Time resolution changed by more than {}% due to rounding".format(warningThreshold * 100))
        logger.info("Using time resolution of {}s".format(timeResolution))
    else:
        raise ValueError("View period is longer than the time resolution")

    if timeResolution <= totalTime:
        numFrames = int(round(totalTime / timeResolution))
        totalTime, oldTotTime = timeResolution * numFrames, totalTime

        if totalTime > (oldTotTime * (1 + warningThreshold)):
            logger.warn("Total time changed by more than {}% due to rounding".format(warningThreshold * 100))
        logger.info("Using a total time of {}s".format(totalTime))
    else:
        raise ValueError("Time resolution is longer than the total time")

    return totalTime, numFrames, timeResolution, numAccumulations


def sweepThreshVals(asic_data_structs, tdir, baseLine, experiment, sweepType):
    maxThreshold = 255
    maxNumMMs = baseLine.dm.N_MODULES
    asicsPerMM = baseLine.dm.N_ASICS_PER_MODULE
    maxNumAsics = 16  # Should be maxNumMMs * asicsPerMM but parseBin2CSV needs 16 to work
    numBins = 6
    stepSize = 1
    VER_ID = baseLine.sensor['ASICREVISION']
    totTime, totFiles, timeResolution, timeResInViews = \
        chkCollectionTimeParams(baseLine.experimentDict[experiment]['max_view_period'],
                                baseLine.experimentDict[experiment]['timeResolution'],
                                baseLine.experimentDict[experiment]['totTime'])

    startThresh, endThresh, threshStep = baseLine.experimentDict[experiment]['startThresh'],\
                                         baseLine.experimentDict[experiment]['endThresh'],\
                                         baseLine.experimentDict[experiment]['threshStep']
    qtrig = baseLine.baseLineDict[baseLine.experiment['BASELINE']]['qtrigExpList'][0]
    for thresh_index, thresh in enumerate(range(startThresh, (endThresh + 1) if endThresh <= maxThreshold else (maxThreshold + 1), threshStep)):
        if sweepType.lower() == 'closed-bin':
            [th0, th1, th2, th3, th4, th5] = genClosedBinThresholds(thresh, stepSize, maxThreshold, numBins)
        elif sweepType.lower() == 'open-bin':
            gain = baseLine.gainDict[getFbC(VER_ID)]['b' if VER_ID >= 5 else 'a']['gain']
            [th0, th1, th2, th3, th4, th5] = genOpenBinThresholds(thresh, qtrig / gain, maxThreshold, -1, numBins)
        else:
            raise ValueError("Invalid Sweep Type: {}".format(sweepType))
        activeAsicNums, passiveAsicNums = baseLine.sensor['ACTIVE_ASIC_NUMS'], baseLine.sensor['PASSIVE_ASIC_NUMS']
        maskAsicList(activeAsicNums, passiveAsicNums, disableTestPattern=1)  # Enable all active ASICs
        pixList = range(864)
        if VER_ID >= 5:
            perPixThresh(tdir, pixList, th0=th0, th1=th1, th2=th2, th3=th3, th4=th4, th5=th5, from_csv=0)
        else:
            perPixThresh(tdir, pixList, th1=th0, th2=th1, th3=th2, th4=th3, th5=th4, from_csv=0)
            setLftMode('lft')
        logger.info("Sweeping thresholds: {} AU, {} AU, {} AU, {} AU, {} AU, {} AU".format(th0, th1, th2, th3, th4, th5))

        if len(activeAsicNums) > 2:
            raise Exception('Currently, only 1 DMM capture is supported at a time')
        rx_data = [[] for _ in range(8)]  # Create RX data list of lists
        for mm in range(maxNumMMs):
            mmAsicNums = [mm * asicsPerMM + i for i in range(asicsPerMM)]
            mmAsicNums = [i for i in mmAsicNums if i in activeAsicNums]
            if mmAsicNums:
                selDataOutputByMM(mm, asicsPerMM)
                rx_data[mm] = collect_data(view_period=baseLine.experimentDict[experiment]['max_view_period'],
                                           N_views=totFiles, N_AccumViews=timeResInViews if timeResInViews > 1 else 0)

        ####################################################
        #   assign binary/boolean accumData
        accumData = not(baseLine.experimentDict[experiment]['max_view_period'] ==
                        baseLine.experimentDict[experiment]['timeResolution'] * 1e6)

        ####################################################
        #   populate data arrays per active asic
        data_output.bin2array(asic_data_structs, thresh_index, rx_data, timeResolution, accumData=accumData, Ncounters=13)

def genSweep(asic_data_structs, cm, hv, pa, shQueue, tdir, baseLine, experiment, energyCalibrated=0):
    maxNumMMs = baseLine.dm.N_MODULES
    asicsPerMM = baseLine.dm.N_ASICS_PER_MODULE
    maxNumAsics = 16        # Should be maxNumMMs * asicsPerMM but parseBin2CSV needs 16 to work
    bLine = baseLine.params['EXPERIMENT']['BASELINE']
    VER_ID = baseLine.sensor['ASICREVISION']

    tubeVoltageList, tubeCurrentList = baseLine.experimentDict[experiment]['tubeVoltageList'],\
                                       baseLine.experimentDict[experiment]['tubeCurrentList']
    filter_thickness, distance, max_view_period = baseLine.experimentDict[experiment]['filter_thickness'],\
                                        baseLine.experimentDict[experiment]['distance'],\
                                        baseLine.experimentDict[experiment]['max_view_period']

    maxFlux = calcFlux(max(tubeVoltageList), max(tubeCurrentList), filter_thickness, distance, max_view_period / 1e6)
    if maxFlux >= 2**16:
        logger.warn('\n\nCounter overflow WILL occur based on the sweep parameters!!!')

    hv.stabTime = baseLine.experimentDict[experiment]['hvStabTime']
    cm.stabTime = baseLine.experimentDict[experiment]['xRayStabTime']
    hv.setHvVoltage(baseLine.experimentDict[experiment]['hvList'][0])
    hv.setHvState('on')
    activeAsicNums, passiveAsicNums = baseLine.sensor['ACTIVE_ASIC_NUMS'], baseLine.sensor['PASSIVE_ASIC_NUMS']
    thresholds = baseLine.experimentDict[experiment]['thresholds']
    setModeBinAlign('off')
    hpf = baseLine.baseLineDict[bLine]['hpfList'][0]
    setHpf(hpf, VER_ID)
    setLftMode('nonlft')
    lkgCompMode = baseLine.baseLineDict[bLine]['modeList'][0]
    setLeakCompMode(lkgCompMode, VER_ID)
    for acs1, acs2 in baseLine.baseLineDict[bLine]['acsWindowExpList']:
        if VER_ID >= 5:
            setACSWindowRevB(acs1, VER_ID)
        else:
            setAcsWindow(acs1, acs2, VER_ID)
        for acsMode in baseLine.baseLineDict[bLine]['acsModeList']:
            setAcsMode(acsMode)
    enrgCalibIds = ['' for _ in range(maxNumAsics)]
    if energyCalibrated:
        for asic in activeAsicNums:
            maskAsicList(activeAsicNums, passiveAsicNums, maskAllOthersOff=1, asicSingle=asic)

            filename = energy_cal_filename(baseLine.experimentDict[experiment])

            loadEnergyCalFromFile(tdir, filename, baseLine.sensor['IDLIST'][asic], range(864), thresholds[0],
                                  thresholds[1], thresholds[2], thresholds[3],
                                  thresholds[4], thresholds[5], program=True)
            enrgCalibIds[asic] = 'cal-file'
        maskAsicList(activeAsicNums, passiveAsicNums, disableTestPattern=1)
    else:
        gainMode = baseLine.experiment['GAIN']
        ths = baseLine.thresholdKevToAU(thresholds, baseLine.gainDict[gainMode]['b']['gain'],
                                        baseLine.gainDict[gainMode]['b']['offset'])
        create_thresh_chain_prog(tdir, sourceFile="thresh_chain_write_vals.csv",
                                 progFile="thresh_chain_prog_vals.csv", pix_list=range(864),
                                 th0=ths[0], th1=ths[1], th2=ths[2], th3=ths[3], th4=ths[4], th5=ths[5])
        perPixThresh(tdir, from_csv=1, verify=True)
        baseLine.experimentDict[experiment]['thresholds'] = ths
        for asic in activeAsicNums:
            enrgCalibIds[asic] = 'default'
        maskAsicList(activeAsicNums, passiveAsicNums, disableTestPattern=1)  # Enable all active ASICs

    xraySettings = [setting for setting in product(tubeVoltageList, tubeCurrentList)]
    # needs only a single for loop to step through multiple voltage and current combos
    for sweep_dim_index, (tubeVoltage, tubeCurrent) in enumerate(xraySettings):
        cm.setTubeVoltage(tubeVoltage)
        cm.setTubeCurrent(tubeCurrent)
        if tubeCurrent > 0:
            cm.shutterControl('on')

        ################################################
        #   added data structure insertion   AD 2018-10-31
        cap(asic_data_structs, sweep_dim_index, baseLine, shQueue, cm)
        ################################################

    cm.shutterControl('off')

    logger.debug(str(enrgCalibIds))
    baseLine.experimentDict[experiment]['thresholds'] = thresholds


def energy_cal_filename(experiment):
    filename = globals.energyCalFile

    if experiment['hvList'][0] != 1000:
        filename = '{}V_{}'.format(experiment['hvList'][0], filename)

    return filename


def cap(asic_data_structs, sweep_dim_index, baseLine, shQueue, cm):
    maxNumMMs = baseLine.dm.N_MODULES
    asicsPerMM = baseLine.dm.N_ASICS_PER_MODULE
    maxNumAsics = maxNumMMs * asicsPerMM
    activeAsicNums, passiveAsicNums = baseLine.sensor['ACTIVE_ASIC_NUMS'], baseLine.sensor['PASSIVE_ASIC_NUMS']
    experiment = baseLine.params['EXPERIMENT']['NAME']
    totTime, totFiles, timeResolution, timeResInViews = \
        chkCollectionTimeParams(baseLine.experimentDict[experiment]['max_view_period'],
                                baseLine.experimentDict[experiment]['timeResolution'],
                                baseLine.experimentDict[experiment]['totTime'])
    if len(activeAsicNums) > 2:
        raise Exception('Currently, only 1 DMM capture is supported at a time')
    rx_data = [[] for _ in range(8)]                            # 8 is the only number that works with parserBin2Csv. Should be dynamic
    for mm in range(maxNumMMs):
        mmAsicNums = [mm * asicsPerMM + i for i in range(asicsPerMM)]
        mmAsicNums = [i for i in mmAsicNums if i in activeAsicNums]
        if mmAsicNums:
            maskAsicList(activeAsicNums, passiveAsicNums)       # Enable only DMMn for capture
            selDataOutputByMM(mm, asicsPerMM, baseLine.dm_info['DM_TYPE'])
            logger.info('Performing Capture on MM{}'.format(mm))
            if baseLine.experimentDict[experiment]['test_type'] == 'DYNAMIC':
                pwm_string = make_pwm_string(baseLine.experimentDict[experiment]['shutter_on'],
                                             baseLine.experimentDict[experiment]['shutter_off'],
                                             baseLine.experimentDict[experiment]['totTime'])
                logger.warning('Shutter will now AUTOMATICALLY open/close after a few seconds...')
                shQueue.put(pwm_string)
            rx_data[mm] = collect_data(view_period=baseLine.experimentDict[experiment]['max_view_period'],
                                        N_views=totFiles, N_AccumViews=timeResInViews if timeResInViews > 1 else 0)
            if 'dynamicresponse' in experiment.lower():
                shQueue.put('close')
                logger.warning('PWM finished, shutter closed...')

    ####################################################
    #   assign binary/boolean accumData
    accumData = not(baseLine.experimentDict[experiment]['max_view_period'] ==
                    baseLine.experimentDict[experiment]['timeResolution'] * 1e6)

    ####################################################
    #   populate data arrays per active asic
    data_output.bin2array(asic_data_structs, sweep_dim_index, rx_data, timeResolution, accumData=accumData, Ncounters=13)


def make_pwm_string(on, off, total_time):
    """creates pwm string for shutter control
    calculates number of shutter cycles according to on/off periods
    increments counter while adding off / on time until reaching totTime"""
    shutter_cycles = 0
    time_accum = 0
    while time_accum < total_time:
        time_accum += off
        if time_accum < total_time:
            shutter_cycles += 1
            time_accum += on
            if time_accum <= total_time:
                shutter_cycles += 1
        else:
            break

    pwm_string = 'pwm {} {} {}'.format(on, off, shutter_cycles)

    return pwm_string


def thresholdSweep(cm, hv, fw, shQueue, ldir, test_suite_file_summary, VER_ID, baseLine, experiment, sweepType, file_name_append):
    logger.info('Threshold sweep starting')
    device_ID = test_suite_file_summary['device']
    activeAsicNums = baseLine.sensor['ACTIVE_ASIC_NUMS']
    sourceCode = baseLine.experimentDict[experiment]['photon_source']
    baseLine.experiment['SOURCE'] = sourceCode

    if 'xray' in sourceCode:
        cm.checkXrayArm()

    viewPeriod = baseLine.experimentDict[experiment]['max_view_period']
    totTime = baseLine.experimentDict[experiment]['totTime']
    timeResolution = baseLine.experimentDict[experiment]['timeResolution']

    hv.stabTime = baseLine.experimentDict[experiment]['hvStabTime']
    cm.stabTime = baseLine.experimentDict[experiment]['xRayStabTime']

    totTime, numFrames, timeResolution, timeResInViews = chkCollectionTimeParams(viewPeriod, timeResolution, totTime)

    baseLine.experimentDict[experiment]['totTime'] = totTime
    baseLine.experimentDict[experiment]['timeResolution'] = timeResolution

    asic_data_structs = data_output.prepare_output_data(baseLine, experiment, activeAsicNums, spectral_sweep=True)
    output_file_names, root_output_dir = data_output.prepare_output_paths(baseLine,
                                                                          experiment,
                                                                          activeAsicNums,
                                                                          device_ID,
                                                                          spectral_sweep=True,
                                                                          extra_append=file_name_append)
    sweepThresh(asic_data_structs, ldir, baseLine, experiment, baseLine.params['EXPERIMENT']['BASELINE'], sweepType,
                hv, cm, autoAppend=0, pa=None)
    logger.info('Saving spectral array data')
    data_output.save_output_data(activeAsicNums, asic_data_structs, output_file_names, root_output_dir)
    data_output.prepare_output_summary(test_suite_file_summary, baseLine, experiment, output_file_names)

    if 'xray' in sourceCode:
        logger.info('Shutting off X-ray')
        cm.shutterControl('off')

    logger.info('Threshold sweep done')

    return root_output_dir


def getSource():
    source = None
    while not source:
        source = choicebox(msg='Select Source', choices=baseline.sourceDict.keys())
    return source


def getUserInput(dm_types):
    """

    :rtype:
    """
    ##Changed and added by Z. Ye on 07/18/2019 to enable reading in by text files
    if globals.enableDevMode:
        baseLine, experimentList = devSetup(dm_types)
    elif globals.enableTextMode:
        baseLine, experimentList = textSetup(dm_types)
    else:
        testType = choicebox(msg='Select Test Type', choices=globals.testTypeDict.keys())
        if testType == 'DM':
            baseLine, experimentList = dmSetup(dm_types)
        elif testType == 'MM':
            baseLine, experimentList = mmSetup(dm_types)
        elif testType == 'Sensor':
            baseLine, experimentList = sensorSetup(dm_types)
        else:
            raise ValueError('Invalid Test Mode: {}'.format(testType))

    if globals.enableTestSuiteGui:
        # instantiate test suite gui
        sui_gui = TestSuiteGUI()
        if not sui_gui.cancelled:
            # get formatted experiment dictionary and list
            experiment_dict, gui_experiment_list = sui_gui.format_suite_for_baseline()
            # populate baseLine.experimentDict
            for experiment in gui_experiment_list:
                baseLine.experimentDict[experiment] = experiment_dict[experiment]
            ##################################
            # Replace experiment list with list from gui
            experimentList = gui_experiment_list
        else:
            raise RuntimeError('Test Selection Cancelled')

    ##Added by Z. Ye on 07/19/2019 to read in input paramters from a file instead of GUIs
    if globals.enableTextMode:
        file_name_in = globals.textModeSettings['Directory'] + globals.textModeSettings['SuiteName'];
        if os.path.exists(file_name_in):
            sui_text = TestSuiteText()
            sui_text.suite_load()
            experiment_dict, text_experiment_list = sui_text.format_suite_for_baseline()

            # populate baseLine.experimentDict
            for experiment in text_experiment_list:
                baseLine.experimentDict[experiment] = experiment_dict[experiment]
            ##################################
            # Replace experiment list with list from gui
            experimentList = text_experiment_list
        else:
            # instantiate test suite gui
            sui_gui = TestSuiteGUI()
            if not sui_gui.cancelled:
                # get formatted experiment dictionary and list
                experiment_dict, gui_experiment_list = sui_gui.format_suite_for_baseline()
                # populate baseLine.experimentDict
                for experiment in gui_experiment_list:
                    baseLine.experimentDict[experiment] = experiment_dict[experiment]
                ##################################
                # Replace experiment list with list from gui
                experimentList = gui_experiment_list
            else:
                raise RuntimeError('Test Selection Cancelled')      
                
    # Convert experiment names
    #experimentList = [baseline.experimentDict[experiment] for experiment in experimentList]
    
       # If not on a Redlen system, don't upload to server
    if baseLine.site['NAME'] not in globals.testSiteList:
        globals.tdir = globals.server = globals.ldir
        createFileStructure(globals.ldir, baseLine.site['NAME'])

    return baseLine, experimentList


def dmSetup(dm_types):
    testType = globals.testTypeDict['DM']
    DMType = None
    if len(dm_types) > 1:
        while not DMType:
            DMType = choicebox(msg='Select DM Type', choices=dm_types.keys())
    else:
        DMType = dm_types.keys()[0]

    dm_class = dm_types[DMType]
    maxNumMMs = dm_class.N_MODULES

    dmID = ''
    while not dmID:
        dmID = enterbox(msg='Enter the DM Serial Number', title='DM Serial Number')

    moduleIDs = []
    while not moduleIDs:
        moduleIDs = multenterbox(msg='Enter the MM Serial Numbers', title='Enter Serial Numbers',
                                 fields=['MM {}'.format(mmNum) for mmNum in range(maxNumMMs)])

    testGain = baseline.gainDict['midgain']

    testSite = choicebox(msg='Select Test Site', choices=globals.testSiteList)
    if testSite is None or testSite == 'Other':
        testSite = enterbox(msg='Enter test site name', default='unknown')

    baseLine = baseline(moduleIDs=moduleIDs, cztId='na', siteName=testSite, bl='c', DMType=dm_class, testType=testType)
    baseLine.experiment['gain'] = testGain
    baseLine.dm_info['SERIAL_NUM'] = dmID

    experimentList = []

    return baseLine, experimentList


def mmSetup(dm_types):
    testType = globals.testTypeDict['MM']
    DMType = None
    if len(dm_types) > 1:
        while not DMType:
            DMType = choicebox(msg='Select DM Type', choices=dm_types.keys())
    else:
        DMType = dm_types.keys()[0]

    dm_class = dm_types[DMType]
    maxNumMMs = dm_class.N_MODULES

    dmID = 'Blue Box'

    moduleIDs = ['' for _ in range(maxNumMMs)]
    slotIDs = []
    while not slotIDs:
        slotIDs = multenterbox(msg='Enter the MM Serial Numbers', title='Enter Serial Numbers',
                                 fields=['MM {}'.format(mmNum) for mmNum in dm_class.BLUE_BOX_TEST_SLOTS])

    for (slot, module) in zip(dm_class.BLUE_BOX_TEST_SLOTS, slotIDs):
        moduleIDs[slot] = module

    testGain = baseline.gainDict['midgain']

    testSite = choicebox(msg='Select Test Site', choices=globals.testSiteList)
    if testSite is None or testSite == 'Other':
        testSite = enterbox(msg='Enter test site name', default='unknown')

    baseLine = baseline(moduleIDs=moduleIDs, cztId='na', siteName=testSite, bl='c', DMType=dm_class, testType=testType)
    baseLine.experiment['gain'] = testGain
    baseLine.dm_info['SERIAL_NUM'] = dmID

    experimentList = []

    return baseLine, experimentList

def sensorSetup(dm_types):
    testType = globals.testTypeDict['Sensor']
    DMType = None
    if len(dm_types) > 1:
        while not DMType:
            DMType = choicebox(msg='Select DM Type', choices=dm_types.keys())
    else:
        DMType = dm_types.keys()[0]

    dm_class = dm_types[DMType]
    maxNumMMs = dm_class.N_MODULES
    asicsPerMM = dm_class.N_ASICS_PER_MODULE

    dmID = 'Yellow Box'

    yellowBoxID = []
    while not yellowBoxID:
        yellowBoxID = multenterbox(msg='Enter Yellow Box Serial Number', title='Yellow Box Serial Number',
                                   fields=['Serial Number' for _ in dm_class.YELLOW_BOX_TEST_SLOTS])

    cztSlots = ['' for _ in range(maxNumMMs * asicsPerMM)]
    cztIDs = []
    while not cztIDs:
        cztIDs = multenterbox(msg='Enter CZT Serial Numbers', title='Enter Serial Numbers',
                              fields=['Serial Number' for _ in dm_class.YELLOW_BOX_ACTIVE_ASICS])

    for (slot, czt) in zip(dm_class.YELLOW_BOX_ACTIVE_ASICS, cztIDs):
        cztSlots[slot] = czt

    moduleIDs = ['' for _ in range(maxNumMMs)]

    for (slot, module) in zip(dm_class.YELLOW_BOX_TEST_SLOTS, yellowBoxID):
        moduleIDs[slot] = module

    testGain = baseline.gainDict['midgain']

    testSite = choicebox(msg='Select Test Site', choices=globals.testSiteList)
    if testSite is None or testSite == 'Other':
        testSite = enterbox(msg='Enter test site name', default='unknown')

    baseLine = baseline(moduleIDs=moduleIDs, cztId=cztSlots, siteName=testSite, bl='c', DMType=dm_class, testType=testType)
    baseLine.experiment['gain'] = testGain
    baseLine.dm_info['SERIAL_NUM'] = dmID
    baseLine.sensor['ACTIVE_ASIC_NUMS'] = dm_class.YELLOW_BOX_ACTIVE_ASICS
    baseLine.sensor['PASSIVE_ASIC_NUMS'] = dm_class.YELLOW_BOX_PASSIVE_ASICS

    experimentList = []

    return baseLine, experimentList


def devSetup(dm_types):
    testType = globals.devModeSettings['TestType']
    DMType = globals.devModeSettings['DMType']
    dmID = globals.devModeSettings['DMID']
    moduleIDs = globals.devModeSettings['ModuleIDs']
    testGain = globals.devModeSettings['Gain']
    testSite = globals.devModeSettings['TestSite']
    experimentList = globals.devModeSettings['ExperimentList']
    reason = globals.devModeSettings['Reason']
    cztIDs = globals.devModeSettings['CZTIDs']

    dm_class = dm_types[DMType]

    baseLine = baseline(moduleIDs=moduleIDs, cztId=cztIDs, siteName=testSite, bl='c', DMType=dm_class, testType=testType)
    baseLine.experiment['gain'] = testGain
    baseLine.dm_info['SERIAL_NUM'] = dmID
    baseLine.experiment['REASON'] = reason

    # Overrides the automatically generated ASIC Active and Passive Nums if they are present in the globals.
    # This is needed if you only want to test individual ASICs in and MM. The baseline automatically makes
    # all the ASICs in an MM active. This may not be desired for example for the sensor screener where only
    # 1 ASIC is present in ASIC slot 15. In this case you would want to set globals.devModeSettings[ActiveAsicNums]
    # to [15] and globals.devModeSettings[PassiveAsicNums] to [14]

    if globals.devModeSettings['ActiveAsicNums']:
        baseLine.sensor['ACTIVE_ASIC_NUMS'] = globals.devModeSettings['ActiveAsicNums']
    if globals.devModeSettings['PassiveAsicNums']:
        baseLine.sensor['PASSIVE_ASIC_NUMS'] = globals.devModeSettings['PassiveAsicNums']

    return baseLine, experimentList

def textSetup(dm_types):
    testType = globals.testTypeDict[globals.textModeSettings['TestType']]
    DMType = globals.textModeSettings['DMType']
    dmID = globals.textModeSettings['DMID']
    moduleIDs = globals.textModeSettings['ModuleIDs']
    testGain = globals.textModeSettings['Gain']
    testSite = globals.textModeSettings['TestSite']
    experimentList = globals.textModeSettings['ExperimentList']
    reason = globals.textModeSettings['Reason']
    cztIDs = globals.textModeSettings['CZTIDs']

    dm_class = dm_types[DMType]

    baseLine = baseline(moduleIDs=moduleIDs, cztId='na', siteName=testSite, bl='c', DMType=dm_class, testType=testType)
    baseLine.experiment['gain'] = testGain
    baseLine.dm_info['SERIAL_NUM'] = dmID
    baseLine.experiment['REASON'] = reason

    return baseLine, experimentList


def initialize(ldir, baseLine, mm=None):
    bLine = baseLine.params['EXPERIMENT']['BASELINE']
    activeAsicNums = baseLine.sensor['ACTIVE_ASIC_NUMS']
    passiveAsicNums = baseLine.sensor['PASSIVE_ASIC_NUMS']

    ## Added by Z. Ye on 07/22/2019, to give a feature of changing IP address from the input files,
    ### instead of hard-codeing in dmBuffer/DMBufferStartUp.py
    if globals.enableTextMode:
        buffer = DMBB.startBuffer(15, globals.textModeSettings['IPAddress'], globals.textModeSettings['IPPort'])
    else:
        buffer = DMBB.startBuffer(15)

    detector_cls = baseLine.dm_info['DM_TYPE']
    detector_serial = baseLine.dm_info['SERIAL_NUM']

    if mm is None:
        module_serials = [module or None for module in baseLine.dm_info['MODULES']]
    else:
        module_serials = [baseLine.dm_info['MODULES'][idx] if idx == mm else None for idx in range(detector_cls.N_MODULES)]

    detector = detector_cls(buffer, detector_serial, module_serials, baseLine.sensor['PASSIVE_ASIC_NUMS'])

    # Assign global class instances
    globals.buffer = buffer
    globals.detector = detector
    baseLine.dm = detector

    detector.reset()
    initDDC0864(baseLine.baseLineDict[bLine]['modeList'][0], baseLine.baseLineDict[bLine]['hpfList'][0], activeAsicNums,
                baseLine.experiment['GAIN'], site=baseLine.params['SITE']['NAME'])
    detector.setup()

    set18_chain_prog(ldir, progFile='set18_chain_prog_vals_B.csv')
    asicsPerMM = baseLine.dm_info['ASICS_PER_MM']

    ################ New Code
    for asic in activeAsicNums:
        selDataOutputByMM(asic // asicsPerMM, asicsPerMM)
        logger.info('performing electrical calibrations for {}'.format(baseLine.sensor['IDLIST'][asic]))
        maskAsicList(activeAsicNums, passiveAsicNums, maskAllOthersOff=1, asicSingle=asic)
        calibration_fn = 'cal_vals_%s_Act2TC_%dns_DT_%dns_QT_%d_B.csv' % (baseLine.sensor['IDLIST'][asic],
                                                                          int(baseLine.baseLineDict[bLine][
                                                                                  'active2TC'] / 1e-9),
                                                                          int(baseLine.baseLineDict[bLine][
                                                                                  'deadTimeExpList'][0] / 1e-9),
                                                                          baseLine.baseLineDict[bLine][
                                                                              'qtrigExpList'][0])
        calibration_dir = os.path.join(ldir, globals.calsDir, baseLine.sensor['IDLIST'][asic])
        loadElectricalCal(ldir, baseLine.sensor['IDLIST'][asic],
                          calFile=os.path.join(calibration_dir, calibration_fn))


def check_isotope(ldir, baseLine, isotope, mm):
    if isotope is not None:
        msgbox(msg='Place {} isotope into the holder below MM {} in the test box'.format(isotope, mm))
    else:
        msgbox(msg='Remove isotope from the holder in the test box')

    initialize(ldir, baseLine, mm)


def createFileStructure(ldir, site):
    folderDict = {'Docs': globals.docDir,
                  'Data': globals.csvDir,
                  'Calibrations': globals.calsDir,
                  'Multi File Append': globals.scratchDir}

    defaultChainDir = r'DDC0864\Default Chain Files'

    logger.info('Creating directory structure')

    for folder in folderDict.values():
        path = os.path.join(ldir, site, folder)
        if not os.path.exists(path):
            os.makedirs(path)

    logger.info('Copying default chain files')

    for chainFile in os.listdir(defaultChainDir):
        copy(os.path.join(defaultChainDir, chainFile), os.path.join(ldir, site, folderDict['Docs'], chainFile))


class Settings(EgStore):
    def __init__(self, filename):
        self.testDirectory = ''
        self.siteName = ''

        self.filename = filename
        self.restore()

class StationInfo():
    def __init__(self):
        self.stationName = ''
        self.lastTest = ''
        self.lastConditioning = ''
