########################################################################################################################
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
########################################################################################################################

import logging
import numpy as np


logger = logging.getLogger(__name__)


def erf(x):
    vals = np.arange(-3, 3.1, 0.1)
    erf_vals = np.array(
        [-0.99997791, -0.999958902, -0.999924987, -0.999865667, -0.999763966, -0.999593048, -0.999311486, -0.998856823,
         -0.998137154, -0.997020533, -0.995322265, -0.992790429, -0.989090502, -0.983790459, -0.976348383, -0.966105146,
         -0.95228512, -0.934007945, -0.910313978, -0.88020507, -0.842700793, -0.796908212, -0.742100965, -0.677801194,
         -0.603856091, -0.520499878, -0.428392355, -0.328626759, -0.222702589, -0.112462916, 0, 0.112462916,
         0.222702589, 0.328626759, 0.428392355, 0.520499878, 0.603856091, 0.677801194, 0.742100965, 0.796908212,
         0.842700793, 0.88020507, 0.910313978, 0.934007945, 0.95228512, 0.966105146, 0.976348383, 0.983790459,
         0.989090502, 0.992790429, 0.995322265, 0.997020533, 0.998137154, 0.998856823, 0.999311486, 0.999593048,
         0.999763966, 0.999865667, 0.999924987, 0.999958902, 0.99997791])
    return np.interp(x, vals, erf_vals)


def scurve_stats(vin, counts, mode_sz=0):
    len_diff = len(vin) - len(counts)
    if len_diff != 0:
        logger.warn("Lengths of arrays are not the same")
        logger.warn("Len(vin) = %d, Len(counts) = %d" % (len(vin), len(counts)))
    if mode_sz == 1:
        if len_diff > 0:
            logger.debug("Padding counts list with %d zero(s)." % len_diff)
            counts = counts + [0] * len_diff
        elif len_diff < 0:
            logger.debug("Deleting last %d entries from count list." % -len_diff)
        counts_integ = []
        for i in range(len(counts)):
            counts_integ.append(np.trapz(counts[0:i]))
        counts = counts_integ

    counts_array = np.array(counts)
    vin_array = np.array(vin)
    max_count = max(counts_array)
    min_count = min(counts_array)

    if (min_count > 0.05 * max_count):
    # if (min_count > 0.05 * max_count):
        logger.debug("Start of S-Curve isn't flat. Setting mean,sigma = -1,-1.")
        return (-1, -1)
    if (counts_array[-1] / counts_array[-2] > 1.01):
        logger.debug("End of S-Curve isn't flat. Setting mean,sigma = -2,-2.")
        logger.debug(str(counts))
        return (-2, -2)

    # Slice off 2nd half if this curve is an S-Z curve
    max_index_arr = np.where(counts_array == max_count)[0]
    max_index = max_index_arr[0]
    counts_array = np.array(counts[0:max_index + 1])
    vin_array = np.array(vin[0:max_index + 1])

    mean_vin = []
    count_samples = np.arange(0.2, 0.801, 0.025) * max_count
    mean_vin = np.interp(count_samples, counts_array, vin_array)
    mean_vin = np.mean(mean_vin)

    sigma_vin = []
    sigmas = np.concatenate([np.arange(1, 2, 0.1), np.arange(-2, -1, 0.1)])
    count_samples = [(1 - (1 - erf(i / 2 ** 0.5)) / 2.0) * max_count for i in sigmas.tolist()]
    sigma_vin = np.interp(count_samples, counts_array, vin_array)
    sigma_vin = np.divide(sigma_vin - mean_vin, sigmas)
    sigma_vin = np.mean(sigma_vin)
    return (mean_vin, sigma_vin)


def scurve_stats5(vin, counts, mode_sz=0, silence=1, zcurve=0, right_half=0, double=0, mcp=0.02, san_check=1):
    '''mcp stands for min counts percentage'''
    len_diff = len(vin) - len(counts)

    if zcurve != 0:
        vin.reverse()
        counts.reverse()
    if len_diff != 0:
        logger.warn("Lengths of arrays are not the same")
        logger.warn("Len(vin) = %d, Len(counts) = %d" % (len(vin), len(counts)))
    if mode_sz == 1:
        if len_diff > 0:
            logger.warn("Padding counts list with %d zero(s)." % len_diff)
            counts = counts + [0] * len_diff
        elif len_diff < 0:
            logger.warn("Deleting last %d entries from count list." % -len_diff)
        # counts_integ = []
        # for i in range(len(counts)):
        # counts_integ.append(np.trapz(counts[0:i]))
        # counts = counts_integ
        ## Slice the S-Z curve
        # counts = np.concatenate([[max_count]*xmax,counts[xmax:None]])
        xmax = np.argmax(counts)
        counts = np.concatenate([counts[0:xmax], [max(counts)] * (len(counts) - xmax)])

    counts = np.array(counts)
    vin = np.array(vin)
    max_count = max(counts)
    min_count = min(counts)

    ## '2' for 2nd 'S', '1' for 1st 'S'
    if double == 2:
        counts = np.maximum(counts, max_count / 2) - max_count / 2
        max_count = max_count / 2
    # log(counts)
    elif double == 1:
        counts = np.minimum(counts, max_count / 2)
        max_count = max_count / 2
    # log(counts)

    if right_half == 1:
        mid_idx = np.where(counts >= max_count / 2)[0][0]
    # warning('Using only right half for computing statistics.')
    elif (min_count > mcp * max_count):
        if (silence == 0):
            logger.warn("Start of S-Curve isn't flat. Setting mean,sigma = -1,-1.")
        # 		return ('badStart','badStart')
        if san_check == 1:
            return (-1, -1, -1)
    if (counts[-1] / counts[-2] > (1 + mcp)):
        if (silence == 0) or (mode_sz == 1):
            logger.warn("End of S-Curve isn't flat. Setting mean,sigma = -2,-2.")
            log(counts)
        # 		return ('badEnd','badEnd')
        if san_check == 1:
            return -2, -2, -2

    # Slice off 2nd half if this curve is an S-Z curve
    max_index_arr = np.where(counts == max_count)[0]
    max_index = max_index_arr[0]
    # counts = np.array(counts[0:max_index+1])
    # vin = np.array(vin[0:max_index+1])

    # weights = np.concatenate([np.arange(0.05,0.45,0.025)] + [np.arange(0.55,0.95,0.025)]) if right_half==0 else np.array([0.4,0.45,0.5,0.55,0.6])
    weights = np.concatenate(
        [np.arange(0.05, 0.45, 0.025)] + [np.arange(0.55, 0.95, 0.025)]) if right_half == 0 else np.arange(0.15, 0.851,
                                                                                                           0.025)
    count_samples = weights * max_count
    mean_vin = np.interp(count_samples, counts, vin)
    # if right_half==1:
    #	log((len(mean_vin),mean_vin))
    mean_vin = np.mean(mean_vin)

    left = [np.arange(-2, -1, 0.1)] if right_half == 0 else []
    sigmas = np.concatenate(left + [np.arange(1, 2, 0.1)])
    ysamps = [(1 - (1 - erf(i / 2 ** 0.5)) / 2.0) * max_count for i in sigmas.tolist()]
    xsamps = np.interp(ysamps, counts, vin)
    sigma_estimates = np.divide(xsamps - mean_vin, sigmas)
    sigma_vin = np.mean(sigma_estimates)
    #if sigma_vin == 0 or sigma_vin == np.NaN: print 'mean_vin = {}\nsigma_vin = {}'.format(mean_vin, sigma_vin)
    try:
        ideal_counts = 0.5 * (1 + erf((vin - mean_vin) / (sigma_vin * 2 ** 0.5))) * max_count
    except (ValueError, ZeroDivisionError):
        logger.warn('division error in scurve_stats5 for vin=%.3f' % vin)
        ideal_counts = 0.5 * max_count
    ideal_counts = np.round(ideal_counts).astype(int)
    # log('ideal_counts = %s' %ideal_counts)
    # warning('real_counts = %s' %counts)
    diff_sq = np.square(counts - ideal_counts)
    # log(diff_sq)
    health = np.around(np.sqrt(np.mean(diff_sq)) / max_count, 3)

    return mean_vin, sigma_vin, health


def inflection(thresholdArray, countsArray):
    for i, thresh in enumerate(thresholdArray):
        if countsArray(i+1)/countsArray(i) < 0.95:
            return thresh, countsArray(i)
