########################################################################################################################
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
########################################################################################################################

import logging


logger = logging.getLogger(__name__)


def a2d(a, n):
    '''Returns n-bit representation of string/integer "a" '''
    a = max(min(int(a), 2 ** n - 1), 0)  # Sanity checks
    return (bin(int(a)))[2:].zfill(n)[0:n]


def row_and_col(pixel_num):
    row = pixel_num / 36
    col = pixel_num % 36 if row % 2 == 0 else 35 - pixel_num % 36
    return (row, col)


def resize_retype(inst, l):
    return [inst] * l if isinstance(inst, int) else [inst[0]] * l if len(inst) == 1 else inst


def dac_code(vin):
    '''Returns code that needs to be programmed into the dac to get VDAC = vin'''
    val = int(vin * 262144 / 2.5)
    if val <= 131072:
        code = val + 131072
    else:
        code = val - 131072
    return max(code, 0)


def xor17(a, b):
    '''17-bit xor used for CRC computation and check'''
    if len(a) != len(b):
        logger.warn("xor17 received strings of unequal length")
        return '0' * 17
    elif len(a) != 17:
        logger.warn("xor17 received strings of length not equal to 17.")
    c = ''
    for i in range(len(a)):
        c += '%d' % (int(a[i]) ^ int(b[i]))
    return c


def pos_in_data(pixel_num, cntr_num=0, all_counters=0):
    if all_counters == 0 and cntr_num != 0:
        logger.debug('Setting cntr_num to 0 as all_counters is 0')
        cntr_num = 0
    (row, col) = row_and_col(pixel_num)
    lane = 0 if row < 12 else 1
    if lane == 0:
        pos = 13 * (col + 12 * all_counters) + 13 * cntr_num + row
    else:
        pos = 13 * (col + 12 * all_counters) + 13 * cntr_num + (23 - row)
    return (lane, pos)


def pixels_of_cols(start_col=0,end_col=36,jump_cols=6):
    start_col = clip(0,start_col,36)
    end_col = clip(0,end_col,36)
    pix_list = []
    #end_col += 1
    for col in range(start_col,end_col,jump_cols):
        pix_list += [col+36*row if row%2==0 else 35-col+36*row for row in range(0,24)]
    return pix_list