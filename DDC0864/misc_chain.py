########################################################################################################################
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
########################################################################################################################

from six.moves import StringIO

import csv
import json
import os
import shutil
import logging

import globals
import pcct.ddc0864
from DDC0864.devInit import a2d, row_and_col
from DDC0864.dmFunctions import writeReg, readReg, waitForBit, checkDir, maskAsicList, readAsicGlobals
from DDC0864.thresh_chain import readPerPixThresh
from pcct.utils import chunk_str, pack_u16_be, unpack_u16_be
from DDC0864.calibration_tables.ddc0864_cal_table_metadata import DDC0864CalTableMetadata
from DDC0864.calibration_tables.pixel_cal_data import PixelCalData
from DDC0864.calibration_tables.ddc0864_cal_table import DDC0864CalTable
from DDC0864.calibration_tables.ddc0864_pixel_chain import DDC0864Chain

logger = logging.getLogger(__name__)


def perPixCal(ldir, VER_ID, pix_list=range(864), adc_offset=8, td_trim_be2=12, td_trim_be=50, td_trim_fe=220, lkg_passive=128,
              lkg_active=128, qtrig=12, iso_pix=0, en_prbs=0, trim_dnl=0, sel_qdump=0,
                sel_pix=0, qdump_del=0, mode_add_cap=15, mode_qdump_a=3, mode_qdump_b=3, mode_bad_pix=0,
                writeValsFile="misc_chain_write_vals.csv", progFile="misc_chain_prog_vals.csv",
                from_csv=1, startChain=0, endChain=11):
    if VER_ID > 4:
        if writeValsFile.find('_B') < 0: #do not add another _B if one already exists
            #insert _B if  VER_ID is Rev B and if it is not already in the string
            index = writeValsFile.find('.')
            writeValsFile = '{}_B{}'.format(writeValsFile[:index], writeValsFile[index:])
        if progFile.find('_B') < 0: #do not add another _B if one already exists
            index = progFile.find('.')
            progFile = '{}_B{}'.format(progFile[:index], progFile[index:])
        lkg = readReg('0x33', shadow=1)
        prbs = readReg('0x19', 1, 15, 0)
        writeReg('0x33', 9, 7, '0x0000', shadow=1, writeNow=1)
        writeReg('0x33', 15, 15, '0x0000', shadow=1, writeNow=1)
        writeReg('0x33', 12, 12, '0x0000', shadow=1, writeNow=1)
        writeReg('0x33', 4, 4, '0x0000', shadow=1, writeNow=1)
        writeReg('0x19', 15, 0, '0x0000', shadow=1, writeNow=1)
    if from_csv == 0:
        writeValsFile = os.path.join(ldir, globals.docDir, writeValsFile)
        writeFile = writeValsFile
        checkDir(writeValsFile)
        with open(writeValsFile, 'w') as fh:
            if VER_ID < 5 :
                fh.write("Row,Col,Pixel,CALIB_ESSENTIAL_1,SEL_PIX,QDUMP_CLK_DEL,ADD_CAP,CDUMP_A,CDUMP_B,CALIB_ESSENTIAL_2," +
                         "MODE_BAD_PIX, CALIB_ESSENTIAL_3,LEAKAGE_ACTIVE,LEAKAGE_PASSIVE,TD_TRIM_FE,TD_TRIM_BE,ADC_OFFSET\n")
                stream_args = [adc_offset, td_trim_be, td_trim_fe, lkg_passive, lkg_active, mode_add_cap, mode_bad_pix,
                               mode_qdump_b, mode_qdump_a, mode_add_cap, qdump_del, sel_pix]
            else:
                fh.write("Row,Col,Pixel,CALIB_ESSENTIAL_1,SEL_QDUMP,QDUMP_DEL,CALIB_ESSENTIAL_2,TRIM_DNL,CALIB_ESSENTIAL_3," +\
                        "EN_PRBS,ISO_PIX,MODE_BAD_PIX,SEL_PIX,CDUMP_A,CDUMP_B,ADD_CAP,QTRIG,LEAKAGE_ACTIVE,LEAKAGE_PASSIVE,TD_TRIM_FE," +\
                        "TD_TRIM_BE,TD_TRIM_BE2,ADC_OFFSET\n")
                stream_args = [adc_offset, td_trim_be2, td_trim_be, td_trim_fe, lkg_passive, lkg_active, qtrig,
                               mode_add_cap, mode_qdump_b, mode_qdump_a, sel_pix, mode_bad_pix, iso_pix, en_prbs,
                               trim_dnl, qdump_del, sel_qdump]
            newstream = gen_misc_stream(pix_list, stream_args, VER_ID)
            slice_length = len(newstream) / (endChain + 1)
            newstream_slice = newstream[:slice_length]
            setCalibChains('enable')
            sendPerPixCal(newstream_slice, VER_ID)
            for i in range(12):
                misc_to_csv(fh, newstream_slice, i, VER_ID)
    else:
        writeValsFile = os.path.join(ldir, globals.docDir, progFile)
        datastream = misc_from_csv(ldir, VER_ID, progFile)
        for i in range(startChain, endChain + 1):
            logger.debug('Writing calibration chain number: %d' % i)
            setCalibChains('enable', chains='0x{:04x}'.format(0x0001 << i))  # Enable Cal Chain# 'chain_num'
            if VER_ID < 5:
                # rev A asic - 64 bit chains
                newstream = datastream[(11 - i) * 72 * 64:(12 - i) * 72 * 64]
            else:
                #rev B asic - 80 bit chains
                newstream = datastream[(11 - i) * 72 * 80:(12 - i) * 72 * 80]
            sendPerPixCal(newstream, VER_ID)
    setCalibChains('disable')
    if globals.miscVerify:
        readFile = readPerPixCal(ldir, VER_ID)
        fMatch = compareChainFiles(writeValsFile, readFile)
        sensorId = globals.sensorId
        if not fMatch:
            readAsicGlobals(ldir, sensorId)
            path, file = os.path.split(writeValsFile)
            sFile = sensorId + '_cfmm_wr.csv'
            shutil.copy(writeValsFile, os.path.join(path, sFile))
            path, file = os.path.split(readFile)
            sFile = sensorId + '_cfmm_rd.csv'
            shutil.copy(readFile, os.path.join(path, sFile))
            datastream = misc_from_csv(ldir, VER_ID, writeValsFile)
            for i in range(startChain, endChain + 1):
                logger.debug('Writing calibration chain number: %d' % i)
                setCalibChains('enable', chains='0x{:04x}'.format(0x0001 << i))  # Enable Cal Chain# 'chain_num'
                if VER_ID < 5:
                    # rev A asic - 64 bit chains
                    newstream = datastream[(11 - i) * 72 * 64:(12 - i) * 72 * 64]
                else:
                    # rev B asic - 80 bit chains
                    newstream = datastream[(11 - i) * 72 * 80:(12 - i) * 72 * 80]
                sendPerPixCal(newstream, VER_ID)
            setCalibChains('disable')
            readFile = readPerPixCal(ldir, VER_ID)
            fMatch = compareChainFiles(writeValsFile, readFile)

    if VER_ID > 4:
        writeReg('0x33', 15, 0, lkg, shadow=1, writeNow=1)
        writeReg('0x19', 15, 0, prbs, shadow=1, writeNow=1)

    return writeValsFile


def program_asic_from_json(ldir, VER_ID, json_prog_file="misc_chain_prog_vals_B.json", start_chain=0, end_chain=11):
    """
    program ASIC cal chain from a JSON data file
    :param ldir:
    :param VER_ID:
    :param json_prog_file:
    :param start_chain:
    :param end_chain:
    :return:
    """
    program_sucess = True
    try:
        if VER_ID > 4:
            lkg = globals.buffer.read_asic_reg(0x33)
            prbs = globals.buffer.read_asic_reg(0x19)
            globals.buffer.write_asic_reg(0x33, 0x0000)
            globals.buffer.write_asic_reg(0x33, 0x0000)
            globals.buffer.write_asic_reg(0x33, 0x0000)
            globals.buffer.write_asic_reg(0x33, 0x0000)
            globals.buffer.write_asic_reg(0x19, 0x0000)

        json_prog_path = os.path.join(ldir, globals.docDir, json_prog_file)
        if not os.path.isfile(json_prog_path):
            logger.error('programming json file does not exist with the specified path')
            raise Exception('programming json file does not exist with the specified path')
        # do programming
        with open(json_prog_path, 'r') as fh:
            cal_data = json.load(fh)
            pixel_cal_list = cal_data["caldata"]
            pixel_per_chain = pcct.ddc0864.N_MISC_CHAIN_PIXELS
            for i in range(start_chain, end_chain + 1):
                logger.debug('Enable/Writing calibration chain number: {}'.format(i))
                setCalibChains('enable', chains='0x{:04x}'.format(0x0001 << i))
                cal_chain_stream = ""
                for j in range(pixel_per_chain):
                    # convert per pixel to bit stream
                    pixel = i * pixel_per_chain + j
                    if VER_ID < 5:
                        stream_args = [
                                        pixel_cal_list[pixel]['ADC_OFFSET'], pixel_cal_list[pixel]["TD_TRIM_BE2"],
                                        pixel_cal_list[pixel]["TD_TRIM_FE"], pixel_cal_list[pixel]["LEAKAGE_PASSIVE"],
                                        pixel_cal_list[pixel]["LEAKAGE_ACTIVE"], pixel_cal_list[pixel]["ADD_CAP"],
                                        pixel_cal_list[pixel]["MODE_BAD_PIX"], pixel_cal_list[pixel]["CDUMP_B"],
                                        pixel_cal_list[pixel]["CDUMP_A"], pixel_cal_list[pixel]["ADD_CAP"],
                                        pixel_cal_list[pixel]["QDUMP_DEL"], pixel_cal_list[pixel]["SEL_PIX"]
                                        ]
                    else:
                        stream_args = [
                                        pixel_cal_list[pixel]['ADC_OFFSET'], pixel_cal_list[pixel]["TD_TRIM_BE2"],
                                        pixel_cal_list[pixel]["TD_TRIM_BE"], pixel_cal_list[pixel]["TD_TRIM_FE"],
                                        pixel_cal_list[pixel]["LEAKAGE_PASSIVE"], pixel_cal_list[pixel]["LEAKAGE_ACTIVE"],
                                        pixel_cal_list[pixel]["QTRIG"], pixel_cal_list[pixel]["ADD_CAP"],
                                        pixel_cal_list[pixel]["CDUMP_B"], pixel_cal_list[pixel]["CDUMP_A"],
                                        pixel_cal_list[pixel]["SEL_PIX"], pixel_cal_list[pixel]["MODE_BAD_PIX"],
                                        pixel_cal_list[pixel]["ISO_PIX"], pixel_cal_list[pixel]["EN_PRBS"],
                                        pixel_cal_list[pixel]["TRIM_DNL"], pixel_cal_list[pixel]["QDUMP_DEL"],
                                        pixel_cal_list[pixel]["SEL_QDUMP"]
                                        ]
                    cal_chain_stream += basic_misc_streams(stream_args, VER_ID)
                # write to the chain
                sendPerPixCal(cal_chain_stream, VER_ID)

        # post programming clean up
        setCalibChains('disable')
        if VER_ID > 4:
            globals.buffer.write_asic_reg(0x33, lkg)
            globals.buffer.write_asic_reg(0x19, prbs)

        # verify after programming
        if globals.miscVerify:
            readFile = readPerPixCal(ldir, VER_ID, file_type='json')
            verify_pass = compareChainFiles(json_prog_path, readFile)
            if not verify_pass:
                program_sucess = False
                logger.error('Failed verification after programming ASIC.')
    except Exception as ex:
        logger.error("failed to program asic." + ex.message)
        raise Exception("failed to program asic." + ex.message)

    return program_sucess

def readPerPixCal(tdir, VER_ID, miscReadValsFile="misc_chain_read_vals.csv", startChain=0, endChain=11,
                  miscReadValsProgFile="misc_chain_prog_vals.csv", writeToProgFile=0, sensorId=None, file_type='csv'):
    """
    Overloading readPerPixCal method, to enable switcch file type: json | csv
    :param tdir:
    :param VER_ID:
    :param miscReadValsFile:
    :param startChain:
    :param endChain:
    :param miscReadValsProgFile:
    :param writeToProgFile:
    :param sensorId:
    :param file_type:
    :return:
    """
    # logger.debug(log.BLUE + 'Reading per pixel calibration chain values back')
    fhl = []
    if VER_ID > 4:
        if miscReadValsFile.find('_B') < 0:
            index = miscReadValsFile.find('.')
            miscReadValsFile = miscReadValsFile[:index] + '_B' + miscReadValsFile[index:]
            if file_type == 'json':
                miscReadValsFile = miscReadValsFile[:index] + '_B.' + 'json'
        if miscReadValsProgFile.find('_B') < 0:
            index = miscReadValsProgFile.find('.')
            miscReadValsProgFile = miscReadValsProgFile[:index] + '_B' + miscReadValsProgFile[index:]
            if file_type == 'json':
                miscReadValsProgFile = miscReadValsProgFile[:index] + '_B.' + 'json'

        lkg = readReg('0x33', 1, 15, 0)
        prbs = readReg('0x19', 1, 15, 0)
        # logger.info('prbs register readback in readPerPixCal = {}'.format(readReg('0x19', 1, 15, 0)))
        writeReg('0x33', 9, 7, '0x0000', shadow=1, writeNow=1)
        writeReg('0x33', 15, 15, '0x0000', shadow=1, writeNow=1)
        writeReg('0x33', 12, 12, '0x0000', shadow=1, writeNow=1)
        writeReg('0x33', 4, 4, '0x0000', shadow=1, writeNow=1)
        writeReg('0x19', 11, 11, '0x0000', shadow=1, writeNow=1)
    # logger.info('prbs register readback in readPerPixCal = {}'.format(readReg('0x19', 1, 15, 0)))
    if sensorId:
        miscReadValsFile = os.path.join(tdir, globals.docDir, '{}_{}.csv'.format(miscReadValsFile[:-4], sensorId))
    else:
        miscReadValsFile = os.path.join(tdir, globals.docDir, miscReadValsFile)
        # miscReadValsFile = tdir + "docs\\" + miscReadValsFile
    checkDir(miscReadValsFile)

    # container list for pixel cal data
    pixel_cal_data_list = []

    with open(miscReadValsFile, 'w') as rfh:
        fhl.append(rfh)
        if writeToProgFile:
            miscReadValsProgFile = os.path.join(tdir, globals.docDir, miscReadValsProgFile)
            checkDir(miscReadValsProgFile)
            wpfh = open(miscReadValsProgFile, 'w')
            fhl.append(wpfh)
        if VER_ID < 5:
            headerString = "Row,Col,Pixel,CALIB_ESSENTIAL_1,SEL_PIX,QDUMP_CLK_DEL, ADD_CAP,CDUMP_A,CDUMP_B," + \
                           "CALIB_ESSENTIAL_2,MODE_BAD_PIX, CALIB_ESSENTIAL_3,LEAKAGE_ACTIVE,LEAKAGE_PASSIVE," +\
                           "TD_TRIM_FE,TD_TRIM_BE,ADC_OFFSET\n"
        else:
            headerString = "Row,Col,Pixel,CALIB_ESSENTIAL_1,SEL_QDUMP,QDUMP_DEL,CALIB_ESSENTIAL_2,TRIM_DNL," +\
                            "CALIB_ESSENTIAL_3,EN_PRBS,ISO_PIX,MODE_BAD_PIX,SEL_PIX,CDUMP_A,CDUMP_B,ADD_CAP,QTRIG," +\
                            "LEAKAGE_ACTIVE,LEAKAGE_PASSIVE,TD_TRIM_FE,TD_TRIM_BE,TD_TRIM_BE2,ADC_OFFSET\n"
        for fh in fhl:
            if file_type != 'json':
                fh.write(headerString)
        for chain_num in range(startChain, endChain + 1):
            # logger.debug('Reading back calibration chain: %d' % chain_num)
            setCalibChains('enable', chains='0x{:04x}'.format(0x0001 << chain_num)) # Enable Cal Chain# 'chain_num'
            misc_stream = receivePerPixCal(VER_ID)
            if file_type == 'json':
                misc_pixel_chain = DDC0864Chain(chain_num)
                pixel_cal_data_list = pixel_cal_data_list + misc_pixel_chain.deserialize_pixel_cal_data(misc_stream, VER_ID)
            else:
                for fh in fhl:
                    misc_to_csv(fh, misc_stream, chain_num, VER_ID)
            setCalibChains('disable')

        # write to json files
        if file_type == 'json':
            metadata = DDC0864CalTableMetadata('16ns', '16kev')
            cal_table = DDC0864CalTable(metadata, pixel_cal_data_list)
            write_string = cal_table.get_json_str()
            for fh in fhl:
                fh.write(write_string)

    for fh in fhl[:]:
        if not fh.closed:
            fh.close()

    if VER_ID > 4:
        writeReg('0x33', 15, 0, lkg, shadow=1, writeNow=1)
        writeReg('0x19', 15, 0, prbs, shadow=1, writeNow=1)
        # logger.info('prbs register readback in readPerPixCal = {}'.format(readReg('0x19', 1, 15, 0)))
    return miscReadValsFile


def sendPerPixCal(stream, VER_ID):
    writeReg('0x3c', 0, 0, '0x0001', 1, 1)  # rst_chain_ctrlr
    writeReg('0x3c', 0, 0, '0x0000', 1, 1)
    if VER_ID > 4:
        writeReg('0x0a23', 3, 0, '0x0005', 0, 1)  # 5 word - 80 bit chains

    writes = StringIO()
    for word in reversed(chunk_str(stream, chunk_len=16)):
        writes.write(pack_u16_be(int(word, base=2)))

    globals.buffer.write_chain(globals.detector.ASIC_MISC_CHAIN_BASE, writes.getvalue())

    writeReg('0x0000e', 7, 7, '0x0000')
    writeReg('0x0000e', 7, 7, '0x0001')
    waitForBit('0x0000e', 5, True)


def receivePerPixCal(VER_ID):
    stream = ""
    num_pixels = 72
    num_reads = num_pixels * 4
    if VER_ID > 4:
        num_reads = num_pixels * 5
        writeReg('0x0a23', 3, 0, '0x0005', 0, 1)   # 5 word - 80 bit chains
    writeReg('0x3c', 0, 0, '0x0001', 1, 1) # Reset chain controller
    writeReg('0x3c', 0, 0, '0x0000', 1, 1)
    writeReg('0x0000e', 6, 6, '0x0000')  # Set CALIBRATION_CHAIN_READ low
    writeReg('0x0000e', 6, 6, '0x0001')  # Set CALIBRATION_CHAIN_READ high
    waitForBit('0x0000e', 4, True)

    reads = globals.buffer.read_chain(globals.detector.ASIC_MISC_CHAIN_BASE, num_reads)

    for word in reversed(chunk_str(reads, chunk_len=2)):
        stream += a2d(unpack_u16_be(word), 16)

    return stream


def setCalibChains(mode, chains = '0x0fff'):
    if mode.lower() == 'enable':
        logger.debug('enabling calibration chains')
        writeReg('0x3d', 11, 0, chains, 1, 1)
    else:
        logger.debug('disabling calibration chains')
        writeReg('0x3d', 11, 0, '0x0000', 1, 1)


def misc_flush_0s(VER_ID):
    '''Writes "0" into all registers in the misc chain'''
    writeReg('0x3d', 15, 0, '0x0fff', 1, 1)
    for i in range(72):
        writeReg('0x40', 15, 0, '0x0000', 1, 1)
        writeReg('0x40', 15, 0, '0x0000', 1, 1)
        writeReg('0x40', 15, 0, '0x0000', 1, 1)
        writeReg('0x40', 15, 0, '0x0000', 1, 1)
        if VER_ID > 4:
            writeReg('0x40', 15, 0, '0x0000', 1, 1)
        writeReg('0x3d', 15, 0, '0x0000', 1, 1)


def misc_flush_1s():
    '''Writes "1" into all registers in the misc chain
    works on rev b only'''
    writeReg('0x3d', 15, 0, '0x0fff', 1, 1)
    for i in range(72):
        writeReg('0x40', 15, 0, '0xffff', 1, 1)
        writeReg('0x40', 15, 0, '0xffff', 1, 1)
        writeReg('0x40', 15, 0, '0xffff', 1, 1)
        writeReg('0x40', 15, 0, '0xffff', 1, 1)
        writeReg('0x40', 15, 0, '0xffff', 1, 1)
    writeReg('0x3d', 15, 0, '0x0000', 1, 1)


def basic_misc_streams(a, VER_ID):
    '''generates pixstream from input arguments
       the actual length and format of the pixstream is dependent on VER_ID'''
    if VER_ID < 5:
        #rev A asic
        pix_stream  = a2d(a[0], 3)      # ADC Offset
        pix_stream += a2d(a[1], 7)      # td_trim_be
        pix_stream += a2d(a[2], 7)      # td_trim_fe
        pix_stream += a2d(a[3], 8)      # lkg_passive
        pix_stream += a2d(a[4], 8)      # lkg_active
        pix_stream += a2d(a[5], 4)[0]   # mode_add_cap
        pix_stream += '00'
        pix_stream += a2d(a[6], 1)      # mode_bad_pix
        pix_stream += '00'
        pix_stream += a2d(a[7], 3)      # mode_qdump_b
        pix_stream += a2d(a[8], 3)      # mode_qdump_a
        pix_stream += a2d(a[9], 4)[1:]  # mode_add_cap
        pix_stream += a2d(a[10], 3)     # qdump_del
        pix_stream += a2d(a[11], 1)     # sel_pix
        pix_stream += '000'
    else:
        #rev B asic
        pix_stream  = a2d(a[0], 4)      # ADC Offset
        pix_stream += a2d(a[1], 4)      # td_trim_be2
        pix_stream += a2d(a[2], 8)      # td_trim_be
        pix_stream += a2d(a[3], 8)      # td_trim_fe
        pix_stream += a2d(a[4], 8)      # lkg_passive
        pix_stream += a2d(a[5], 8)      # lkg_active
        pix_stream += a2d(a[6], 6)      # qtrig
        pix_stream += a2d(a[7], 4)      # mode_add_cap
        pix_stream += a2d(a[8], 3)      # mode_qdump_b
        pix_stream += a2d(a[9], 3)      # mode_qdump_a
        pix_stream += a2d(a[10], 1)     # sel_pix
        pix_stream += a2d(a[11], 1)     # mode_bad_pix
        pix_stream += a2d(a[12], 1)     # iso_pix
        pix_stream += a2d(a[13], 1)     # en_prbs
        pix_stream += '0000'
        pix_stream += a2d(a[14], 2)     # trim_dnl
        pix_stream += '00000'
        pix_stream += a2d(a[15], 3)     # qdump__clk_del
        pix_stream += a2d(a[16], 3)     # sel_qdump
        pix_stream += '000'
    return pix_stream


def gen_misc_stream(pix_list, stream_args, VER_ID):
    '''Generates string of bit stream based on args in the correct order for pixels from start_pixel to end_pixel'''
    stream = basic_misc_streams(stream_args, VER_ID)
    data_stream = ""
    for pixel in pix_list:
        if VER_ID < 5:
            # pad data stream with '000000000' for rev A asics
            data_stream += stream + ("0" * 9)  # Pad 9 Zeros for CALIB_PADDING
        else:
            data_stream += stream
    return data_stream


def misc_json_to_csv(tdir, VER_ID, json_file="misc_chain_prog_vals.json", csv_file="misc_chain_prog_vals.csv"):
    pass_flag = True
    try:
        if VER_ID > 4:
            if json_file.find('_B') < 0:
                index = json_file.find('.')
                json_file = json_file[:index] + '_B' + json_file[index:]
            if csv_file.find('_B') < 0:
                index = csv_file.find('.')
                csv_file = csv_file[:index] + '_B' + csv_file[index:]

        json_file_path = os.path.join(tdir, globals.docDir, json_file)
        csv_file_path = os.path.join(tdir, globals.docDir, csv_file)
        if not os.path.isfile(json_file_path):
            logger.error('json file does not exist with the specified path')
            raise Exception('json file does not exist with the specified path')

        # de-serializing the file to a list a per pixel cal data
        pixel_cal_list = []
        with open(json_file_path, 'r') as fh:
            cal_data = json.load(fh)
            pixel_cal_list = cal_data["caldata"]

        # serializing cal dat to csv file
        with open(csv_file_path,'wb') as fh:
            csv_writer = csv.writer(fh)
            if VER_ID < 5:
                csv_writer.writerow(['Row','Col','Pixel','CALIB_ESSENTIAL_1','SEL_PIX,QDUMP_CLK_DEL','ADD_CAP,CDUMP_A','CDUMP_B','CALIB_ESSENTIAL_2','MODE_BAD_PIX', 'CALIB_ESSENTIAL_3','LEAKAGE_ACTIVE','LEAKAGE_PASSIVE','TD_TRIM_FE','TD_TRIM_BE','ADC_OFFSET'])
            else:
                csv_writer.writerow(['Row','Col','Pixel','CALIB_ESSENTIAL_1','SEL_QDUMP','QDUMP_DEL','CALIB_ESSENTIAL_2','TRIM_DNL','CALIB_ESSENTIAL_3','EN_PRBS','ISO_PIX','MODE_BAD_PIX','SEL_PIX','CDUMP_A','CDUMP_B','ADD_CAP','QTRIG','LEAKAGE_ACTIVE','LEAKAGE_PASSIVE','TD_TRIM_FE','TD_TRIM_BE','TD_TRIM_BE2','ADC_OFFSET'])
            for i in range(pcct.ddc0864.N_PIXELS):
                csv_writer.writerow([
                                    pixel_cal_list[i]['Row'],
                                    pixel_cal_list[i]['Col'],
                                    pixel_cal_list[i]['Pixel'],
                                    pixel_cal_list[i]['CALIB_ESSENTIAL_1'],
                                    pixel_cal_list[i]['SEL_QDUMP'],
                                    pixel_cal_list[i]['QDUMP_DEL'],
                                    pixel_cal_list[i]['CALIB_ESSENTIAL_2'],
                                    pixel_cal_list[i]['TRIM_DNL'],
                                    pixel_cal_list[i]['CALIB_ESSENTIAL_3'],
                                    pixel_cal_list[i]['EN_PRBS'],
                                    pixel_cal_list[i]['ISO_PIX'],
                                    pixel_cal_list[i]['MODE_BAD_PIX'],
                                    pixel_cal_list[i]['SEL_PIX'],
                                    pixel_cal_list[i]['CDUMP_A'],
                                    pixel_cal_list[i]['CDUMP_B'],
                                    pixel_cal_list[i]['ADD_CAP'],
                                    pixel_cal_list[i]['QTRIG'],
                                    pixel_cal_list[i]['LEAKAGE_ACTIVE'],
                                    pixel_cal_list[i]['LEAKAGE_PASSIVE'],
                                    pixel_cal_list[i]['TD_TRIM_FE'],
                                    pixel_cal_list[i]['TD_TRIM_BE'],
                                    pixel_cal_list[i]['TD_TRIM_BE2'],
                                    pixel_cal_list[i]['ADC_OFFSET']
                                    ])

    except Exception as ex:
        logger.error("failed to convert misc file from json to csv: " + ex.message)
        raise Exception("failed to convert misc file from json to csv: " + ex.message)

    return pass_flag

def misc_to_csv(fh, stream, chain_num, VER_ID):
    """Writes stream data into fh in appropriate location determined by chain_num"""
    if VER_ID < 5:
        #rev A asic - 64 bit chains
        no_of_pix_writes = int(len(stream) / 64)
        writeStr = ""
        for chunk64 in reversed(range(no_of_pix_writes)):
            b = stream[chunk64 * 64:(chunk64 + 1) * 64]
            pixel = 72 * chain_num + 71 - chunk64
            (row, col) = row_and_col(pixel)
            writeStr = '%d,%d,%d,' % (row, col, pixel)
            writeStr = writeStr + '%d,' % int(b[52:55], base=2)             # ESSENTIAL_1
            writeStr = writeStr + '%d,' % int(b[51], base=2)                # SEL_PIX
            writeStr = writeStr + '%d,' % int(b[48:51], base=2)             # QDUMP_CLK_DEL
            writeStr = writeStr + '%d,' % int(b[33] + b[45:48], base=2)     # ADD_CAP
            writeStr = writeStr + '%d,' % int(b[42:45], base=2)             # CDUMP_A
            writeStr = writeStr + '%d,' % int(b[39:42], base=2)             # CDUMP_B
            writeStr = writeStr + '%d,' % int(b[37:39], base=2)             # ESSENTIAL_2
            writeStr = writeStr + '%d,' % int(b[36], base=2)                # MODE_BAD_PIX
            writeStr = writeStr + '%d,' % int(b[34:36], base=2)             # ESSENTIAL_3
            writeStr = writeStr + '%d,' % int(b[25:33], base=2)             # LEAKAGE_ACTIVE
            writeStr = writeStr + '%d,' % int(b[17:25], base=2)             # LEAKAGE_PASSIVE
            writeStr = writeStr + '%d,' % int(b[10:17], base=2)             # TD_TRIM_FE
            writeStr = writeStr + '%d,' % int(b[3:10], base=2)              # TD_TRIM_BE
            writeStr = writeStr + '%d\n' % int(b[0:3], base=2)              # ADC_OFFSET
            fh.write(writeStr)
    else:
        # rev B asic - 80 bit per pixel
        no_of_pix_writes = int(len(stream) / 80)
        for chunk80 in reversed(range(no_of_pix_writes)):
            b = stream[chunk80 * 80:(chunk80 + 1) * 80]
            pixel = 72 * chain_num + 71 - chunk80
            (row, col) = row_and_col(pixel)
            writeStr = '%d,%d,%d,' % (row, col, pixel)
            writeStr = writeStr + '%d,' % int(b[77:80], base=2)  # ESSENTIAL_1[2:0]
            writeStr = writeStr + '%d,' % int(b[74:77], base=2)  # SEL_QDUMP[2:0]
            writeStr = writeStr + '%d,' % int(b[71:74], base=2)  # QDUMP_DEL[2:0]
            writeStr = writeStr + '%d,' % int(b[66:71], base=2)  # ESSENTIAL_2[4:0]
            writeStr = writeStr + '%d,' % int(b[64:66], base=2)  # TRIM_DNL[1:0]
            writeStr = writeStr + '%d,' % int(b[60:64], base=2)  # ESSENTIAL_3[3:0]
            writeStr = writeStr + '%d,' % int(b[59], base=2)     # EN_PRBS
            writeStr = writeStr + '%d,' % int(b[58], base=2)     # ISO_PIX
            writeStr = writeStr + '%d,' % int(b[57], base=2)     # MODE_BAD_PIX
            writeStr = writeStr + '%d,' % int(b[56], base=2)     # SEL_PIX
            writeStr = writeStr + '%d,' % int(b[53:56], base=2)  # CDUMP_A
            writeStr = writeStr + '%d,' % int(b[50:53], base=2)  # CDUMP_B
            writeStr = writeStr + '%d,' % int(b[46:50], base=2)  # ADD_CAP
            writeStr = writeStr + '%d,' % int(b[40:46], base=2)  # QTRIG
            writeStr = writeStr + '%d,' % int(b[32:40], base=2)  # LEAKAGE_ACTIVE
            writeStr = writeStr + '%d,' % int(b[24:32], base=2)  # LEAKAGE_PASSIVE
            writeStr = writeStr + '%d,' % int(b[16:24], base=2)  # TD_TRIM_FE
            writeStr = writeStr + '%d,' % int(b[8:16], base=2)   # TD_TRIM_BE
            writeStr = writeStr + '%d,' % int(b[4:8], base=2)    # TD_TRIM_BE2
            writeStr = writeStr + '%d\n' % int(b[0:4], base=2)   # ADC_OFFSET
            fh.write(writeStr)
    return


def misc_from_csv(tdir, VER_ID, progFile="misc_chain_prog_vals.csv"):
    '''Generates from csv file data_stream of bits which shall later be broadcast into device'''
    data_stream = ""
    progFile = os.path.join(tdir, globals.docDir, progFile)
    try:
        with open(progFile, 'r') as fh:
            content = fh.readlines()
    except IOError:
        logger.error('File %s was not found.' % progFile)
        return
    else:
        numPixels = 864
        for chunk in [numPixels - i for i in range(numPixels)]:     # start read from end of file
            a = content[chunk].replace('\n', '').split(',')
            if VER_ID < 5:
                new_stream = a2d(a[-1], 3) + a2d(a[-2], 7) + a2d(a[-3], 7) + a2d(a[-4], 8) + a2d(a[-5], 8) + a2d(a[-11], 4)[
                    0] + a2d(a[-6], 2) + a[-7] + a2d(a[-8], 2) + a2d(a[-9], 3) + a2d(a[-10], 3) + a2d(a[-11], 4)[1:] + a2d(
                    a[-12], 3) + a[-13] + a2d(a[-14], 3) + "0" * 9
            else:
                new_stream = a2d(a[-1], 4) + a2d(a[-2], 4) + a2d(a[-3], 8) + a2d(a[-4], 8) + a2d(a[-5], 8) + \
                    a2d(a[-6], 8) + a2d(a[-7], 6) + a2d(a[-8], 4) + a2d(a[-9], 3) + a2d(a[-10], 3) + \
                    a2d(a[-11], 1)+ a2d(a[-12], 1) + a2d(a[-13], 1) + a2d(a[-14], 1) + a2d(a[-15], 4) + a2d(a[-16], 2) + \
                    a2d(a[-17], 5) + a2d(a[-18], 3) + a2d(a[-19], 3) + a2d(a[-20], 3)
            data_stream = data_stream + new_stream
    return data_stream


def create_misc_chain_prog(tdir, VER_ID, prog_mode=0, sourceFile="misc_chain_write_vals.csv",
                           progFile="misc_chain_prog_vals.csv", pix_list=[], td_trim_fe=[], sel_qdump=[], qdump_del=[],
                           en_prbs=[], iso_pix =[], qtrig=[], trim_dnl=[], td_trim_be=[], td_trim_be2=[], adc_offset=[],
                           mode_add_cap=[], lkg_passive=[], lkg_active=[], sel_pix=[], mode_bad_pix=[], cdump_a=[],
                           cdump_b=[],file_type='csv'):
    """
    Overloading of create_misc_chain_prog method.
    use file_type parameter to switch file type: csv | json.
    :param tdir:
    :param VER_ID:
    :param prog_mode:
    :param sourceFile:
    :param progFile:
    :param pix_list:
    :param td_trim_fe:
    :param sel_qdump:
    :param qdump_del:
    :param en_prbs:
    :param iso_pix:
    :param qtrig:
    :param trim_dnl:
    :param td_trim_be:
    :param td_trim_be2:
    :param adc_offset:
    :param mode_add_cap:
    :param lkg_passive:
    :param lkg_active:
    :param sel_pix:
    :param mode_bad_pix:
    :param cdump_a:
    :param cdump_b:
    :param file_type:
    :return:
    """

    if VER_ID < 5:
        columns = {
            'Row': 0,
            'Col': 1,
            'Pixel': 2,
            'CALIB_ESSENTIAL_1': 3,
            'SEL_PIX': 4,
            'QDUMP_CLK_DEL': 5,
            'ADD_CAP': 6,
            'CDUMP_A': 7,
            'CDUMP_B': 8,
            'CALIB_ESSENTIAL_2': 9,
            'MODE_BAD_PIX': 10,
            'CALIB_ESSENTIAL_3': 11,
            'LEAKAGE_ACTIVE': 12,
            'LEAKAGE_PASSIVE': 13,
            'TD_TRIM_FE': 14,
            'TD_TRIM_BE': 15,
            'ADC_OFFSET': 16
        }
    else:
        columns = {
            'Row': 0,
            'Col': 1,
            'Pixel': 2,
            'CALIB_ESSENTIAL_1': 3,
            'SEL_QDUMP': 4,
            'QDUMP_DEL': 5,
            'CALIB_ESSENTIAL_2': 6,
            'TRIM_DNL': 7,
            'CALIB_ESSENTIAL_3': 8,
            'EN_PRBS': 9,
            'ISO_PIX': 10,
            'MODE_BAD_PIX': 11,
            'SEL_PIX': 12,
            'CDUMP_A': 13,
            'CDUMP_B': 14,
            'ADD_CAP': 15,
            'QTRIG': 16,
            'LEAKAGE_ACTIVE': 17,
            'LEAKAGE_PASSIVE': 18,
            'TD_TRIM_FE': 19,
            'TD_TRIM_BE': 20,
            'TD_TRIM_BE2': 21,
            'ADC_OFFSET': 22
        }
        if sourceFile.find('_B') < 0:
            index = sourceFile.find('.')
            sourceFile = sourceFile[:index] + '_B' + sourceFile[index:]
        if progFile.find('_B') < 0:
            index = progFile.find('.')
            progFile = progFile[:index] + '_B' + progFile[index:]
        if file_type == 'json':
            index = progFile.find('.')
            progFile = progFile[:index] + '.' + 'json'
    sourceFile = os.path.join(tdir, globals.docDir, sourceFile)
    progFile = os.path.join(tdir, globals.docDir, progFile)
    if prog_mode == 0 or prog_mode == 2:
        sel_pix = [1] * len(pix_list)
        mode_bad_pix = [0] * len(pix_list)
    # pix_list.sort()
    # logger.info('Source file: %s' % sourceFile)
    pixel_cal_data_list = []
    try:
        sourceFh = open(sourceFile, 'r')
    except IOError:
        logger.error('File %s was not found.' % sourceFile)
        return
    else:
        source_content = sourceFh.readlines()
        sourceFh.close()
        source_rows = [i.replace('\n', '').split(',') for i in source_content]
        dest_rows = source_rows
        writeString = source_content[0]

        for i in range(1, 865):
            row = source_content[i].replace('\n', '').split(',')
            if VER_ID < 5:
                if i - 1 in pix_list:
                    if len(pix_list) == len(td_trim_fe):
                        row[columns['TD_TRIM_FE']] = '%d' % td_trim_fe[pix_list.index(i - 1)]
                    if len(pix_list) == len(td_trim_be):
                        row[columns['TD_TRIM_BE']] = '%d' % td_trim_be[pix_list.index(i - 1)]
                    if len(pix_list) == len(adc_offset):
                        row[columns['ADC_OFFSET']] = '%d' % adc_offset[pix_list.index(i - 1)]
                    if len(pix_list) == len(lkg_passive):
                        row[columns['LEAKAGE_PASSIVE']] = '%d' % lkg_passive[pix_list.index(i - 1)]
                    if len(pix_list) == len(lkg_active):
                        row[columns['LEAKAGE_ACTIVE']] = '%d' % lkg_active[pix_list.index(i - 1)]
                    if len(pix_list) == len(sel_pix):
                        row[columns['SEL_PIX']] = '%d' % sel_pix[pix_list.index(i - 1)]
                    if len(pix_list) == len(mode_bad_pix):
                        row[columns['MODE_BAD_PIX']] = '%d' % mode_bad_pix[pix_list.index(i - 1)]
                    if len(pix_list) == len(mode_add_cap):
                        row[columns['ADD_CAP']] = '%d' % mode_add_cap[pix_list.index(i - 1)]
                elif prog_mode == 2:
                    row[columns['SEL_PIX']] = '0'  # Disable all pixels not in pix_list
                    row[columns['MODE_BAD_PIX']] = '1'  # mode_bad_pix = 1
                elif prog_mode == 0:
                    row[columns['SEL_PIX']] = '0'  # Disable all pixels not in pix_list
                    row[columns['MODE_BAD_PIX']] = '0'  # mode_bad_pix = 0
                else:
                    pass
            else:
                if i - 1 in pix_list:
                    if len(pix_list) == len(td_trim_fe):
                        row[columns['TD_TRIM_FE']] = '%d' % td_trim_fe[pix_list.index(i - 1)]
                    if len(pix_list) == len(td_trim_be):
                        row[columns['TD_TRIM_BE']] = '%d' % td_trim_be[pix_list.index(i - 1)]
                    if len(pix_list) == len(td_trim_be2):
                        row[columns['TD_TRIM_BE2']] = '%d' % td_trim_be2[pix_list.index(i - 1)]
                    if len(pix_list) == len(adc_offset):
                        row[columns['ADC_OFFSET']] = '%d' % adc_offset[pix_list.index(i - 1)]
                    if len(pix_list) == len(lkg_passive):
                        row[columns['LEAKAGE_PASSIVE']] = '%d' % lkg_passive[pix_list.index(i - 1)]
                    if len(pix_list) == len(lkg_active):
                        row[columns['LEAKAGE_ACTIVE']] = '%d' % lkg_active[pix_list.index(i - 1)]
                    if len(pix_list) == len(sel_pix):
                        row[columns['SEL_PIX']] = '%d' % sel_pix[pix_list.index(i - 1)]
                    if len(pix_list) == len(mode_bad_pix):
                        row[columns['MODE_BAD_PIX']] = '%d' % mode_bad_pix[pix_list.index(i - 1)]
                    if len(pix_list) == len(mode_add_cap):
                        row[columns['ADD_CAP']] = '%d' % mode_add_cap[pix_list.index(i - 1)]
                    if len(pix_list) == len(sel_qdump):
                        row[columns['SEL_QDUMP']] = '%d' % sel_qdump[pix_list.index(i - 1)]
                    if len(pix_list) == len(qdump_del):
                        row[columns['QDUMP_DEL']] = '%d' % qdump_del[pix_list.index(i - 1)]
                    if len(pix_list) == len(trim_dnl):
                        row[columns['TRIM_DNL']] = '%d' % trim_dnl[pix_list.index(i - 1)]
                    if len(pix_list) == len(en_prbs):
                        row[columns['EN_PRBS']] = '%d' % en_prbs[pix_list.index(i - 1)]
                    if len(pix_list) == len(iso_pix):
                        row[columns['ISO_PIX']] = '%d' % iso_pix[pix_list.index(i - 1)]
                    if len(pix_list) == len(qtrig):
                        row[columns['QTRIG']] = '%d' % qtrig[pix_list.index(i - 1)]
                    if len(pix_list) == len(cdump_a):
                        row[columns['CDUMP_A']] = '%d' % cdump_a[pix_list.index(i - 1)]
                    if len(pix_list) == len(cdump_b):
                        row[columns['CDUMP_B']] = '%d' % cdump_b[pix_list.index(i - 1)]
                elif prog_mode == 2:
                    row[columns['SEL_PIX']] = '0'  # Disable all pixels not in pix_list
                    row[columns['MODE_BAD_PIX']] = '1'  # mode_bad_pix = 1
                elif prog_mode == 0:
                    row[columns['SEL_PIX']] = '0'  # Disable all pixels not in pix_list
                    row[columns['MODE_BAD_PIX']] = '0'  # mode_bad_pix = 0
                else:
                    pass
            ##Do not touch any other register
            writeString += ','.join(row)
            writeString += '\n'
            if file_type == 'json':
                pixel_cal = PixelCalData('str_list', row)
                pixel_cal_data_list.append(pixel_cal)

        if file_type == 'json':
            metadata = DDC0864CalTableMetadata('16ns', '16kev')
            cal_table = DDC0864CalTable(metadata, pixel_cal_data_list)
            writeString = cal_table.get_json_str()

        with open(progFile, 'w') as progFh:
            progFh.write(writeString)

    return progFile


def multiSensorChainsToVals(tdir, VER_ID, activeAsicNums, passiveAsicNums):
    if VER_ID >= 5:
        [addCapList, td_trim_feList, td_trim_beList, deadTimeList, adcOffsetList, threshList, tL0, tL1, tL2, tL3, tL4, tL5] \
            = [[[]] * 16, [[]] * 16, [[]] * 16, [[]] * 16, [[]] * 16, [[]] * 16, [[]] * 16, [[]] * 16, [[]] * 16, [[]] * 16, [[]] * 16, [[]] * 16]
    else:
        [addCapList, td_trim_feList, td_trim_beList, deadTimeList, adcOffsetList, threshList, tL0, tL1, tL2, tL3, tL4, tL5] \
            = [[[]] * 16, [[]] * 16, [[]] * 16, [[]] * 16, [[]] * 16, [[]] * 16, [[]] * 16, [[]] * 16, [[]] * 16, [[]] * 16, [[]] * 16, [[]] * 16]
    for asic in activeAsicNums:
        maskAsicList(activeAsicNums, passiveAsicNums, maskAllOthersOff=1, asicSingle=asic)
        [addCapListTmp, td_trim_feListTmp, td_trim_beListTmp, deadTimeListTmp, adcOffsetListTmp, threshListTmp, tL0Tmp, tL1Tmp, tL2Tmp, tL3Tmp, tL4Tmp, tL5Tmp] = getChainVals(tdir, VER_ID)
        [addCapList[asic],td_trim_feList[asic],td_trim_beList[asic],deadTimeList[asic],adcOffsetList[asic],threshList[asic],tL0[asic],tL1[asic],tL2[asic],tL3[asic],tL4[asic],tL5[asic]] = [addCapListTmp,td_trim_feListTmp,td_trim_beListTmp,deadTimeListTmp,adcOffsetListTmp,threshListTmp,tL0Tmp,tL1Tmp,tL2Tmp,tL3Tmp,tL4Tmp,tL5Tmp]
    maskAsicList(activeAsicNums, passiveAsicNums, disableTestPattern=1)  # Enable all active ASICs
    return [addCapList, td_trim_feList, td_trim_beList, deadTimeList, adcOffsetList, threshList, tL0, tL1, tL2, tL3, tL4, tL5]


def getChainVals(tdir, VER_ID, readFromDev=1, miscProgFile="misc_chain_prog_vals.csv", threshProgFile="thresh_chain_prog_vals.csv"):
    addCap_Idx, td_fe_Idx, td_be_idx, adcOff_Idx = 6, 14, 15, 16
    if VER_ID >= 5:
        index = miscProgFile.find('.')
        miscProgFile = miscProgFile[:index] + '_B' + miscProgFile[index:]
        index = threshProgFile.find('.')
        threshProgFile = threshProgFile[:index] + '_B' + threshProgFile[index:]
        addCap_Idx, td_fe_Idx, td_be_idx, adcOff_Idx = 15, 19, 20, 22
    if readFromDev:
        logger.debug('Calibration Data will be read from the device')
        readPerPixCal(tdir, VER_ID)
        if VER_ID > 4:
            rhm = open(os.path.join(tdir, globals.docDir, "misc_chain_read_vals_B.csv"), 'rb')   # todo fix file naming for rev B
        else:
            rhm = open(os.path.join(tdir, globals.docDir, "misc_chain_read_vals.csv"), 'rb')
        readPerPixThresh(tdir)
        rht = open(os.path.join(tdir, globals.docDir, "thresh_chain_read_vals.csv"), 'rb')
    else:
        logger.debug('Calibration Data will be read from files: %s and %s' % (miscProgFile, threshProgFile))
        rhm = open(os.path.join(tdir, globals.docDir, miscProgFile), 'rb')
        rht = open(os.path.join(tdir, globals.docDir, threshProgFile), 'rb')
    miscReader = csv.reader(rhm)
    addCapList = []
    td_trim_feList = []
    td_trim_beList = []
    deadTimeList = []
    adcOffsetList = []
    next(miscReader)
    for row in miscReader:
        addCapList.append(int(row[addCap_Idx]))
        td_trim_fe = int(row[td_fe_Idx])
        td_trim_feList.append(td_trim_fe)
        td_trim_be = int(row[td_be_idx])
        td_trim_beList.append(td_trim_be)
        if VER_ID >= 5:
            deadTime = 39e-9 - (106e-12 * td_trim_fe)
        else:
            deadTime = 34.9e-9 - (186e-12 * td_trim_fe)
        deadTimeList.append('%2.3g' % deadTime)
        adcOffsetList.append(int(row[adcOff_Idx]))
    rhm.close()
    threshReader = csv.reader(rht)
    threshList = []
    thrList0 = []
    thrList1 = []
    thrList2 = []
    thrList3 = []
    thrList4 = []
    thrList5 = []
    next(threshReader)
    for row in threshReader:
        threshList.append([int(row[3]), int(row[4]), int(row[5]), int(row[6]), int(row[7]), int(row[8])])
        thrList0.append(int(row[3]))
        thrList1.append(int(row[4]))
        thrList2.append(int(row[5]))
        thrList3.append(int(row[6]))
        thrList4.append(int(row[7]))
        thrList5.append(int(row[8]))
    rht.close()
    return addCapList, td_trim_feList, td_trim_beList, deadTimeList, adcOffsetList, threshList, thrList0, thrList1, thrList2, thrList3, thrList4, thrList5


def compareChainFiles(File1, File2):
    fmatch = True
    with open(File1, 'rb') as f1:
        d1 = f1.readlines()
    with open(File2, 'rb') as f2:
        d2 = f2.readlines()
    assert len(d1) == len(d2)
    for i in range(len(d1)):
        if d1[i] == d2[i]:
            continue
        else:
            fmatch = False
            diffLine = i
            break
    if fmatch:
        pass
    else:
        logger.error('Files {}, {} Do Not Match at line {}'.format(File1, File2, diffLine))
    return fmatch

