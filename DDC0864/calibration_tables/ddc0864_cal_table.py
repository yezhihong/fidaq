########################################################################################################################
#
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
########################################################################################################################

import json
import logging

logger = logging.getLogger(__name__)


class DDC0864CalTable(dict):
    """Data structure for DDC0864 cal data
    """
    def __init__(self, metadata, pixel_cal):
        """
        Create cal table by taking metadata and pixel_cal
        :param metadata:
        :param pixel_cal:
        """
        if metadata is not None and pixel_cal is not None:
            self.metadata = metadata
            self.pixel_cal = pixel_cal
            self.caldata_update()

    def load_from_json_file(self, json_file):
        """
        load cal data from a JSON file
        :param json_file:
        :return:
        """
        try:
            self.clear()
            with open(json_file, 'r') as f:
                read_data = f.read()
                self.update(json.loads(read_data))
        except Exception as e:
            logger.error("error to load cal data json file, is the file format correct?")
            raise ('error to load cal data json file, is the file format correct?' + e.message)

    def __table_update(self):
        self.update({'metadata': self.metadata, 'caldata': self.pixel_cal})

    def metadata_update(self, key, value):
        self.metadata[key] = value
        self.__table_update()

    def caldata_update(self):
        self.__table_update()

    def get_json_str(self):
        """
        return json string of the object
        :return:
        """
        return json.dumps(self, indent=4)

    def dump_to_file(self, file_path):

        with open(file_path, 'w') as dump_file:
            json.dump(self, dump_file)
