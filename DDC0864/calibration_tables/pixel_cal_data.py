########################################################################################################################
#
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
########################################################################################################################

import json


class PixelCalData(dict):
    """DDC0864 per pixel cal data dictionary

    """
    def __init__(self, pixel_id):
        """
        init a blank instance with id
        :param pixel_id:
        """
        self.update({
                    'Row': 0,
                    'Col': 1,
                    'Pixel': 2,
                    'CALIB_ESSENTIAL_1': 3,
                    'SEL_QDUMP': 4,
                    'QDUMP_DEL': 5,
                    'CALIB_ESSENTIAL_2': 6,
                    'TRIM_DNL': 7,
                    'CALIB_ESSENTIAL_3': 8,
                    'EN_PRBS': 9,
                    'ISO_PIX': 10,
                    'MODE_BAD_PIX': 11,
                    'SEL_PIX': 12,
                    'CDUMP_A': 13,
                    'CDUMP_B': 14,
                    'ADD_CAP': 15,
                    'QTRIG': 16,
                    'LEAKAGE_ACTIVE': 17,
                    'LEAKAGE_PASSIVE': 18,
                    'TD_TRIM_FE': 19,
                    'TD_TRIM_BE': 20,
                    'TD_TRIM_BE2': 21,
                    'ADC_OFFSET': 22
                    })
        self['Pixel'] = pixel_id

    def __init__(self, str_type, cal_data):
        """
        instance initialized by a data string, either a json str or a csv str, specified by str_type
        :param str_type:[csv | json | str_list]
        """
        try:
            if str_type == 'csv':
                cal_data_list = cal_data.split(',')
                self.update({
                            'Row': int(cal_data_list[0]),
                            'Col': int(cal_data_list[1]),
                            'Pixel': int(cal_data_list[2]),
                            'CALIB_ESSENTIAL_1': int(cal_data_list[3]),
                            'SEL_QDUMP': int(cal_data_list[4]),
                            'QDUMP_DEL': int(cal_data_list[5]),
                            'CALIB_ESSENTIAL_2': int(cal_data_list[6]),
                            'TRIM_DNL': int(cal_data_list[7]),
                            'CALIB_ESSENTIAL_3': int(cal_data_list[8]),
                            'EN_PRBS': int(cal_data_list[9]),
                            'ISO_PIX': int(cal_data_list[10]),
                            'MODE_BAD_PIX': int(cal_data_list[11]),
                            'SEL_PIX': int(cal_data_list[12]),
                            'CDUMP_A': int(cal_data_list[13]),
                            'CDUMP_B': int(cal_data_list[14]),
                            'ADD_CAP': int(cal_data_list[15]),
                            'QTRIG': int(cal_data_list[16]),
                            'LEAKAGE_ACTIVE': int(cal_data_list[17]),
                            'LEAKAGE_PASSIVE': int(cal_data_list[18]),
                            'TD_TRIM_FE': int(cal_data_list[19]),
                            'TD_TRIM_BE': int(cal_data_list[20]),
                            'TD_TRIM_BE2': int(cal_data_list[21]),
                            'ADC_OFFSET': int(cal_data_list[22])
                            })
            elif str_type == 'str_list':
                cal_data_list = cal_data
                self.update({
                            'Row': int(cal_data_list[0]),
                            'Col': int(cal_data_list[1]),
                            'Pixel': int(cal_data_list[2]),
                            'CALIB_ESSENTIAL_1': int(cal_data_list[3]),
                            'SEL_QDUMP': int(cal_data_list[4]),
                            'QDUMP_DEL': int(cal_data_list[5]),
                            'CALIB_ESSENTIAL_2': int(cal_data_list[6]),
                            'TRIM_DNL': int(cal_data_list[7]),
                            'CALIB_ESSENTIAL_3': int(cal_data_list[8]),
                            'EN_PRBS': int(cal_data_list[9]),
                            'ISO_PIX': int(cal_data_list[10]),
                            'MODE_BAD_PIX': int(cal_data_list[11]),
                            'SEL_PIX': int(cal_data_list[12]),
                            'CDUMP_A': int(cal_data_list[13]),
                            'CDUMP_B': int(cal_data_list[14]),
                            'ADD_CAP': int(cal_data_list[15]),
                            'QTRIG': int(cal_data_list[16]),
                            'LEAKAGE_ACTIVE': int(cal_data_list[17]),
                            'LEAKAGE_PASSIVE': int(cal_data_list[18]),
                            'TD_TRIM_FE': int(cal_data_list[19]),
                            'TD_TRIM_BE': int(cal_data_list[20]),
                            'TD_TRIM_BE2': int(cal_data_list[21]),
                            'ADC_OFFSET': int(cal_data_list[22])
                            })

            elif str_type == 'json':
                self.update(json.loads(cal_data))
            else:
                raise KeyError
        except KeyError as e:
            print(e.message)
        except Exception as e:
            print(e.message)

    def get_field(self, key):
        """
        return a single field indexed by the key
        :param key:
        :return:
        """
        val = dict.__getitem__(self, key)
        return val

    def set_field(self, key, val):
        """
        set the field value, or update the value if the key exists
        :param key:
        :param val:
        :return:
        """
        dict.__setitem__(self, key, val)

    def get_json_str(self):
        """
        return json string of the object
        :return:
        """
        return json.dumps(self)

