########################################################################################################################
#
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
########################################################################################################################

import json


class DDC0864CalTableMetadata(dict):
    """DDC0864 cal data metadata
    """
    def __init__(self, target_dead_time, target_qtrig):
        self.update({
            'TargetDeadTime': target_dead_time,
            'TargetQtrig': target_qtrig})

    def get_field(self, key):
        """
        return a single field indexed by the key
        :param key:
        :return:
        """
        val = dict.__getitem__(self, key)
        return val

    def set_field(self, key, val):
        """
        set the field value, or update the value if the key exists
        :param key:
        :param val:
        :return:
        """
        dict.__setitem__(self, key, val)

    def get_json_str(self):
        """
        return json string of the object
        :return:
        """
        return json.dumps(self, indent=4)

