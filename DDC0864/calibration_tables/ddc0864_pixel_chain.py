########################################################################################################################
#
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
#
########################################################################################################################

from DDC0864.dmFunctions import waitForBit
from time import sleep
from DDC0864.devInit import a2d, row_and_col
from dmBuffer import DM
from pixel_cal_data import PixelCalData
import logging
import globals


class DDC0864Chain:
    """
    Class to abstract DDC0864 chain
    DDC0864 uses the chain concept to group pixel register access
    In sequence, 864 pixels are grouped into 12 chains. Each chain contains 72 pixels.
    """
    def __init__(self, chain_id):
        self.chain_id = chain_id

    def deserialize_pixel_cal_data(self, stream, VER_ID):
        """
        convert the output stream from receive_per_pixel_cal to list of PixelCalData x 72
        """
        pixel_cal_list = []
        if VER_ID < 5:
            # rev A asic - 64 bit chains
            no_of_pix_writes = int(len(stream) / 64)
            for chunk64 in reversed(range(no_of_pix_writes)):
                b = stream[chunk64 * 64:(chunk64 + 1) * 64]
                pixel = 72 * self.chain_id + 71 - chunk64
                (row, col) = row_and_col(pixel)
                write_str = '%d,%d,%d,' % (row, col, pixel)
                write_str = write_str + '%d,' % int(b[52:55], base=2)  # ESSENTIAL_1
                write_str = write_str + '%d,' % int(b[51], base=2)  # SEL_PIX
                write_str = write_str + '%d,' % int(b[48:51], base=2)  # QDUMP_CLK_DEL
                write_str = write_str + '%d,' % int(b[33] + b[45:48], base=2)  # ADD_CAP
                write_str = write_str + '%d,' % int(b[42:45], base=2)  # CDUMP_A
                write_str = write_str + '%d,' % int(b[39:42], base=2)  # CDUMP_B
                write_str = write_str + '%d,' % int(b[37:39], base=2)  # ESSENTIAL_2
                write_str = write_str + '%d,' % int(b[36], base=2)  # MODE_BAD_PIX
                write_str = write_str + '%d,' % int(b[34:36], base=2)  # ESSENTIAL_3
                write_str = write_str + '%d,' % int(b[25:33], base=2)  # LEAKAGE_ACTIVE
                write_str = write_str + '%d,' % int(b[17:25], base=2)  # LEAKAGE_PASSIVE
                write_str = write_str + '%d,' % int(b[10:17], base=2)  # TD_TRIM_FE
                write_str = write_str + '%d,' % int(b[3:10], base=2)  # TD_TRIM_BE
                write_str = write_str + '%d\n' % int(b[0:3], base=2)  # ADC_OFFSET
                logging.error("ASIC version is not supported anymore.")
        else:
            # rev B asic - 80 bit per pixel
            no_of_pix_writes = int(len(stream) / 80)
            for chunk80 in reversed(range(no_of_pix_writes)):
                b = stream[chunk80 * 80:(chunk80 + 1) * 80]
                pixel = 72 * self.chain_id + 71 - chunk80
                (row, col) = row_and_col(pixel)
                write_str = '%d,%d,%d,' % (row, col, pixel)
                # ESSENTIAL_1[2:0]
                write_str = write_str + '%d,' % int(b[77:80], base=2)
                # SEL_QDUMP[2:0]
                write_str = write_str + '%d,' % int(b[74:77], base=2)
                # QDUMP_DEL[2:0]
                write_str = write_str + '%d,' % int(b[71:74], base=2)
                # ESSENTIAL_2[4:0]
                write_str = write_str + '%d,' % int(b[66:71], base=2)
                # TRIM_DNL[1:0]
                write_str = write_str + '%d,' % int(b[64:66], base=2)
                # ESSENTIAL_3[3:0]
                write_str = write_str + '%d,' % int(b[60:64], base=2)
                # EN_PRBS
                write_str = write_str + '%d,' % int(b[59], base=2)
                # ISO_PIX
                write_str = write_str + '%d,' % int(b[58], base=2)
                # MODE_BAD_PIX
                write_str = write_str + '%d,' % int(b[57], base=2)
                # SEL_PIX
                write_str = write_str + '%d,' % int(b[56], base=2)
                # CDUMP_A
                write_str = write_str + '%d,' % int(b[53:56], base=2)
                # CDUMP_B
                write_str = write_str + '%d,' % int(b[50:53], base=2)
                # ADD_CAP
                write_str = write_str + '%d,' % int(b[46:50], base=2)
                # QTRIG
                write_str = write_str + '%d,' % int(b[40:46], base=2)
                # LEAKAGE_ACTIVE
                write_str = write_str + '%d,' % int(b[32:40], base=2)
                # LEAKAGE_PASSIVE
                write_str = write_str + '%d,' % int(b[24:32], base=2)
                # TD_TRIM_FE
                write_str = write_str + '%d,' % int(b[16:24], base=2)
                # TD_TRIM_BE
                write_str = write_str + '%d,' % int(b[8:16], base=2)
                # TD_TRIM_BE2
                write_str = write_str + '%d,' % int(b[4:8], base=2)
                # ADC_OFFSET
                write_str = write_str + '%d\n' % int(b[0:4], base=2)
                pixel_cal = PixelCalData('csv', write_str)
                pixel_cal_list.append(pixel_cal)

        return pixel_cal_list

    @staticmethod
    def receive_per_pixel_cal(VER_ID):
        """
                Ported from misc_chain.py to avoid import loop
                To read pixels' cal data in the current enabled chain
                depends on: setCalibChains to enable the target chain

                VER_ID: ASIC reveision, currently 5
                :return: string contains 72 pixels' cal data
        """
        stream = ""
        num_pixels = 72
        num_reads = num_pixels * 4
        adr = 0x2011F
        if VER_ID > 4:
            num_reads = num_pixels * 5
            globals.buffer.write_asic_reg(0x0a23, 0x0005)
            # 5 word - 80 bit chains
            adr = 0x20167
        # Reset chain controller
        globals.buffer.write_asic_reg(0x3c, 0x0001)
        globals.buffer.write_asic_reg(0x3c, 0x0000)
        sleep(0.001)
        # Set CALIBRATION_CHAIN_READ low
        globals.buffer.write_asic_reg(0x0000e, 0x0000)
        # Set CALIBRATION_CHAIN_READ high
        globals.buffer.write_asic_reg(0x0000e, 0x0001)
        waitForBit('0x0000e', 4, True)
        for i in range(num_reads):
            stream = stream + a2d(int(DM.SPI_read(hex(adr)), 0), 16)
            adr -= 1
        return stream

    @staticmethod
    def set_calib_single_chain(mode, chain_num):
        """
        enable or disable a single cal chain
        ported from misc_chain.py. this is the proper place for chain operation
        enable/disable by set/unset the bit indexed by the channel number. to set all channels, use "set_calib_chains"

        :param mode: 'enable' | 'disable'
        :param chain_num: chain number(0 to 11)
        :return:
        """
        if chain_num > 11 or chain_num < 0:
            raise("ddc0864 chain number is from 0 to 11")
        chain = 0x0001 << chain_num
        if mode.lower() == 'enable':
            logging.debug('enabling calibration chains')
            globals.buffer.write_asic_reg(0x3d, chain)
        else:
            logging.debug('disabling calibration chains')
            globals.buffer.write_asic_reg(0x3d, 0x0000)
        sleep(0.1)

    @staticmethod
    def set_calib_chains(mode):
        """
        enable/disable all cal chains
        ported from misc_chain.py. this is the proper place for chain operation

        :param mode: 'enable' | 'disable'
        :return:
        """
        chains = 0x0fff
        if mode.lower() == 'enable':
            logging.debug('enabling calibration chains')
            globals.buffer.write_asic_reg(0x3d, chains)
        else:
            logging.debug('disabling calibration chains')
            globals.buffer.write_asic_reg(0x3d, 0x0000)
        sleep(0.1)
