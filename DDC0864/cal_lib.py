########################################################################################################################
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
########################################################################################################################

import os
import logging
import cStringIO
from time import time
import numpy as np
import pandas as pd
import matplotlib.pyplot as pyplot
import matplotlib.gridspec as gridspec

import globals

from DDC0864.dmFunctions import writeReg, dacSet, dacSet0, readReg, is_set, checkDir, asicDMMPatternEnable, chkCrcError
from DDC0864.misc_chain import create_misc_chain_prog, perPixCal, readPerPixCal
from DDC0864.thresh_chain import perPixThresh, readPerPixThresh
from DDC0864.devInit import row_and_col

from dmBuffer.DM_Buffer_udp_lib_20180704 import dump_buffer, set_DATA_reset, set_VTRIG_reset, \
    set_DATA_preamble, set_DATA_crc_length, set_DATA_frame_length, set_DATA_enable, set_DATA_in_select, \
    set_DATA_out_select, set_DATA_field_offset, set_DATA_test_delay, set_DATA_accum_mode, set_DATA_accum_num, \
    set_VTRIG_num_cycles, set_VTRIG_periods, set_VTRIG_enable, SPI_read, check_VTRIG_num_cycles, \
    read_DATA_crc_err_count, clear_DATA_crc_err, set_DATA_bypass


logger = logging.getLogger(__name__)


def dumpCLKSet(clkFreq):
    # clkFreq input in Hz
    sysClk = 35e6                                  # 35Mhz system clock
    dumpClk = int(float(sysClk) / float(clkFreq))  # get divisor setting
    dumpClkFreq = float(sysClk) / (dumpClk + 1)    # calc resulting generated frequency
    logger.debug('Setting DFT2 Clock Freq to: %1.3eHz ...actual divisor=%d; resulting freq= %1.3eHz' % (
        clkFreq, dumpClk, dumpClkFreq))
    wdata1 = "0x{:04x}".format(dumpClk)
    writeReg('0x00A0A', 15, 3, wdata1)             # DFT2_divisor = dumpClk
    return (dumpClkFreq)


def dumpCLKEnable(clkOn, VER_ID):
    if (clkOn):
        logger.debug("DFT2 Clock ON")
        writeReg('0x00A0A', 2, 2, '0x0001')        # DFT2_clk_on = 1
        if VER_ID < 5:
            writeReg('0x33', 1, 0, '0x0003', shadow=1, writeNow=1)    # enable DFT2 dump clock and QDUMP
            writeReg('0x77', 13, 13, '0x0001', shadow=1, writeNow=1)  # enable Global dump clock
        else:
            writeReg('0x35', 1, 0, '0x0001', shadow=1, writeNow=1)    # select DFT2 dump clock REV B?
    else:
        logger.debug("DFT2 Clock OFF")
        writeReg('0x00A0A', 2, 2, '0x0000')  # DFT2_clk_on = 0
        if VER_ID < 5:
            writeReg('0x33', 1, 0, '0x0000', shadow=1, writeNow=1)  # disable DFT2 dump clock and QDUMP
        else:
            writeReg('0x35', 1, 0, '0x0000', shadow=1, writeNow=1)  # disable DFT2 dump clock REV B?
    return


def setModeBinAlign(mode):
    if mode.lower() == 'on':
        logger.debug('set mode_bin_align ON')
        val = '0x0001'
    else:
        logger.debug('set mode_bin_align OFF')
        val = '0x0000'
    writeReg('0x17', 0, 0, val, shadow=1, writeNow=1)  # set mode_bin_align to value


def setEnableAdcCalib(enable):
    if enable:
        logger.debug('set en_adc_calib to enabled')
        val = '0x0001'
    else:
        logger.debug('set en_adc_calib to disabled')
        val = '0x0000'
    writeReg('0x33', 15, 15, val, shadow=1, writeNow=1)  # set en_adc_calib


def qdumpEnable(enable):
    if enable:
        logger.debug('set qdump_enable to enabled')
        val = '0x0001'
    else:
        logger.debug('set qdump_enable to disabled')
        val = '0x0000'
    writeReg('0x33', 0, 0, val, shadow=1, writeNow=1)                                          # enable QDUMP


def bytes2dec(bytes, Ncounters=13, stride_len=4):
    """Converts binary data from bytes list to decimal data array"""
    numCounts = 12 * Ncounters * 36
    if (len(bytes) != numCounts * stride_len):
        logger.warn('Length of byte list is wrong....please check the data.')
        return
    if (stride_len == 8):
        # regular view data using 16-bit counters
        data00 = np.fromstring(bytes[0::stride_len], 'u1') * 256 + np.fromstring(bytes[4::stride_len], 'u1')
        data01 = np.fromstring(bytes[1::stride_len], 'u1') * 256 + np.fromstring(bytes[5::stride_len], 'u1')
        data10 = np.fromstring(bytes[2::stride_len], 'u1') * 256 + np.fromstring(bytes[6::stride_len], 'u1')
        data11 = np.fromstring(bytes[3::stride_len], 'u1') * 256 + np.fromstring(bytes[7::stride_len], 'u1')
    else:
        # accumulated view data using 32-bit counters
        data00 = np.fromstring(bytes[0::stride_len], 'u1') * 16777216 + np.fromstring(bytes[4::stride_len],'u1') * 65536 + \
                 np.fromstring(bytes[8::stride_len], 'u1') * 256 + np.fromstring(bytes[12::stride_len], 'u1')
        data01 = np.fromstring(bytes[1::stride_len], 'u1') * 16777216 + np.fromstring(bytes[5::stride_len], 'u1') * 65536 + \
                 np.fromstring(bytes[9::stride_len], 'u1') * 256 + np.fromstring(bytes[13::stride_len], 'u1')
        data10 = np.fromstring(bytes[2::stride_len], 'u1') * 16777216 + np.fromstring(bytes[6::stride_len], 'u1') * 65536 + \
                 np.fromstring(bytes[10::stride_len], 'u1') * 256 + np.fromstring(bytes[14::stride_len], 'u1')
        data11 = np.fromstring(bytes[3::stride_len], 'u1') * 16777216 + np.fromstring(bytes[7::stride_len], 'u1') * 65536 + \
                 np.fromstring(bytes[11::stride_len], 'u1') * 256 + np.fromstring(bytes[15::stride_len], 'u1')
    return np.array([data00, data01, data10, data11])


def findPreamble(byteStr, startPtr, stride, preamble=0xfedc):
    # [rc 090917] Routine to scan through a single datalane for the location of the preamble pattern
    #             used to detect when the DM_Buffer inserts a corrupt word
    # step through byteStr starting at startPtr and grouping bytes into width defined by stride.
    # compare for preamble pattern and return new position of view start.
    dataLen = len(byteStr)
    #print "dataLen = ", dataLen
    #print "startPtr = ", startPtr
    #print "stride=", stride
    #print "preamble= 0x%x" % preamble
    if (stride == 2):
        dataWord = sum(ord(byteStr[startPtr + j]) << (i * 8) for i, j in [(1, 0), (0, 1)])
    else:
        dataWord = sum(ord(byteStr[startPtr + j]) << (i * 8) for i, j in [(3, 0), (2, 1), (1, 2), (0, 3)])
    while (startPtr < dataLen) and (dataWord <> preamble):
        startPtr += 1
        if (stride == 2):
            dataWord = sum(ord(byteStr[startPtr + j]) << (i * 8) for i, j in [(1, 0), (0, 1)])
        else:
            dataWord = sum(ord(byteStr[startPtr + j]) << (i * 8) for i, j in [(3, 0), (2, 1), (1, 2), (0, 3)])
    if (startPtr == dataLen):
        logger.error( "...findPreamble failed....")
        exit()
    return startPtr


def parse_bin2sum(data_buf, accumData=1, Ncounters=13, asicPosition=1):
    #
    # walks through buffer dump binary data, accumulates 13 counters for each pixel across (N_view-1)
    # ... if not accum mode, we skip the first view (may have errors from DM_Buffer output routine)
    # ... build 'counters' list of lists containing:
    # counters[pixel#] = [ row, col, viewCount, EC_tot, CC5_tot, CC4_tot, CC3_tot, CC2_tot, CC1_tot, CC0_tot, \
    #                                                 SEC5_tot, SEC4_tot, SEC3_tot, SEC2_tot, SEC1_tot, SEC0_tot]
    #
    # if accumData<>0, binary data contains accumulated views, each counter is now 32-bits instead of 16-bit
    # ...modify counter length from 2 bytes to 4 bytes
    debug = 0
    rowIndex = 0
    colIndex = 1
    viewNumIndex = 2
    counterListInt = range(3, Ncounters + 3)
    counters = [[0 for i in xrange(Ncounters + 3)] for j in xrange(864)]
    # create four lists of 12*36*13 counter locations for summing
    # ... each list handles an output stream (1/2 of an ASIC each)
    # ... results of accumulation for complete ASIC requires pairing
    #     two lists
    #
    counts = np.array([np.array([0] * 432 * Ncounters)] * 4)
    if (accumData == 0):
        vc_len = 4  # 4 bytes ViewCount
        preamble_len = 8  # 8 bytes Preamble header
        stride_len = 8  # 16-bit counters
    else:
        vc_len = 16  # 16 bytes ViewCount
        preamble_len = 16  # 16 bytes preamble header
        stride_len = 16  # 32-bit counters
    frameDataLen = 12 * Ncounters * 36 * stride_len  # 12 rows * 13 counters * 36 columns
    hdr_len = vc_len + preamble_len
    rdPtr = 0
    bytes = data_buf[rdPtr:rdPtr + hdr_len]  # read 4 bytes ViewCount + 8 bytes header
    rdPtr += hdr_len
    dataLen = len(data_buf)
    if debug:
        logger.debug("length(data_buf)=" + str(dataLen))
        logger.debug("vc_len=" + str(vc_len))
        logger.debug("preamble_len=" + str(preamble_len))
        logger.debug("stride_len=" + str(stride_len))
        logger.debug("hdr_len=" + str(hdr_len))
        logger.debug("frameDataLen=" + str(frameDataLen))
    processedViews = 0
    while rdPtr < dataLen:
        # Decode header bytes.
        viewCount = sum(ord(bytes[j]) << (i * 8) for i, j in enumerate(range(vc_len - 4, vc_len)))
        if preamble_len == 8:
            data0 = sum(ord(bytes[vc_len + j]) << (i * 8) for i, j in [(1, 0), (0, 4)])  # ASIC 0 lane 0
            data1 = sum(ord(bytes[vc_len + 1 + j]) << (i * 8) for i, j in [(1, 0), (0, 4)])  # ASIC 0 lane 1
            data2 = sum(ord(bytes[vc_len + 2 + j]) << (i * 8) for i, j in [(1, 0), (0, 4)])  # ASIC 1 lane 0
            data3 = sum(ord(bytes[vc_len + 3 + j]) << (i * 8) for i, j in [(1, 0), (0, 4)])  # ASIC 1 lane 1
        else:
            data0 = sum(ord(bytes[vc_len + j]) << (i * 8) for i, j in [(3, 0), (2, 4), (1, 8), (0, 12)])  # ASIC 0 lane 0
            data1 = sum(ord(bytes[vc_len + 1 + j]) << (i * 8) for i, j in [(3, 0), (2, 4), (1, 8), (0, 12)])  # ASIC 0 lane 1
            data2 = sum(ord(bytes[vc_len + 2 + j]) << (i * 8) for i, j in [(3, 0), (2, 4), (1, 8), (0, 12)])  # ASIC 1 lane 0
            data3 = sum(ord(bytes[vc_len + 3 + j]) << (i * 8) for i, j in [(3, 0), (2, 4), (1, 8), (0, 12)])  # ASIC 1 lane 1
        if ((data0 == 0xfedc and data1 == 0xfedc) or (data2 == 0xfedc and data3 == 0xfedc)):
            # found header, process the view frame
            bytes = data_buf[rdPtr:rdPtr + frameDataLen]
            rdPtr += frameDataLen
            if ((viewCount > 0) or (stride_len == 16)):
                processedViews += 1
                counts += bytes2dec(bytes, Ncounters, stride_len)
        bytes = data_buf[rdPtr:rdPtr + hdr_len]  # get next view frame start
        rdPtr += hdr_len
    # build 'counters' data structure:
    for col in range(0, 36):
        for ctr in counterListInt:
            for row in range(0, 12):
                ctrIndex = (col * Ncounters * 12) + (ctr - 3) * 12 + row
                if (row % 2 == 0):
                    pixel0 = col + 36 * row
                else:
                    pixel0 = (35 - col) + 36 * row
                if ((23 - row) % 2 == 0):
                    pixel1 = col + 36 * (23 - row)
                else:
                    pixel1 = (35 - col) + 36 * (23 - row)
                counters[pixel0][rowIndex] = row
                counters[pixel0][colIndex] = col
                counters[pixel1][rowIndex] = 23 - row
                counters[pixel1][colIndex] = col
                if asicPosition == 0:
                    counters[pixel0][ctr] = counts[0][ctrIndex]  # ASIC 0
                elif asicPosition == 1:
                    counters[pixel0][ctr] = counts[2][ctrIndex]  # ASIC 1
                if asicPosition == 0:
                    counters[pixel1][ctr] = counts[1][ctrIndex]  # ASIC 0
                if asicPosition == 1:
                    counters[pixel1][ctr] = counts[3][ctrIndex]  # ASIC 1
                # viewCount is updated each view
                # ....last time through has (total_num_views-1)
                counters[pixel0][viewNumIndex] = processedViews
                counters[pixel1][viewNumIndex] = processedViews
    return counters


def parse_bin2csv(csvfPath, data_bufList, timeResolution, activeAsicNums, accumData=0, Ncounters=13, commonHdr='',
                  commonArgVals='', perAsicHdr='', perAsicArgs='', perAsicPerPixHdr='', perAsicPerPixArgVals=''):
    # walks through buffer dump binary data, accumulates counters for each pixel
    # ... outputs to csv file a 'counters' list containing:
    # [pixel#, row, col, viewCount, EC_tot, CC5_tot, CC4_tot, CC3_tot, CC2_tot, CC1_tot, CC0_tot, SEC5_tot, SEC4_tot, SEC3_tot, SEC2_tot, SEC1_tot, SEC0_tot]
    # if accumData<>0, binary data contains accumulated views, each counter is now 32-bits instead of 16-bit
    # ...modify counter length from 2 bytes to 4 bytes
    checkDir(csvfPath)
    fw = open(csvfPath, "w")
    CCounters = 'CC5,CC4,CC3,CC2,CC1,CC0,'
    SECounters = 'SEC5,SEC4,SEC3,SEC2,SEC1,SEC0'
    header = 'Pixel,elapsedTime(s),'
    for asic0, asic1, dmm in zip(range(0, 16, 2), range(1, 17, 2), range(0, 8)):
        data_buf = data_bufList[dmm]
        if data_buf == '[]' or data_buf == []:
            continue

        for i in commonHdr:
            header += '%s, ' % i
        for i in perAsicHdr:
            header += '%s, ' % i
        for i in perAsicPerPixHdr:
            header += '%s, ' % i
        header += 'Row,Column,ViewCount,'
        if (Ncounters == 13):
            header += ' EC,' + CCounters + SECounters + ',SumCC'
        elif (Ncounters == 12):
            header += CCounters + SECounters + ',SumCC'
        elif (Ncounters == 7):
            header = ' EC,' + CCounters + ',SumCC'
        else:
            header = CCounters + ',SumCC'
        fw.write('%s\n' % header)
        rowIndex = 0
        colIndex = 1
        viewNumIndex = 2
        counterListInt = range(3, Ncounters + 3)
        countersA0 = [[0 for i in xrange(Ncounters + 3)] for j in xrange(864)]
        countersA1 = [[0 for i in xrange(Ncounters + 3)] for j in xrange(864)]
        # create four lists of 12*36*13 counter locations for summing
        # ... each list handles an output stream (1/2 of an ASIC each)
        # ... results of accumulation for complete ASIC requires pairing
        #     two lists
        if (accumData == 0):
            vc_len = 4              # 4 bytes ViewCount
            preamble_len = 8        # 8 bytes Preamble header
            stride_len = 8          # 16-bit counters
        else:
            vc_len = 16             # 16 bytes ViewCount
            preamble_len = 16       # 16 bytes preamble header
            stride_len = 16         # 32-bit counters
        #
        frameDataLen = 12 * Ncounters * 36 * stride_len  # 12 rows * 13 counters * 36 columns
        hdr_len = vc_len + preamble_len
        rdPtr = 0
        bytes = data_buf[rdPtr:rdPtr + hdr_len]  # read 4 bytes ViewCount + 8 bytes header
        rdPtr += hdr_len
        dataLen = len(data_buf)
        while rdPtr < dataLen:
            # Decode header bytes.
            viewCount = sum(ord(bytes[j]) << (i * 8) for i, j in enumerate(range(vc_len - 4, vc_len)))
            if viewCount > 0:
                elapsedTime = viewCount * timeResolution
                if preamble_len == 8:
                    data0 = sum(ord(bytes[vc_len + j]) << (i * 8) for i, j in [(1, 0), (0, 4)])      # ASIC 0 lane 0
                    data1 = sum(ord(bytes[vc_len + 1 + j]) << (i * 8) for i, j in [(1, 0), (0, 4)])  # ASIC 0 lane 1
                    data2 = sum(ord(bytes[vc_len + 2 + j]) << (i * 8) for i, j in [(1, 0), (0, 4)])  # ASIC 1 lane 0
                    data3 = sum(ord(bytes[vc_len + 3 + j]) << (i * 8) for i, j in [(1, 0), (0, 4)])  # ASIC 1 lane 1
                else:
                    data0 = sum(ord(bytes[vc_len + j]) << (i * 8) for i, j in [(3, 0), (2, 4), (1, 8), (0, 12)])      # ASIC 0 lane 0
                    data1 = sum(ord(bytes[vc_len + 1 + j]) << (i * 8) for i, j in [(3, 0), (2, 4), (1, 8), (0, 12)])  # ASIC 0 lane 1
                    data2 = sum(ord(bytes[vc_len + 2 + j]) << (i * 8) for i, j in [(3, 0), (2, 4), (1, 8), (0, 12)])  # ASIC 1 lane 0
                    data3 = sum(ord(bytes[vc_len + 3 + j]) << (i * 8) for i, j in [(3, 0), (2, 4), (1, 8), (0, 12)])  # ASIC 1 lane 1
                # look in all 4 data lanes for 2 containing pre-amble 0xfedc
                if ((data0 == 0xfedc and data1 == 0xfedc) or (data2 == 0xfedc and data3 == 0xfedc)):
                    bytes = data_buf[rdPtr:rdPtr + frameDataLen]  # found header, process the view frame
                    rdPtr += frameDataLen
                    counts = bytes2dec(bytes, Ncounters, stride_len)
                    for col in range(0, 36):  # build 'counters' data structure:
                        for ctr in counterListInt:
                            for row in range(0, 12):
                                ctrIndex = (col * Ncounters * 12) + (ctr - 3) * 12 + row
                                if (row % 2 == 0):
                                    pixel0 = col + 36 * row
                                else:
                                    pixel0 = (35 - col) + 36 * row
                                if ((23 - row) % 2 == 0):
                                    pixel1 = col + 36 * (23 - row)
                                else:
                                    pixel1 = (35 - col) + 36 * (23 - row)
                                countersA0[pixel0][rowIndex] = row
                                countersA0[pixel0][colIndex] = col
                                countersA0[pixel0][ctr] = counts[0][ctrIndex]
                                countersA0[pixel1][rowIndex] = 23 - row
                                countersA0[pixel1][colIndex] = col
                                countersA0[pixel1][ctr] = counts[1][ctrIndex]
                                countersA0[pixel0][viewNumIndex] = viewCount
                                countersA0[pixel1][viewNumIndex] = viewCount
                                countersA1[pixel0][rowIndex] = row
                                countersA1[pixel0][colIndex] = col
                                countersA1[pixel0][ctr] = counts[2][ctrIndex]
                                countersA1[pixel1][rowIndex] = 23 - row
                                countersA1[pixel1][colIndex] = col
                                countersA1[pixel1][ctr] = counts[3][ctrIndex]
                                countersA1[pixel0][viewNumIndex] = viewCount
                                countersA1[pixel1][viewNumIndex] = viewCount
                    for pixel in range(864):  # write current view to file
                        if asic0 in activeAsicNums:
                            rowDataA0 = "%d,%3.5f," % (pixel, elapsedTime)
                            for commonArg in commonArgVals:
                                rowDataA0 += '%s,' % commonArg
                            for perAsicArg in perAsicArgs:
                                rowDataA0 += '%s,' % perAsicArg[asic0]
                            for perAsicPerPixArg in perAsicPerPixArgVals:
                                rowDataA0 += '%s,' % perAsicPerPixArg[asic0][pixel]
                        if asic1 in activeAsicNums:
                            rowDataA1 = "%d,%3.5f," % (pixel, elapsedTime)
                            for commonArg in commonArgVals:
                                rowDataA1 += '%s,' % commonArg
                            for perAsicArg in perAsicArgs:
                                rowDataA1 += '%s,' % perAsicArg[asic1]
                            for perAsicPerPixArg in perAsicPerPixArgVals:
                                rowDataA1 += '%s,' % perAsicPerPixArg[asic1][pixel]
                        sumCCA0 = 0
                        sumCCA1 = 0
                        for pos in range(0, Ncounters + 3):
                            if pos == (Ncounters + 2):
                                if asic0 in activeAsicNums:
                                    rowDataA0 += "%d,%d\n" % (countersA0[pixel][pos], sumCCA0)
                                if asic1 in activeAsicNums:
                                    rowDataA1 += "%d,%d\n" % (countersA1[pixel][pos], sumCCA1)
                            else:
                                if asic0 in activeAsicNums:
                                    rowDataA0 += "%d," % countersA0[pixel][pos]
                                if asic1 in activeAsicNums:
                                    rowDataA1 += "%d," % countersA1[pixel][pos]
                                if pos >= (3 + Ncounters % 2) and pos <= (8 + Ncounters % 2):
                                    sumCCA0 += countersA0[pixel][pos]
                                    sumCCA1 += countersA1[pixel][pos]
                        if asic0 in activeAsicNums:
                            fw.write(rowDataA0)
                        if asic1 in activeAsicNums:
                            fw.write(rowDataA1)
            bytes = data_buf[rdPtr:rdPtr + hdr_len]  # get next view frame start
            rdPtr += hdr_len
    fw.close()
    return


def genVtrigs(viewPeriod=400, NumTriggers=10):
    logger.debug('[genVtrigs]...Reset VTRIG Module')
    set_VTRIG_reset(1)  # Reset VTRIG Module
    set_VTRIG_reset(0)
    logger.debug('[genVtrigs]...enable DM_adapter data lane config')
    set_DATA_in_select(0)  # Enable the DM_Adapter data lane config
    set_DATA_out_select('10')
    logger.debug('[genVtrigs]...disable Data Bypass...')
    set_DATA_bypass(0)
    # Set the VTRIG generator with sufficient a period to cover the view duration
    # consisting of the autozero time and other overhead. The DM Buffer requires an
    # extra 24 clock cycles at 35MHz to cover preamble detection and byte alignment.
    # The VTRIG period is in usec.
    set_VTRIG_periods(viewPeriod + 10)
    set_VTRIG_num_cycles(NumTriggers)
    # read View Trigger Count and Period registers before test
    # -- Trigger count is = (trig-cycles - 1)
    # -- Trigger Period is equal to "period" in 140MHz cycles
    logger.debug('[genVtrigs]...read View Trigger Count and Period registers before Triggers applied...')
    for i in ['0x00010','0x00011'] :
            rdata = SPI_read(i)
            logger.debug("DM_REG %s = %s" % (i, rdata))
    set_VTRIG_enable(1)  # Enable the VTRIG generator.
    set_DATA_test_delay(NumTriggers, viewPeriod)  # Wait for the specified number of VTRIG cycles.
    set_VTRIG_enable(0)  # Disable the VTRIG generator.
    # read View Trigger Count and Period registers again after test
    # -- Trigger count is = (trig-cycles - 1)
    # -- Trigger Period is equal to "period" in 140MHz cycles
    logger.debug('[genVtrigs]...read View Trigger Count and Period registers after Triggers applied...')
    for i in ['0x00010','0x00011']:
            rdata = SPI_read(i)  # 0x00010[12:0] = view_trig_count
            logger.debug("DM_REG %s = %s" % (i, rdata))
    logger.debug('[genVtrigs]...done...')


def ddrCal(activeAsicNums, passiveAsicNums):
    ddrCalPass = True
    logger.info('Performing DDR calibration')
    logger.debug('enable CRC and bypass LSFR for test patterns')
    writeReg('0x1e', 15, 0, '0x8001', 1, 1)                  # SPI_write('0x0201e', '0x8001')
    logger.debug('enable datalane pattern, column counter')
    writeReg('0x2b', 15, 0, '0x3d3d', 1, 1)                     # SPI_write('0x0202b', '0x3d3d')
    asicDMMPatternEnable(activeAsicNums, passiveAsicNums)       # enable preamble on unused ASIC slot
    set_DATA_preamble("0xFEDC")
    crcErrStat = chkCrcError()                                  # Check and clear CRC errors
    logger.debug('0x00a1d crc error before cal = %s' % crcErrStat)
    for asic in activeAsicNums:
        writeReg('0x00a05', asic, asic, '0x0000')               # clear LVDS cal mask for each ASIC
    logger.debug('0x00a05 = %s' % readReg('0x00a05', 0))
    logger.debug('0x00a06 = %s' % readReg('0x00a06', 0))
    logger.debug('0x00a07 = %s' % readReg('0x00a07', 0))
    for asic in activeAsicNums:
        writeReg('0x00a05', asic, asic, '0x0001')                # set LVDS cal mask for each ASIC
    logger.debug('0x00a05 = %s' % readReg('0x00a05', 0))
    logger.debug('0x00a06 = %s' % readReg('0x00a06', 0))
    logger.debug('0x00a07 = %s' % readReg('0x00a07', 0))
    genVtrigs(NumTriggers=200)
    crcErrStat = chkCrcError()                                  # Check and clear CRC errors
    logger.debug('0x00a1d crc error after cal = %s' % crcErrStat)
    statusBits = int(readReg('0x00a06'), 0)                      # ASIC Receive LVDS Calibration Done Status
    logger.debug('statusBits= %s, calErrStatusBits= %s' % (readReg('0x00a06'), readReg('0x00a07')))
    for asic in activeAsicNums:
        if not is_set(statusBits, asic):
            logger.error('LVDS Calibration Status bits did not get set for ASIC%s' % asic)
            ddrCalPass = False
    if ddrCalPass:
        logger.info('DDR calibration for DMM completed successfully')
    else:
        logger.error('DDR calibration for DMM failed')
    logger.debug('disable datalane pattern, column counter')
    writeReg('0x2b', 15, 0, '0x0000', 1, 1)                     # SPI_write('0x0202b', '0x0000')
    logger.debug('enable CRC...disable bypassing LSFR for test patterns')
    writeReg('0x1e', 15, 0, '0x0000', 1, 1)                     # SPI_write('0x0201e', '0x0000') new state as of Nov 2018 per TI 1->0
    return ddrCalPass


def collect_data(view_period=1000, N_views=10, N_AccumViews=0, N_counters=13, Azero_period=16):
    if hasattr(globals.buffer, 'config_capture'):
        globals.buffer.config_capture(N_views, view_period, Azero_period, N_AccumViews, N_counters)
        globals.buffer.capture()
        return str(globals.buffer.dump(parse=False))
    else:
        asicCols = 36  # define some output data length constants
        asicRows = 12
        N_checkBlockBytes = 2 * asicRows
        N_rowBlocks = asicCols * N_counters
        N_CRCbytes = 2
        # logger.info('Capture in progress')
        set_DATA_reset(1)  # Reset DATA Module and VTRIG Module
        set_DATA_reset(0)
        set_VTRIG_reset(1)
        set_VTRIG_reset(0)
        # Setup DATA module with expected frame length in bytes and the preamble.
        # The preamble length is fixed at 2 bytes. The CRC length is in bytes.
        frame_length = (N_checkBlockBytes + N_CRCbytes) * N_rowBlocks
        crc_length = N_CRCbytes * N_rowBlocks
        set_DATA_preamble('0xFEDC')
        set_DATA_frame_length(frame_length)
        set_DATA_crc_length(crc_length)
        # Enable the DM_Adapter data lane config
        set_DATA_in_select(0)
        set_DATA_out_select('10')
        # Set the VTRIG generator period
        set_VTRIG_periods(view_period + Azero_period)
        # Set Accumulation Mode. This must occur before the check_VTRIG_num_cycles()
        # function.
        # In Accumulation Mode the number of maximum number of views to accumulate is
        # limited to a 16-bit number (65535). After the specified number of views are
        # accumulated, a single Accumulated View is stored in buffer memory. The number
        # of VTRIG cycles is a product of the number of views to accumulate specified
        # with the set_DATA_accum_num() function and the desired number of accumulated
        # views to be saved in buffer memory.
        if (N_AccumViews > 0):
            set_DATA_accum_mode(1)
            set_DATA_field_offset(2)
            N_views += 1  # Edit by LL Apr. 07, 2017 to add an extra accumulated view because the first must be discarded
            vtrig_cycles = N_views * N_AccumViews
            set_DATA_accum_num(N_AccumViews)
        else:
            set_DATA_accum_mode(0)
            vtrig_cycles = N_views
        set_VTRIG_num_cycles(vtrig_cycles)
        # Check that the number of views (vtrig_cycles) fits in the available
        # buffer memory. The check_VTRIG_num_cycles() returns the number of VTRIG
        # cycles with valid data and the number of VTRIG cycles with extra overhead
        # VTRIG cycles. The function automatically updates the value previously set by
        # set_VTRIG_num_cycles() if there was an inconsistency. The returned values
        # may be used instead of manually specifying the number of VTRIG cycles in
        # later test functions.
        (vcycles, vcycles_w_overhead) = check_VTRIG_num_cycles()
        if (vcycles != vtrig_cycles):
            logger.warn("*** Data storage insufficient for" + str(vtrig_cycles) + "view cycles")
            logger.warn("*** Number of Views has been adjusted to" + str(vcycles))
        # Enable the DATA module and VTRIG generator. The VTRIG enable should be
        # after the DATA enable.
        # readPerPixCal('\\\\qn3\Public\\RedlenB\\', 5 , 'post_926_misc_chain.csv')
        clear_DATA_crc_err(1)
        clear_DATA_crc_err(0)
        set_DATA_enable(1)
        set_VTRIG_enable(1)
        # Wait before retrieving the data for the specified 'vcycles_w_overhead' VTRIG cycles.
        # The variable 'view_period' is the VTRIG period. The set_DATA_test_delay function
        # perform consistency checking to ensure the number of cycles is not greater than the
        # number specified.
        set_DATA_test_delay(vcycles_w_overhead, view_period)
        # Disable the DATA module and VTRIG generator.
        set_DATA_enable(0)
        set_VTRIG_enable(0)
        # readPerPixCal('\\\\qn3\Public\\RedlenB\\', 5, 'post_942_misc_chain.csv')
        # read View Trigger Count and Period registers again after test
        # -- Trigger count is = (trig-cycles - 1)
        # -- Trigger Period is equal to 'period' in 140MHz cycles
        tcdata = SPI_read('0x00010')
        tpdata = SPI_read('0x00011')
        # check if view_period exceeds max count value for trigger period register
        # ... period reg wraps around, determine number of wraps to add to residual value from register
        tpdata = (view_period * 140) & 0xFFFF0000 + int(tpdata, 0)
        # Read the DM Buffer memory and return as binary
        data_buf = cStringIO.StringIO()
        dump_buffer(data_buf)
        rx_data = data_buf.getvalue()
        data_buf.close()
        logger.debug(
            'CRC Check:\nCRC Errors Lane 0: %s\nCRC Errors Lane 1: %s\nCRC Errors Lane 2: %s\nCRC Errors Lane 3: %s' % (
            read_DATA_crc_err_count(0), read_DATA_crc_err_count(1), read_DATA_crc_err_count(2),
            read_DATA_crc_err_count(3)))
        logger.debug('Capture finished')
        crcErrStat = chkCrcError()  # Check and clear CRC errors
        logger.debug('0x00a1d crc error in collect data = %s' % crcErrStat)
        return (rx_data)


def calSCurve(tdir, asicPosition, addCap=0, thresh=128, iso=1, vstep=0.005, xtalkExp=0, arbitraryV=0, vmin=0.0, vmax=0.25, view_period=1000, N_views=10, N_AccumViews=0, N_counters=13, Azero_period=4, vdft0=0.0, DMType=None):
    logger.info('Performing S-Curve')
    scurveStartTime = time()
    # S-CURVE GENERATION
    # ...sweep DFT1 Vin and capture counts for given threshold

    writeReg('0x18', 12, 12, '0x0000', 1, 1)  # threshold_scan OFF
    vmid = float(thresh) / 256 * 0.25
    if not arbitraryV:
        if xtalkExp:
            vmin = 0.0
            vmax = 0.2
            vstep = 0.0025
        else:
            if addCap:
                vmin = 0
                vmax = 0.050
            else:
                vmin = max((vmid - 0.09), 0)
                vmax = vmid + 0.05
    rawCurves = []
    data_raw = []
    vinArray = []

    dacSet0(vdft0, DMType=DMType)
    dtime = time()
    for vin in np.arange(vmin, vmax + vstep, vstep):
        dacSet(round(vin + vdft0, 6), DMType=DMType)  # DFT<1> voltage corresponding to Vin
        data_raw.append(collect_data(view_period=view_period, N_views=N_views, N_counters=N_counters))
        vinArray.append(vin)
    ptime = time()
    for i, data in enumerate(data_raw):
        rawCurves.append([vinArray[i], parse_bin2sum(data, accumData=0, asicPosition=asicPosition, Ncounters=N_counters)])
    return (vinArray, rawCurves)


def calZCurve(tdir, asicPosition, iso=1, vin=0.1, th_step=1):
    logger.info('Performing Z-Curve')
    # Z-CURVE GENERATION
    # ...fix DFT1 Vin and capture counts as TH0 swept
    #
    if (iso):
        writeReg('0x7e', 13, 13, '0x0001', 1, 1)  # iso_panel on
    else:
        writeReg('0x7e', 13, 13, '0x0000', 1, 1)  # iso_panel off
    writeReg('0x18', 12, 12, '0x0000', 1, 1)  # threshold_scan OFF
    dacSet(vin)  # DFT<1> voltage corresponding to Vin
    rawCurves = []
    thArray = []
    if vin >= 0.05:
        maxThresh = 64
        th_step = 2
    else:
        maxThresh = 48
    for thresh in np.arange(0, maxThresh, th_step):
        logger.debug("...setting threshold to " + str(thresh))
        perPixThresh(tdir, th0=thresh, th1=255, th2=255, th3=255, th4=255, th5=255, from_csv=0)
        data_raw = collect_data()
        rawCurves.append([thresh, parse_bin2sum(data_raw, asicPosition=asicPosition)])
        thArray.append(thresh)
    return (thArray, rawCurves)


def calZCurve5(tdir, asicPosition, iso=1, vin=0.128, th_start=16, th_step=32, DMType=None):
    logger.info('Performing Z-Curve for adcCal2')
    # Z-CURVE GENERATION
    # ...fix DFT1 Vin and capture counts as TH5 swept
    #
    dacSet(vin, DMType=DMType)  # DFT<1> voltage corresponding to Vin
    rawCurves = []
    thArray = []
    for thresh in np.arange(th_start, 255, th_step):
        logger.debug("...setting threshold to " + str(thresh))
        perPixThresh(tdir, th0=0, th1=0, th2=0, th3=0, th4=0, th5=thresh, from_csv=0)
        data_raw = collect_data()
        rawCurves.append([thresh, parse_bin2sum(data_raw, accumData=0, asicPosition=asicPosition)])
        thArray.append(thresh)
    return (thArray, rawCurves)


def SCurvetoCSV(csvfPath, curveData, xtalkExp=0, dftFreq=0, tubeCurrent=0, shieldedCols=0, dmm=False, asic=False, pix_list=range(864), N_counters=13):
    fw = open(csvfPath, "w")
    if N_counters == 13:
        CCounters = 'CC5, CC4, CC3, CC2, CC1, CC0,'
        SECounters = 'SEC5, SEC4, SEC3, SEC2, SEC1, SEC0'
        if xtalkExp == 0:
            header = 'Vin, Pixel, Row, Column, ViewCount, EC,' + CCounters + SECounters + ', SumCC'
        elif xtalkExp == 1:
            header = 'Vin, Pixel, Row, Column, ViewCount, EC,' + CCounters + SECounters + ', SumCC, DFTfreq, xrayTubeCurrent, shieldedCols'
        fw.write('%s\n' % header)
        for vin in curveData:
            counters = vin[1]
            for pixel in pix_list:
                rowData = "%f," % vin[0]
                rowData += "%d," % pixel
                sumCC = 0
                for pos in range(0, 16):
                    if pos == 15:
                        if xtalkExp == 0:
                            if dmm:
                                rowData += "%d, %d\n" % (counters[asic][pixel][pos], sumCC)
                            else:
                                rowData += "%d, %d\n" % (counters[pixel][pos], sumCC)
                        elif xtalkExp == 1:
                            if dmm:
                                rowData += "%d, %d, %d, %2.1f, %s\n" % (counters[asic][pixel][pos], sumCC, dftFreq, tubeCurrent, shieldedCols)
                            else:
                                rowData += "%d, %d, %d, %2.1f, %s\n" % (counters[pixel][pos], sumCC, dftFreq, tubeCurrent, shieldedCols)
                    else:
                        if dmm:
                            rowData += "%d," % counters[asic][pixel][pos]
                        else:
                            rowData += "%d," % counters[pixel][pos]
                        if pos >= 4 and pos <= 9:
                            if dmm:
                                sumCC += counters[asic][pixel][pos]
                            else:
                                sumCC += counters[pixel][pos]
                fw.write(rowData)
    elif N_counters == 7:
        header = 'Vin, Pixel, Row, Column, EC, CC5, CC4, CC3, CC2, CC1, CC0'
        fw.write('%s\n' % header)
        for vin in curveData:
            counters = vin[1]
            for pixel in pix_list:
                rowData = "%f," % vin[0]
                rowData += "%d," % pixel
                rowData += "%d," % (counters[pixel][0])
                rowData += "%d," % (counters[pixel][1])
                rowData += "%d," % (counters[pixel][3])
                rowData += "%d," % (counters[pixel][4])
                rowData += "%d," % (counters[pixel][5])
                rowData += "%d," % (counters[pixel][6])
                rowData += "%d," % (counters[pixel][7])
                rowData += "%d," % (counters[pixel][8])
                rowData += "%d\n" % (counters[pixel][9])
                fw.write(rowData)

    elif N_counters == 1:
        header = 'Vin, Pixel, Row, Column, EC'
        fw.write('%s\n' % header)
        for vin in curveData:
            counters = vin[1]
            for pixel in pix_list:
                rowData = "%f," % vin[0]
                rowData += "%d," % pixel
                rowData += "%d," % (counters[pixel][0])
                rowData += "%d," % (counters[pixel][1])
                rowData += "%d\n" % (counters[pixel][3])
                fw.write(rowData)

    fw.close()
    return


def ZCurvetoCSV(csvfPath, curveData):
    with open(csvfPath, "w") as fw:
        CCounters = 'CC5, CC4, CC3, CC2, CC1, CC0,'
        SECounters = 'SEC5, SEC4, SEC3, SEC2, SEC1, SEC0'
        header = 'Thresh[4:0], Pixel, Row, Column, ViewCount, EC,' + CCounters + SECounters
        fw.write('%s\n' % header)
        for threshIn in curveData:
            counters = threshIn[1]
            for pixel in range(864):
                rowData = "%f," % threshIn[0]
                rowData += "%d," % pixel
                for pos in range(0, 16):
                    if pos == 15:
                        rowData += "%d\n" % (counters[pixel][pos])
                    else:
                        rowData += "%d," % counters[pixel][pos]
                fw.write(rowData)
    return


def loadElectricalCal(tdir, sensorId, calFile=''):
    logger.info(calFile)

    if not os.path.isfile(calFile):
        raise IOError('Could not find a calibration file for current settings')

    VER_ID = 5
    create_misc_chain_prog(tdir, VER_ID, pix_list=range(globals.numPixels), sel_pix=[0] * globals.numPixels,
                           prog_mode=3, sourceFile=calFile)
    [adcCalibId, qTrigCalibId, addCapCalibId, dtCalibId] = ['Chain', 'Chain', 'Chain', 'Chain']
    for x in range(3):
        logger.info('Attempt %d to write Electrical Calibration to ASIC' % (x+1))
        perPixCal(tdir, VER_ID)
        if validateElecCalib(tdir, VER_ID, progFile=calFile):
            logger.info('Successfully loaded calibration from deviceSpecificCals for %s' % sensorId)
            break

    return ['None', 'None', 'None', 'None']


def load_energy_cal_csv(path, sensor_id):
    cols = (1, 2) if 'A0' in sensor_id else (3, 4)
    cvrt = {1: float, 2: float} if 'A0' in sensor_id else {3: float, 4: float}

    with open(path, 'r') as ecalFh:
        gain, offset = np.loadtxt(ecalFh, delimiter=',', skiprows=1, usecols=cols, unpack=True, converters=cvrt)

    return gain, offset


def loadEnergyCalFromFile(tdir, filename, sensorId, pix_list, desEnergy0, desEnergy1, desEnergy2,
                          desEnergy3, desEnergy4, desEnergy5, program=True):
    # requires a "deviceSpecificCals\<sensorId>\energy" directory to exist
    # with a file called energyCal.csv in that folder.
    # The program will load the energy cal based on the sensor A0 or A1 from the energyCal.csv file
    logger.info('loadEnergyCalFromFile')
    energyCalSourceFile = os.path.join(tdir, globals.calsDir, sensorId, globals.nrgCalDir, filename)
    logger.info('Energy Calibration Data will be read from: %s' % energyCalSourceFile)
    auVals = []
    headerRow = 'Row,Col,Pixel,Th0,Th1,Th2,Th3,Th4,Th5'
    desEnergy = [desEnergy0, desEnergy1, desEnergy2, desEnergy3, desEnergy4, desEnergy5]
    gain_list, offset_list = load_energy_cal_csv(energyCalSourceFile, sensorId)
    for i in pix_list:
        row, col = row_and_col(i)
        perPixList = [row, col, i] + [(min(max(0, int(round((float(e) - offset_list[i]) / gain_list[i]))), 255))
                                      for e in desEnergy]
        auVals.append(perPixList)
    with open(os.path.join(tdir, globals.docDir, "thresh_energy_cal.csv"), 'w') as calThFile:
        np.savetxt(calThFile, auVals, delimiter=',', comments='', header=headerRow, fmt='%d,%d,%d,%d,%d,%d,%d,%d,%d')
    if program:
        for x in range(3):
            logger.info('Attempt %d to write Energy Calibration to ASIC' % (x+1))
            perPixThresh(tdir, progFile="thresh_energy_cal.csv")
            if validatePerPixThresh(tdir, progFile="thresh_energy_cal.csv"):
                break

def validatePerPixThresh(dir, progFile="thresh_energy_cal.csv"):
    writeFile = os.path.join(dir, globals.docDir, progFile)
    readFile = readPerPixThresh(dir)
    match = compareChainFiles(writeFile, readFile)
    if match:
        logger.info('Energy Calibration write to ASIC verified')
    else:
        logger.error('Energy Calibration write to ASIC failed')
    return match


def validateElecCalib(dir, VER_ID, progFile="misc_chain_prog_vals_B.csv"):
    writeFile = os.path.join(dir, globals.docDir, progFile)
    readFile = readPerPixCal(dir, VER_ID)
    match = compareChainFiles(writeFile, readFile)
    if match:
        logger.info('Electrical Calibration write to ASIC verified')
    else:
        logger.error('Electrical Calibration write to ASIC failed')
    return match


def compareChainFiles(File1, File2):
    fmatch = True
    with open(File1, 'r') as f1:
        d1 = f1.readlines()
    with open(File2, 'r') as f2:
        d2 = f2.readlines()
    assert len(d1) == len(d2)
    for i in range(1, len(d1)):
        if len(d1[1].split(',')) != 23:
            if d1[i] == d2[i]:  # threshold chains
                continue
            else:
                fmatch = False
                diffLine = i
                break
        else:
            if d1[i].split(',')[13:] == d2[i].split(',')[13:]:  # calibration chains
                continue
            else:
                fmatch = False
                diffLine = i
                break
    if fmatch:
        logger.info('Files {}, {} Match'.format(File1, File2))
    else:
        logger.error('Files {}, {} Do Not Match at line {}'.format(File1, File2, diffLine))
    return fmatch


def generate_pix_list(start_col, col_gap, num_cols=36, num_rows=24):
    # Needs checks for values
    pix_list = []
    for col in range(start_col, num_cols, col_gap):
        pix_list += [col + num_cols * row if row % 2 == 0 else (num_cols - 1) - col + num_cols * row for row in range(0, num_rows)]
    return pix_list

def dnlScrub(spectraList, thresholdList, bin=1):
    # spectraList, thresholdList are a single spectra/threshold dataset
    # or a list of spectra/threshold datasets (single list not list of lists)
    # The counter value is the bin number of counter from which
    # the spectra are extracted e.g. bin = 1 for SEC1 or CC1
    dnlList = [x + bin for x in [30,62,94,126,158]]
    for i in range(len(spectraList)):
        if thresholdList[i] in dnlList:  # removing dnl using average of two points
            if spectraList[i] > 50:
                if all(y > 1.2 for y in [float(spectraList[i])/spectraList[i - 1],
                                         float(spectraList[i])/spectraList[i + 1]]):
                    spectraList[i] = (spectraList[i - 1] + spectraList[i + 1]) * 0.5
    return spectraList


def checkForEnergyCal(ldir, baseLine, filename):
    sensorIds = [baseLine.sensor['IDLIST'][i] for i in baseLine.sensor['ACTIVE_ASIC_NUMS']]
    energyCalExists = []
    for serialId in sensorIds:
        checkFile = os.path.join(ldir, globals.calsDir, serialId, globals.nrgDir, filename)
        energyCalExists.append(os.path.isfile(checkFile))
    return any(energyCalExists)


def most(vals, threshold=132, valRange=range(198, 230)):
    count = len([v for v in vals if v in valRange])
    return count >= threshold


def electrical_calib_heatmap(calFile, dstFolder):

    checkDir(dstFolder)

    mdata = pd.read_csv(calFile, low_memory=False, header=0, usecols=[0, 1, 2, 15, 16, 17, 18, 19, 20, 21, 22])
    row, col, pixel, acap, qtrig, lact, lpass, tdfe, tdbe, tdbe2, adc = mdata.T.values

    heatmap_acap = np.empty((24, 36))
    heatmap_acap[:] = np.nan

    heatmap_td_fe = np.empty((24, 36))
    heatmap_td_fe[:] = np.nan

    heatmap_td_be = np.empty((24, 36))
    heatmap_td_be[:] = np.nan

    heatmap_td_be2 = np.empty((24, 36))
    heatmap_td_be2[:] = np.nan

    heatmap_adc = np.empty((24, 36))
    heatmap_adc[:] = np.nan

    heatmap_qtrig = np.empty((24, 36))
    heatmap_qtrig[:] = np.nan

    heatmap_leakage_active = np.empty((24, 36))
    heatmap_leakage_active[:] = np.nan

    heatmap_leakage_passive = np.empty((24, 36))
    heatmap_leakage_passive[:] = np.nan

    acap_array = []
    fe_array = []
    be_array = []
    be2_array = []
    adc_array = []
    qtrig_array = []
    lkga_array = []
    lkgp_array = []

    col_array = []
    row_array = []
    pix_array = []

    for i in range(len(pixel)):
        acap_array.append(acap[i])
        fe_array.append(tdfe[i])
        be_array.append(tdbe[i])
        be2_array.append(tdbe2[i])
        adc_array.append(adc[i])
        qtrig_array.append(qtrig[i])
        lkga_array.append(lact[i])
        lkgp_array.append(lpass[i])
        col_array.append(int(col[i]))
        row_array.append(int(row[i]))
        pix_array.append(int(pixel[i]))

    for c in range(len(row_array)):
        heatmap_adc[row_array[c], col_array[c]] = adc_array[pix_array[c]]
        heatmap_acap[row_array[c], col_array[c]] = acap_array[pix_array[c]]
        heatmap_td_fe[row_array[c], col_array[c]] = fe_array[pix_array[c]]

        heatmap_td_be[row_array[c], col_array[c]] = be_array[pix_array[c]]
        heatmap_td_be2[row_array[c], col_array[c]] = be2_array[pix_array[c]]

        heatmap_qtrig[row_array[c], col_array[c]] = qtrig_array[pix_array[c]]
        heatmap_leakage_active[row_array[c], col_array[c]] = lkga_array[pix_array[c]]
        heatmap_leakage_passive[row_array[c], col_array[c]] = lkgp_array[pix_array[c]]

    fig1 = pyplot.figure(figsize=(20, 10))
    gs = gridspec.GridSpec(2, 2, width_ratios=[2, 1], height_ratios=[3, 1])
    pyplot.viridis()
    pyplot.subplot(gs[0])
    pyplot.pcolor(heatmap_acap, vmin=0, vmax=8)
    pyplot.colorbar()
    pyplot.subplot(gs[1])
    pyplot.hist(acap_array, 50, density=True, facecolor='g', alpha=0.75)
    pyplot.suptitle('Add Cap Map and Histogram')
    pyplot.subplots_adjust(wspace=0.05)
    fig1.savefig(os.path.join(dstFolder,'add_cap.png'))

    fig2 = pyplot.figure(figsize=(20, 10))
    gs = gridspec.GridSpec(2, 2, width_ratios=[2, 1], height_ratios=[3, 1])
    pyplot.viridis()
    pyplot.subplot(gs[0])
    pyplot.pcolor(heatmap_adc, vmin=0, vmax=15)
    pyplot.colorbar()
    pyplot.subplot(gs[1])
    pyplot.hist(adc_array, 50, density=True, facecolor='g', alpha=0.75)
    pyplot.suptitle('Quantizer Offset Map and Histogram')
    pyplot.subplots_adjust(wspace=0.05)
    fig2.savefig(os.path.join(dstFolder,'quantizer_offset.png'))

    fig3 = pyplot.figure(figsize=(20, 10))
    gs = gridspec.GridSpec(2, 2, width_ratios=[2, 1], height_ratios=[3, 1])
    pyplot.viridis()
    pyplot.subplot(gs[0])
    pyplot.pcolor(heatmap_td_fe, vmin=200, vmax=240)
    pyplot.colorbar()
    pyplot.subplot(gs[1])
    pyplot.hist(fe_array, 50, density=True, facecolor='g', alpha=0.75)
    pyplot.suptitle('TD Trim Front End Map and Histogram')
    pyplot.subplots_adjust(wspace=0.05)
    fig3.savefig(os.path.join(dstFolder,'td_trim_fe.png'))

    fig4 = pyplot.figure(figsize=(20, 10))
    gs = gridspec.GridSpec(2, 2, width_ratios=[2, 1], height_ratios=[3, 1])
    pyplot.viridis()
    pyplot.subplot(gs[0])
    pyplot.pcolor(heatmap_td_be, vmin=0, vmax=100)
    pyplot.colorbar()
    pyplot.subplot(gs[1])
    pyplot.hist(be_array, 50, density=True, facecolor='g', alpha=0.75)
    pyplot.suptitle('TD Trim Back End Map and Histogram')
    pyplot.subplots_adjust(wspace=0.05)
    fig4.savefig(os.path.join(dstFolder,'td_trim_be.png'))

    fig5 = pyplot.figure(figsize=(20, 10))
    gs = gridspec.GridSpec(2, 2, width_ratios=[2, 1], height_ratios=[3, 1])
    pyplot.viridis()
    pyplot.subplot(gs[0])
    pyplot.pcolor(heatmap_td_be2, vmin=0, vmax=15)
    pyplot.colorbar()
    pyplot.subplot(gs[1])
    pyplot.hist(be2_array, 50, density=True, facecolor='g', alpha=0.75)
    pyplot.suptitle('ACSW Width Trim Map and Histogram')
    pyplot.subplots_adjust(wspace=0.05)
    fig5.savefig(os.path.join(dstFolder,'td_trim_be2.png'))

    fig6 = pyplot.figure(figsize=(20, 10))
    gs = gridspec.GridSpec(2, 2, width_ratios=[2, 1], height_ratios=[3, 1])
    pyplot.viridis()
    pyplot.subplot(gs[0])
    pyplot.pcolor(heatmap_qtrig, vmin=8, vmax=18)
    pyplot.colorbar()
    pyplot.subplot(gs[1])
    pyplot.hist(qtrig_array, 50, density=True, facecolor='g', alpha=0.75)
    pyplot.suptitle('Qtrig Map and Histogram')
    pyplot.subplots_adjust(wspace=0.05)
    fig6.savefig(os.path.join(dstFolder,'qtrig.png'))

    fig7 = pyplot.figure(figsize=(20, 10))
    gs = gridspec.GridSpec(2, 2, width_ratios=[2, 1], height_ratios=[3, 1])
    pyplot.viridis()
    pyplot.subplot(gs[0])
    pyplot.pcolor(heatmap_leakage_active, vmin=0, vmax=250)
    pyplot.colorbar()
    pyplot.subplot(gs[1])
    pyplot.hist(lkga_array, 50, density=True, facecolor='g', alpha=0.75)
    pyplot.suptitle('leakage active map and histogram')
    pyplot.subplots_adjust(wspace=0.05)
    fig7.savefig(os.path.join(dstFolder,'lkg_active.png'))

    pyplot.close('all')


def energy_calib_heatmap(calFile, dstFolder):

    checkDir(dstFolder)
    mdata = pd.read_csv(calFile, low_memory=False, header=0, usecols=[0, 1, 2, 3, 4])
    pixel, gain_asic_0, offset_asic_0, gain_asic_1, offset_asic_1 = mdata.T.values

    heatmap_gain_asic_0 = np.empty((24, 36))
    heatmap_gain_asic_0[:] = np.nan

    heatmap_gain_asic_1 = np.empty((24, 36))
    heatmap_gain_asic_1[:] = np.nan

    heatmap_offset_asic_0 = np.empty((24, 36))
    heatmap_offset_asic_0[:] = np.nan

    heatmap_offset_asic_1 = np.empty((24, 36))
    heatmap_offset_asic_1[:] = np.nan

    gain_asic_0_array = []
    gain_asic_1_array = []
    offset_asic_0_array = []
    offset_asic_1_array = []

    pix_array = []

    for i in range(len(pixel)):
        gain_asic_0_array.append(gain_asic_0[i])
        gain_asic_1_array.append(gain_asic_1[i])
        offset_asic_0_array.append(offset_asic_0[i])
        offset_asic_1_array.append(offset_asic_1[i])
        pix_array.append(int(pixel[i]))

    for row in range(24):
        for column in range(36):
            heatmap_gain_asic_0[row][column] = gain_asic_0_array[36 * row + column]
            heatmap_gain_asic_1[row][column] = gain_asic_1_array[36 * row + column]
            heatmap_offset_asic_0[row][column] = offset_asic_0_array[36 * row + column]
            heatmap_offset_asic_1[row][column] = offset_asic_1_array[36 * row + column]

    fig1 = pyplot.figure(figsize=(20, 10))
    gs = gridspec.GridSpec(2, 2, width_ratios=[2, 1], height_ratios=[3, 1])
    pyplot.viridis()
    pyplot.subplot(gs[0])
    pyplot.pcolor(heatmap_gain_asic_0, vmin=0, vmax=1.0)
    pyplot.colorbar()
    pyplot.subplot(gs[1])
    pyplot.hist(gain_asic_0_array, bins=None, density=None, facecolor='g', alpha=0.75)
    pyplot.suptitle('ASIC 0 Gain Map - keV/AU and Gain Distribution')
    pyplot.subplots_adjust(wspace=0.05)
    fig1.savefig(os.path.join(dstFolder,'asic_0_gain.png'))

    fig2 = pyplot.figure(figsize=(20, 10))
    gs = gridspec.GridSpec(2, 2, width_ratios=[2, 1], height_ratios=[3, 1])
    pyplot.viridis()
    pyplot.subplot(gs[0])
    pyplot.pcolor(heatmap_offset_asic_0, vmin=0, vmax=15)
    pyplot.colorbar()
    pyplot.subplot(gs[1])
    pyplot.hist(offset_asic_0_array, bins=None, density=None, facecolor='g', alpha=0.75)
    pyplot.suptitle('ASIC 0 Offset Map - keV and Offset Distribution')
    pyplot.subplots_adjust(wspace=0.05)
    fig2.savefig(os.path.join(dstFolder,'asic_0_offset.png'))

    fig3 = pyplot.figure(figsize=(20, 10))
    gs = gridspec.GridSpec(2, 2, width_ratios=[2, 1], height_ratios=[3, 1])
    pyplot.viridis()
    pyplot.subplot(gs[0])
    pyplot.pcolor(heatmap_gain_asic_1, vmin=0, vmax=1.0)
    pyplot.colorbar()
    pyplot.subplot(gs[1])
    pyplot.hist(gain_asic_1_array, bins=None, density=None, facecolor='g', alpha=0.75)
    pyplot.suptitle('ASIC 1 Gain Map - keV/AU and Gain Distribution')
    pyplot.subplots_adjust(wspace=0.05)
    fig3.savefig(os.path.join(dstFolder,'asic_1_gain.png'))

    fig4 = pyplot.figure(figsize=(20, 10))
    gs = gridspec.GridSpec(2, 2, width_ratios=[2, 1], height_ratios=[3, 1])
    pyplot.viridis()
    pyplot.subplot(gs[0])
    pyplot.pcolor(heatmap_offset_asic_1, vmin=0, vmax=15)
    pyplot.colorbar()
    pyplot.subplot(gs[1])
    pyplot.hist(offset_asic_1_array, bins=None, density=None, facecolor='g', alpha=0.75)
    pyplot.suptitle('ASIC 1 Offset Map - keV and Offset Distribution')
    pyplot.subplots_adjust(wspace=0.05)
    fig4.savefig(os.path.join(dstFolder,'asic_1_offset.png'))

    pyplot.close('all')
