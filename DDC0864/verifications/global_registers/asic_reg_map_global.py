########################################################################################################################
#
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
#
########################################################################################################################

import xml.etree.ElementTree
import numpy as np
from DDC0864.devInit import a2d
import logging
import globals
import os

logger = logging.getLogger(__name__)

TI_REF_REG_VALUE = os.path.join(globals.ROOT_DIR, 'reference_docs', 'ti_ddc0864', 'TI_global_regs_read_vals.csv')
TI_DDC0864_REG_MAP_DML = os.path.join(globals.ROOT_DIR, 'reference_docs', 'ti_ddc0864', 'uCurie_RevB_customer_30Mar2018.dml')


class ASICRegMapGlobal:
    """
    Class to abstract the ASIC DDC0864 global register operations.
    References:
    1. DDC0864_DS_v5p5.pdf
    2. uCurie_RevB_customer_30Mar2018.dml

     This class will handle DDC0864 global registers.
     address: 8 bits
     data: 16bits
     Register id: 0 to 123, data length varies.

    """
    def __init__(self, dump_file_path, dump_file_type='Redlen',
                 dml_file_path=TI_DDC0864_REG_MAP_DML):
        self.dump_file_path = dump_file_path
        self.dump_file_type = dump_file_type
        self.dml_file_path = dml_file_path

    def compare(self, ref):
        """
        Operation on dumped ASIC file
        :param ref:
        :return:
        """
        dml = self.dml_file_path
        write_str = 'Address,CustData,RefData,RegNames,CustVals,RefVals\n'
        cust_data = self.deserialize_dump_file()
        ref_data = ref.deserialize_dump_file()
        ignore_addr_list = range(6, 14) + [64, 65, 66] + range(229, 256) + [68, 227, 228]
        e = xml.etree.ElementTree.parse(dml).getroot()
        x = e[2][0][0].findall('property')
        compare_flag = True

        for addr in cust_data:
            if addr not in ignore_addr_list and cust_data[addr] != ref_data[addr]:
                compare_flag = False
                d1, d2 = a2d(cust_data[addr], 16), a2d(ref_data[addr], 16)
                diff_positions = [15 - i for i in range(16) if d1[i] != d2[i]]

                reg_ids = []
                for i in range(len(x)):
                    if len(x[i][0]) > 0:
                        if int(x[i][0][0].attrib['address']) == int(addr):
                            reg_ids += [x[i].attrib['id']]

                reg_names, cust_vals, ref_vals = [], [], []

                for reg_id in reg_ids:
                    prop_ranges = e[2].find('.//property[@id="%d"]' % int(reg_id)).findall('.//range')
                    pos_in_btw = [False]
                    cust_val, ref_val = 0, 0
                    for prop_range in prop_ranges:
                        msb, lsb, address = int(prop_range.attrib['msb']), int(prop_range.attrib['lsb']), int(
                            prop_range.attrib['address'])
                        cust_val *= 2 ** (msb + 1 - lsb)
                        cust_val += (cust_data[address] / 2 ** lsb) % 2 ** (msb + 1 - lsb)
                        ref_val *= 2 ** (msb + 1 - lsb)
                        ref_val += (ref_data[address] / 2 ** lsb) % 2 ** (msb + 1 - lsb)
                        pos_in_btw += [lsb <= _ <= msb for _ in diff_positions]
                    if any(pos_in_btw):
                        reg_name = e[3].find('.//property[@propertyId="%d"]' % int(reg_id)).attrib['name']
                        # reg_names += ['%s:%s' %(reg_id,reg_name)]
                        reg_names += ['%s' % reg_name]
                        cust_vals += ['%s' % cust_val]
                        ref_vals += ['%s' % ref_val]

                write_str += '%s,%s,%s,%s,%s,%s\n' % (
                    addr, cust_data[addr], ref_data[addr], ' ; '.join(reg_names), ' ; '.join(cust_vals),
                    ' ; '.join(ref_vals))
        print(write_str)
        return compare_flag, write_str

    def deserialize_dump_file(self):
        """
        deserialize instance's dump file to a {addr: value} dictionary

        """
        try:
            dump_data = np.genfromtxt(self.dump_file_path, dtype=np.int, skip_header=1, delimiter=',')
            if self.dump_file_type == 'Redlen':
                dump_data = {i[2]: i[3] for i in dump_data}
            elif self.dump_file_type == 'TI':
                dump_data = {i[0]: i[1] for i in dump_data}
        except IOError as e:
            print("File I/O error({0}): {1}".format(e.errno, e.strerror))
            raise Exception('deserialize_dump_file file access error, ' + e.message)
        return dump_data

    def parse_dml(self):
        e = xml.etree.ElementTree.parse(self.dml_file_path).getroot()
        x = e[2][0][0].findall('property')
        return x

    def list_reg_id(self):

        ignore_addr_list = range(6, 14) + [64, 65, 66] + range(229, 256) + [68, 227, 228]
        reg_addr = self.deserialize_dump_file()
        reg_ids = []
        dml_data = self.parse_dml()
        for addr in reg_addr:
            if addr not in ignore_addr_list:
                for i in range(len(dml_data)):
                    if len(dml_data[i][0]) > 0:
                        if int(dml_data[i][0][0].attrib['address']) == int(addr):
                            reg_ids += [dml_data[i].attrib['id']]
        reg_ids.sort(key=int)
        return reg_ids

    def get_reg_info_by_id(self, reg_id):
        try:
            dml = self.dml_file_path
            cust_data = self.deserialize_dump_file()
            e = xml.etree.ElementTree.parse(dml).getroot()
            prop_items = e[2].find('.//property[@id="%d"]' % int(reg_id))
            prop_ranges = []
            if prop_items is not None:
                prop_ranges = prop_items.findall('.//range')
            cust_val, ref_val = 0, 0
            for prop_range in prop_ranges:
                msb, lsb, address = int(prop_range.attrib['msb']), int(prop_range.attrib['lsb']), int(
                    prop_range.attrib['address'])
                cust_val *= 2 ** (msb + 1 - lsb)
                cust_val += (cust_data[address] / 2 ** lsb) % 2 ** (msb + 1 - lsb)
        except Exception as e:
            print("register ID is not found.")
            raise Exception('register ID is not found, ' + e.message)
        return cust_val

