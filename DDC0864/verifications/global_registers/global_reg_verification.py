#######################################################################################################################
#
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
#
#######################################################################################################################

import argparse
from asic_reg_map_global import TI_REF_REG_VALUE
from DDC0864.dmFunctions import readAsicGlobals, maskAsicList
import logging
import time
import numpy as np
import globals

logger = logging.getLogger(__name__)


"""
Module to compare the data in the file dumped from DUT with data from TI's reference file

References:
1. DDC0864_DS_v5p5.pdf
2. uCurie_RevB_customer_30Mar2018.dml

output:
At the same folder as <path to dumped reg>, ending dumped file with "_cmp.csv"
"""


def compare_global_regs(dumped_file, ref_file):
    """ compare dumped register data file with TI reference
        usage: compare_global_regs(<path to dumpedFile>, [optional],path to TI ref file>)

        returns: a diff file(ignoring addr in the ignore_addr_list) at the same folder as <path to dumpedFile>,
        ended with _cmp.csv.
    """
    try:
        # deserialize the data files into dictionaries
        dumped_data = np.genfromtxt(dumped_file, dtype=None, skip_header=1, delimiter=',')
        dumped_data = {i[2]: i[3] for i in dumped_data}
        logger.info(dumped_data)
        ref_data = np.genfromtxt(ref_file, dtype=None, skip_header=1, delimiter=',')
        ref_data = {i[0]: i[1] for i in ref_data}
        logger.info(ref_data)

        # define the flag of comparing results
        cmp_flag = True
        # open a result file
        cmp_file = dumped_file.replace('.csv', '_cmp.csv')
        cmp_file_writer = open(cmp_file, "w+")
        write_str = 'Address,DumpedData\n'

        ignore_addr_list = range(6, 14) + [64, 65, 66] + range(229, 256) + [68, 227, 228]

        # iterate the addr in dumped register data to compare with the value of the same address in reference data file,
        # except that contains in "ignore_add_list"
        for addr in dumped_data:
            if addr not in ignore_addr_list and dumped_data[addr] != ref_data[addr]:
                cmp_flag = False
                value = dumped_data[addr]
                diff_item = str(addr) + "," + str(value) + "\n"
                write_str += diff_item
        cmp_file_writer.write(write_str)
        cmp_file_writer.close()
    except IOError as e:
        logger.error("Error: can't find file or read data" + e.message)
        raise('file io error' + e.message)
    except Exception as e:
        logger.error("Unknown Error" + e.message)
        raise('Unknown Error' + e.message)
    return cmp_flag


def verify_multi_ddc0864_global_registers(active_asic_nums=[6, 7], dm_type='Hitachi'):
    passive_asic_nums = []
    pass_flag = True
    dump_dir = globals.DUMP_DIR
    for asic in active_asic_nums:
        maskAsicList(active_asic_nums, passive_asic_nums, maskAllOthersOff=1, asicSingle=asic)
        if not verify_asic_global_registers(asic, dump_dir):
            pass_flag = False
    # Enable all active ASICs
    maskAsicList(active_asic_nums, passive_asic_nums, disableTestPattern=1)
    return pass_flag


def verify_asic_global_registers(asic, dump_dir):
    """ verify a single ASIC's registers
    :arguments:
    asic: int, the number of a single asic
    dump_dir: the place to store the dumped and compared register files
    returns: True | False
    """
    pass_flag = True

    time_str = time.strftime("%Y%m%d-%H%M%S")
    reg_dump_file = readAsicGlobals(dump_dir, 'Asic-{0}-Reg-'.format(asic) + time_str)
    if compare_global_regs(reg_dump_file, TI_REF_REG_VALUE):
        logger.info("Dumped register from ASIC {} is identical to TI ref. ".format(asic))
    else:
        logger.warn("Dumped register from ASIC {} is NOT identical to TI ref. ".format(asic))
        pass_flag = False
    return pass_flag
