########################################################################################################################
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
########################################################################################################################

from six.moves import StringIO

import os
import logging

import globals

from DDC0864.devInit import a2d, row_and_col
from DDC0864.dmFunctions import writeReg, waitForBit, checkDir

from pcct.utils import chunk_str, pack_u16_be, unpack_u16_be


logger = logging.getLogger(__name__)


def setThreshChains(mode, chains='0x0fff'):
    if mode.lower() == 'enable':
        logger.debug('enabling threshold chains: ' + chains )
        writeReg('0x3e', 11, 0, chains, 1, 1)
    else:
        logger.debug('disabling threshold chains')
        writeReg('0x3e', 11, 0, '0x0000', 1, 1)


def perPixThresh(tdir, pix_list=range(864), msb=11, th0=40, th1=80, th2=120, th3=160, th4=200, th5=240,
                 writeFile="thresh_chain_write_vals.csv", progFile="thresh_chain_prog_vals.csv", from_csv=1, verify=False):
    logger.debug('Performing per pixel threshold chain writes')
    writeFilePath = os.path.join(tdir, globals.docDir, writeFile)
    if from_csv == 0:
        # logger.info('Threshold Chain Data will be written into file: %s' % writeFilePath)
        with open(writeFilePath, 'w') as fh:
            fh.write("Row,Col,Pixel,Th0,Th1,Th2,Th3,Th4,Th5\n")
            thresholds = [th0, th1, th2, th3, th4, th5]
            setThreshChains('enable')
            # sleep(0.01)
            newstream = gen_thresh_stream(len(pix_list), thresholds)
            slice_length = len(newstream) / (msb + 1)
            newstream_slice = newstream[:slice_length]
            sendPerPixThresh(newstream_slice)
            parallel_thresh_to_csv(fh, newstream)
    else:
        data_stream = thresh_from_csv(tdir=tdir, progFile=progFile)
        # with open(os.path.join(tdir, r"docs\thresh_from_csv_wr.csv"), 'w') as fw:
        with open(writeFilePath, 'w') as fw:
            fw.write("Row,Col,Pixel,Th0,Th1,Th2,Th3,Th4,Th5\n")
            for chain in range(msb + 1):
                logger.debug("Writing threshold chain: %d..." % chain)
                setThreshChains('enable', chains='0x{:04x}'.format(0x0001 << chain))  # Enable Threshold Chain
                # sleep(0.01)
                sendPerPixThresh(data_stream[(11 - chain) * 72 * 48:(11 - chain + 1) * 72 * 48])
                thresh_to_csv(fw, data_stream[(11 - chain) * 72 * 48:(11 - chain + 1) * 72 * 48], chain)
    setThreshChains('disable')  # Disable All Threshold Chains

    verified = False
    readBackPath = ""
    if globals.threshVerify:
        readBackFile = "thresh_chain_readback.csv"
        # readBackPath = os.path.join(tdir, "docs", "thresh_chain_readback.csv")
        readBackPath = readPerPixThresh(tdir, threshReadValsFile=readBackFile)

        verified = compareChainFiles(writeFilePath, readBackPath)
            # raise RuntimeError("Read back file does not match programming file")

    return writeFilePath, readBackPath, verified


def readPerPixThresh(tdir, threshReadValsFile="thresh_chain_read_vals.csv", startChain=0, endChain=11, threshReadValsProgFile="thresh_chain_prog_vals.csv", writeToProgFile=0, sensorId=None):
    logger.debug('Reading per pixel threshold chain values back')
    fhl = []
    if sensorId:
        threshReadValsFile = os.path.join(tdir, globals.docDir, '{}_{}.csv'.format(threshReadValsFile[:-4], sensorId))
    else:
        threshReadValsFile = os.path.join(tdir, globals.docDir, threshReadValsFile)
    checkDir(threshReadValsFile)
    rfh = open(threshReadValsFile, 'w')
    fhl.append(rfh)
    if writeToProgFile:
        threshReadValsProgFile = os.path.join(tdir, globals.docDir, threshReadValsProgFile)
        checkDir(threshReadValsProgFile)
        wpfh = open(threshReadValsProgFile, 'w')
        fhl.append(wpfh)
    for fh in fhl:
        fh.write("Row,Col,Pixel,Th0,Th1,Th2,Th3,Th4,Th5\n")
    for chain_num in range(startChain, endChain + 1):
        logger.debug('Reading back threshold chain: %d' % chain_num)
        writeReg('0x3e', 11, 0, '0x%05x' % (0x0001 << chain_num), 1, 1)  # Enable Thresh Chain# 'chain_num'
        # sleep(0.001)
        thresh_stream = receivePerPixelThresh()
        for fh in fhl:
            thresh_to_csv(fh, thresh_stream, chain_num)
    for fh in fhl[:]:
        fh.close()
    writeReg('0x3e', 11, 0, '0x0000', 1, 1)  # Disable all Thresh Chains
    # rfh.close()
    return threshReadValsFile


def sendPerPixThresh(stream):
    no_of_reg_writes = int(len(stream) / 16)

    writes = StringIO()
    for word in reversed(chunk_str(stream, chunk_len=16)):
        writes.write(pack_u16_be(int(word, base=2)))

    globals.buffer.write_chain(globals.detector.ASIC_THRESH_CHAIN_BASE, writes.getvalue())
    writeThreshChain()


def receivePerPixelThresh():
    stream = ""
    num_pixels = 72
    num_reads = num_pixels * 3
    writeReg('0x0000e', 10, 10, '0x0000')  # Set THRESHOLD_CHAIN_READ low
    writeReg('0x0000e', 10, 10, '0x0001')  # Set THRESHOLD_CHAIN_READ high
    waitForBit('0x0000e', 8, True)

    reads = globals.buffer.read_chain(globals.detector.ASIC_THRESH_CHAIN_BASE, num_reads)

    for word in reversed(chunk_str(reads, chunk_len=2)):
        stream += a2d(unpack_u16_be(word), 16)

    return stream


def writeThreshChain():
    writeReg('0x0000e', 11, 11, '0x0000')
    writeReg('0x0000e', 11, 11, '0x0001')
    waitForBit('0x0000e', 9, True)


def basic_thresh_streams(thresholds):
    return a2d(thresholds[-1], 8) + a2d(thresholds[-2], 8) + a2d(thresholds[-3], 8) + a2d(thresholds[-4], 8) + a2d(thresholds[-5], 8) + a2d(thresholds[-6], 8)


def gen_thresh_stream(num_pixels, thresholds):
    return basic_thresh_streams(thresholds) * num_pixels


def parallel_thresh_to_csv(fh, stream, threshold_chars=48):
    pixels = int(len(stream) / threshold_chars)
    for pixel in range(pixels):
        (row, col) = row_and_col(pixel)
        b = stream[pixel * threshold_chars:(pixel + 1) * threshold_chars]
        writeStr = '%d,%d,%d,' % (row, col, pixel)
        writeStr += '%d,' % int(b[40:48], base=2)  # Th0
        writeStr += '%d,' % int(b[32:40], base=2)  # Th1
        writeStr += '%d,' % int(b[24:32], base=2)  # Th2
        writeStr += '%d,' % int(b[16:24], base=2)  # Th3
        writeStr += '%d,' % int(b[8:16], base=2)  # Th4
        writeStr += '%d\n' % int(b[0:8], base=2)  # Th5
        fh.write(writeStr)


def thresh_to_csv(fh, stream, chain_num):
    no_of_pix_writes = int(len(stream) / 48)
    writeStr = ""
    for chunk48 in reversed(range(no_of_pix_writes)):
        b = stream[chunk48 * 48:(chunk48 + 1) * 48]
        pixel = 72 * chain_num + 71 - chunk48
        (row, col) = row_and_col(pixel)
        writeStr = '%d,%d,%d,' % (row, col, pixel)
        writeStr = writeStr + '%d,' % int(b[40:48], base=2)  # Th5
        writeStr = writeStr + '%d,' % int(b[32:40], base=2)  # Th4
        writeStr = writeStr + '%d,' % int(b[24:32], base=2)  # Th3
        writeStr = writeStr + '%d,' % int(b[16:24], base=2)  # Th2
        writeStr = writeStr + '%d,' % int(b[8:16], base=2)  # Th1
        writeStr = writeStr + '%d\n' % int(b[0:8], base=2)  # Th0
        fh.write(writeStr)


def create_thresh_chain_prog(tdir, sourceFile = "thresh_chain_write_vals.csv", progFile = "thresh_chain_prog_vals.csv",
                             pix_list=[],th0=[],th1=[],th2=[],th3=[],th4=[],th5=[]):
    """Modifies existing progFile, updates only select fields of pixels in pix_list."""
    sourceFile = os.path.join(tdir, globals.docDir, sourceFile)
    progFile = os.path.join(tdir, globals.docDir, progFile)
    # logger.info("Source file: %s" % sourceFile)
    try:
        sourceFh = open(sourceFile, 'r')
    except IOError:
        logger.error('File %s was not found.' % sourceFile)
        return
    else:
        pix_list = list(pix_list)
        lenPixList = len(pix_list)
        th0 = [th0]*lenPixList if type(th0) == int else th0
        th1 = [th1]*lenPixList if type(th1) == int else th1
        th2 = [th2]*lenPixList if type(th2) == int else th2
        th3 = [th3]*lenPixList if type(th3) == int else th3
        th4 = [th4]*lenPixList if type(th4) == int else th4
        th5 = [th5]*lenPixList if type(th5) == int else th5
        thList = [th0, th1, th2, th3, th4, th5]
        i = 0
        for th in thList:
            if th != [] and len(th) != lenPixList:
                raise Exception('Th%s list length should not = 1' % i)
            i += 1
        source_content = sourceFh.readlines()
        sourceFh.close()
        writeString = source_content[0]
        source_content = source_content[1:]
        for i in range(len(source_content)):
            row = source_content[i].replace('\n', '').split(',')
            if i in pix_list:
                row[3] = '%d' % th0[i] if len(th0) == lenPixList else row[3]
                row[4] = '%d' % th1[i] if len(th1) == lenPixList else row[4]
                row[5] = '%d' % th2[i] if len(th2) == lenPixList else row[5]
                row[6] = '%d' % th3[i] if len(th3) == lenPixList else row[6]
                row[7] = '%d' % th4[i] if len(th4) == lenPixList else row[7]
                row[8] = '%d' % th5[i] if len(th5) == lenPixList else row[8]
            writeString += ','.join(row)
            writeString += '\n'
        with open(progFile, 'w') as progFh:
            progFh.write(writeString)
    return


def thresh_from_csv(tdir, progFile, numPixels=864):
    """Converts thresholds from csv file into binary stream"""
    progFileFull = os.path.join(tdir, globals.docDir, progFile)
    # logger.info("Reading from progFile = " + str(progFileFull))
    try:
        with open(progFileFull, 'r') as fh:
            content = fh.readlines()
            if len(content) != (numPixels + 1): # +1 for the column headers
                raise Exception("%s has incompatible number of pixel thresholds. Expected=%d, actual=%d" % (progFile, numPixels+1, len(content)))
            data_stream = ""
            # for pix_index
            for reversed_index in range(numPixels, 0, -1):
                threshold_list = content[reversed_index].replace('\n', '').split(',')
                data_stream += basic_thresh_streams(threshold_list)
            return data_stream
    except IOError:
        raise Exception('File %s was not found.' % progFileFull)


def compareChainFiles(File1, File2):
    fmatch = True
    with open(File1, 'r') as f1:
        d1 = f1.readlines()
    with open(File2, 'r') as f2:
        d2 = f2.readlines()
    assert len(d1) == len(d2)
    for i in range(len(d1)):
        if d1[i] == d2[i]:
            continue
        else:
            fmatch = False
            diffLine = i
            break
    if fmatch:
        logger.info('Files {}, {} Match'.format(File1, File2))
    else:
        logger.error('Files {}, {} Do Not Match at line {}'.format(File1, File2, diffLine))
    return fmatch
