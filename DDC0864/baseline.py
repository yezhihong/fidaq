########################################################################################################################
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
########################################################################################################################

from collections import OrderedDict

import globals


class baseline:
    gainDict = OrderedDict([('stdgain', {'a': {'gain': 0.80, 'thresHolds': [20, 50, 70, 90, 120, 150]},
                                         'b': {'gain': 0.95, 'thresHolds': [16, 56, 89, 123, 145, 190]}}),
                            ('midgain', {'a': {'gain': 0.68},
                                         'b': {'gain': 0.725, 'offset': 2}}),
                            ('higain', {'a': {'gain': 0.56},
                                        'b': {'gain': 0.65}})
                ])
    sourceDict = OrderedDict([('Americium', {'Code': 'am241', 'Voltage': [0], 'Current': [0]}),
                              ('Cobalt', {'Code': 'co57', 'Voltage': [0], 'Current': [0]}),
                              ('Copper 1mm', {'Code': 'xray-1mmCu-K-edge', 'Voltage': [120], 'Current': [2]}),
                              ('Lead K-Edge', {'Code': 'xray-Pb-K-edge', 'Voltage': [140], 'Current': [2]}),
                              ('Cerium K-Edge', {'Code': 'xray-CeO2-K-edge', 'Voltage': [140], 'Current': [2]}),
                              ('Other', {'Code': 'Other', 'Voltage': [0], 'Current': [0]}),
                              ('Other With X-ray', {'Code': 'xray-Other', 'Voltage': [0], 'Current': [0]})
                              ])

    experimentDict = OrderedDict([('Energy Calibration', 'energycalibration'),
                                  ('Uniformity', 'testitemid22_23_24_28crp_uniformity_background-ecal'),
                                  ('1min Stability', 'testitemid30shorttermstability-ecal'),
                                  ('1s Stability', 'testitemid30adynamicresponse-ecal'),
                                  ('Threshold Sweep', 'testitemid20energyresolution-closedbin'),
                                  ('Uniformity Uncalibrated', 'testitemid22_23_24_28crp_uniformity_background'),
                                  ('1min Stability Uncalibrated', 'testitemid30shorttermstability'),
                                  ('1s Stability Uncalibrated', 'testitemid30adynamicresponse'),
                                  ('60cm Uniformity', 'testitemid22_23_24_28crp_uniformity_background_dm-ecal'),
                                  ('60cm 1min Stability', 'testitemid30shorttermstability_dm-ecal'),
                                  ('60cm 1s Stability', 'testitemid30adynamicresponse_dm-ecal'),
                                  ('60cm Uniformity Copper', 'testitemid22_23_24_28crp_uniformity_background_cu_60cm-ecal'),
                                  ('45cm 1s Stability', 'testitemid30adynamicresponse_45cm-ecal'),
                                  ('Copper Spectrum', 'copper_sweep'),
                                  ('Americium Spectrum', 'americium_sweep'),
                                  ])

    def __init__(self, DMType, moduleIDs=None, cztId='na', siteName='RedlenB', bl='c', testType=globals.testTypeDict['DM']):

        self.testType = testType
        self.params = {}
        self.sensor = {}
        self.dm = None
        self.dm_info = {}
        self.site = {}
        self.calibs = OrderedDict()
        self.experiment = {}
        self.timestamp = ''
        self.baseLineDict = {
            'a': {
                'deadTimeExpList': [1.6e-08],
                'qtrigExpList': [16],
                'modeList': ['active2_nom'],
                'hpfList': [5],
                'acsModeList': [0],
                'acsWindowExpList': [['w', 'w']],
                'add_cap': ([0.0045] * 16),
                'dftClk': 6.125e6,
                'adcThresh': 128,
                'active2TC': 90e-9,
                'acswWidth': 5e-9
            },
            'b': {
                'deadTimeExpList': [2.0e-08],
                'qtrigExpList': [16],
                'modeList': ['active2_nom'],
                'hpfList': [10],
                'acsModeList': [0],
                'acsWindowExpList': [['w', 'w']],
                'add_cap': [0.0045] * 16,
                'dftClk': 6.125e6,
                'adcThresh': 128,
                'active2TC': 90e-9,
                'acswWidth': 5e-9
            },
            'c': {
                'deadTimeExpList': [1.6e-08],
                'qtrigExpList': [16],
                'modeList': [globals.lkgActHigh],
                'hpfList': [5],
                'acsModeList': [0],
                'acsWindowExpList': [['w', 'w']],
                #'acsWindowExpList': [['2w', '2w']], ##change from 'w' to '2w', XL and ZY, 09/04/2019
                'add_cap': [0.0045] * 16,
                'dftClk': 1e6,
                'adcThresh': 128,
                'active2TC': 90e-9,
                'acswWidth': 5e-9
            },
            'd': {
                'deadTimeExpList': [20e-09],
                'qtrigExpList': [16],
                'modeList': ['active2_high'],
                'hpfList': [5],
                'acsModeList': [0],
                'acsWindowExpList': [['w', 'w']],
                'add_cap': [0.0045] * 16,
                'dftClk': 1e6,
                'adcThresh': 128,
                'active2TC': 90e-9,
                'acswWidth': 5e-9
            },
            'uncalibrated': {
                'deadTimeExpList': [1.6e-08],
                'qtrigExpList': [16],
                'modeList': ['active2_nom'],
                'hpfList': [5],
                'acsModeList': [0],
                'acsWindowExpList': [['w', 'w']],
                'add_cap': [0.0045] * 16,
                'dftClk': 6.125e6,
                'adcThresh': 128,
                'active2TC': 90e-9,
                'acswWidth': 5e-9
            }
        }
        # define parameter dictionary
        self.params['DM'] = self.dm_info
        self.params['SENSOR'] = self.sensor
        self.params['SITE'] = self.site
        self.params['CALIBS'] = self.calibs
        self.params['EXPERIMENT'] = self.experiment

        self.dm_info['SERIAL_NUM'] = ''
        self.dm_info['MODULES'] = moduleIDs
        self.dm_info['DM_TYPE'] = DMType
        self.dm_info['MM_TYPE'] = DMType.MODULE_TYPE
        self.dm_info['ASICS_PER_MM'] = DMType.N_ASICS_PER_MODULE
        self.dm_info['ACTIVE_MMS'] = []
        self.dm_info['CUSTOMER'] = DMType.CUSTOMER

        sensorIdList = []
        for module in moduleIDs:
            if module:
                # if testType != globals.testTypeDict['Sensor']:
                sensorIdList += ['{}-A{}'.format(module, i) for i in range(DMType.N_ASICS_PER_MODULE)]
                # else:
                # sensorIdList += ['{}'.format(module) for _ in range(DMInfo[DMType]['asicsPerMM'])]
            else:
                sensorIdList += ['' for _ in range(DMType.N_ASICS_PER_MODULE)]

        self.sensor['REVISION']          = 1
        self.sensor['CZTID']             = cztId
        self.sensor['IDLIST']            = sensorIdList   # [False] * 16
        # self.sensor['ASICSPASSIVE']      = [False] * 16
        # self.sensor['ASICSACTIVE']       = [False] * 16
        # self.sensor['ASICPSNLIST']       = [0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1]
        self.sensor['ASICREVISION']      = 5    # [-1 for _ in sensorIdList]

        self.sensor['ACTIVE_ASIC_NUMS'] = [asicNum for asicNum, sensorId in enumerate(sensorIdList) if sensorId]
        self.sensor['PASSIVE_ASIC_NUMS'] = [] # if testType != globals.testTypeDict['Sensor'] else [14] # Needed to active only one side of MM for sensor screening


        # Site - unique text description of test site
        self.site['NAME']                = siteName
        self.site['REVISION']            = 1
        self.site['DEPENDENCY']          = False
        # Revisions
        self.calibs['ADC']               = 1
        self.calibs['COARSE_QTRIG']      = 1
        self.calibs['ADDCAP']            = 1
        self.calibs['DEADTIME']          = 1
        self.calibs['COARSE_ENERGY_CAL'] = 1
        self.calibs['QDUMP']             = 1
        self.calibs['FINE_QTRIG']        = 1
        self.calibs['FINE_BIN_THRESH']   = 1
        self.experiment['NAME'] = 'energycalibration'
        # 'am241', 'co57', 'xray', 'xray-Pb-K-edge', 'xray-CeO2-K-edge'
        self.experiment['SOURCE'] = 'am241'
        self.experiment['BASELINE'] = bl
        self.experiment['GAIN'] = 'midgain'          #stdgain, midgain or higain
        self.experiment['FORCE_RECALIBRATION'] = []  # if desired, add a list of desired recalibrations ie. ['ADC', 'QTRIG', 'ADDCAP', 'DEADTIME'] todo not currently functional
        self.experiment['EXPERIMENT_LIST'] = []
        self.experiment['REASON'] = ''
        totTime, timeResolution, startThresh, endThresh = self.getSpectrumParams([self.sensor['ASICREVISION']][0],      # Remove the brackets when we record per asic revision
                                                                                 self.experiment['SOURCE'],
                                                                                 self.experiment['GAIN'])
        self.experimentDict = {
            'testitemid20energyresolution-singlebin':
                {'test_type': 'SPECTRUM',
                 'photon_source': 'xray',
                 'energy_cal': False,
                 'hvList': [1000],
                 'threshStep': 1,
                 'thresholds': 'N/A',
                 'max_view_period': 1000,
                 'hvStabTime': 0,
                 'tubeVoltageList': [140] if 'K-edge' in self.experiment['SOURCE'] else 'N/A',
                 'tubeCurrentList': [2.0] if 'K-edge' in self.experiment['SOURCE'] else 'N/A',
                 'distance': 30 if 'K-edge' in self.experiment['SOURCE'] else 1.5,
                 'filter_type': 'N/A',
                 'filter_thickness': 'N/A',
                 'xRayStabTime': 10,
                 'totTime': totTime,
                 'timeResolution': timeResolution,
                 'startThresh': startThresh,
                 'endThresh': endThresh
                 },
            'testitemid20energyresolution-closedbin':
                {'test_type': 'SPECTRUM',
                 'photon_source': 'xray',
                 'energy_cal': False,
                 'hvList': [1000],
                 'threshStep': 1,
                 'thresholds': 'N/A',
                 'max_view_period': 1000,
                 'hvStabTime': 0,
                 'tubeVoltageList': [140] if 'K-edge' in self.experiment['SOURCE'] else 'N/A',
                 'tubeCurrentList': [2.0] if 'K-edge' in self.experiment['SOURCE'] else 'N/A',
                 'distance': 30 if 'K-edge' in self.experiment['SOURCE'] else 1.5,
                 'filter_type': 'N/A',
                 'filter_thickness': 'N/A',
                 'xRayStabTime': 10,
                 'totTime': totTime,
                 'timeResolution': timeResolution,
                 'startThresh': startThresh,
                 'endThresh': endThresh
                 },
            'energycalibration':
                {'test_type': 'ENERGY_CAL',
                 'photon_source': 'xray',
                 'energy_cal': False,
                 'hvList': [1000],
                 'threshStep': 1,
                 'thresholds': 'N/A',
                 'max_view_period': 1000,
                 'hvStabTime': 0,
                 'tubeVoltageList': [140],
                 'tubeCurrentList': [2.0],
                 'distance': 30,
                 'filter_type': 'N/A',
                 'filter_thickness': 'N/A',
                 'xRayStabTime': 10,
                 'totTime': totTime,
                 'timeResolution': timeResolution,
                 'startThresh': startThresh,
                 'endThresh': endThresh
                 },
            'testitemid22_23_24_28crp_uniformity_background-ecal':
                {'test_type': 'UNIFORMITY',
                 'photon_source': 'xray',
                 'energy_cal': True,
                 'hvList': [1000],
                 'threshStep': 'N/A',
                 'thresholds': [16, 30, 50, 70, 90, 120],
                 'max_view_period': 1000,
                 'hvStabTime': 0,
                 'tubeVoltageList': [120],
                 'tubeCurrentList': [0, 0.4, 0.6, 0.8, 1, 1.5, 3, 7, 10, 15, 20, 25],
                 'distance': 30,
                 'filter_type': 'Cu',
                 'filter_thickness': 1,
                 'xRayStabTime': 10,
                 'totTime': 1,
                 'timeResolution': 0.25,
                 'startThresh': 'N/A',
                 'endThresh': 'N/A'
                 },
            'testitemid22_23_24_28crp_uniformity_background_cu_60cm-ecal':
                {'test_type': 'UNIFORMITY',
                 'photon_source': 'xray',
                 'energy_cal': True,
                 'hvList': [1000],
                 'threshStep': 'N/A',
                 'thresholds': [16, 30, 50, 70, 90, 120],
                 'max_view_period': 1000,
                 'hvStabTime': 0,
                 'tubeVoltageList': [120],
                 'tubeCurrentList': [0, 0.4, 0.6, 0.8, 1, 1.5, 3, 7, 10, 15, 20, 25],
                 'distance': 60,
                 'filter_type': 'Cu',
                 'filter_thickness': 1,
                 'xRayStabTime': 10,
                 'totTime': 1,
                 'timeResolution': 0.25,
                 'startThresh': 'N/A',
                 'endThresh': 'N/A'
                 },
            'testitemid22_23_24_28crp_uniformity_background_dm-ecal':
                {'test_type': 'UNIFORMITY',
                 'photon_source': 'xray',
                 'energy_cal': True,
                 'hvList': [1000],
                 'threshStep': 'N/A',
                 'thresholds': [16, 30, 50, 70, 90, 120],
                 'max_view_period': 1000,
                 'hvStabTime': 0,
                 'tubeVoltageList': [120],
                 'tubeCurrentList': [0, 0.37, 0.56, 0.76, 0.94, 1.41, 2.83, 6.59, 9.42, 14.12, 18.84, 23.55],
                 'distance': 60,
                 'filter_type': 'Cu',
                 'filter_thickness': 1,
                 'xRayStabTime': 10,
                 'totTime': 1,
                 'timeResolution': 0.25,
                 'startThresh': 'N/A',
                 'endThresh': 'N/A'
                 },
            'testitemid22_23_24_28crp_uniformity_background':
                {'test_type': 'UNIFORMITY',
                 'photon_source': 'xray',
                 'energy_cal': False,
                 'hvList': [1000],
                 'threshStep': 'N/A',
                 'thresholds': [16, 30, 50, 70, 90, 120],
                 'max_view_period': 1000,
                 'hvStabTime': 0,
                 'tubeVoltageList': [120],
                 'tubeCurrentList': [0, 0.4, 0.6, 0.8, 1, 1.5, 3, 7, 10, 15, 20, 25],
                 'distance': 30,
                 'filter_type': 'Cu',
                 'filter_thickness': 1,
                 'xRayStabTime': 10,
                 'totTime': 1,
                 'timeResolution': 0.25,
                 'startThresh': 'N/A',
                 'endThresh': 'N/A'
                 },
            'testitemid30shorttermstability-ecal':
                {'test_type': 'STABILITY',
                 'photon_source': 'xray',
                 'energy_cal': True,
                 'hvList': [1000],
                 'threshStep': 'N/A',
                 'thresholds': [16, 30, 50, 70, 90, 120],
                 'max_view_period': 1000,
                 'hvStabTime': 0, #1800,
                 'tubeVoltageList': [120],
                 'tubeCurrentList': [1.0],
                 'distance': 30,
                 'filter_type': 'Cu',
                 'filter_thickness': 1,
                 'xRayStabTime': 10,
                 'totTime': 60,
                 'timeResolution': 0.025,
                 'startThresh': 'N/A',
                 'endThresh': 'N/A'
                 },
            'testitemid30shorttermstability':
                {'test_type': 'STABILITY',
                 'photon_source': 'xray',
                 'energy_cal': False,
                 'hvList': [1000],
                 'threshStep': 'N/A',
                 'thresholds': [16, 30, 50, 70, 90, 120],
                 'max_view_period': 1000,
                 'hvStabTime': 0,  # 1800,
                 'tubeVoltageList': [120],
                 'tubeCurrentList': [1.0],
                 'distance': 30,
                 'filter_type': 'Cu',
                 'filter_thickness': 1,
                 'xRayStabTime': 10,
                 'totTime': 60,
                 'timeResolution': 0.025,
                 'startThresh': 'N/A',
                 'endThresh': 'N/A'
                 },
            'testitemid30shorttermstability_dm-ecal':
                {'test_type': 'STABILITY',
                 'photon_source': 'xray',
                 'energy_cal': True,
                 'hvList': [1000],
                 'threshStep': 'N/A',
                 'thresholds': [16, 30, 50, 70, 90, 120],
                 'max_view_period': 1000,
                 'hvStabTime': 0,  # 1800,
                 'tubeVoltageList': [120],
                 'tubeCurrentList': [4.0],
                 'distance': 60,
                 'filter_type': 'Cu',
                 'filter_thickness': 1,
                 'xRayStabTime': 10,
                 'totTime': 60,
                 'timeResolution': 0.025,
                 'startThresh': 'N/A',
                 'endThresh': 'N/A'
                 },
            'testitemid30adynamicresponse-ecal':
                {'test_type': 'DYNAMIC',
                 'photon_source': 'xray',
                 'energy_cal': True,
                 'hvList': [1000],
                 'threshStep': 'N/A',
                 'thresholds': [16, 30, 35, 45, 75, 95],
                 'max_view_period': 1000,
                 'hvStabTime': 0,
                 'tubeVoltageList': [120],
                 'tubeCurrentList': [5.0],
                 'distance': 30,
                 'filter_type': 'Cu',
                 'filter_thickness': 2,
                 'xRayStabTime': 10,
                 'totTime': 12,
                 'timeResolution': 0.003,
                 'startThresh': 'N/A',
                 'endThresh': 'N/A',
                 'shutter_on': 1.21,
                 'shutter_off': 1,
                 },
            'testitemid30adynamicresponse_dm-ecal':
                {'test_type': 'DYNAMIC',
                 'photon_source': 'xray',
                 'energy_cal': True,
                 'hvList': [1000],
                 'threshStep': 'N/A',
                 'thresholds': [16, 30, 35, 45, 75, 95],
                 'max_view_period': 1000,
                 'hvStabTime': 0,
                 'tubeVoltageList': [120],
                 'tubeCurrentList': [25.0],
                 'distance': 60,
                 'filter_type': 'Cu',
                 'filter_thickness': 2,
                 'xRayStabTime': 10,
                 'totTime': 12,
                 'timeResolution': 0.003,
                 'startThresh': 'N/A',
                 'endThresh': 'N/A',
                 'shutter_on': 1.21,
                 'shutter_off': 1,
                 },
            'testitemid30adynamicresponse':
                {'test_type': 'DYNAMIC',
                 'photon_source': 'xray',
                 'energy_cal': False,
                 'hvList': [1000],
                 'threshStep': 'N/A',
                 'thresholds': [16, 30, 35, 45, 75, 95],
                 'max_view_period': 1000,
                 'hvStabTime': 0,
                 'tubeVoltageList': [120],
                 'tubeCurrentList': [5.0],
                 'distance': 30,
                 'filter_type': 'Cu',
                 'filter_thickness': 2,
                 'xRayStabTime': 10,
                 'totTime': 12,
                 'timeResolution': 0.003,
                 'startThresh': 'N/A',
                 'endThresh': 'N/A',
                 'shutter_on': 1.21,
                 'shutter_off': 1,
                 },
            'testitemid30adynamicresponse_45cm-ecal':
                {'test_type': 'DYNAMIC',
                 'photon_source': 'xray',
                 'energy_cal': True,
                 'hvList': [1000],
                 'threshStep': 'N/A',
                 'thresholds': [16, 30, 35, 45, 75, 95],
                 'max_view_period': 1000,
                 'hvStabTime': 0,
                 'tubeVoltageList': [120],
                 'tubeCurrentList': [11.47],
                 'distance': 45,
                 'filter_type': 'Cu',
                 'filter_thickness': 2,
                 'xRayStabTime': 10,
                 'totTime': 12,
                 'timeResolution': 0.003,
                 'startThresh': 'N/A',
                 'endThresh': 'N/A',
                 'shutter_on': 1.21,
                 'shutter_off': 1,
                 },
            'copper_sweep':
                {'test_type': 'SPECTRUM',
                 'photon_source': 'xray',
                 'energy_cal': False,
                 'hvList': [1000],
                 'threshStep': 1,
                 'thresholds': 'N/A',
                 'max_view_period': 1000,
                 'hvStabTime': 0,
                 'tubeVoltageList': [120],
                 'tubeCurrentList': [2],
                 'distance': 30,
                 'filter_type': 'Cu',
                 'filter_thickness': 1,
                 'xRayStabTime': 10,
                 'totTime': 0.25,
                 'timeResolution': 0.25,
                 'startThresh': 30,
                 'endThresh': 200
                 },
            'americium_sweep':
                {'test_type': 'SPECTRUM',
                 'photon_source': 'am241',
                 'energy_cal': False,
                 'hvList': [1000],
                 'threshStep': 1,
                 'thresholds': 'N/A',
                 'max_view_period': 1000,
                 'hvStabTime': 0,
                 'tubeVoltageList': 'N/A',
                 'tubeCurrentList': 'N/A',
                 'distance': 1.5,
                 'filter_type': 'N/A',
                 'filter_thickness': 'N/A',
                 'xRayStabTime': 'N/A',
                 'totTime': 16,
                 'timeResolution': 16,
                 'startThresh': 35,
                 'endThresh': 93
                 },
        }


    def thresholdKevToAU(self, thresholdsKev, kevPerAU, offsetAU):
        thresholdsAU = []
        for i in range(len(thresholdsKev)):
            th = (thresholdsKev[i] - offsetAU) / kevPerAU
            if 0 <= th <= 255:
                thresholdsAU.append(int(round(th)))
            elif th > 255:
                thresholdsAU.append(255)
            elif th < 0:
                thresholdsAU.append(0)
                # If we get the logger working in this script, we should put a warning that the threshold setting was maxed
                # out and modified the threshold
        return thresholdsAU


    def getSpectrumParams(self, VER_ID, source, gainMode):
        if VER_ID < 5:
            kevPerAU = baseline.gainDict[gainMode]['a']['gain']
            offsetAU = 0
        else:
            kevPerAU = baseline.gainDict[gainMode]['b']['gain']
            offsetAU = baseline.gainDict[gainMode]['b']['offset']

        if source.lower() == 'am241':
            totTime = 16
            timeResolution = 16
            startKev = 35
            endKev = 93 #70 #63
            startThresh, endThresh = self.thresholdKevToAU([startKev, endKev], kevPerAU, offsetAU)
        elif source.lower() == 'co57':
            totTime = 20
            timeResolution = 20
            startKev = 14
            endKev = 152
            startThresh, endThresh = self.thresholdKevToAU([startKev, endKev], kevPerAU, offsetAU)
        elif source == globals.pbKedgeName:
            totTime = 2
            timeResolution = 2
            startThresh = 50
            endThresh = 180
        elif source == globals.ceKedgeName:
            totTime = 2
            timeResolution = 2
            startThresh = 10
            endThresh = 70
        elif source == globals.cuKedgeName:
            totTime = 0.25
            timeResolution = 0.25
            startThresh = 30
            endThresh = 200
        else:
            raise Exception('Source: {} not defined!'.format(source))
        return [totTime, timeResolution, startThresh, endThresh]

    def getRevision(self, revNumber):
        if revNumber >= 5:
            return 'b'
        else:
            return 'a'

    def get_unique_hv_list(self, experiment_list):
        """search active experiments and for total set of unique hv bias values"""
        hv_list_all = []
        for experiment_name in experiment_list:
            exp_hv_list = self.experimentDict[experiment_name]['hvList']
            hv_list_all.extend(exper for exper in exp_hv_list)
        hvList = list(set(hv_list_all))
        hvList.sort()

        return hvList
