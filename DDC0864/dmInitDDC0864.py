########################################################################################################################
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
########################################################################################################################

import os
import time
import logging

import globals

from dmBuffer import DM

from DDC0864.dmFunctions import writeReg, asic2ram, setLeakCompMode, setFbC, setHpf, getAsicRevision, numCounters
from DDC0864.misc_chain import misc_flush_1s
from DDC0864.set18_chain import set18_chain_prog


logger = logging.getLogger(__name__)


def initDDC0864(lkgMode, hpf, activeAsicNums, gainMode, site='RedlenB'):
    for asic in activeAsicNums:
        writeReg('0x0000f', asic, asic, '0x0000', 0)  # Release ASIC from reset
        writeReg('0x0000c', asic, asic, '0x0001', 0)  # enable ASIC for global reg update
    writeReg('0x00', 15, 0, '0x0001', 1, 1)    # apply soft_reset to ASIC  -- write reg 0x00 with 0x0001

    # read FPGA revision and date
    logger.info('FPGA Revision: {}'.format(DM.SPI_read('0x00000')))
    logger.info('FPGA Revision Date: {}'.format(DM.SPI_read('0x00001')))

    for asic in activeAsicNums:
        VER_ID = getAsicRevision(asic)

    # Configure ASIC defaults
    logger.debug('Configure ASIC defaults')

    # asicrev = 5, rev B
    writeReg('0x6a', 4, 4, '0x0001', 1, 1)  # essential1 = 3 (msb)
    writeReg('0x6a', 1, 1, '0x0001', 1, 1)  # essential1 = 3 (lsb)
    writeReg('0x61', 15, 15, '0x0001', 1, 1)  # essential2 =  1, dev_curie.writeReg(97,0x8000)
    writeReg('0x89', 4, 4, '0x0001', 1, 1)  # essential3 =  1, dev_curie.writeReg(137,0x0010)
    writeReg('0x13', 15, 15, '0x0001', 1, 1)  # essential4 =  1, dev_curie.writeReg(19,0x8000)
    writeReg('0x4c', 15, 15, '0x0000', 1, 1)  # essential5 =  1  (msb)
    writeReg('0x4c', 10, 9, '0x0001', 1, 1)  # essential5 =  1  (lsb)
    writeReg('0x4c', 14, 12, '0x0001', 1, 1)  # essential6 =  1, dev_curie.writeReg(76,0x1000)
    writeReg('0x63', 7, 7, '0x0001', 1, 1)  # essential7 =  1, dev_curie.writeReg(99,0x0080)
    writeReg('0x5f', 9, 8, '0x0003', 1, 1)  # essential8 =  3, dev_curie.writeReg(95,0x0300)
    writeReg('0x5f', 11, 10, '0x0003', 1, 1)  # essential9 =  3, dev_curie.writeReg(95,0x0C00)
    writeReg('0x71', 12, 12, '0x0001', 1, 1)  # essential10 = 1, dev_curie.writeReg(113,0x1000)
    writeReg('0x31', 11, 10, '0x0001', 1, 1)  # essential11 = 1, dev_curie.writeReg(49,0x0400)
    writeReg('0x85', 7, 5, '0x0006', 1, 1)  # essential12 = 6, dev_curie.writeReg(81,0x0180)
    writeReg('0x77', 2, 2, '0x0001', 1, 1)  # essential13 = 1, dev_curie.writeReg(124,0x0004)
    writeReg('0x31', 9, 8, '0x0001', 1, 1)  # essential14 = 1
    writeReg('0x68', 8, 8, '0x0001', 1, 1)  # essential15 = 1
    writeReg('0xcc', 5, 4, '0x0003', 1, 1)  # essential16 = 3
    writeReg('0xaf', 3, 0, '0x0001', 1, 1)  # essential17 = 1
    writeReg('0xb0', 7, 0, '0x00c8', 1, 1)  # essential18 = 200

    writeReg('0x31', 5, 4, '0x0003', 1, 1)  # tg_clk_sel = 0b011, TG_CLK = MCLK/4, dev_curie.writeReg(49,0x0003)
    writeReg('0x1f', 2, 2, '0x0000', 1, 1)  # Frame Clock disable Free Running
    writeReg('0x1e', 0, 0, '0x0000', 1, 1)  # mode_crc_en = 0, dev_curie.writeReg(30,0x0000)
    writeReg('0x2a', 8, 8, '0x0001', 1, 1)  # dis_col_cntr_info = 1,
    ######################################################################################
    writeReg('0x67', 9, 6, '0x0000', 1, 1)  # 100ohm termination on MCLK
    writeReg('0x32', 7, 6, '0x0000', 1, 1)  # autozero duration = 140 TGCLKs) changed Oct 12, 2018 as per Rakul email GT
    writeReg('0x35', 1, 0, '0x0001', 1, 1)  # Use DFT<2> as the charge dump clock
    writeReg('0x69', 3, 3, '0x0001', 1, 1)  # disable ping-pong qdump
    writeReg('0x0a23', 3, 0, '0x0005', 0, 1)  # 5 word - 80 bit chains
    writeReg('0x79', 15, 13, '0x0003', 1, 1)  # mode acsw = 4w
    writeReg('0x5b', 3, 3, '0x0000', 1, 1)  # range_ldo_1v
    writeReg('0x18', 12, 12, '0x0000', 1, 1)        # Threshold scan = 0
    writeReg('0x33', 0, 0, '0x0001', 1, 1)          # Enable QDUMP
    setHpf(hpf, VER_ID)                             # set hpf value
    setLeakCompMode(lkgMode, VER_ID)                # set leakage comp mode
    setFbC(gainMode, VER_ID)                        # sets Cfb specified gain setting
    numCounters(EC=True, CC=True, SEC=True)         # set number of counters=13, all counters enabled

    set18_chain_prog(os.path.join(globals.ldir, site), progFile=globals.set18File)  # writes default values to set18 chain

    for asic in activeAsicNums:
        logger.info("Unmasking ASIC{} for data capture".format(asic))
        writeReg('0x00007', asic, asic, '0x0001', 0, 1)  # Unmask ASIC for data capture
        # ldir = os.path.join(globals.ldir, site, globals.scratchDir)
        # readAsicGlobals(ldir, 'b4elecCals_A{}'.format(asic % 2))



