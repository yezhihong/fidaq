import os
import pytest
from unittest import TestCase
from DDC0864.verifications.global_registers.asic_reg_map_global import ASICRegMapGlobal
from DDC0864.verifications.global_registers.asic_reg_map_global import TI_REF_REG_VALUE


@pytest.fixture
def redlen_asic():
    return ASICRegMapGlobal(os.path.join(os.path.dirname(__file__), '..', '..', 'fixtures', 'reg_dump.csv'))


@pytest.fixture
def ti_asic():
    return ASICRegMapGlobal(TI_REF_REG_VALUE, 'TI')


def test_compare(redlen_asic,ti_asic):
    assert not redlen_asic.compare(ti_asic)[0]
