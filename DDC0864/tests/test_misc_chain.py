import logging
from time import sleep
import pytest
import globals
import dmBuffer.DMBufferBringUp as DMBB
from DDC0864.dmInitDDC0864 import initDDC0864
from DDC0864.misc_chain import readPerPixCal, program_asic_from_json, create_misc_chain_prog, misc_json_to_csv
import pcct
import os

logger = logging.getLogger(__name__)
pcct.config_logging(console_level=logging.DEBUG, file_level=None)


@pytest.fixture
def detector():
    CLIENT = 'Hitachi'

    MODULES = [
        None,
        None,
        None,
        'MM3',
        None,
        None,
        None,
        None,
    ]

    PASSIVE_ASICS = []

    pcct.load_packages()

    buffer = globals.buffer = DMBB.startBuffer(15)
    sleep(2)

    detector_cls = pcct.detector_types()[CLIENT]
    detector = globals.detector = detector_cls(buffer, '', MODULES, passive_asics=PASSIVE_ASICS)

    detector.reset()
    initDDC0864('active2_high', 5, detector.active_asics, 'midgain')
    detector.setup()


def test_create_misc_chain_prog():
    create_misc_chain_prog(os.path.join(os.path.dirname(__file__), 'fixtures'), 5,
                           prog_mode=0, sourceFile="misc_chain_write_vals.csv",
                           progFile="misc_chain_prog_vals.csv", pix_list=range(864), td_trim_fe=[],
                           sel_qdump=[], qdump_del=[], en_prbs=[], iso_pix=[], qtrig=[], trim_dnl=[],
                           td_trim_be=[], td_trim_be2=[], adc_offset=[], mode_add_cap=[], lkg_passive=[],
                           lkg_active=[], sel_pix=[], mode_bad_pix=[], cdump_a=[], cdump_b=[],
                           file_type='csv')

    create_misc_chain_prog(os.path.join(os.path.dirname(__file__), 'fixtures'), 5,
                           prog_mode=0, sourceFile="misc_chain_write_vals.csv",
                           progFile="misc_chain_prog_vals.csv", pix_list=range(864), td_trim_fe=[],
                           sel_qdump=[], qdump_del=[], en_prbs=[], iso_pix=[], qtrig=[], trim_dnl=[],
                           td_trim_be=[], td_trim_be2=[], adc_offset=[], mode_add_cap=[], lkg_passive=[],
                           lkg_active=[], sel_pix=[], mode_bad_pix=[], cdump_a=[], cdump_b=[],
                           file_type='json')


@pytest.mark.skip
def test_readPerPixCal(detector):
    readPerPixCal(os.path.join(os.path.dirname(__file__), 'fixtures'), 5,
                  miscReadValsFile="misc_chain_read_vals.csv", startChain=0,
                  endChain=11, miscReadValsProgFile="misc_chain_prog_vals.csv", writeToProgFile=1,
                  sensorId=None, file_type='csv')

    readPerPixCal(os.path.join(os.path.dirname(__file__),'fixtures'), 5,
                  miscReadValsFile="misc_chain_read_vals.csv", startChain=0,
                  endChain=11, miscReadValsProgFile="misc_chain_prog_vals.csv", writeToProgFile=1,
                  sensorId=None, file_type='json')


@pytest.mark.skip
def test_program_asic_from_json(detector):
    assert program_asic_from_json(os.path.join(os.path.dirname(__file__), 'fixtures'), 5)


def test_misc_json_to_csv():
    assert misc_json_to_csv(os.path.join(os.path.dirname(__file__), 'fixtures'), 5,
                            json_file="misc_chain_prog_vals.json",
                            csv_file="misc_chain_test_vals.csv")
