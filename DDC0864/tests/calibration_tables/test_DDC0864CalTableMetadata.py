import pytest
from DDC0864.calibration_tables.ddc0864_cal_table_metadata import DDC0864CalTableMetadata


@pytest.fixture
def meta_data():
    return DDC0864CalTableMetadata('16ns', '16kev')


def test_get_field(meta_data):
    assert meta_data.get_field('TargetQtrig'), '16kev'

def test_set_field(meta_data):
    meta_data.set_field('Temp', 22)
    assert meta_data.get_field('Temp'), 22

def test_get_json_str(meta_data):
    assert meta_data.get_json_str(), "TargetDeadTime': '16ns', 'TargetQtrig': '16kev"