import pytest
from DDC0864.calibration_tables.ddc0864_cal_table_metadata import DDC0864CalTableMetadata
from DDC0864.calibration_tables.ddc0864_cal_table import DDC0864CalTable
from DDC0864.calibration_tables.pixel_cal_data import PixelCalData




@pytest.fixture
def cal_data():
    m = DDC0864CalTableMetadata('16ns', '16kev')
    p1 = PixelCalData('csv', '0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,3,10,16,128,220,48,9,8')
    p2 = PixelCalData('csv', '0,1,1,0,0,0,0,0,0,0,0,0,0,3,3,2,9,64,128,218,57,9,10')
    p = [p1, p2]
    x = DDC0864CalTable(m, p)
    x.caldata_update()
    return x


def test_get_json_str(cal_data):
    assert cal_data.get_json_str(), '{"caldata": [{"LEAKAGE_ACTIVE": 16, "ADD_CAP": 3, "TD_TRIM_BE2": 9, "SEL_PIX": 0, "CDUMP_B": 3, "CDUMP_A": 3, "TD_TRIM_FE": 220, "SEL_QDUMP": 0, "TRIM_DNL": 0, "EN_PRBS": 0, "Col": 0, "ADC_OFFSET": 8, "TD_TRIM_BE": 48, "CALIB_ESSENTIAL_1": 0, "CALIB_ESSENTIAL_2": 0, "CALIB_ESSENTIAL_3": 0, "QDUMP_DEL": 0, "ISO_PIX": 0, "MODE_BAD_PIX": 0, "LEAKAGE_PASSIVE": 128, "Pixel": 0, "QTRIG": 10, "Row": 0}, {"LEAKAGE_ACTIVE": 64, "ADD_CAP": 2, "TD_TRIM_BE2": 9, "SEL_PIX": 0, "CDUMP_B": 3, "CDUMP_A": 3, "TD_TRIM_FE": 218, "SEL_QDUMP": 0, "TRIM_DNL": 0, "EN_PRBS": 0, "Col": 1, "ADC_OFFSET": 10, "TD_TRIM_BE": 57, "CALIB_ESSENTIAL_1": 0, "CALIB_ESSENTIAL_2": 0, "CALIB_ESSENTIAL_3": 0, "QDUMP_DEL": 0, "ISO_PIX": 0, "MODE_BAD_PIX": 0, "LEAKAGE_PASSIVE": 128, "Pixel": 1, "QTRIG": 9, "Row": 0}], "metadata": {"TargetDeadTime": "16ns", "TargetQtry": "16kev"}}'

