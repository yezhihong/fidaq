########################################################################################################################
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
########################################################################################################################

import os
import re
import csv
import glob
import shutil
import logging
import zipfile
import subprocess
from time import sleep, time, clock

import globals

from dmBuffer import DM
from dmBuffer.DM_Buffer_udp_lib_20180704 import SPI_write


logger = logging.getLogger(__name__)


writeQuiet = 1
essential5 = '0x4c'


def hex2bin(val, numBits=16):
    binVal = bin(int(val, 16))[2:].zfill(numBits)
    return binVal


def hex2dec(val):
    return int(val, 16)


def is_set(x, n):
    return x & 2 ** n != 0  # For value 'x' check if bit 'n' is set


def checkDir(path):
    strippedPath = ''
    for item in path.split('\\'):
        if '.' not in item:
            strippedPath += item + '\\'
    if not os.path.exists(strippedPath):
        os.makedirs(strippedPath)


def maskAsicList(asicsActiveList, asicsPassiveList, maskAllOthersOff=0, disableTestPattern=0, asicSingle=None):
    if isinstance(asicsActiveList, int):
        asicsActiveList = [asicsActiveList]
    if isinstance(asicsPassiveList, int):
        asicsPassiveList = [asicsPassiveList]
    if maskAllOthersOff:
        writeReg('0x0000c', 15, 0, '0x0000')                                        # Disable all ASICs for global reg update
        writeReg('0x0000c', asicSingle, asicSingle, '0x0001')     # enable ASICn for global reg update
        asicsActiveList.remove(asicSingle)
        pattEn = asicsPassiveList+asicsActiveList
        asicDMMPatternEnable(asicSingle, pattEn)
        asicsActiveList.append(asicSingle)
        asicsActiveList.sort()
        enabledList = [asicSingle]
    else:
        writeReg('0x0000c', 15, 0, '0x0000')  # Disable all ASICs for global reg update
        for asicActive in asicsActiveList:
            writeReg('0x0000c', asicActive, asicActive, '0x0001')                  # enable ASICn for global reg update
        asicDMMPatternEnable(asicsActiveList, asicsPassiveList)
        enabledList = asicsActiveList
    if disableTestPattern:
        asicDMMPatternEnable(asicsActiveList, asicsPassiveList, disableTestPattern=disableTestPattern)
        enabledList = asicsActiveList

    logger.debug('Enabling ASICs: %s for register update' % enabledList)
    return enabledList

def updateMessage(textVar, window, text=''):
    logger.info(text)
    textVar.set(text)
    window.update_idletasks()
    window.update()


def getAsicPosition(asic):
    return asic % 2

def setModeX():
    if globals.modeX:
        writeReg('0x31', 11, 10, '0x0001', 1, 0)
        writeReg('0xcc', 5, 4, '0x0003', 1, 0)
        writeReg('0xaf', 3, 0, '0x000a', 1, 0)
        writeReg('0xb0', 7, 0, '0x00c8', 1, 0)
        logger.debug('Mode X enabled')
    else:
        writeReg('0x31', 11, 10, '0x0002', 1, 0)
        logger.debug('Mode X disabled')


def getAsicRevision(asic):
    # preserve ASIC access mask
    asicEn = readReg('0x0000c', 0, 15, 0)
    # disable all ASICs
    writeReg('0x0000c', 15, 0, '0x0000', 0, 1)
    # enable specific ASIC for revision request
    writeReg('0x0000c', asic, asic, '0x0001', 0, 1)  # enable ASIC for global reg update
    # read asic revision register
    VER_ID = int(readReg('5', 1), 0)
    if VER_ID != 5:
        logger.info('ASIC revision {} found for ASIC{}'.format(VER_ID, asic))
    else:
        logger.info('ASIC revision {} found for ASIC{}'.format(VER_ID, asic))
    # restore ASIC access mask
    writeReg('0x0000c', 15, 0, asicEn, 0, 1)
    return VER_ID


def numCounters(EC=True, CC=True, SEC=True):
    counters = [EC*1, CC*1, SEC*1]
    if counters == [1, 0, 0]:
        #EC Only
        n_counters = '0x0001'
        cntr_bits = '0x3'
        N_cntrs = 1
    elif counters == [0, 1, 0]:
        # 6 counters, CC only
        n_counters = '0x0006'
        cntr_bits = '0x5'
        N_cntrs = 6
    elif counters == [0, 0, 1]:
        # 6 counters, SEC only
        n_counters = '0x0006'
        cntr_bits = '0x6'
        N_cntrs = 6
    elif counters == [1, 1, 0]:
        # 7 counters, EC and CC
        n_counters = '0x0007'
        cntr_bits = '0x1'
        N_cntrs = 7
    elif counters == [1, 0, 1]:
        # 7 counters, EC and SEC
        n_counters = '0x0007'
        cntr_bits = '0x2'
        N_cntrs = 7
    elif counters == [0, 1, 1]:
        # 12 counters, SEC and CC
        n_counters = '0x000c'
        cntr_bits = '0x4'
        N_cntrs = 12
    elif counters == [1, 1, 1]:
        # 13 counters, EC, SEC and CC
        n_counters = '0x000d'
        cntr_bits = '0x0'
        N_cntrs = 13
    else:
        logger.info('Invalid Counters Configuration, setting to 13 registers')
        # 13 counters, EC, SEC and CC
        n_counters = '0x000d'
        cntr_bits = '0x0'
        N_cntrs = 13
    writeReg('0x00a04', 3, 0, n_counters)       # set fpga counter number
    writeReg('0x15', 2, 0, cntr_bits, 1, 1)     # set ASIC counter enables
    return N_cntrs


def asicDMMPatternEnable(asicNoPattList, asicPattList, disableTestPattern=0):
    # configure Test Pattern Generation: Start value='d0; Stop value='h7ff
    if disableTestPattern:
        logger.debug('Disabling test pattern')
        SPI_write('0x00A02', '0x0000')                              # No test pattern
    else:
        if isinstance(asicNoPattList, int):
            asicNoPattList = [asicNoPattList]
        if isinstance(asicPattList, int):
            asicPattList = [asicPattList]
        logger.debug("Test pattern off for ASIC(s): %s, pattern on for ASIC(s): %s" % (asicNoPattList, asicPattList))
        SPI_write('0x00A00', '0x0000')                              # pattern start
        SPI_write('0x00A01', '0x07FF')                              # pattern stop
        for asicPatt in asicPattList:
            logger.debug("Enabling test pattern on ASIC%s" % asicPatt)
            writeReg('0x00A02', asicPatt, asicPatt, '0x0001')       # test pattern enable on ASICn
        for asicNoPatt in asicNoPattList:
            logger.debug("Disabling test pattern on ASIC%s" % asicNoPatt)
            writeReg('0x00A02', asicNoPatt, asicNoPatt, '0x0000')   # test pattern disable on ASICn
    SPI_write('0x00A03', '0xFFFF')                                  # enable CRC on all asics
    for asic in asicPattList:
        logger.debug("Set up trigger mask for ASIC%s" % asic)
        writeReg('0x00007', asic, asic, '0x0001')                   # enable trigger mask on ASIC
    for asic in asicNoPattList:
        logger.debug("Set up trigger mask for ASIC%d" % asic)
        writeReg('0x00007', asic, asic, '0x0001')                   # enable trigger mask on ASIC


def writeReg(adr, msb, lsb, val, shadow=0, writeNow=1):
    if shadow:
        if msb == 15 and lsb == 0:
            globals.buffer.write_asic_reg(int(adr, 16), int(val, 16))
        else:
            globals.buffer.modify_asic_reg(int(adr, 16), int(val, 16), msb=msb, lsb=lsb)
    else:
        if msb == 15 and lsb == 0:
            globals.buffer.write_detector_reg(int(adr, 16), int(val, 16))
        else:
            globals.buffer.modify_detector_reg(int(adr, 16), int(val, 16), msb=msb, lsb=lsb)


def extractBits(value, msb, lsb):
    mask = (1 << (msb - lsb + 1)) - 1
    return (int(value, 0) & (mask << lsb)) >> lsb


def readReg(adr, shadow=0, msb=15, lsb=0):
    if shadow:
        value = globals.buffer.read_asic_reg(int(adr, 16))

        width = msb - lsb + 1
        mask = (2**width - 1) << lsb

        return '{:#06x}'.format((value & mask) >> lsb)
    else:
        tmpAdr = adr
        adr = "0x%05x" % int(adr, 0)
        if shadow == 1:
            adr = "0x%05x" % (int('0x02000', 0) + int(adr, 0))
            asic2ram(tmpAdr, tmpAdr)
        data = DM.SPI_read(adr)
        # if msb != 15 or lsb != 0:
        data = format(extractBits(data, msb, lsb), '#06x')
            #data = extractBits(data, msb, lsb)
        return data


def asic2ram(startAdr='0x01', endAdr='0xFF'):
    adr = "0x%04x" % (int(startAdr, 16) << 8 | int(endAdr, 16))
    DM.SPI_write('0x0000b', adr)
    rdBits()


def ram2asic(startAdr='0x01', endAdr='0xFF'):
    adrList = [i for i in range(int(startAdr, 16), int(endAdr, 16) + 1) if i not in globals.regExclList]
    for a in adrList:
        adr = "0x%04x" % (a << 8 | a)
        DM.SPI_write('0x0000b', adr)
        wrBits()


def resize_retype(inst, l):
    return [inst] * l if isinstance(inst, int) else [inst[0]] * l if len(inst) == 1 else inst


def wrBits():
    DM.SPI_write('0x0000e', '0x0000')  # write bit low
    DM.SPI_write('0x0000e', '0x0008')  # then high
    waitForBit('0x0000e', 1, True)


def rdBits():
    DM.SPI_write('0x0000e', '0x0000')  # write bit low, then high
    DM.SPI_write('0x0000e', '0x0004')  # Set ASIC_REG_READ
    waitForBit('0x0000e', 0, True)


def waitForBit(addr, bit, expectedVal, timeout=10):
    """Waits timeout seconds for bit in addr to be set/unset (True/False). raises Error if value not reached by timeout"""
    startTime = time()
    expected_bit_val = "set" if expectedVal else "unset"
    while (time() - startTime) < timeout:
        currentVal = is_set(int(DM.SPI_read(addr), 0), bit)
        if currentVal == expectedVal:
            return
        # logger.debug('Waiting %1.3f more seconds for %s, bit %d to be %s' % ((timeout - (time() - startTime)), addr, bit, expected_bit_val))
        # sleep(0.01)
    raise Exception(logger.error('%s bit %d was not %s in time' % (addr, bit, expected_bit_val)))


def readAsicGlobals(tdir, fName):
    logger.info('Reading ASIC global registers')
    logger.info(os.path.join(globals.regDumps, "regDump_%s.csv" % fName))
    checkDir(globals.regDumps)
    asic2ram('0x01', '0xff')
    adrList = [i for i in range(1, 256) if i not in globals.regExclList]
    with open(os.path.join(globals.regDumps, "regDump_%s.csv" % fName), 'wb') as fh:
        fh.write('Register (Hex), Value (Hex), Register, Value\n')
        for i in adrList:
            gRegAddr = "0x020" + ("%02x" % i)
            regData = int(DM.SPI_read(gRegAddr), 0)
            tmpData = "0x%05x, 0x%05x, %d, %d" % (int(gRegAddr, 0), regData, i, regData)
            fh.write(tmpData + '\n')
    return os.path.join(globals.regDumps, "regDump_{}.csv".format(fName))


def dacSet(vin, DMType, writeTimeOut=1):
    # logger.info('Setting DFT1 Voltage to: %1.3fV' % vin)
    dacMaxV = DMType.V_DFT_DAC_MAX
    if not 0 <= vin <= dacMaxV:
        raise ValueError('Voltage {}v is not between 0V and {}V'.format(vin, dacMaxV))
    # Vdump = int(round(vin * 4095 / 2.5))  # DAC code for vin
    Vdump = int(round(vin * 4095 / dacMaxV))  # DAC code for vin
    wdata1 = "0x0" + ("%03x" % Vdump)
    writeReg('0x00A0C', 11, 0, wdata1)  # DFT1 = vin
    writeReg('0x00A0A', 1, 1, '0x0000')
    writeReg('0x00A0A', 1, 1, '0x0001')  # Load Pulser data
    currentTime = time()
    wrFin = is_set(int(DM.SPI_read('0x00A0A'), 0), 0)
    while not wrFin and (time() - currentTime < writeTimeOut):
        # sleep(0.020)
        wrFin = is_set(int(DM.SPI_read('0x00A0A'), 0), 0)
    # sleep(1)  # wait 1s for DAC to settle
    writeReg('0x00A0A', 1, 1, '0x0000')
    return [wrFin, Vdump]


def dacSet0(vin, DMType, writeTimeOut=1):
    # logger.info('Setting DFT0 Voltage to: %1.3fV' % vin)
    dacMaxV = DMType.V_DFT_DAC_MAX
    if not 0 <= vin <= dacMaxV:
        raise ValueError('Voltage {}v is not between 0V and {}V'.format(vin, dacMaxV))
    #Vdump0 = int(round(vin * 4095 / 2.5))  # DAC code for vin
    Vdump0 = int(round(vin * 4095 / dacMaxV))  # DAC code for vin
    wdata0 = "0x0" + ("%03x" % Vdump0)
    writeReg('0x00A0B', 11, 0, wdata0)  # DFT0 = vin
    writeReg('0x00A0A', 1, 1, '0x0000')
    writeReg('0x00A0A', 1, 1, '0x0001')  # Load Pulser data
    currentTime = time()
    wrFin = is_set(int(DM.SPI_read('0x00A0A'), 0), 0)
    while not wrFin and (time() - currentTime < writeTimeOut):
        # sleep(0.020)
        wrFin = is_set(int(DM.SPI_read('0x00A0A'), 0), 0)
    # sleep(1)  # wait 1s for DAC to settle
    writeReg('0x00A0A', 1, 1, '0x0000')
    return [wrFin, Vdump0]


def dmCapture(tdir, fName, time_sec=2, max_view_period=400, max_views=500, appendToName=''):
    fPathBin = tdir + "docs\\binData\\"
    checkDir(fPathBin)
    # runs DM+DM_buffer data collection for duration "time_sec"
    # data is collected in (default) 500 views of 2ms duration, repeated
    # 'N_views' times to total the given acquistion time
    N_views = int(float(time_sec) / (max_view_period) * 1000000 / max_views)
    for i in range(N_views):
        logger.debug('Capture iteration %d/%d' % (i + 1, N_views))
        DM.set_DATA_reset(1)  # Reset DATA Module and VTRIG Module
        DM.set_DATA_reset(0)
        DM.set_VTRIG_reset(1)
        DM.set_VTRIG_reset(0)
        frame_length = (
                           24 + 2) * 468  # Setup DATA module with expected frame length in bytes (468 = 13 counters * 36 columns)
        crc_length = 2 * 468  # The CRC length is in bytes
        DM.set_DATA_preamble('0xFEDC')  # The preamble length is fixed at 2 bytes
        DM.set_DATA_frame_length(frame_length)
        DM.set_DATA_crc_length(crc_length)
        DM.set_DATA_in_select(0)  # Enable the DM_Adapter data lane config
        DM.set_DATA_out_select('10')
        DM.set_VTRIG_periods(
            max_view_period)  # Set the VTRIG generator to a large enough period to cover the view integration time of (max_view_period/1000)ms
        DM.set_VTRIG_num_cycles(max_views)
        DM.set_DATA_enable(1)  # Enable the DATA module
        DM.set_VTRIG_enable(1)  # Enable the VTRIG generator
        # Wait before retrieving the data for the specified VTRIG cycles. The variable
        # 'max_view_period' is the VTRIG period. The set_DATA_test_delay function perform consistency
        # checking to ensure the number of VTRIG cycles fits in the available buffer memory.
        DM.set_DATA_test_delay(max_views, max_view_period)
        DM.set_DATA_enable(0)  # Disable the DATA module
        DM.set_VTRIG_enable(0)  # Disable the VTRIG generator
        # read View Trigger Count and Period registers again after test
        # -- Trigger count is = (trig-cycles - 1)
        # -- Trigger Period is equal to "period" in 140MHz cycles
        # logger.debug "...read View Trigger Count and Period registers"
        # for j in ['0x00010', '0x00011']:
        #     rdata = DM.SPI_read(j)
        #     logger.debug "DM_REG ", j, "=", rdata
        # Read the DM Buffer memory and save as an binary file.
        logger.debug(fPathBin + "\\" + fName + "_i" + str(i) + ".bin")
        DM.dump_to_file(fPathBin + "\\" + fName + appendToName + "_i" + str(i) + ".bin")
    return [N_views, max_view_period]


def parseBin(binfPath, counters=[[0 for i in xrange(15)] for j in xrange(864)], delBin=0):
    rowIndex = 0
    colIndex = 1
    counterListInt = range(2, 15)
    logger.debug('Parsing file: %s to .csv' % binfPath)
    f = open(binfPath, "rb")
    bytes = f.read(12)  # read 4 bytes ViewCount + 8 bytes header
    while bytes != "":
        # Decode header bytes.
        viewCount = ord(bytes[3]) * 1024 + ord(bytes[2]) * 512 + ord(bytes[1]) * 256 + ord(bytes[0])
        data0 = ord(bytes[4]) * 256 + ord(bytes[8])  # ASIC 0 lane 0
        data1 = ord(bytes[5]) * 256 + ord(bytes[9])  # ASIC 0 lane 1
        data2 = ord(bytes[6]) * 256 + ord(bytes[10])  # ASIC 1 lane 0
        data3 = ord(bytes[7]) * 256 + ord(bytes[11])  # ASIC 1 lane 1
        # only look at ASIC 1 Lanes 0 & 1
        if data2 == 0xfedc and data3 == 0xfedc:
            # found header, process the view frame
            for col in range(0, 36):
                for ctr in counterListInt:
                    for row in range(0, 12):
                        bytes = f.read(8)
                        if (row % 2 == 0):
                            pixel0 = col + 36 * row
                        else:
                            pixel0 = (35 - col) + 36 * row
                        if ((23 - row) % 2 == 0):
                            pixel1 = col + 36 * (23 - row)
                        else:
                            pixel1 = (35 - col) + 36 * (23 - row)
                        data0 = ord(bytes[0]) * 256 + ord(bytes[4])  # ASIC 0 lane 0
                        data1 = ord(bytes[1]) * 256 + ord(bytes[5])  # ASIC 0 lane 1
                        data2 = ord(bytes[2]) * 256 + ord(bytes[6])  # ASIC 1 lane 0
                        data3 = ord(bytes[3]) * 256 + ord(bytes[7])  # ASIC 1 lane 1
                        if viewCount != 0:
                            counters[pixel0][rowIndex] = row
                            counters[pixel0][colIndex] = col
                            counters[pixel0][ctr] += data2
                            counters[pixel1][rowIndex] = 23 - row
                            counters[pixel1][colIndex] = col
                            counters[pixel1][ctr] += data3
        bytes = f.read(12)  # get next view frame start
    f.close()
    if delBin == 1:
        os.remove(binfPath)
    return counters


def parseBinDir(binfPath='C:\\Users\\0218\\Desktop\\SVN\\Marie PCCT\\Code\\docs\\binData\\multiFileParse'):
    dataFiles = []
    counters = [[0 for i in xrange(15)] for i in xrange(864)]
    for fname in os.listdir(binfPath):
        if '.bin' in fname:
            dataFiles.append(fname)
    cnt = 1
    for file in dataFiles:
        logger.debug('Parsing file: %d/%d' % (cnt, len(dataFiles)))
        parseFile = binfPath + "\\" + file
        counters = parseBin(parseFile, counters)
        cnt += 1
    return counters


def appendCsvDir(tdir, dataFileList = [], fNameApp='appendedFiles.csv', csvfPath=globals.scratchDir):
    csvfPath = os.path.join(tdir, csvfPath)
    dataFiles = []
    if len(dataFileList) != 0:
        for f in dataFileList:
            dataFiles.append(os.path.join(tdir, globals.scratchDir, f))
    else:
        badNames = ['regDump', 'appendedFiles', 'chain', 'scurve', 'post', 'calsweep', 'threshsweep']
        for fname in glob.glob(os.path.join(csvfPath, '*.csv')):
            if not any(x in fname for x in badNames):
                if '.csv' in fname:
                    dataFiles.append(fname)
    bufferSize = 64 * 1024 * 1024
    with open(os.path.join(csvfPath, fNameApp), 'wb') as outFile:
        cnt = 1
        for appfile in dataFiles:
            logger.debug('Appending file: %s, %d/%d' % (os.path.split(appfile)[1], cnt, len(dataFiles)))
            with open(appfile, 'rb') as inFile:
                if cnt == 1:
                    shutil.copyfileobj(inFile, outFile, length=bufferSize)
                else:
                    inFile.readline()
                    shutil.copyfileobj(inFile, outFile, length=bufferSize)
                cnt += 1
    return os.path.join(csvfPath, fNameApp)


def storeExpFiles(ldir, tdir, experiment):
    mfPath = os.path.join(ldir, globals.scratchDir)
    dataFiles = []
    logFiles = []
    dumpFiles = []
    chainFiles = []
    postCalFiles = []
    badNames = ['regDump', 'appendedFiles', 'chain', 'post']
    for fname in sorted(os.listdir(mfPath)):
        if not any(x in fname for x in badNames):
            if '.csv' in fname:
                dataFiles.append(fname)
            if 'log' in fname:
                logFiles.append(fname)
        elif 'regDump' in fname:
            dumpFiles.append(fname)
        elif 'chain' in fname:
            chainFiles.append(fname)
        elif 'post' in fname:
            postCalFiles.append(fname)
    logger.debug('dataFiles=%s' % dataFiles)
    logger.debug('logFiles=%s' % logFiles)
    logger.debug('dumpFiles=%s' % dumpFiles)
    logger.debug('chainFiles=%s' % chainFiles)
    logger.debug('postCalFiles=%s' % postCalFiles)
    zipName = dataFiles[0].replace('.csv', '.zip')
    zF = zipfile.ZipFile(os.path.join(mfPath, zipName), 'w', zipfile.ZIP_DEFLATED)
    for zfile in dataFiles:
        zF.write(os.path.join(mfPath, zfile), arcname=zfile)
        os.remove(os.path.join(mfPath, zfile))
    for zfile in logFiles:
        zF.write(os.path.join(mfPath, zfile), arcname=zfile)
        os.remove(os.path.join(mfPath, zfile))
    for zfile in dumpFiles:
        zF.write(os.path.join(mfPath, zfile), arcname=zfile)
        os.remove(os.path.join(mfPath, zfile))
    for zfile in chainFiles:
        zF.write(os.path.join(mfPath, zfile), arcname=zfile)
        os.remove(os.path.join(mfPath, zfile))
    for zfile in postCalFiles:
        zF.write(os.path.join(mfPath, zfile), arcname=zfile)
        os.remove(os.path.join(mfPath, zfile))
    zF.close()
    checkDir(os.path.join(tdir, globals.csvDir, experiment))
    shutil.move(os.path.join(ldir, globals.scratchDir, zipName),
                os.path.join(tdir, globals.csvDir, experiment, zipName))
    if os.path.exists(os.path.join(ldir, globals.scratchDir, 'appendedFiles.csv')):
        shutil.move(os.path.join(ldir, globals.scratchDir, 'appendedFiles.csv'),
                    os.path.join(tdir, globals.csvDir, experiment, dataFiles[0]))
    return os.path.join(tdir, globals.csvDir, experiment, dataFiles[0])


def storeFATFiles(ldir, tdir, experimentList, expFiles, baseLine):
    mfPath = os.path.join(ldir, globals.scratchDir)
    senNames = '_'.join([x for x in baseLine.sensor['IDLIST'] if x is not False])
    dataFiles = []
    logFiles = []
    dumpFiles = []
    chainFiles = []
    postCalFiles = []
    for fname in sorted(os.listdir(mfPath)):
        if not any(x in fname for x in ['regDump', 'appendedFiles', 'chain', 'post']):
            if 'log' in fname:
                logFiles.append(fname)
        elif 'regDump' in fname:
            dumpFiles.append(fname)
        elif 'chain' in fname:
            chainFiles.append(fname)
        elif 'post' in fname:
            postCalFiles.append(fname)
    logger.debug('dataFiles=%s' % dataFiles)
    logger.debug('logFiles=%s' % logFiles)
    logger.debug('dumpFiles=%s' % dumpFiles)
    logger.debug('chainFiles=%s' % chainFiles)
    logger.debug('postCalFiles=%s' % postCalFiles)
    zipFiles = []
    # for i, experiment in enumerate(experimentList):
    #     zipName = '{}_{}_{}.zip'.format(senNames, experiment, strftime('%Y-%m-%d_%H-%M-%S'))
    #     zF = zipfile.ZipFile(os.path.join(mfPath, zipName), 'w', zipfile.ZIP_DEFLATED)
    #     for zfile in expFiles[i]:
    #         zF.write(os.path.join(mfPath, zfile), arcname=zfile)
    #         os.remove(os.path.join(mfPath, zfile))
    #     zF.close()
    #     zipFiles.append(zipName)
    resultPath = os.path.join(tdir, globals.csvDir, 'FATTestResults', senNames)
    checkDir(resultPath)
    for f in dumpFiles:
        shutil.move(os.path.join(ldir, globals.scratchDir, f),
                    os.path.join(resultPath, f))
    # for z in zipFiles:
    #     shutil.move(os.path.join(ldir, globals.scratchDir, z),
    #                 os.path.join(resultPath, z))
    #     if os.path.exists(os.path.join(ldir, globals.scratchDir, 'appendedFiles.csv')):
    #         shutil.move(os.path.join(ldir, globals.scratchDir, 'appendedFiles.csv'),
    #                     os.path.join(resultPath, expFiles[0][0] if experimentList[0] != globals.nrgCal
    #                                         else expFiles[1][0]))
    logFile = os.listdir(r'C:\CTData\Logs')
    shutil.move(os.path.join(r'C:\CTData\Logs', logFile[0]), os.path.join(resultPath, logFile[0]))
    anyFiles = os.listdir(os.path.join(ldir, globals.scratchDir))
    if len(anyFiles) != 0:
        for f in anyFiles:
            shutil.move(os.path.join(ldir, globals.scratchDir, f), os.path.join(resultPath, f))
    return resultPath


def storeMMFiles(ldir, tdir, moduleNum, experimentList, expFiles, baseLine):
    mfPath = os.path.join(ldir, globals.scratchDir)
    senNames = '_'.join([x for x in baseLine.sensor['IDLIST'] if x is not False])
    dataFiles = []
    logFiles = []
    dumpFiles = []
    chainFiles = []
    postCalFiles = []

    testStartTime = baseLine.timestamp
    dmSerialNum = baseLine.dm_info['SERIAL_NUM']

    excludedFiles = ['regDump', 'appendedFiles', 'chain', 'post']
    for fname in sorted(os.listdir(mfPath)):
        if not any([x in fname for x in excludedFiles]):
    # for fname in sorted(os.listdir(mfPath)):
    #     if not 'regDump' in fname and not 'appendedFiles' in fname and not 'chain' in fname and not 'post' in fname:
            if 'log' in fname:
                logFiles.append(fname)
        elif 'regDump' in fname:
            dumpFiles.append(fname)
        elif 'chain' in fname:
            chainFiles.append(fname)
        elif 'post' in fname:
            postCalFiles.append(fname)
    logger.debug('dataFiles=%s' % dataFiles)
    logger.debug('logFiles=%s' % logFiles)
    logger.debug('dumpFiles=%s' % dumpFiles)
    logger.debug('chainFiles=%s' % chainFiles)
    logger.debug('postCalFiles=%s' % postCalFiles)
    zipFiles = []


    zipName = '{}-{}.zip'.format(moduleNum, baseLine.dm_info['MODULES'][moduleNum])
    # zF = zipfile.ZipFile(os.path.join(mfPath, zipName), 'w', zipfile.ZIP_DEFLATED)
    with zipfile.ZipFile(os.path.join(mfPath, zipName), 'w', zipfile.ZIP_DEFLATED) as zF:
        for i, experiment in enumerate(experimentList):


            # zipName = '{}_{}_{}.zip'.format(senNames, experiment, strftime('%Y-%m-%d_%H-%M-%S'))
            # zipName = '{}.zip'.format(baseLine.dm['MODULES'][moduleNum])
            # zF = zipfile.ZipFile(os.path.join(mfPath, zipName), 'w', zipfile.ZIP_DEFLATED)
            for expFile in expFiles[i]:
                zF.write(os.path.join(mfPath, expFile), arcname=os.path.join(experiment, os.path.basename(expFile)))
                os.remove(os.path.join(mfPath, expFile))
            # for zfile in logFiles:
            #     zF.write(os.path.join(mfPath, zfile), arcname=zfile)
            #     os.remove(os.path.join(mfPath, zfile))
        for zfile in dumpFiles:
            zF.write(os.path.join(mfPath, zfile), arcname=zfile)
            os.remove(os.path.join(mfPath, zfile))
        for zfile in chainFiles:
            zF.write(os.path.join(mfPath, zfile), arcname=zfile)
            os.remove(os.path.join(mfPath, zfile))
            # for zfile in postCalFiles:
            #     zF.write(os.path.join(mfPath, zfile), arcname=zfile)
            #     os.remove(os.path.join(mfPath, zfile))
    # zF.close()
    zipFiles.append(zipName)

    testPath = os.path.join(tdir, globals.csvDir, globals.testResultsFolder, dmSerialNum, testStartTime)
    checkDir(testPath)

    # for f in dumpFiles:
    #     shutil.move(os.path.join(ldir, globals.scratchDir, f),
    #                 os.path.join(testPath, f))
    for z in zipFiles:
        shutil.move(os.path.join(ldir, globals.scratchDir, z),
                    os.path.join(testPath, z))

    return testPath

def storeRawData(ldir, root_output_dir, baseLine, mm, additionalFiles=(), testFolder=None):
    testType = baseLine.testType
    deviceDir = genDeviceDir(baseLine, mm)

    for folder in globals.dirStruct[testType]['perDevice'].values():
        folderPath = os.path.join(deviceDir, folder)
        if not os.path.exists(folderPath):
            os.makedirs(folderPath)
    testFolderPatt = re.compile('Test \d\d')
    exsistingTestFolders = [folder for folder in os.listdir(deviceDir) if testFolderPatt.search(folder)]
    exsistingTestFolders.sort()

    if exsistingTestFolders:
        nextTestFolderNum = int(exsistingTestFolders[-1][-3:]) + 1
    else:
        nextTestFolderNum = 1

    if testFolder is None:
        testFolder = os.path.join(deviceDir, 'Test {:02d}'.format(nextTestFolderNum))

    for folder in globals.dirStruct[testType]['perTest'].values():
        folderPath = os.path.join(testFolder, folder)
        if not os.path.exists(folderPath):
            os.makedirs(folderPath)

    if testType is not globals.testTypeDict['Sensor']:
        # Need to make filter to catch specific and energy cal
        for asic in baseLine.sensor['ACTIVE_ASIC_NUMS']:
            calibration_dir = os.path.join(ldir, globals.calsDir, baseLine.sensor['IDLIST'][asic])
            shutil.copytree(calibration_dir,
                            os.path.join(testFolder, globals.dirStruct[testType]['perTest']['calibrations'], baseLine.sensor['IDLIST'][asic]),
                            ignore=shutil.ignore_patterns('{}*'.format('Visualizations'),'*.zip'))

    mmFolder = getMMFolderName(baseLine, mm)
    try:
        if globals.run_analysis:
            shutil.copytree(os.path.join(root_output_dir, globals.visualFolder),
                            os.path.join(testFolder, globals.dirStruct[testType]['perTest']['visualizations'],
                                         mmFolder))
        shutil.copytree(os.path.join(root_output_dir),
                        os.path.join(testFolder, globals.dirStruct[testType]['perTest']['rawData'],
                                     mmFolder),
                        ignore=shutil.ignore_patterns('{}*'.format(globals.visualFolder)))
    except (IOError, OSError):
        logger.warn(
            'Problem moving data to network. Check network connection.\nData will remain on local drive')
    else:
        shutil.rmtree(root_output_dir)

    for additionalFile in additionalFiles:
        shutil.copy(additionalFile,
                    os.path.join(testFolder, globals.dirStruct[testType]['perTest']['rawData'], mmFolder,
                                 os.path.basename(additionalFile)))

    return testFolder

def getMMFolderName(baseLine, mm):
    testType = baseLine.testType
    if testType in [globals.testTypeDict['DM']]:
        mmFolderName = 'Slot {:02d}-{}'.format(mm, baseLine.dm_info['MODULES'][mm])
    elif testType in [globals.testTypeDict['MM']]:
        mmFolderName = baseLine.dm_info['MODULES'][mm]
    elif testType in [globals.testTypeDict['Sensor']]:
        mmFolderName = baseLine.sensor['CZTID'][baseLine.dm.YELLOW_BOX_ACTIVE_ASICS[0]] # Needs to be fixed so that other slots can be used on sensor screener
    else:
        raise ValueError('Invalid Test Type: {}'.format(testType))

    return mmFolderName

def genDeviceDir(baseLine, mm):
    testType = baseLine.testType
    if testType in [globals.testTypeDict['DM']]:
        serialNum = baseLine.dm_info['SERIAL_NUM']
    elif testType in [globals.testTypeDict['MM']]:
        serialNum = baseLine.dm_info['MODULES'][mm]
    elif testType in [globals.testTypeDict['Sensor']]:
        serialNum = baseLine.sensor['CZTID'][baseLine.dm.YELLOW_BOX_ACTIVE_ASICS[0]]
    else:
        raise ValueError('Invalid Test Type: {}'.format(testType))

    if not globals.enableDevMode:
        return os.path.join(globals.server, globals.mfgDirectory, globals.productDirDict[testType],
                            baseLine.dm_info['CUSTOMER'], serialNum)
    else:
        return os.path.join(globals.devModeSettings['Directory'], serialNum)

# def saveLog(ldir, tdir, logFile, baseLine, testStartTime):
#     dmSerialNum = baseLine.dm['SERIAL_NUM']
#
#     testPath = os.path.join(tdir, globals.csvDir, globals.testResultsFolder, dmSerialNum, testStartTime)
#     checkDir(testPath)
#
#     shutil.copy(logFile, os.path.join(testPath, os.path.basename(logFile)))

def saveLog(ldir, testFolder, logFile, baseLine, testStartTime):
    testType = baseLine.testType
    shutil.copy(logFile, os.path.join(testFolder, globals.dirStruct[testType]['perTest']['rawData'], os.path.basename(logFile)))

def storeDumpFiles(tdir, dumpFiles, baseLine):
    dmSerialNum = baseLine.dm_info['SERIAL_NUM']
    testStartTime = baseLine.timestamp
    testPath = os.path.join(tdir, globals.csvDir, globals.testResultsFolder, dmSerialNum, testStartTime)
    checkDir(testPath)

    for dumpFile in dumpFiles:
        shutil.copy(dumpFile, os.path.basename(dumpFile))


def zipdir(path, ziph):
    # ziph is zipfile handle
    for root, dirs, files in os.walk(path):
        for file in files:
            filePath = os.path.join(root, file)
            relPath = os.path.relpath(filePath, path)
            ziph.write(filePath, arcname=relPath)


def appendCsvDirSelective(tdir, pivot, keepCounterList=[], fNameApp='appendedFiles.csv', decimateFactor=0,
                          csvfPath='docs\\csvData\\multiFileAppend\\', pixList=[i for i in range(0, 864)]):
    csvfPath = tdir + csvfPath
    dataFiles = []
    decimateCounter = 0
    if pivot == 1:
        keepCounterList.append('Counter')
    for fname in os.listdir(csvfPath):
        if '.csv' in fname:
            dataFiles.append(fname)
    wh = open(csvfPath + fNameApp, 'wb')
    writer = csv.writer(wh)
    fileCnt = 1
    rowCnt = 1
    for file in dataFiles:
        logger.debug('Appending file: %s, %d/%d' % (file, fileCnt, len(dataFiles)))
        appendFile = csvfPath + "\\" + file
        rh = open(appendFile, 'rb')
        reader = csv.reader(rh)
        if fileCnt != 1 or pivot == 0:
            header = next(reader)
        if pivot == 0 and rowCnt == 1:
            logger.debug('header=%s' % header)
            # counterRange = range(header.index(' EC'), header.index(' SEC0') + 1)
            counterRange = range(header.index(' EC'), header.index('SEC0') + 1)
            totalKeepList = range(counterRange[0])
            keepIdxList = []
            for counter in keepCounterList:
                keepIdxList.append(header.index(counter))
            for keepIdx in keepIdxList:
                totalKeepList.append(keepIdx)
            newHeader = []
            for idx in totalKeepList:
                newHeader.append(header[idx])
            writer.writerow(newHeader)
        for row in reader:
            for pixel in pixList:
                if row[1] == ' %s' % pixel:
                    if pivot == 1:
                        for keepCounter in keepCounterList:
                            if keepCounter in row[3]:
                                if decimateFactor == 0 or (decimateCounter % decimateFactor == 0):
                                    writer.writerow(row)
                    else:
                        newData = []
                        if decimateFactor == 0 or (decimateCounter % decimateFactor == 0):
                            for idx in totalKeepList:
                                newData.append(row[idx])
                            writer.writerow(newData)
            decimateCounter += 1
            rowCnt += 1
        rh.close()
        fileCnt += 1
    wh.close()


def readAsicTemp(useDefaultOffset=True, singleRead=False):
    # The useDefaultOffset parameter is needed to ensure compatibility with the existing calls. In future it should be
    # removed with setAsicTempOffset() being used in future for setting the offset
    #
    # The singleRead parameter is needed to ensure compatibility with the existing calls. In future is should be removed
    # with enableAsicTemp() and disableAsicTemp() being used in the future for enabling and disabling the ASIC's
    # temperature sensor
    #
    # Todo: Remove the offset from asicTemp since it can be compensated for with the ASIC's temperature offset.

    if useDefaultOffset:
        setAsicTempOffset(130)

    #Redlean suggested to enable it and don't turn off, also make sure at lease 1.0sec to get new value updated ZYE 08/23/2019
    enableAsicTemp()
    if singleRead:
        sleep(2.0)
    else:
        #I am not sure if I should do 1 sec but Brent doesn't want more wait time! ZYE 08/23/2019
        sleep(0.01)
    
    tempCode = int(readReg('0x44', 1, 8, 0), 0)
    tempCode = tempCode - 512 if tempCode > 256 else tempCode
    asicTemp = 1.1603 * tempCode - 3.9711
    cztTemp = 1.1538 * tempCode - 12.659
    asicTemp = '%2.1f' % asicTemp
    cztTemp = '%2.1f' % cztTemp
    tempCode = '%d' % tempCode
    return [tempCode, asicTemp, cztTemp]


def setAsicTempOffset(offset):
    # This function writes the 9-bit signed int offset to the ASIC. The offset is stored across 2 registers.
    # Bit 0 of the address 0xA2 is the MSB of the offset. Bits 8 - 15 of 0xA3 are the 8 LSBs of the offset

    if not -256 <= offset <= 255:
        raise ValueError('Offset {} was outside the range of -256 and 255'.format(offset))

    offsetHexString = hex(offset)                   # Conversion to string needed to use the extractBits function

    offsetMSB = extractBits(offsetHexString, 8, 8)
    offsetLSBs = extractBits(offsetHexString, 7, 0)

    writeReg('0xa3', 15, 8, hex(offsetLSBs), 1, 1)  # dev_curie.temp_offset LSBs [0 - 7]
    writeReg('0xa2', 0, 0, hex(offsetMSB), 1, 1)    # dev_curie.temp_offset MSB


def readAsicTempOffset():
    offsetLSB = int(readReg('0xa3', 1, 15, 8), 0)
    offsetMSB = int(readReg('0xa2', 1, 0, 0), 0)

    offset = offsetLSB

    if offsetMSB == 1:
        offset -= 256
    return offset


def enableAsicTemp():
    writeReg('0x44', 15, 15, '0x0001', 1, 1)  # dev_curie.tsense_enable = 1


def disableAsicTemp():
    writeReg('0x44', 15, 15, '0x0000', 1, 1)  # dev_curie.tsense_enable = 0


def readDmVmon(railList):
    if type(railList) == str:
        railList = [railList]
    VMonList = []
    writeReg('0x00a1e', 3, 3, '0x0001')                             # VTMON enable
    waitForBit('0x00a1e', 1, True)                                  # wait for MON_DATA_VALID
    for rail in railList:
        if rail == '1.8VA_1':
            VMonList.append(hex2dec(readReg('0x00a10', 0, 11, 0)) * 0.002)
        elif rail == '1.8VA_2':
            VMonList.append(hex2dec(readReg('0x00a11', 0, 11, 0)) * 0.002)
        elif rail == '1.2VA_1':
            VMonList.append(hex2dec(readReg('0x00a12', 0, 11, 0)) * 0.002)
        elif rail == '1.2VA_2':
            VMonList.append(hex2dec(readReg('0x00a13', 0, 11, 0)) * 0.002)
        elif rail == '1.2VA_3':
            VMonList.append(hex2dec(readReg('0x00a14', 0, 11, 0)) * 0.002)
        elif rail == '1.2VA_4':
            VMonList.append(hex2dec(readReg('0x00a15', 0, 11, 0)) * 0.002)
        elif rail == '1.2V':
            VMonList.append(hex2dec(readReg('0x00a16', 0, 11, 0)) * 0.002)
        elif rail == '1.8V':
            VMonList.append(hex2dec(readReg('0x00a17', 0, 11, 0)) * 0.002)
        elif rail == '2.5V':
            VMonList.append(hex2dec(readReg('0x00a18', 0, 11, 0)) * 0.002)
        elif rail == '2.5V_DAC':
            VMonList.append(hex2dec(readReg('0x00a19', 0, 11, 0)) * 0.002)
        elif rail == 'ALL':
            VMonList.append(hex2dec(readReg('0x00a10', 0, 11, 0)) * 0.002)
            VMonList.append(hex2dec(readReg('0x00a11', 0, 11, 0)) * 0.002)
            VMonList.append(hex2dec(readReg('0x00a12', 0, 11, 0)) * 0.002)
            VMonList.append(hex2dec(readReg('0x00a13', 0, 11, 0)) * 0.002)
            VMonList.append(hex2dec(readReg('0x00a14', 0, 11, 0)) * 0.002)
            VMonList.append(hex2dec(readReg('0x00a15', 0, 11, 0)) * 0.002)
            VMonList.append(hex2dec(readReg('0x00a16', 0, 11, 0)) * 0.002)
            VMonList.append(hex2dec(readReg('0x00a17', 0, 11, 0)) * 0.002)
            VMonList.append(hex2dec(readReg('0x00a18', 0, 11, 0)) * 0.002)
            VMonList.append(hex2dec(readReg('0x00a19', 0, 11, 0)) * 0.002)
    writeReg('0x00a1e', 3, 3, '0x0000')                             # VTMON disable
    return VMonList


def readDmTempMon(tempMonList):
    if type(tempMonList) == str:
        tempMonList = [tempMonList]
    temperatureList = []
    writeReg('0x00a1e', 15, 0, '0x0008')                             # VTMON enable
    waitForBit('0x00a1e', 1, True)                                  # wait for MON_DATA_VALID
    sleep(1)
    for tempMon in tempMonList:
        if tempMon == 'MON_TEMP_1':
            temperatureList.append(hex2dec(readReg('0x00a1a', 0, 11, 0)) * 0.0625)
        elif tempMon == 'MON_TEMP_2':
            temperatureList.append(-46.85 + 175.72 * (hex2dec(readReg('0x00a1b', 0, 13, 0))/2. ** 14))
        elif tempMon == 'MON_HUMIDITY':
            temperatureList.append(-6 + 125 * (hex2dec(readReg('0x00a1c', 0, 11, 0)) / 2. ** 12))
        elif tempMon == 'ALL':
            temperatureList.append(hex2dec(readReg('0x00a1a', 0, 11, 0)) * 0.0625)
            temperatureList.append(-46.85 + 175.72 * (hex2dec(readReg('0x00a1b', 0, 13, 0)) / 2. ** 14))
            temperatureList.append(-6 + 125 * (hex2dec(readReg('0x00a1c', 0, 11, 0)) / 2. ** 12))
    writeReg('0x00a1e', 3, 3, '0x0000')                             # VTMON disable
    return temperatureList


def chkCrcError(clear=1):
    crcErrStat = readReg('0x00a1d', 0, 15, 0)
    if hex2dec(crcErrStat) and clear:
        writeReg('0x00a1d', 15, 0, '0xffff')
    return crcErrStat


def setEssential5(value):
    # set essential5 bits
    # essential5 bits are at asic address 0x4c
    # this is a three bit field with the high bit at bit 15
    # and the low bits are in bits 10:9
    assert value < 8
    highBit = '0x{:04x}'.format(value >> 2)
    lowBits = '0x{:04x}'.format(value & 3)
    writeReg(essential5, 15, 15, highBit, 1, 1)
    writeReg(essential5, 10, 9, lowBits, 1, 1)


def find_delay_unit():
    start_time = clock()
    for y in range(10):
        sleep(0.1)
    elapsed_time = clock() - start_time
    return elapsed_time


def setQtrigLevel(value):
    if value.lower() == '380':
        logger.debug('set qtrig_level to 380aC')
        writeReg('0x18', 5, 4, '0x0000', 1, 1)
    elif value.lower() == '570':
        logger.debug('set qtrig_level to 570aC')
        writeReg('0x18', 5, 4, '0x0001', 1, 1)
    elif value.lower() == '760':
        logger.debug('set qtrig_level to 760aC')
        writeReg('0x18', 5, 4, '0x0002', 1, 1)
    elif value.lower() == '950':
        logger.debug('set qtrig_level to 950aC')
        writeReg('0x18', 5, 4, '0x0003', 1, 1)


def setLeakCompMode(mode, VER_ID):
    if VER_ID < 5:
        du = find_delay_unit()
        if mode in ['passive', 0]:
            logger.debug('Setting passive mode')
            writeReg('0x18', 0, 0, '0x0001', 1, 1)    # dev_curie.mode_disable_rate_control = 1
            writeReg('0x7e', 11, 11, '0x0000', 1, 1)  # dev_curie.en_active2 = 0
            writeReg('0x6a', 1, 1, '0x0000', 1, 1)    # dev_curie.tm0_active2 = 0
            writeReg('0x33', 4, 4, '0x0001', 1, 1)    # dev_curie.en_lkg_calib = 1
            sleep(50 * 200e-6 / du)
            writeReg('0x33', 4, 4, '0x0000', 1, 1)    # dev_curie.en_lkg_calib = 0
        elif mode in ['active', 1]:
            logger.debug('Setting active mode')
            writeReg('0x18', 0, 0, '0x0001', 1, 1)    # dev_curie.mode_disable_rate_control = 1
            writeReg('0x7e', 11, 11, '0x0000', 1, 1)  # dev_curie.en_active2 = 0
            writeReg('0x6a', 1, 1, '0x0000', 1, 1)    # dev_curie.tm0_active2 = 0
            writeReg('0x33', 4, 4, '0x0001', 1, 1)    # dev_curie.en_lkg_calib = 1
            sleep(1 / du)
            writeReg('0x33', 4, 4, '0x0000', 1, 1)    # dev_curie.en_lkg_calib = 0
        elif mode in ['none', 'dis_lkg']:
            logger.debug('Setting dis_lkg mode')
            writeReg('0x7e', 11, 11, '0x0001', 1, 1)  # dev_curie.en_active2 = 1
            writeReg('0x6a', 1, 1, '0x0000', 1, 1)    # dev_curie.tm0_active2 = 0
        elif mode in ['hybrid']:
            logger.debug('Setting hybrid lkg mode')
            writeReg('0x18', 0, 0, '0x0000', 1, 1)    # dev_curie.mode_disable_rate_control = 0
            writeReg('0x7e', 11, 11, '0x0000', 1, 1)  # dev_curie.en_active2 = 0
            writeReg('0x6a', 1, 1, '0x0000', 1, 1)    # dev_curie.tm0_active2 = 0
        elif mode in ['active2_high']:
            logger.debug('Setting active2_high mode')
            writeReg('0x7e', 11, 11, '0x0001', 1, 1)  # dev_curie.en_active2 = 1
            writeReg('0x6a', 1, 1, '0x0001', 1, 1)    # dev_curie.tm0_active2 = 1
            writeReg('0x72', 5, 4, '0x0001', 1, 1)    # dev_curie.active2_tc = 'High'
        elif mode in ['active2_low']:
            logger.debug('Setting active2_low mode')
            writeReg('0x7e', 11, 11, '0x0001', 1, 1)  # dev_curie.en_active2 = 1
            writeReg('0x6a', 1, 1, '0x0001', 1, 1)    # dev_curie.tm0_active2 = 1
            writeReg('0x72', 5, 4, '0x0003', 1, 1)    # dev_curie.active2_tc = 'low'
        else:
            logger.debug('Setting active2_nom mode')
            writeReg('0x7e', 11, 11, '0x0001', 1, 1)  # dev_curie.en_active2 = 1
            writeReg('0x6a', 1, 1, '0x0001', 1, 1)    # dev_curie.tm0_active2 = 1
            writeReg('0x72', 5, 4, '0x0000', 1, 1)    # dev_curie.active2_tc = 'nom'
    else:
        if mode in [globals.lkgActHigh]:
            logger.debug('Setting active2_high mode')
            writeReg('0x33', 9, 7, '0x0000', 1, 1)    # set active2 mode
            writeReg('0x6a', 4, 4, '0x0001', 1, 1)    # active2 mode high
            writeReg('0x6a', 1, 1, '0x0001', 1, 1)    # active2 mode high
        elif mode in [globals.lkgActLow]:
            logger.debug('Setting active2_low mode')
            writeReg('0x33', 9, 7, '0x0000', 1, 1)    # set active2 mode
            writeReg('0x6a', 4, 4, '0x0000', 1, 1)    # active2 mode low
            writeReg('0x6a', 1, 1, '0x0000', 1, 1)    # active2 mode low
        elif mode in ['none', globals.lkgDisabled]:
            logger.debug('Setting dis_lkg mode')
            writeReg('0x33', 9, 7, '0x0005', 1, 1)    # set leakage comp mode to none
        elif mode in [globals.lkgPassive, 0]:
            logger.debug('Setting passive mode')
            writeReg('0x33', 9, 7, '0x0003', 1, 1)  # dev_curie.en_lkg_calib = 0
        else:
            logger.debug('Setting active2_nom mode')
            writeReg('0x33', 9, 7, '0x0000', 1, 1)    # set active2 mode
            writeReg('0x6a', 4, 4, '0x0001', 1, 1)    # active2 mode med
            writeReg('0x6a', 1, 1, '0x0000', 1, 1)    # active2 mode med


def getLeakCompMode():
    mode_disable_rate_control = readReg('0x18', shadow=1, msb=0, lsb=0)
    en_active2 = readReg('0x7e', shadow=1, msb=11, lsb=11)
    tm0_active2 = readReg('0x6a', shadow=1, msb=1, lsb=1)
    active2_tc = readReg('0x72', shadow=1, msb=5, lsb=4)
    if mode_disable_rate_control == '0x0001' and en_active2 == '0x0000' and tm0_active2 == '0x0000':
        mode = 'active'
    elif en_active2 == '0x0001' and tm0_active2 == '0x0000':
        mode = 'dis_lkg'
    elif mode_disable_rate_control == '0x0000' and en_active2 == '0x0000' and tm0_active2 == '0x0000':
        mode = 'hybrid'
    elif en_active2 == '0x0001' and tm0_active2 == '0x0001':
        mode = 'active2'
        if active2_tc == '0x0000':
            mode += '_nom'
        elif active2_tc == '0x0001':
            mode += '_high'
        elif active2_tc == '0x0003':
            mode += '_low'
        else:
            raise Exception('Not a valid active2 time constant')
    else:
        raise Exception('Invalid BLR mode!')
    return mode


def setFbC(capVal, VER_ID):
    if VER_ID < 5:
        if capVal == 'stdgain':
            writeReg('0x5f', 14, 11, '0x0000', 1, 1)
        elif capVal == 'higain':
            writeReg('0x5f', 14, 11, '0x0008', 1, 1)
        elif capVal == 'midgain':
            writeReg('0x5f', 14, 11, '0x000c', 1, 1)
        else:
            raise Exception('Invalid feedback capacitor setting')
    else:
        if capVal == 'stdgain':
            writeReg('0x5f', 7, 4, '0x000e', 1, 1)
        elif capVal == 'higain':
            writeReg('0x5f', 7, 4, '0x0002', 1, 1)
        elif capVal == 'midgain':
            writeReg('0x5f', 7, 4, '0x0000', 1, 1)
        else:
            raise Exception('Invalid feedback capacitor setting')


def getFbC(VER_ID):
    if VER_ID < 5:
        fbc = readReg('0x5f', shadow=1, msb=14, lsb=11)
        if fbc == '0x0000':
            capVal = 'stdgain'
        elif fbc == '0x0008':
            capVal = 'higain'
        elif fbc == '0x000c':
            capVal = 'midgain'
        else:
            raise Exception('Invalid feedback capacitor setting')
    else:
        fbc = readReg('0x5f', shadow=1, msb=7, lsb=4)
        if fbc == '0x000e':
            capVal = 'stdgain'
        elif fbc == '0x0002':
            capVal = 'higain'
        elif fbc == '0x0000':
            capVal = 'midgain'
        else:
            raise Exception('Invalid feedback capacitor setting')
    return capVal


def getHpf(VER_ID):
    filterSetting = None
    if VER_ID < 5:
        hpf = int(readReg('0x69', 1), 0)
        if is_set(hpf, 4):
            hpfb4 = 1
        else:
            hpfb4 = 0
        if is_set(hpf, 5):
            hpfb5 = 1
        else:
            hpfb5 = 0
        if is_set(hpf, 6):
            hpfb6 = 1
        else:
            hpfb6 = 0
        if is_set(hpf, 7):
            hpfb7 = 1
        else:
            hpfb7 = 0
        val = (hpfb7 << 3) | (hpfb6 << 2) | (hpfb5 << 1) | hpfb4
        if val == 2:
            filterSetting = 5
        elif val == 10:
            filterSetting = 10
        elif val == 13:
            filterSetting = 16
        elif val == 14:
            filterSetting = 20
        else:
            logger.error('Invalid HPF, Getting: 0x%x' % val)
    else:
        hpf = int(readReg('0x69', shadow=1, msb=7, lsb=4), 0)
        if hpf == 0:
            filterSetting = 4.4
        elif hpf == 1:
            filterSetting = 5
        elif hpf == 3:
            filterSetting = 7
        elif hpf == 5:
            filterSetting = 11
        elif hpf == 6:
            filterSetting = 16
        elif hpf == 7:
            filterSetting = 26
        elif hpf == 9:
            filterSetting = 2.5
        else:
            logger.error('Invalid HPF, Getting: 0x%x' % hpf)
    return filterSetting


def setHpf(hpf, VER_ID):
    logger.debug('setHpf')
    if VER_ID < 5:
        if hpf == 5:
            writeReg('0x69', 7, 4, '0x0002', 1, 1)
        elif hpf == 10:
            writeReg('0x69', 7, 4, '0x000a', 1, 1)
        elif hpf == 16:
            writeReg('0x69', 7, 4, '0x000d', 1, 1)
        elif hpf == 20:
            writeReg('0x69', 7, 4, '0x000e', 1, 1)
        else:
            logger.error('Invalid HPF Setting: %d' % hpf)
            raise ValueError
    else:
        if hpf == 4.4:
            writeReg('0x69', 7, 4, '0x0000', 1, 1)
        elif hpf == 5:
            writeReg('0x69', 7, 4, '0x0001', 1, 1)
        elif hpf == 7:
            writeReg('0x69', 7, 4, '0x0003', 1, 1)
        elif hpf == 11:
            writeReg('0x69', 7, 4, '0x0005', 1, 1)
        elif hpf == 16:
            writeReg('0x69', 7, 4, '0x0006', 1, 1)
        elif hpf == 26:
            writeReg('0x69', 7, 4, '0x0007', 1, 1)
        elif hpf == 2.5:
            writeReg('0x69', 7, 4, '0x0009', 1, 1)
        else:
            logger.error('Invalid HPF Setting: %2.1f' % hpf)
            raise ValueError
    return


def getAcsMode():
    return int(readReg('0x17', 1, 5, 4), 0)


def setAcsMode(acsMode):
    logger.debug('setAcsMode')
    if acsMode == 0:
        writeReg('0x17', 5, 4, '0x0000', 1, 1)
    elif acsMode == 1:
        writeReg('0x17', 5, 4, '0x0001', 1, 1)
    elif acsMode == 2:
        writeReg('0x17', 5, 4, '0x0010', 1, 1)
    else:
        raise ValueError('Undefined ACS mode')


def setAcsWindow(acs1, acs2, VER_ID):
    if VER_ID < 5:
        if acs1 in ['w', 'W']:
            writeReg('0x7a', 6, 4, '0x0000', 1, 1)
        elif acs1 in ['1.2w', '1.2W']:
            writeReg('0x7a', 6, 4, '0x0001', 1, 1)
        elif acs1 in ['1.6w', '1.6W']:
            writeReg('0x7a', 6, 4, '0x0002', 1, 1)
        elif acs1 in ['2.5w', '2.5W']:
            writeReg('0x7a', 6, 4, '0x0003', 1, 1)
        else:
            logger.error('Invalid ACS1 Window Setting: %s' % acs1)
        if acs2 in ['w', 'W']:
            writeReg('0x7a', 2, 0, '0x0000', 1, 1)
        elif acs2 in ['1.2w', '1.2W']:
            writeReg('0x7a', 2, 0, '0x0001', 1, 1)
        elif acs2 in ['1.6w', '1.6W']:
            writeReg('0x7a', 2, 0, '0x0002', 1, 1)
        elif acs2 in ['2.5w', '2.5W']:
            writeReg('0x7a', 2, 0, '0x0003', 1, 1)
        else:
            logger.error('Invalid ACS2 Window Setting: %s' % acs2)
    else:
        logger.error("ASIC revision 5 or higher only takes one arguement for the ACS Window Setting")


acsDict = {
    'w': '0x0000',
    '2w': '0x0001',
    '3w': '0x0002',
    '4w': '0x0003',
    '5w': '0x0004',
    '6w': '0x0005',
    '7w': '0x0006',
    '8w': '0x0007'}


def setACSWindowRevB(acs, VER_ID):
    logger.debug('setACSWindowRevB')
    if VER_ID >= 5:
        try:
            writeReg('0x79', 15, 13, acsDict[acs], 1, 1)
        except KeyError:
            logger.error("Invalid ACS Window setting: {}".format(acs))
    else:
        logger.error("Revision 4 and under requires ACSW1 and ACSW2")
        raise RuntimeError


def getAcsWindow(VER_ID):
    if VER_ID >= 5:
        acswCode = readReg('0x79', 1, 15, 13)
        for acsw, code in acsDict.items():
            if code == acswCode:
                return acsw, "No ACSW2 for Rev > 4"
        logger.error("Unable to read ASC Window Setting for code: {}".format(acswCode))
        return "", ""

    else:
        r122 = int(readReg('0x7a', 1), 0)  # ACS Window first and second half
        [r122b6, r122b5, r122b4, r122b2, r122b1, r122b0] = [0, 0, 0, 0, 0, 0]
        if is_set(r122, 6):
            r122b6 = 1
        if is_set(r122, 5):
            r122b5 = 1
        if is_set(r122, 4):
            r122b4 = 1
        if is_set(r122, 2):
            r122b2 = 1
        if is_set(r122, 1):
            r122b1 = 1
        if is_set(r122, 0):
            r122b0 = 1
        acs1h = ((r122b6 << 2) | (r122b5 << 1)) | r122b4
        if acs1h == 0:
            acs1 = 'w'
        elif acs1h == 1:
            acs1 = '1.2w'
        elif acs1h == 2:
            acs1 = '1.6w'
        elif acs1h == 3:
            acs1 = '2.5w'
        else:
            acs1 = 'NA'
            logger.error('Invalid ACS1 window Setting: Reg122[6:4] = 0x%04x' % acs1h)
        acs2h = ((r122b2 << 2) | (r122b1 << 1)) | r122b0
        if acs2h == 0:
            acs2 = 'w'
        elif acs2h == 1:
            acs2 = '1.2w'
        elif acs2h == 2:
            acs2 = '1.6w'
        elif acs2h == 3:
            acs2 = '2.5w'
        else:
            acs2 = 'NA'
            logger.error('Invalid ACS2 window Setting: Reg122[2:0] = 0x%04x' % acs2h)
        return [acs1, acs2]


def setLftMode(mode):
    logger.debug('setLftMode')
    if mode.lower() == 'lft':
        writeReg('0x17', 8, 8, '0x0001', 1, 1)  # enable LFT_MODE
    elif mode.lower() == 'nonlft':
        writeReg('0x17', 8, 8, '0x0000', 1, 1)  # disable LFT_MODE
    else:
        raise Exception('Invalid LFT setting: %s' % mode)


def getLftMode():
    lft = int(readReg('0x17', 1), 0)
    if is_set(lft, 8):
        mode = 'lft'
    else:
        mode = 'nonlft'
    return mode


def getModeBinAlign():
    binAlign = int(readReg('0x17', 1), 0)
    if is_set(binAlign, 0):
        mode = '0x0001'
    else:
        mode = '0x0000'
    return mode


def setModeBinAlign(mode):
    if mode.lower() == 'on':
        logger.debug('set mode_bin_align ON')
        val = '0x0001'
    else:
        logger.debug('set mode_bin_align OFF')
        val = '0x0000'
    writeReg('0x17', 0, 0, val, 1, 1)  # set mode_bin_align to value


def enableSet18Chain(mode):
    if mode.lower() == 'enable':
        writeReg('0x3c', 8, 8, '0x0001')  # Enable Set18 Chain
    else:
        writeReg('0x3c', 8, 8, '0x0000')  # Disable Set18 Chain


def enableCalVtrigFreerun(enable):
    if enable:
        writeReg('0x0A25', 1, 1, '0x0001', 0, 1)
    else:
        writeReg('0x0A25', 1, 1, '0x0000', 0, 1)


def enableSuppressVtrigOnSPIEN(enable):
    if enable:
        writeReg('0x0A25', 0, 0, '0x0001', 0, 1)
    else:
        writeReg('0x0A25', 0, 0, '0x0000', 0, 1)


def setCalVtrigPeriod(period):
    # Converts period in seconds to number of master clock cycles then sends that to the FPGA
    # to set the internal VTRIG period

    mclk = 140e6                        # Master clock of 140 MHz
    n_mclk_cycles = int(round(period * mclk))

    if n_mclk_cycles > (2 ** 32 - 1):
        logger.error('Period in setCalVtrigPeriod is too long')
    else:
        lsb = n_mclk_cycles & 0x0000FFFF
        msb = (n_mclk_cycles & 0xFFFF0000) >> 16

        #print(hex(lsb))
        #print(hex(int(msb)))

        writeReg('0x0A26', 15, 0, hex(int(lsb)), 0, 1)
        writeReg('0x0A27', 15, 0, hex(int(msb)), 0, 1)


def calVtrig50PCT(enable):
    if enable:
        writeReg('0x0A25', 2, 2, '0x0001', 0, 1)
    else:
        writeReg('0x0A25', 2, 2, '0x0000', 0, 1)


def enableExtraCountFix(viewPeriod, mclkFreq=140e6, preTrig=16, mclksInAWorkCycle=8000):
    # Temporary code for extra counts workaround
    viewPeriod /= 1e6

    clkMultiple = int(viewPeriod * mclkFreq / mclksInAWorkCycle)
    nMclks = clkMultiple * mclksInAWorkCycle + preTrig
    mclkActive = nMclks / mclkFreq

    logger.debug('Conv Period: {} s'.format(viewPeriod))
    logger.debug('MCLK Active Period {} s'.format(mclkActive))

    setPreTrigMCLKDelay(preTrig)
    setMCLKVtrigPeriod(nMclks)
    enableMCLKWorkaround(True)

    logger.debug('0x0A25_L read back: {}'.format(readReg('0x0A25', 0, 7, 0)))
    logger.debug('0x0A25_H read back: {}'.format(readReg('0x0A25', 0, 15, 8)))
    logger.debug('0x0A28 read back: {}'.format(readReg('0x0A28')))
    logger.debug('0x0A29 read back: {}'.format(readReg('0x0A29')))


def setMCLKVtrigPeriod(nMclks):
    if nMclks >= 2 ** 32:
        raise ValueError("MCLK VTRIG Period of {} is too long".format(nMclks))
    nMclkLSB = nMclks & 0x0000FFFF
    nMclkMSB = int((nMclks & 0xFFFF0000) >> 16)

    logger.debug('Data sent to 0x0A28: {}'.format(hex(nMclkLSB)))
    logger.debug('Data sent to 0x0A29: {}'.format(hex(nMclkMSB)))
    logger.debug('MCLK VTRIG Period: {}'.format(nMclkLSB + nMclkMSB * 2 ** 16))

    writeReg('0x0A28', 15, 0, hex(int(nMclkLSB)), 0, 1)
    writeReg('0x0A29', 15, 0, hex(int(nMclkMSB)), 0, 1)


def setPreTrigMCLKDelay(delay):
    if delay >= 2 ** 8:
        raise ValueError("Pre Trigger MCLK Delay of {} is too long".format(delay))
    writeReg('0x0A25', 15, 8, hex(delay))


def enableMCLKWorkaround(enable):
    if enable:
        writeReg('0x0A25', 2, 2, '0x0001', 0, 1)  # MCLK_WORKAROUND_EN = 1
    else:
        writeReg('0x0A25', 2, 2, '0x0000', 0, 1)  # MCLK_WORKAROUND_EN = 0


def selDataOutputByMM(MMsToActivate, asicsPerMM, DMType='Other'):
    if isinstance(MMsToActivate, int):
        MMsToActivate = [MMsToActivate]

    if len(MMsToActivate) > 1:
        raise RuntimeError('Only one MM at time for data output is supported')

    writeReg('0x00015', 15, 0, '0x0000', 0, 1)  # clear selected ASIC0 & 1 for DM_Adapter data output

    for MM in MMsToActivate:
        asics = [MM * asicsPerMM + i for i in range(asicsPerMM)]
        for asic in asics:
            writeReg('0x00015', asic, asic, '0x0001', 0, 1)  # enable ASIC0 & 1 for DM_Adapter data output
    logger.info('DM_adapter SelectMM setting {}'.format(readReg('0x00015')))


def selDataOutputByAsic(activeAsicNums, passiveAsicNums):
    writeReg('0x00015', 15, 0, '0x0000', 0)  # clear selected ASIC0 & 1 for DM_Adapter data output

    for asic in activeAsicNums:
        writeReg('0x00015', asic, asic, '0x0001', 0)  # enable ASIC0 & 1 for DM_Adapter data output
    for asic in passiveAsicNums:
        writeReg('0x00015', asic, asic, '0x0001', 0)  # enable ASIC0 & 1 for DM_Adapter data output
    logger.debug('active ASIC numbers {}'.format(activeAsicNums))
    logger.debug('passive ASIC numbers {}'.format(passiveAsicNums))
    logger.debug('DM_adapter SelectMM setting {}'.format(readReg('0x00015')))


def enableHV(enable):
    if enable:
        writeReg('0x00016', 7, 7, '0x0001', 0, 1)          # Enables both HV Channels (Register address will change)
    else:
        writeReg('0x00016', 7, 7, '0x0000', 0, 1)


HVRegDict = {'HV0': {'LOAD_BIT': 5, 'DONE_BIT': 4, 'DAC_REG': '0x00017'},
             'HV1': {'LOAD_BIT': 2, 'DONE_BIT': 1, 'DAC_REG': '0x00018'}}


def setHV(voltage, writeTimeOut=1):
    # voltage is in volts

    minVoltage = 0
    maxVoltage = 1270

    if not minVoltage <= voltage <= maxVoltage:
        ValueError('Voltage out of range. Please enter a voltage between {} and {}'.format(minVoltage, maxVoltage))

    HVDACSetting = int(round(float(voltage) / maxVoltage * (2 ** 12 - 1)))

    for hv in HVRegDict:
        writeReg(HVRegDict[hv]['DAC_REG'], 11, 0, hex(HVDACSetting), 0, 1)
        writeReg('0x00016', HVRegDict[hv]['LOAD_BIT'], HVRegDict[hv]['LOAD_BIT'], '0x0000', 0, 1)
        writeReg('0x00016', HVRegDict[hv]['LOAD_BIT'], HVRegDict[hv]['LOAD_BIT'], '0x0001', 0, 1)
        currentTime = time()
        while not is_set(int(DM.SPI_read('0x00016'), 0), HVRegDict[hv]['DONE_BIT']) and (time() - currentTime < writeTimeOut):
            pass

        if not is_set(int(DM.SPI_read('0x00016'), 0), HVRegDict[hv]['DONE_BIT']):
            raise RuntimeError('Write to {} DAC Timed Out'.format(hv))

        writeReg('0x00016', HVRegDict[hv]['LOAD_BIT'], HVRegDict[hv]['LOAD_BIT'], '0x0000', 0, 1) # Must be dropped low to free SPI bus


def readHV():
    HV0Voltage = int(round(float.fromhex(readReg(HVRegDict['HV0']['DAC_REG'], 0, 11, 0)) * 1270 / 4095))
    HV1Voltage = int(round(float.fromhex(readReg(HVRegDict['HV1']['DAC_REG'], 0, 11, 0)) * 1270 / 4095))
    return HV0Voltage, HV1Voltage


def setHVPDown(setting, HVDACNum):
    if HVDACNum not in (0, 1):
        raise ValueError('Invalid HV DAC Number: {}'.format(HVDACNum))

    PDownReg = '0x00017' if HVDACNum == 0 else '0x00018'
    writeReg(PDownReg, 13, 12, hex(setting), 0, 1)


def readHVPDown(HVDACNum):
    if HVDACNum not in (0, 1):
        raise ValueError('Invalid HV DAC Number: {}'.format(HVDACNum))

    PDownReg = '0x00017' if HVDACNum == 0 else '0x00018'
    return int(readReg(PDownReg, 0, 13, 12), 0)


def enableHVMon(enable):
    if enable:
        writeReg('0x00016', 6, 6, '0x0001', 0, 1)       # Enables both HV Channels (Register address will change)
        writeReg('0x00016', 3, 3, '0x0001', 0, 1)
    else:
        writeReg('0x00016', 6, 6, '0x0000', 0, 1)
        writeReg('0x00016', 3, 3, '0x0000', 0, 1)


def readHVMon():
    hv0Mon = readReg('0x00016', 0, 3, 3)
    hv1Mon = readReg('0x00016', 0, 6, 6)
    return hv0Mon, hv1Mon


def restoreHVMon(hv0Mon, hv1Mon):
    writeReg('0x00016', 3, 3, hv0Mon, 0, 1)
    writeReg('0x00016', 6, 6, hv1Mon, 0, 1)

def enableADCVREF():
    # Enable DFT ADC voltage reference.
    writeReg('0x00014', 15, 0, '0x8045', 0, 1)

def getAzeroTime():
    # Calculates the auto-zero period and returns it in microseconds
    mclk = 140e6
    tgClkDiv = getTgClkDiv()
    azero = getAzeroWidth()

    azeroTime = int(round(tgClkDiv / mclk * azero * 1e6))
    # logger.info("TG CLK DIV: {}\nAZERO_WIDTH: {}\nAZERO_TIME: {}".format(tgClkDiv, azero, azeroTime))
    return azeroTime


def getAzeroWidth():
    # Returns the auto zero setting in TGCLK cycles
    azeroSetting = int(readReg('0x32', shadow=1, msb=7, lsb=6), 0)

    azeroWidthDict = {0: 140,
                      1: 70,
                      2: 280,
                      3: 560}

    return azeroWidthDict.get(azeroSetting, azeroWidthDict[3])


def getTgClkDiv():
    tgClkDivSetting = int(readReg('0x31', shadow=1, msb=5, lsb=4), 0)

    tgClkDivDict = {0: 1,
                    1: 2,
                    2: 3,
                    3: 4}

    return tgClkDivDict.get(tgClkDivSetting, tgClkDivDict[3])


def gitLocalRevHash():
    return subprocess.check_output(['git', 'rev-parse', 'HEAD']).strip('\n')


def gitCloudRevHash():
    return subprocess.check_output(['git', 'ls-remote', 'https://bitbucket.org/redlenonly/dm-tp/', 'HEAD']).strip('\tHEAD\n')


def checkGitLocalVsCloud():
    if gitLocalRevHash() != gitCloudRevHash():
        raise gitError('Hashes do not match')


class gitError(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)
