########################################################################################################################
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
########################################################################################################################

import socket
import logging

import globals

from pcct.buffer import UdpBuffer, ZynqBuffer


logger = logging.getLogger(__name__)


def startBuffer(busy_timeout=15, address='192.168.1.23', port=4):
    if globals.buffer is not None:
        globals.buffer.stop()

    try:
        zynq_buffer = ZynqBuffer(address=address, port=port)
        zynq_buffer.start()
        zynq_buffer.reset()
        zynq_buffer.reset_detector(power_cycle=True, busy_wait=busy_timeout)

        return zynq_buffer

    except socket.error:
        udp_buffer = UdpBuffer(address=address, port=port)
        globals.buffer = udp_buffer
        udp_buffer.start()
        udp_buffer.reset()
        udp_buffer.reset_detector()

        return udp_buffer
