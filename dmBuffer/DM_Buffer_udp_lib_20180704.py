########################################################################################################################
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
########################################################################################################################

## DM_Buffer_udp_lib_20180704
#
# Revisions:
# - changed PL_REV.
# - revised dump_to_file function
# - added dump_buffer function from Lyndon L.'s test program
# - added imports used in Lyndon L.'s test program
#
# -- [rc 080217] changed string search in dump_to_file() and dump_buffer()
#                to limit itself to the beginning of the packet
#                to avoid falsely finding the string inside actual ASIC data.
#                Searching the entire packet data caused buffer to crash
#                prematurely when random data matched the special handshake
#                command strings "EOF", "END', "ERR", or "BRK"
# -- [rc 101517] changed socket.recv() to socket.recvfrom() to get up to RX_DGRAM_SIZE
#                bytes without blocking and timing out the socket.
#
# -- [rc 111117] added new dump_buffer() routine to collect unfragmented UDP packets until EOB
#                flag. This does large data buffer send without IP layer fragmentation (frag
#                done at application layer)
#
# -- [rc 010918] modified dump_buffer() routine to parse recv buffer in case of multiple appended pkts.
#                Removed support for unfragmented UDP packets + EOB flag.
#
# -- [rc 011118] modified dump_buffer() routine to send "D 0xE" NAK command in event of timeout.
#                DM_Buffer server code modified to support retransmission of buffer on reception of NAK.
#
# -- [rc 022118] modified dump_buffer() routine to add Clinical2 buffer sizes/remainders of UDP packets.
#               
#
# -- [rc 070318] modified dump_buffer() routine to add Remainder buffer sizes of UDP packets for all options
#                of N_counters and accumulation/non-accumulation modes.
#               
#
import sys
import time
import socket
import logging

import globals

# Compatible versions
LIB_REV = '20170509'
#APP_REV = 0x17050900  # original DM_Buffer version from DA-I
#PL_REV  = 0x17050900
#APP_REV = 0x17101300  # Oct 13/18 DM_Buffer that adds NAK
#PL_REV  = 0x17101300
APP_REV = 0x18010900   # Clincal 2/B & DM_adapter modes support only
PL_REV  = 0x18031600

## NOTE: Address offsets and bit indices are hardcoded.
## At this time, Python and Application Software parameters are
## kept in sync manually.

# Base addresses in hex numbers, not strings.
SYS_BASE_ADDR       = 0x43C00000
SPI_BASE_ADDR       = 0x43C01000
VTRIG_BASE_ADDR     = 0x43C02000
TEST_BASE_ADDR      = 0x43C03000
TEST_RAM0_BASE_ADDR = 0x43C04000
TEST_RAM1_BASE_ADDR = 0x43C08000
TEST_RAM2_BASE_ADDR = 0x43C0C000
TEST_RAM3_BASE_ADDR = 0x43C10000
DATA_BASE_ADDR      = 0x43C14000
TEST_BUFF_BASE_ADDR = 0x43C20000
DMA_BASE_ADDR       = 0x80400000

APP_CONTROL_ADDR = 0x43C1403C

# Address in OCM used for CPU0 and CPU1 messages
EXIT_FSM_ADDR   = 0xFFFF0020
NUM_VIEW_BYTES  = 0xFFFF0024
TOTAL_NUM_BYTES = 0xFFFF002C

ERR_FLAGS = 0xFFFF0054

## Misc parameters
BLOCK_LENGTH            = 24    # CRC Block Length in bytes for DM Adapter
PREAMBLE                = '0xFEDC'  # String expected for preamble
DEFAULT_VTRIG_PERIOD    = 0x1C041C04
REFERENCE_FREQ          = 35.0

## Estimated download rates in Windows 7 (used for calculating estimated download times)
## Faster under Linux Mint 17.3
EST_BINARY_RATE   = 8500.0    ## KBytes per sec (binary download)(about 10.0 MBs under Linux).
EST_ASCII_RATE    = 1850.0    ## KBytes per sec (ASCII download)(about 1.9 MBs under Linux).
EST_ASCII_NL_RATE =  970.0    ## KBytes per sec (ASCII download with \n added every 4 lanes).

## Datagram memory parameters on DM Buffer (APP_REV = 0x16111000)
NUM_DGRAM_WORDS     = 2928  
DGRAM_SIZE          = (NUM_DGRAM_WORDS*8) ## =23424
S2MM_BUFFER_LENGTH  = (NUM_DGRAM_WORDS*4) ## =11712

NUM_S2MM_BUFFS      = 80000
TOTAL_DDR_BUFF_SIZE = (NUM_S2MM_BUFFS * S2MM_BUFFER_LENGTH)

## Constants for remainder Buffer length in various View frames
# ... for non_accumulated
#  Packet Size = ((N_counters*12*36 + 1 Preamble)*2 bytes/word + 1 byte ViewNum)*4 datalanes
# ... for accumulated
#  Packet Size = ((N_counters*12*36 + 1 Preamble)*4 bytes/word + 4 byte ViewNum)*4 datalanes
#
#  Packet is segmented into S2MM_BUFFER_LENGTH chunks, with last packet containing remainder
#  #S2MM_BUFFER_LENGTH packets = int(packet_size/S2MM_BUFFER_LENGTH)
#  Remainder_size = mod(packet_size, S2MM_BUFFER_LENGTH)
#
# N_Counters    Accum   Packet_length   #S2MM_buf   Remainder
#    13          NO        44940           3          9804
#    13          YES       89888           7          7904
#    12          NO        41484           3          6348
#    12          YES       82976           7          992
#    7           NO        24204           2          780
#    7           YES       48416           4          1568
#    6           NO        20748           1          9036
#    6           YES       41504           3          6368
#    1           NO        3468            0          3468
#    1           YES       6944            0          6944
# 12 (Clinical2) NO        40076           3          4940
#
# Remainder definitions (must be sorted in decreasing magnitude)
#
REMAINDER_BUF_LEN1  = 9804   ## N_ctr=13     Accum= NO  Pkt_len= 44940 
REMAINDER_BUF_LEN2  = 9036   ## N_ctr=6      Accum= NO  Pkt_len= 20748 
REMAINDER_BUF_LEN3  = 7904   ## N_ctr=13     Accum= YES Pkt_len= 89888 
REMAINDER_BUF_LEN4  = 6944   ## N_ctr=1      Accum= YES Pkt_len= 6944  
REMAINDER_BUF_LEN5  = 6368   ## N_ctr=6      Accum= YES Pkt_len= 41504 
REMAINDER_BUF_LEN6  = 6348   ## N_ctr=12     Accum= NO  Pkt_len= 41484 
REMAINDER_BUF_LEN7  = 4940   ## N_ctr=12CL2  Accum= NO  Pkt_len= 40076 
REMAINDER_BUF_LEN8  = 3468   ## N_ctr=1      Accum= NO  Pkt_len= 3468  
REMAINDER_BUF_LEN9  = 1568   ## N_ctr=7      Accum= YES Pkt_len= 48416 
REMAINDER_BUF_LEN10 = 992    ## N_ctr=12     Accum= YES Pkt_len= 82976 
REMAINDER_BUF_LEN11 = 780    ## N_ctr=7      Accum= NO  Pkt_len= 24204 
HS_PKT_LEN          = 4      ## handshake command packets length

#define MAX_UDP_SNGL_FRAME		1464
MAX_UDP_SNGL_FRAME  = 1464

MAX_MSG_SIZE = 1024  ## For UDP commands and messages
MAX_DGRAM_SIZE = 16*1464  ## (=23424) For UDP dump payload
RX_DGRAM_SIZE = DGRAM_SIZE + 6 ## Add 6 bytes for 3 ASCII control bits
MAX_RX_DGRAM_SIZE = 65536   ## 64KB 

## Autozero time added to VTRIG period in usec
AUTOZERO = 2.0

#--- Logging Object --------------
logger = logging.getLogger(__name__)

#----------------------------------
def hexstr2bits(hexstr, num_bits):
    "Input number as a Hex string and return converted to specified number of bits."
    formattype = "{0:0"+str(num_bits)+"b}"
    return(formattype.format(int(hexstr, 16)))

#----------------------------------
def bits2hex(bits_str):
    "Convert bit string to 8 digital hex string."
    if (len(bits_str)<=32):
        return(padhex8('0x%x' % int(bits_str, 2)).lower())  # Python uses lower case for hex characters
    else:
        logger.error('ERROR: bits2hex: input longer than 32 bits.')
        sys.exit(1)
    
#----------------------------------
def padhex8(strin):
    "Ensure hex number string always 0x + 8 hex digits."
    if (len(strin)<=10):
        return('0x' + strin[2:].zfill(8))
    else:
        logger.error('ERROR: padhex8: input longer than 8 hex digits: %s' %strin)
        sys.exit(1)
       
#----------------------------------
def padbin16(strin):
    "Ensure binary number string always 0b + 16 bits."
    return('0b' + strin[2:].zfill(16))
    if (len(strin)<=18):
        return('0b' + strin[2:].zfill(16))
    else:
        logger.error('ERROR: padbin16: input longer than 16 bits.')
        sys.exit(1)

#----------------------------------
def reverse_str(strin):
    "Reverse input string."
    return(strin[::-1])

#----------------------------------
def get_msg_bits(strin):
    "Convert hex string digits to reversed binary string"
    return(reverse_str(hexstr2bits(strin, 32)))

#----------------------------------
def update_field(*args):
    "Updates a binary field in a binary word starting at specified index."
    if (len(args) != 3):
        error = 1
    elif ((len(args[0])<len(args[1])) | ((args[2]+len(args[1]))>len(args[0]))):
        error = 1
    else:
        error = 0;
    if (error):
        error_message = """
        Usage: update_field(binary_word, binary_field, start_index)
            binary_word  = bin string with LSB on the left
            binary_field = bin string of length <= binary_word length
            start_index  = int index in bin word where LSB of binary_field starts
        """
        logger.critical(error_message)
        sys.exit(1)
    L0 = len(args[0])
    L1 = len(args[1])
    B0 = args[0]
    B1 = args[1]
    i = args[2]   
    if (i == 0):
        return(B1 + B0[L1:])
    elif (i == L0-1):
        return(B0[:i] + B1)
    else:
        return(B0[:i] + B1 + B0[L1+i:])
 
#----------------------------------
def get_field(*args):
    "Returns a binary field from a binary word starting at specified index."
    if (len(args) != 3):
        err = 1
    elif ((len(args[0])<args[1]) | ((args[2]+args[1])>len(args[0]))):
        err = 1
    else:
        err = 0;
    if (err):
        error_message = """
        Usage: get_field(binary_word, field_length, start_index)
            binary_word  = bin string with LSB on the left
            field_length = binary field length
            start_index  = int index in bin word where LSB of binary field starts
        """
        logger.critical(error_message)
        sys.exit(1)
    L0 = len(args[0])
    B0 = args[0]
    L1 = args[1]
    i = args[2]
    return(B0[i:i+L1-1])

#----------------------------------
def check_msg(message):
    "Checks reply fom DM Buffer TCP server in UDP mode is the expected format."
    # Format of message from DM Buffer is '0x00000000 EOF'
    if (message[0] == '\n'): # Strip off any previous new-line characters
        message = message[1:15]
    if ((message[0:2] != '0x') | (message[11:14] != 'EOF')):
        #logger.error('ERROR:' + str(caller) + ': Received unexpected message format =' + message)
        sys.exit(1)
    else:
        return(message)
    
#----------------------------------
def get_msg_hex(strin):
    "Extracts the hex string register value from the TCP server return message."
    # Format of message from DM Buffer is '0x00000000 EOF'
    msg = check_msg(strin)
    return(msg[0:10].lower())       ## Python uses lower case for hex characters

#----------------------------------
def send_term_msg(msg_str):
    "Sends message to serial terminal. NO SPACES."
    logger.debug('send_term_msg:' + msg_str)
    cmd = 'M ' + msg_str
    globals.buffer.socket.send(cmd)
    status = globals.buffer.socket.recv(MAX_MSG_SIZE)    ## Every UDP command requires a handshaking response
    status = get_msg_hex(status)
    if (status):
        return(status)
    else:
        sys.exit(1)
  
#----------------------------------
def write_reg(addr_str, hex_str):
    "Writes DM Buffer register. Address and data are both hex strings of 32-bit values."
    addr = int(addr_str, 16)
    globals.buffer._write_reg(addr, int(hex_str, 16))
    return '{:#010x}'.format(globals.buffer._read_reg(addr & 0xFFFFFF00))
    
#----------------------------------
def read_reg(addr_str):
    '''Reads DM Buffer register and return hex string. Address is hex string of 32-bit.'''
    rdata = '{:#010x}'.format(globals.buffer._read_reg(int(addr_str, 16)))
    if (rdata):
        return(rdata)
    else:
        sys.exit(1)

#----------------------------------
def read_modify_write(*args):
    "Reads DM Buffer register and updates binary field starting at specified index."
    if (len(args) != 3):
        err = 1
    elif ((len(args[1])+args[2])>32):
        err = 1
    else:
        err = 0
    if (err):
        error_message = """
        Usage: read_modify_write(reg_address, binary_field, start_index)
            reg_address  = register address as a hex string
            binary_field = bin string with LSB on the left
            start_index  = int index in bin word where LSB of binary_field starts
        """
        logger.critical(error_message)
        sys.exit(1)
    reg_bits = get_msg_bits(read_reg(args[0]))
    reg_bits = update_field(reg_bits, args[1], args[2])    ## Update field starting at input index
    tmp = hex(int(reverse_str(reg_bits), 2))
    tmp = tmp.strip('L')
    reg_hex = padhex8(tmp)
    reg_hex = reg_hex.strip('L')           ## Python hex function sometimes puts a trailing L
    return(write_reg(args[0], reg_hex))                    ## Return status message

#============================================================================
## SYSTEM Functions #########################################################

def set_CPU1_FSM_disable(value):
    "Set CPU1_FSM disable signal to 1=disabled, 0=running."
# EXIT_FSM is a flag shared between CPU0 and CPU1 in On-Chip Memory (OCM).
# EXIT_FSM = 1 means disable FSM
# It allows CPU0 TCP Server to disable CPU1's FSM.
    logger.debug('set_CPU1_FSM_disable =' + str(value))
    addr_str = hex(EXIT_FSM_ADDR)
    addr_str = addr_str.strip('L')
    if (value != 0):
        reg_hex = '0x1'
        write_reg(addr_str, reg_hex)
        if (read_reg(addr_str) != '0x00000001'):    
            logger.error('set_CPU1_FSM_disable: not set')
            return(0)
        return(1)       
    else:
        reg_hex = '0x0'
        write_reg(addr_str, reg_hex)
        if (read_reg(addr_str) != '0x00000000'):    
            logger.error('set_CPU1_FSM_disable: not cleared')
            return(0)
        return(1)

#----------------------------------    
def set_SYS_reset(value):
    "Set reset signal for all programmable logic to 1=reset, 0=functional mode."
#SYSTEM_status Register addr=0x43C00000
#SYS_errors	    31:30
#SYS_clinicalA      29
#SYS_DMadapter      28
#SYS_connector_ID   27:24
#SYS_debug          16
#SYS_pll_lock       12
#SYS_DM_busy        8
#SYS_PKGRSTN        4
#SYS_reset          0

#SYSTEM_control Register addr=0x43C00004
#SYS_clinicalA	29
#SYS_DMadapter	28
#SYS_debug	16
#PKGRSTN	4
#SYS_reset	0
    addr_str = hex(SYS_BASE_ADDR + 0x00000008)  ## PL Version address
    PL_version = read_reg(addr_str)
    if (PL_version != hex(PL_REV)):
        logger.warn('Detected PL Version' + PL_version)
        logger.warn('PL version ' + hex(PL_REV) + ' must be used with DM Buffer Python library ' + str(LIB_REV))
    addr_str = hex(SYS_BASE_ADDR + 0x00000014)  ## Application Software Version address
    APP_version = read_reg(addr_str)
    if (APP_version != hex(APP_REV)):
        logger.warn('Detected APP Version ' + APP_version)
        logger.warn('APP version '+ hex(APP_REV) + ' must be used with DM Buffer Python library' + str(LIB_REV))
        
    logger.debug('set_SYS_reset =' + str(value))
    addr_str = hex(SYS_BASE_ADDR + 0x00000004)  ## Control address
    if (value != 0):
        logger.debug('DM Buffer programmable logic version =' + PL_version[2:10])
        logger.debug('DM Buffer application software version =' + APP_version[2:10])
        set_CPU1_FSM_disable(1)     ## CPU1 FSM only enabled through Python interface
        status = read_modify_write(addr_str, '1', 0)   ## Set reset bit at index 0
        status = get_msg_bits(status)
        if (status[0:5] != '10001'):
            logger.error('set_SYS_reset: SYS_reset=' + str(value) + 'unexpected SYSTEM_status[4:0] =' + status[0:5])
            return(0)
        return(1)
    else:
        set_CPU1_FSM_disable(0)
        addr_str = hex(SYS_BASE_ADDR + 0x00000000)      ## Status address
        status = read_reg(addr_str)
        status = get_msg_bits(status)
        if (status[12] == '1'):                        ## Redundant, no AXI4 access unless it was locked
            logger.debug('set_SYS_reset: DM Buffer PLL locked.')
        else:
            logger.error('set_SYS_reset: DM Buffer PLL lock bit NOT set.')
            return(0)
        addr_str = hex(SYS_BASE_ADDR + 0x00000004)      ## Control address
        status = read_modify_write(addr_str, '0', 0)    ## Clear PL reset bit at index 0
        status = get_msg_bits(status)
        if ((status[0:5] != '00001') | (status[9:13] != '0001')):
            logger.error('set_SYS_reset: SYS_reset=' + str(value) + 'unexpected SYSTEM_status =' + status)
            return(0)
        else:
            return(1)

#----------------------------------   
def set_DM_PKGRSTN(value):
    "Sets the PKGRSTN output value to the DM. 0=PKGRSTN output low, 1=PKGRSTN output high."
#SYSTEM_status Register addr=0x43C00000
#SYS_errors	    31:30
#SYS_clinicalA      29
#SYS_DMadapter      28
#SYS_connector_ID   27:24
#SYS_debug          16
#SYS_pll_lock       12
#SYS_DM_busy        8
#SYS_PKGRSTN        4
#SYS_reset          0

#SYSTEM_control Register addr=0x43C00004
#SYS_clinicalA	29
#SYS_DMadapter	28
#SYS_debug	16
#PKGRSTN	4
#SYS_reset	0

    logger.debug('set_DM_PKGRSTN =' + str(value))
    addr_str = hex(SYS_BASE_ADDR + 0x00000004)
    if (value != 0):
        status = read_modify_write(addr_str, '1', 4)    ## Set PKGRSTN bit at index 4
        status = get_msg_bits(status)
        if (status[4] != '1'):
            logger.error('set_DM_PKGRSTN: SYS_PKGRSTN not set to 1')
            return(0)
        return(1)
    else:
        status = read_modify_write(addr_str, '0', 4)    ## Clear PKGRSTN bit at index 4
        status = get_msg_bits(status)
        if (status[4] != '0'):
            logger.error('set_DM_PKGRSTN: SYS_PKGRSTN not cleared to 0')
            return(0)
        return(1)

#----------------------------------   
def set_SYS_debug(value):
    "Sets System Debug control for more terminal messages. 0=disabled, 1=enabled."
#SYSTEM_status Register addr=0x43C00000
#SYS_errors	    31:30
#SYS_clinicalA      29
#SYS_DMadapter      28
#SYS_connector_ID   27:24
#SYS_debug          16
#SYS_pll_lock       12
#SYS_DM_busy        8
#SYS_PKGRSTN        4
#SYS_reset          0

#SYSTEM_control Register addr=0x43C00004
#SYS_clinicalA	29
#SYS_DMadapter	28
#SYS_debug	16
#PKGRSTN	4
#SYS_reset	0
    logger.debug('set_SYS_debug =' + str(value))
    addr_str = hex(SYS_BASE_ADDR + 0x00000004)
    if (value != 0):
        status = read_modify_write(addr_str, '1', 16)    ## Set SYS_debug bit at index 16
        status = get_msg_bits(status)
        if (status[16] != '1'):
            logger.error('set_SYS_debug: SYS_debug not set to 1')
            return(0)
        return(1)
    else:
        status = read_modify_write(addr_str, '0', 16)    ## Clear SYS_debug bit at index 16
        status = get_msg_bits(status)
        if (status[16] != '0'):
            logger.error('set_SYS_debug: SYS_debug not cleared to 0')
            return(0)
        return(1)

#----------------------------------   
def set_SYS_DMadapter(value):
    "Sets System DM Adapter mode. 0=disabled, 1=enabled."
#SYSTEM_status Register addr=0x43C00000
#SYS_errors	    31:30
#SYS_clinicalA      29
#SYS_DMadapter      28
#SYS_connector_ID   27:24
#SYS_debug          16
#SYS_pll_lock       12
#SYS_DM_busy        8
#SYS_PKGRSTN        4
#SYS_reset          0

#SYSTEM_control Register addr=0x43C00004
#SYS_clinicalA	29
#SYS_DMadapter	28
#SYS_debug	16
#PKGRSTN	4
#SYS_reset	0
    logger.debug('set_SYS_DMadapter =' + str(value))
    addr_str = hex(SYS_BASE_ADDR + 0x00000004)
    if (value != 0):
        status = read_modify_write(addr_str, '1', 28)    ## Set SYS_DMadapter bit at index 28
        status = get_msg_bits(status)
        if (status[28] != '1'):
            logger.error('set_SYS_DMadapter: SYS_DMadapter not set to 1')
            return(0)
        return(1)
    else:
        status = read_modify_write(addr_str, '0', 28)    ## Clear SYS_DMadapter bit at index 28
        status = get_msg_bits(status)
        if (status[28] != '0'):
            logger.error('set_SYS_DMadapter: SYS_DMadapter not cleared to 0')
            return(0)
        return(1)

#----------------------------------   
def set_SYS_clinicalA(value):
    "Sets System Clinical A mode. 0=disabled, 1=enabled."
#SYSTEM_status Register addr=0x43C00000
#SYS_errors	    31:30
#SYS_clinicalA      29
#SYS_DMadapter      28
#SYS_connector_ID   27:24
#SYS_debug          16
#SYS_pll_lock       12
#SYS_DM_busy        8
#SYS_PKGRSTN        4
#SYS_reset          0

#SYSTEM_control Register addr=0x43C00004
#SYS_clinicalA	29
#SYS_DMadapter	28
#SYS_debug	16
#PKGRSTN	4
#SYS_reset	0
    logger.debug('set_SYS_clinicalA =' + str(value))
    addr_str = hex(SYS_BASE_ADDR + 0x00000004)
    if (value != 0):
        status = read_modify_write(addr_str, '1', 29)    ## Set SYS_clinicalA bit at index 29
        status = get_msg_bits(status)
        if (status[29] != '1'):
            logger.error('set_SYS_clinicalA: SYS_clinicalA not set to 1')
            return(0)
        return(1)
    else:
        status = read_modify_write(addr_str, '0', 29)    ## Clear SYS_clinicalA bit at index 29
        status = get_msg_bits(status)
        if (status[29] != '0'):
            logger.error('set_SYS_clinicalA: SYS_clinicalA not cleared to 0')
            return(0)
        return(1)

    
#----------------------------------    
def get_SYS_errors():
    "Gets the SYSTEM Error Bits."
#SYSTEM_status Register addr=0x43C00000
#SYS_errors	    31:30
#SYS_clinicalA      29
#SYS_DMadapter      28
#SYS_connector_ID   27:24
#SYS_debug          16
#SYS_pll_lock       12
#SYS_DM_busy        8
#SYS_PKGRSTN        4
#SYS_PL_reset       0

#SYSTEM_control Register addr=0x43C00004
#SYS_clinicalA	29
#SYS_DMadapter	28
#SYS_debug	16
#PKGRSTN	4
#SYS_reset	0
    addr_str = hex(SYS_BASE_ADDR + 0x00000000)
    status = read_reg(addr_str)
    status = get_msg_bits(status)
    return(status[30:32])
    
#----------------------------------    
def get_DM_busy():
    "Gets the DM_busy input signal value."
#SYSTEM_status Register addr=0x43C00000
#SYS_errors	    31:30
#SYS_clinicalA      29
#SYS_DMadapter      28
#SYS_connector_ID   27:24
#SYS_debug          16
#SYS_pll_lock       12
#SYS_DM_busy        8
#SYS_PKGRSTN        4
#SYS_PL_reset       0

#SYSTEM_control Register addr=0x43C00004
#SYS_clinicalA	29
#SYS_DMadapter	28
#SYS_debug	16
#PKGRSTN	4
#SYS_reset	0
    addr_str = hex(SYS_BASE_ADDR + 0x00000000)
    status = read_reg(addr_str)
    status = get_msg_bits(status)
    if (status):
        return(status[8])
    else:
        logger.error('get_DM_busy: unexpected status format=' + status)
        return(0)

#----------------------------------    
def wait_DM_not_busy():
    "Wait until the DM busy signal is de-asserted, poll every second."
#SYSTEM_status Register addr=0x43C00000
#SYS_errors	    31:30
#SYS_clinicalA      29
#SYS_DMadapter      28
#SYS_connector_ID   27:24
#SYS_debug          16
#SYS_pll_lock       12
#SYS_DM_busy        8
#SYS_PKGRSTN        4
#SYS_PL_reset       0
    
    timeout = 30    ## Timeout time in seconds
  
    addr_str = hex(SYS_BASE_ADDR + 0x00000000)
    status = read_reg(addr_str)
    status = get_msg_bits(status)
    if (status[4] == '0'):
        logger.error('wait_DM_not_busy: PKGRSTN is still set to 0.')
        return(0)

    logger.debug('wait_DM_not_busy: In progress...')
    while (timeout != 0):
        time.sleep(1)               ## Delay in seconds
        status = read_reg(addr_str)
        status = get_msg_bits(status)
        if (status[8] == '0'):
            logger.debug('wait_DM_not_busy: DM_BSY input is 0.')
            return(1)
        else:
            timeout = timeout-1
    logger.error('wait_DM_not_busy: Timeout')
    return(0)

#============================================================================
## SPI MASTER Functions ##################################################### 
       
def set_SPI_reset(value):
    "Reset SPI Master to default values." 
#SPI_master_status Register addr=0x43C01000
#SPI_rdata_prty     28
#SPI_test_parity    25
#SPI_test_loop      24
#SPI_invert_txframe 17
#SPI_invert_rxframe 16
#SPI_clinical_A_SDO 12
#SPI_slave_perr     9
#SPI_master_perr    8
#SPI_parity         4
#SPI_master_reset   0

#SPI_master_control Register addr=0x43C01004
#SPI_test_parity    25
#SPI_test_loop      24
#SPI_invert_txframe 17
#SPI_invert_rxframe 16
#SPI_clinical_A_SDO 12
#SPI_CPHA           9
#SPI_CPOL           8
#SPI_parity         4
#SPI_reset          0
    logger.debug('set_SPI_reset =' + str(value))
    if (value != 0):
        addr_str = hex(SPI_BASE_ADDR + 0x00000004)
        status = read_modify_write(addr_str, '1', 0)             ## Set reset bit at index 0
        if (status != '0x00000001'):    
            logger.error('set_SPI_reset: SPI_master_reset=1, unexpected SPI_master_status =' + status)
            return(0)
        for i in range(1,4):                        ## Check SPI Master registers
            addr_str = hex(SPI_BASE_ADDR + i*4)
            rdata = read_reg(addr_str)
            if (i == 1):
                expected = '0x00000001'     ## SPI_reset bit set
            else:
                expected = '0x00000000'
            if (rdata != expected):
                logger.error('set_SPI_reset: SPI_master_reset=1, register' + addr_str + 'not reset.')
                return(0)
        return(1)
    else:
        addr_str = hex(SPI_BASE_ADDR + 0x00000004)
        status = read_modify_write(addr_str, '0', 0)    ## Clear reset bit at index 0
        if (status != '0x00000000'):    
            logger.error('set_SPI_reset: SPI_master_reset=0, unexpected SPI_master_status =' + status)
            return(0)
    return(1)

#----------------------------------
def SPI_write (waddr, wdata):
    "SPI Write to DM register. Requires addr and wdata as hex format strings." 
    globals.buffer.write_detector_reg(int(waddr, 16), int(wdata, 16))
    return(1)

#----------------------------------
def SPI_read (raddr_ptr):
    "SPI Read from DM address pointer location. Requires addr_ptr as hex format string." 
    rdata = '{:#010x}'.format(globals.buffer.read_detector_reg(int(raddr_ptr, 16)))
    return(rdata)

#----------------------------------
def set_SPI_test_parity(value):
    "Set SPI_test_parity signal to 1=invert parity, 0=parity unchanged." 
#SPI_master_status Register addr=0x43C01000
#SPI_rdata_prty     28
#SPI_test_parity    25
#SPI_test_loop      24
#SPI_invert_txframe 17
#SPI_invert_rxframe 16
#SPI_clinical_A_SDO 12
#SPI_slave_perr     9
#SPI_master_perr    8
#SPI_parity         4
#SPI_master_reset   0

#SPI_master_control Register addr=0x43C01004
#SPI_test_parity    25
#SPI_test_loop      24
#SPI_invert_txframe 17
#SPI_invert_rxframe 16
#SPI_clinical_A_SDO 12
#SPI_CPHA           9
#SPI_CPOL           8
#SPI_parity         4
#SPI_reset          0
    logger.debug('set_SPI_test_parity =' + str(value))
    addr_str = hex(SPI_BASE_ADDR + 0x00000004)
    if (value != 0):
        status = read_modify_write(addr_str, '1', 25)    ## Set test_parity bit at index 25
        status = get_msg_bits(status)
        if (status[25] != '1'):    
            logger.error('set_SPI_test_parity=1, status bit not set.')
            return(0)
        return(1)
    else:
        status = read_modify_write(addr_str, '0', 25)    ## Clear test_parity bit at index 25
        status = get_msg_bits(status)
        if (status[25] != '0'):    
            logger.error('set_SPI_test_parity=0, status bit not clear.')
            return(0)
        return(1)

#----------------------------------
def set_SPI_tx_parity(value):
    "Set SPI Tx parity signal to 1=odd parity, 0=even parity." 
#SPI_master_status Register addr=0x43C01000
#SPI_rdata_prty     28
#SPI_test_parity    25
#SPI_test_loop      24
#SPI_invert_txframe 17
#SPI_invert_rxframe 16
#SPI_clinical_A_SDO 12
#SPI_slave_perr     9
#SPI_master_perr    8
#SPI_parity         4
#SPI_master_reset   0

#SPI_master_control Register addr=0x43C01004
#SPI_test_parity    25
#SPI_test_loop      24
#SPI_invert_txframe 17
#SPI_invert_rxframe 16
#SPI_clinical_A_SDO 12
#SPI_CPHA           9
#SPI_CPOL           8
#SPI_parity         4
#SPI_reset          0
    logger.debug('set_SPI_tx_parity =' + str(value))
    addr_str = hex(SPI_BASE_ADDR + 0x00000004)
    if (value != 0):
        status = read_modify_write(addr_str, '1', 4)    ## Set tx_parity bit at index 4
        status = get_msg_bits(status)
        if (status[4] != '1'):    
            logger.error('set_SPI_tx_parity=1, status bit not set.')
            return(0)
        return(1)
    else:
        status = read_modify_write(addr_str, '0', 4)    ## Clear tx_parity bit at index 4
        status = get_msg_bits(status)
        if (status[4] != '0'):    
            logger.error('set_SPI_tx_parity=0, status bit not clear.')
            return(0)
        return(1)

#----------------------------------
def set_SPI_test_loop(value):
    "Set SPI_test_loop signal to 1=loop-back test, 0=functional mode." 
#SPI_master_status Register addr=0x43C01000
#SPI_rdata_prty     28
#SPI_test_parity    25
#SPI_test_loop      24
#SPI_invert_txframe 17
#SPI_invert_rxframe 16
#SPI_clinical_A_SDO 12
#SPI_slave_perr     9
#SPI_master_perr    8
#SPI_parity         4
#SPI_master_reset   0

#SPI_master_control Register addr=0x43C01004
#SPI_test_parity    25
#SPI_test_loop      24
#SPI_invert_txframe 17
#SPI_invert_rxframe 16
#SPI_clinical_A_SDO 12
#SPI_CPHA           9
#SPI_CPOL           8
#SPI_parity         4
#SPI_reset          0
    logger.debug('set_SPI_test_loop =' + str(value))
    addr_str = hex(SPI_BASE_ADDR + 0x00000004)
    if (value != 0):
        status = read_modify_write(addr_str, '1', 24)    ## Set loop-back bit at index 24
        status = get_msg_bits(status)
        if (status[24] != '1'):    
            logger.error('set_SPI_test_loop=1, status bit not set.')
            return(0)
        return(1)
    else:
        status = read_modify_write(addr_str, '0', 24)    ## Clear loop-back bit at index 24
        status = get_msg_bits(status)
        if (status[24] != '0'):    
            logger.error('set_SPI_test_loop=0, status bit not clear.')
            return(0)
        return(1)

#============================================================================
## VTRIG Functions ##########################################################

def set_VTRIG_num_cycles(num):
    "Sets the number of VTRIG clock cycles."
#VTRIG_gen_control address=0x43C02004
#VTRIG_num_values   30:28
#VTRIG_num_cycles   27:8
#VTRIG_test         6
#VTRIG_enable       4
#VTRIG_reset        0
    msb_flag = 0
    msbs = 0
    num = int(num); # In case it was entered as floating point.
    if (num > 1048575):   ## 1048575=0xFFFFF
        msbs = num>>20
        logger.warn("set_VTRIG_num_cycles: Value %d (%s) exceeded 20 bits" % (num, hex(num)))
        logger.warn('        Setting MSBs in VTRIG_msb_cycles register')
        set_VTRIG_num_msb_cycles(msbs)
        msb_flag = 1
    if (num < 0):
        logger.error('set_VTRIG_num_cycles: Negative number of cycles.')
        sys.exit(1)
        
    num20 = num-(msbs<<20)
    bit_field = reverse_str(hexstr2bits(hex(num20), 20))   ## Bit field for VTRIG_num_cycles, length = 20
    addr_str = hex(VTRIG_BASE_ADDR + 0x00000004)
    
    if (read_modify_write(addr_str, bit_field, 8)):      ## Update field starting at index 8
        if (msb_flag == 1):
            logger.debug('set_VTRIG_num_cycles: Total number of cycles =' + str(num))
        else:
            logger.debug('set_VTRIG_num_cycles:' +str(num20) + 'cycles.')
        return(1)
    else:
        return(0)

#----------------------------------
def set_VTRIG_num_msb_cycles(num):
    "Sets the 12 MSBs for VTRIG clock cycle count if set_VTRIG_num_cycles() exceeds 20 bits."
#VTRIG_msb_reg address=0x43C0200C
#VTRIG_msb_cycles   11:0

    num = int(num); # In case it was entered as floating point.
    if (num > 4095):
        logger.error('set_VTRIG_num_msb_cycles: Value limited to 12 bits.')
        sys.exit(1)
    if (num < 0):
        logger.error('set_VTRIG_num_msb_cycles: Negative number of cycles.')
        sys.exit(1)
    bit_field = reverse_str(hexstr2bits(padhex8(hex(num)), 12))   ## MSB Bit field for VTRIG_num_cycles, length = 12
    addr_str = hex(VTRIG_BASE_ADDR + 0x0000000C)

    word32 = padhex8(hex(num))
    if (write_reg(addr_str, word32)):      
        logger.debug('set_VTRIG_num_msb_cycles: Setting VTRIG Count MSBs to' + hex(num))
        return(1)
    else:
        return(0)

#----------------------------------------------
def get_VTRIG_num_cycles():
    "Gets the number of programmed VTRIG clock cycles, including extra MSBs in VTRIG_msb_reg."
#VTRIG_gen_control address=0x43C02004
#VTRIG_num_values   30:28
#VTRIG_num_cycles   27:8
#VTRIG_test         6
#VTRIG_enable       4
#VTRIG_reset        0

#VTRIG_msb_reg    address=0x43C0200C
#VTRIG_msb_cycles   11:0

    rdata = read_reg(hex(VTRIG_BASE_ADDR + 0x00000004))
    rdata = get_msg_bits(rdata)
    lsb_cycles = int(reverse_str(rdata[8:28]), 2)

    msb_cycles = read_reg(hex(VTRIG_BASE_ADDR + 0x0000000C))
    msb_cycles = int(msb_cycles, 16)<<20
    return(msb_cycles+lsb_cycles)
    
#----------------------------------
def set_VTRIG_periods(*args):
    "Input up to 8 VTRIG periods in usec (35MHz reference clock used)."
#VTRIG_gen_control address=0x43C02004
#VTRIG_num_values   30:28
#VTRIG_num_cycles   27:8
#VTRIG_test         6
#VTRIG_enable       4
#VTRIG_reset        0
    if ((len(args) > 8)|(len(args) == 0)):
        error_message = """
        Usage: set_VTRIG_periods(period0, <period1, ... period7>)
        """
        logger.error(error_message)
        return(0)
    ref_freq = REFERENCE_FREQ
    plist = args
#Period is specified as 2 half-period values to allow programming of different duty cycles if required.
    for period in plist:
        half_cycle = int(period*ref_freq/2)
        if ((half_cycle > 65520) | (half_cycle < 35)):
            logger.error('set_VTRIG_periods: Set period between 2usec and 3744usec')
            return(0)
    addr = VTRIG_BASE_ADDR + 0x00000010     ## Starting address of VTRIG period registers
    i = 0
    for period in plist:
        addr_str = hex(addr + i*4)
        half_cycle = hex(int(period*ref_freq/2))
        half_cycle = reverse_str(hexstr2bits(half_cycle, 16))
        read_modify_write(addr_str, half_cycle, 0)      ## Half-cycle in two 16-bit fields
        read_modify_write(addr_str, half_cycle, 16)
        i += 1
    num_values = reverse_str(hexstr2bits(hex(len(args)-1), 3))   ## Bit field for number of values, length = 3
    addr_str = hex(VTRIG_BASE_ADDR + 0x00000004)
    if (read_modify_write(addr_str, num_values, 28)):         ## Update field starting at index 28
        logger.debug('set_VTRIG_periods:' + str(len(args)) + 'period values set.')
    logger.debug('Read back values to check (may have round-off differences due to integer conversion):')
    i = 0
    for period in plist:
        addr_str = hex(addr + i*4)
        rdata = read_reg(addr_str)
        rdata = hexstr2bits(rdata, 32)
        rperiod = (int(rdata[0:16],2) + int(rdata[16:32],2))/ref_freq
        logger.debug("Read Period = %-9.3fusec" % rperiod)
        i += 1
    return 1
    
#----------------------------------
def set_VTRIG_reset(value):
    "Set VTRIG block reset signal to 1=reset, 0=functional mode."
#VTRIG_gen_status address=0x43C02000
#DATA_preamble_length	30:28
#DATA_frame_length	27:12
#VTRIG_period_err	4
#VTRIG_test_done	3
#VTRIG_cycles_done	2
#VTRIG_enable           1
#VTRIG_reset            0

#VTRIG_gen_control address=0x43C02004
#VTRIG_num_values   30:28
#VTRIG_num_cycles   27:8
#VTRIG_test         6
#VTRIG_enable       4
#VTRIG_reset        0
    logger.debug('set_VTRIG_reset =' + str(value))
    if (value != 0):
        addr_str = hex(VTRIG_BASE_ADDR + 0x00000004)
        status = read_modify_write(addr_str, '1', 0)             ## Update reset bit at index 0
        status = get_msg_bits(status)
        if (status[0:5] != '10000'):    
            logger.error('set_VTRIG_reset: VTRIG_reset=1, unexpected VTRIG_gen_status[0:5] =' + status[0:5])
            return(0)
        addr_str = hex(VTRIG_BASE_ADDR + 0x00000004)
        control = read_reg(addr_str)
        control = get_msg_bits(control)
        if (control[0:9] != '100000000'):
            logger.error('set_VTRIG_reset: VTRIG_reset=1, unexpected VTRIG_gen_control =' + control)
            return(0)

        for i in range(0,7):
            addr_str = hex(VTRIG_BASE_ADDR + 0x00000010 + i*4)
            period = read_reg(addr_str)
            if (period != hex(DEFAULT_VTRIG_PERIOD)):
                logger.error('set_VTRIG_reset: VTRIG_reset=1, unexpected default VTRIG_period =' + str(period))
                return(0)
        return(1)
    else:
        addr_str = hex(VTRIG_BASE_ADDR + 0x00000004)
        status = read_modify_write(addr_str, '0', 0)    ## Clear reset bit at index 0
        status = get_msg_bits(status)
        if (status[0:5] != '00000'):
            logger.error('set_VTRIG_reset: VTRIG_reset=0, unexpected VTRIG_gen_status[0:5] =' + status[0:5])
            return(0)
    return(1)

#----------------------------------
def set_VTRIG_test(value):
    "Set VTRIG block self-test signal to 1=enable self-test, 0=functional mode."
#VTRIG_gen_status address=0x43C02000
#DATA_preamble_length	30:28
#DATA_frame_length	27:12
#VTRIG_period_err	4
#VTRIG_test_done	3
#VTRIG_cycles_done	2
#VTRIG_enable           1
#VTRIG_reset            0

#VTRIG_gen_control address=0x43C02004
#VTRIG_num_values   30:28
#VTRIG_num_cycles   27:8
#VTRIG_test         6
#VTRIG_enable       4
#VTRIG_reset        0
    logger.debug('set_VTRIG_test =' + str(value))
    addr_str = hex(VTRIG_BASE_ADDR + 0x00000004)
    if (value != 0):
        if (read_modify_write(addr_str, '1', 6)):    ## Update test bit at index 6
            return(1)
        return(0)
    else:
        if (read_modify_write(addr_str, '0', 6)):    ## Update test bit at index 6
            return(1)
        return(0)
    
#----------------------------------
def set_VTRIG_enable(value):
    "Set VTRIG enable signal to 1=running, 0=not running."
## Test also includes consistency checking before a test is started.
#
#VTRIG_gen_status address=0x43C02000
#DATA_preamble_length	30:28
#DATA_frame_length	27:12
#VTRIG_period_err	4
#VTRIG_test_done	3
#VTRIG_cycles_done	2
#VTRIG_enable           1
#VTRIG_reset            0

#VTRIG_gen_control address=0x43C02004
#VTRIG_num_values   30:28
#VTRIG_num_cycles   27:8
#VTRIG_test         6
#VTRIG_enable       4
#VTRIG_reset        0

    err = 0
    logger.debug('set_VTRIG_enable =' + str(value))
    addr_str = hex(VTRIG_BASE_ADDR + 0x00000004)
    if (value != 0):      
        if (read_modify_write(addr_str, '1', 4)):    ## Update enable bit at index 4
            return(1)
        return(err)
    else:
        if (read_modify_write(addr_str, '0', 4)):    ## Update enable bit at index 4
            return(1)
        return(err)

#----------------------------------
def check_VTRIG_num_cycles():
    "Check consistency of frame length, number of VTRIG cycles, buffer size, returns corrected VTRIG cycles."
# Requires frame parameters to be previousy programmed.

#VTRIG_gen_control address=0x43C02004
#VTRIG_num_values   30:28
#VTRIG_num_cycles   27:8
#VTRIG_test         6
#VTRIG_enable       4
#VTRIG_reset        0

# In Normal Mode, view bytes from the Serdes input are loaded directly to buffer memory and download
# to the PC. In Accumulation Mode, only the Accumulated View payload bytes are downloaded to the PC.
# Compared to the number of bytes downloaded in Normal Mode, the number of downloaded Accumulated View
# bytes is reduced by a factor equal to the number of views accumulated (set by set_DATA_accum_num(value)).

# overhead is view number prefixed to a view data lane in bytes, value fixed in FPGA.
    overhead = 1            # Normal Mode
    acc_mode_overhead = 2   # Accumulator Mode
    preamble_length = 1   ## Hard coded in FPGA logic

    VTRIG_num_cycles = get_VTRIG_num_cycles()
    vtrigCyclesDes = VTRIG_num_cycles
    logger.debug('check_VTRIG_num_cycles: Original programmed VTRIG_num_cycles=' + str(VTRIG_num_cycles))
    accum_mode = check_DATA_accum_mode_set()
    rdata = read_reg(hex(DATA_BASE_ADDR + 0x00000008))
    rdata = get_msg_bits(rdata)
    crc_length = int(reverse_str(rdata[16:28]), 2)
    logger.debug('check_VTRIG_num_cycles: crc_length=' + str(crc_length))
    frame_length = int(reverse_str(rdata[0:16]), 2)
    logger.debug('check_VTRIG_num_cycles: frame_length with crc=' + str(frame_length))
    max_num_bytes = NUM_S2MM_BUFFS * S2MM_BUFFER_LENGTH  ## Maximum buffer memory capacity in bytes
    logger.debug('check_VTRIG_num_cycles: Maximum buffer memory capacity=' + str(max_num_bytes)+'bytes.')

    if (accum_mode):
        fbytes = 2*preamble_length + frame_length - crc_length + 2  ## 2 extra for view number (16-bit word)
        logger.debug('check_VTRIG_num_cycles: Downloaded bytes per frame in one lane with preamble, view number and no crc=%d' % fbytes)
        bytes_per_view = 4*fbytes                 ## 4 Lanes, crc bytes removed in stored data with 2 for view number.
        abytes = 2*bytes_per_view
        logger.debug('check_VTRIG_num_cycles: Downloaded bytes per Accumulated View (4 lanes)=' + str(abytes))
        
        num_buffs_per_view = 1+int((abytes)/S2MM_BUFFER_LENGTH)  ## Next highest integer number
        logger.debug('check_VTRIG_num_cycles: Number of Buffers per downloaded Accumulated View=' + str(num_buffs_per_view))

        accum_num = get_DATA_accum_num()
        logger.debug('check_VTRIG_num_cycles: Number of views accumulated=' + str(accum_num))
        max_buff_cycles = int(NUM_S2MM_BUFFS/(num_buffs_per_view))
        max_vtrig_cycles = max_buff_cycles*accum_num
        logger.debug('check_VTRIG_num_cycles: Maximum allowed VTRIG Cycles in Accumulation Mode=' + str(max_vtrig_cycles))
        bytes_per_view = abytes  ## bytes_per_view NOW means bytes in downloaded Accumulated View

        acycles = int(VTRIG_num_cycles/accum_num)
        if (acycles == 0):
            acycles = 1
        logger.debug('check_VTRIG_num_cycles: Accumulated View cycles=' + str(acycles))
        VTRIG_num_cycles = acycles*accum_num
        logger.debug("check_VTRIG_num_cycles: Final number of VTRIG cycles (no overhead)= %dx%d = %d" % (acycles, accum_num, VTRIG_num_cycles))
        cycles_w_overhead = VTRIG_num_cycles+acc_mode_overhead
        cycles = VTRIG_num_cycles
        set_VTRIG_num_cycles(cycles_w_overhead)
    else:
        fbytes = 2*preamble_length + frame_length - crc_length + 1  ## 1 extra for view number (8-bit word)
        logger.debug('check_VTRIG_num_cycles: Downloaded bytes per frame in one lane with preamble, view number and no crc=' + str(fbytes))
        bytes_per_view = 4*fbytes                 ## 4 Lanes, crc bytes removed in stored data with 1 for view number.
        logger.debug('check_VTRIG_num_cycles: Downloaded bytes per view from DM (4 lanes)=' + str(bytes_per_view))
        
        num_buffs_per_view = 1+int(bytes_per_view/S2MM_BUFFER_LENGTH)  ## Next highest integer number
        logger.debug('check_VTRIG_num_cycles: Number of Buffers per View=' + str(num_buffs_per_view))
        
        max_buff_cycles = int(NUM_S2MM_BUFFS/num_buffs_per_view)
        max_vtrig_cycles = max_buff_cycles
        logger.debug('check_VTRIG_num_cycles: Maximum allowed VTRIG Cycles=' + str(max_buff_cycles))
        
        if (max_vtrig_cycles < VTRIG_num_cycles):
            cycles_w_overhead = max_vtrig_cycles+overhead
            cycles = max_vtrig_cycles
            logger.warn('check_VTRIG_num_cycles: Based on buffer memory size, max number of DM Views saved is' + str(cycles))
        else:
            cycles_w_overhead = VTRIG_num_cycles+overhead
            cycles = VTRIG_num_cycles
        logger.debug('check_VTRIG_num_cycles: Final number of DM Views saved=' + str(cycles) + '(VTRIG cycles no overhead)')
        set_VTRIG_num_cycles(cycles_w_overhead)
        
    actual_total_bytes = bytes_per_view*(cycles)
    logger.debug('check_VTRIG_num_cycles: Total Number of bytes from Serial Interface=' + str(actual_total_bytes))

    if (accum_mode):
        total_accum_bytes = bytes_per_view*acycles
        logger.debug('check_VTRIG_num_cycles: Total Number of downloaded Accumulated View bytes=' + str(total_accum_bytes))
    if vtrigCyclesDes == cycles:
        return (cycles, cycles_w_overhead)
    else:
        log.error('Set VTRIG cycles = %s, but only %s cycles are possible!' % (vtrigCyclesDes, cycles))
        sys.exit(1)

#----------------------------------
def get_VTRIG_test_period(delay):
    "Checks test is complete and returns measured period in usec after specified delay in usec."
#VTRIG_gen_status address=0x43C02000
#DATA_preamble_length	30:28
#DATA_frame_length	27:12
#VTRIG_period_err	4
#VTRIG_test_done	3
#VTRIG_cycles_done	2
#VTRIG_enable           1
#VTRIG_reset            0

#VTRIG_gen_control address=0x43C02004
#VTRIG_num_values   30:28
#VTRIG_num_cycles   27:8
#VTRIG_test         6
#VTRIG_enable       4
#VTRIG_reset        0
    
#VTRIG_test_period address=0x43C02008
    delay = delay * 1e-6            ## Delay in seconds
    logger.debug('get_VTRIG_test_period: after delay of' + str(delay) + 'sec.')
    ref_freq = REFERENCE_FREQ
    addr_str = hex(VTRIG_BASE_ADDR + 0x00000004)    ## VTRIG control
    control = read_reg(addr_str)
    control = get_msg_bits(control)
    if (control[6] != '1'):
        logger.error('get_VTRIG_test_period: VTRIG_test bit not set.')
        return(0)

    time.sleep(delay)               ## Delay in seconds
    addr_str = hex(VTRIG_BASE_ADDR + 0x00000000)    ## VTRIG status
    status = read_reg(addr_str)
    status = get_msg_bits(status)
    if (status[3] != '1'):
        logger.error('get_VTRIG_test_period: VTRIG_test_done bit not set after a delay of' + str(delay/1e-6)  + 'usec.')
        return(0)

    addr_str = hex(VTRIG_BASE_ADDR + 0x00000008)    ## VTRIG test period
    test_period = read_reg(addr_str)
    return(int(test_period, 16)/ref_freq)    
       
#============================================================================
## TEST DATA Functions ######################################################

def set_TEST_data_reset(value):
    "Reset TEST_data generator to default values." 
#TEST_data_gen_status Register addr=0x43C03000
#DATA_preamble_length   30:28
#DATA_frame_length      27:12
#TEST_data_ram      8
#TEST_en_data3      7
#TEST_en_data2      6
#TEST_en_data1      5
#TEST_en_data0      4
#TEST_data_enable   1
#TEST_data_reset    0

#TEST_data_gen_control Register addr=0x43C03004
#TEST_valid_delay   23:16
#TEST_data_ram      8
#TEST_en_data3      7
#TEST_en_data2      6
#TEST_en_data1      5
#TEST_en_data0      4
#TEST_data_enable   1
#TEST_data_reset    0
    logger.debug('set_TEST_data_reset =' + str(value))
    if (value != 0):
        addr_str = hex(TEST_BASE_ADDR + 0x00000004)
        status = read_modify_write(addr_str, '1', 0)             ## Set reset bit at index 0
        status = get_msg_bits(status)
        if (status[0:4] != '1000'):    
            logger.error('set_TEST_data_reset: TEST_data_reset=1, unexpected TEST_data_gen_status[0:4] =' + status[0:4])
            return(0)
        for i in range(1,6):                        ## Check TEST generator registers
            addr_str = hex(TEST_BASE_ADDR + i*4)
            rdata = read_reg(addr_str)
            if (i == 1):
                expected = '0x000000f1'     ## TEST_en_data bits and Reset bit set (Python used lower case)
            else:
                hexi = hex(i-2)
                hexi = hexi[2:].zfill(1)
                expected = '0x01' + hexi + '0aa55'
            if (rdata != expected):
                logger.error('set_TEST_data_reset: TEST_data_reset=1, register' + addr_str + 'not reset to default value.')
                return(0)
        return(1)
    else:
        addr_str = hex(TEST_BASE_ADDR + 0x00000004)
        status = read_modify_write(addr_str, '0', 0)    ## Clear reset bit at index 0
        status = get_msg_bits(status)
        if (status[0:4] != '0000'):
            logger.error('set_TEST_data_reset: TEST_data_reset=0, unexpected TEST_data_gen_status[0:4] =' + status[0:4])
            return(0)
    return(1)

#----------------------------------
def set_TEST_data_enable(value):
    "Set TEST Data generator enable signal to 1=running, 0=not running."
#TEST_data_gen_control Register addr=0x43C03004
#TEST_valid_delay   23:16
#TEST_data_ram      8
#TEST_en_data3      7
#TEST_en_data2      6
#TEST_en_data1      5
#TEST_en_data0      4
#TEST_data_enable   1
#TEST_data_reset    0
    logger.debug('set_TEST_data_enable =' + str(value))
    addr_str = hex(TEST_BASE_ADDR + 0x00000004)
    if (value != 0):
        if (read_modify_write(addr_str, '1', 1)):    ## Update enable bit at index 1
            return(1)
        return(0)
    else:
        if (read_modify_write(addr_str, '0', 1)):    ## Update enable bit at index 1
            return(1)
        return(0)
    
#----------------------------------
def set_TEST_data_ram_enable(value):
    "Set TEST Data RAM enable signal to 1=use RAMs, 0=use generated TEST data."
#TEST_data_gen_control Register addr=0x43C03004
#TEST_valid_delay   23:16
#TEST_data_ram      8
#TEST_en_data3      7
#TEST_en_data2      6
#TEST_en_data1      5
#TEST_en_data0      4
#TEST_data_enable   1
#TEST_data_reset    0
    logger.debug('set_TEST_data_ram_enable =' + str(value))
    addr_str = hex(TEST_BASE_ADDR + 0x00000004)
    if (value != 0):
        if (read_modify_write(addr_str, '1', 8)):    ## Update RAM bit at index 8
            return(1)
        return(0)
    else:
        if (read_modify_write(addr_str, '0', 8)):    ## Update RAM bit at index 8
            return(1)
        return(0)

#----------------------------------
def set_TEST_valid_delay(value):
    "Set TEST delay in 35MHz clock cycles before TEST Data is sent."
#TEST_data_gen_control Register addr=0x43C03004
#TEST_valid_delay   23:16
#TEST_data_ram      8
#TEST_en_data3      7
#TEST_en_data2      6
#TEST_en_data1      5
#TEST_en_data0      4
#TEST_data_enable   1
#TEST_data_reset    0
    delay = int(value)      ## No floating point
    logger.debug('set_TEST_valid_delay =' + str(value))
    if ((delay < 0) | (delay > 255)):
        logger.error('set_TEST_valid_delay: Delay must be between 0 and 255 clock cycles. No value set.')
        return(0)
    addr_str = hex(TEST_BASE_ADDR + 0x00000004)     ## TEST_data_gen_control Register
    bit_field = reverse_str(hexstr2bits(hex(delay), 8))   ## 8 = Length of bit field
    if (read_modify_write(addr_str, bit_field, 16)):    ## Update delay at index 16
        return(1)
    return(0)

#----------------------------------
def set_TEST_ram_enables(en_bits):
    "Set TEST RAM enables as a 4-bit string where the LSB is RAM0 for DATA0 lane."
#TEST_data_gen_control Register addr=0x43C03004
#TEST_valid_delay   23:16
#TEST_data_ram      8
#TEST_en_data3      7
#TEST_en_data2      6
#TEST_en_data1      5
#TEST_en_data0      4
#TEST_data_enable   1
#TEST_data_reset    0
    logger.debug('set_TEST_ram_enables =' + str(en_bits))
    if ((int(en_bits, 2) > 255) | (int(en_bits, 2) < 0)):
        logger.error('set_TEST_ram_enables: input must be a string of 4 bits. 1=enable, 0=disable.')
        return(0)
    addr_str = hex(TEST_BASE_ADDR + 0x00000004)     ## TEST_data_gen_control Register
    en_bits = reverse_str(en_bits)
    if (read_modify_write(addr_str, en_bits, 4)):     ## Update enable bits starting at index 4
        return(1)
    return(0)

#----------------------------------
def gen_TEST_ram_data(*args):
    "Generate data for TEST RAMs of the same format as the default generated TEST data (except preamble=0xFECD)."
    addr_str = args[0]
    if ((len(args) != 3) | (len(args[2]) != 4) | (len(args[0]) != 10) | (addr_str[0:2] != '0x')):
        error_message = """
        Usage: gen_TEST_ram_data(addr_str, num_bytes, start_str)
            addr_str  = Base Address of RAM as an 8 character hex string
            num_bytes = Number of frame bytes, not including 2 byte preamble (will be rounded up to 32-bit word)
            start_str = Starting byte as a 2 character hex string
        """
        logger.error(error_message)
        return(0)
    num_bytes = args[1]
    start_str = args[2]
    
    if (num_bytes % 4):
        num_words = int(num_bytes/4)+1
    else:
        num_words = int(num_bytes/4)
    num_bytes = num_words*4

    err = 0
    next_value = (int(start_str,16) + 1) % 256      ## Values limited to 8-bit fields
    next_byte = hex(next_value)
    word = '0x' + 'FE' + 'DC' + start_str[2:4].zfill(2) + next_byte[2:4].zfill(2)
    err = err | (not write_reg(addr_str, word))
        
    for i in range(1,num_words+1):
        next_value = (next_value + 1) % 256
        field3 = hex(next_value)
        field3 = field3[2:4].zfill(2)
        
        next_value = (next_value + 1) % 256
        field2 = hex(next_value)
        field2 = field2[2:4].zfill(2)
        
        next_value = (next_value + 1) % 256
        field1 = hex(next_value)
        field1 = field1[2:4].zfill(2)
        
        next_value = (next_value + 1) % 256
        field0 = hex(next_value)
        field0 = field0[2:4].zfill(2)
        word = '0x' + field3 + field2 + field1 + field0

        addr_str = hex(int(addr_str, 16)+4)
        err = err | (not write_reg(addr_str, word))
    if (err):
        return(0)
    else:
        logger.debug('gen_TEST_ram_data: Finished RAM that started at address' + addr_str)
        return(1)

#----------------------------------
def gen_TEST_ram_data_crc(*args):
    "Generate data for TEST RAMs that includes CRC."
        
    addr_str = args[0]
    offset = args[3]
    if ( ((len(args) != 4) & (len(args) != 5)) | (len(args[2]) != 4) | (len(args[0]) != 10) | (addr_str[0:2] != '0x')| (offset<0) | (offset>7)):
        error_message = """
        Usage: gen_TEST_ram_data_crc (addr_str, num_blocks, start_str, offset)
            addr_str    = Base Address of RAM as an 8 character hex string
            num_blocks  = Number of frame crc blocks
            start_str   = Starting byte as a 2 character hex string
            offset      = Bit offset (0 to 7) to test byte alignment
            no_inc_flag = Optional flag (1= do not increment data)
        """
        logger.error(error_message)
        return(0)

    if (len(args) == 5):
        no_inc_flag = 1
    else:
        no_inc_flag = 0
        
    num_blocks = args[1]
    start_str = args[2]

    ## Default preamble
    PREAMBLE0 = [0,1,1,1,1,1,1,1]	# FE
    PREAMBLE1 = [0,0,1,1,1,0,1,1]	# DC

    ## New preamble if PREAMBLE assigned to a 16-bit hex string.
    if (len(PREAMBLE) == 6):
        logger.debug('gen_TEST_ram_data_crc: Preamble =' + PREAMBLE)
        first_byte = PREAMBLE[0:4]
        second_byte = PREAMBLE[0:2]+PREAMBLE[4:6]
        first_byte = list(reverse_str(hexstr2bits(first_byte, 8)))
        second_byte = list(reverse_str(hexstr2bits(second_byte, 8)))

        for i in range(0,7):
            PREAMBLE0[i] = int(first_byte[i])
            PREAMBLE1[i] = int(second_byte[i])

# CRC based on polynomial 1+x^2+x^4+x^5+x^8+x^11+x^12+x^14+x^16 (AC9A)
# Code is based on the original Verilog.
    def crc_calc(data_in, crc):
        crc_next=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0] 
        crc_next[0] = crc[8] ^ crc[10] ^ crc[13] ^ crc[14] ^ data_in[0] ^ data_in[2] ^ data_in[5] ^ data_in[6]
        crc_next[1] = crc[9] ^ crc[11] ^ crc[14] ^ crc[15] ^ data_in[1] ^ data_in[3] ^ data_in[6] ^ data_in[7]
        crc_next[2] = crc[8] ^ crc[12] ^ crc[13] ^ crc[14] ^ crc[15] ^ data_in[0] ^ data_in[4] ^ data_in[5] ^ data_in[6] ^ data_in[7]
        crc_next[3] = crc[9] ^ crc[13] ^ crc[14] ^ crc[15] ^ data_in[1] ^ data_in[5] ^ data_in[6] ^ data_in[7]
        crc_next[4] = crc[8] ^ crc[13] ^ crc[15] ^ data_in[0] ^ data_in[5] ^ data_in[7]
        crc_next[5] = crc[8] ^ crc[9] ^ crc[10] ^ crc[13] ^ data_in[0] ^ data_in[1] ^ data_in[2] ^ data_in[5]
        crc_next[6] = crc[9] ^ crc[10] ^ crc[11] ^ crc[14] ^ data_in[1] ^ data_in[2] ^ data_in[3] ^ data_in[6]
        crc_next[7] = crc[10] ^ crc[11] ^ crc[12] ^ crc[15] ^ data_in[2] ^ data_in[3] ^ data_in[4] ^ data_in[7]
        crc_next[8] = crc[0] ^ crc[8] ^ crc[10] ^ crc[11] ^ crc[12] ^ crc[14] ^ data_in[0] ^ data_in[2] ^ data_in[3] ^ data_in[4] ^ data_in[6]
        crc_next[9] = crc[1] ^ crc[9] ^ crc[11] ^ crc[12] ^ crc[13] ^ crc[15] ^ data_in[1] ^ data_in[3] ^ data_in[4] ^ data_in[5] ^ data_in[7]
        crc_next[10] = crc[2] ^ crc[10] ^ crc[12] ^ crc[13] ^ crc[14] ^ data_in[2] ^ data_in[4] ^ data_in[5] ^ data_in[6]
        crc_next[11] = crc[3] ^ crc[8] ^ crc[10] ^ crc[11] ^ crc[15] ^ data_in[0] ^ data_in[2] ^ data_in[3] ^ data_in[7]
        crc_next[12] = crc[4] ^ crc[8] ^ crc[9] ^ crc[10] ^ crc[11] ^ crc[12] ^ crc[13] ^ crc[14] ^ data_in[0] ^ data_in[1] ^ data_in[2] ^ data_in[3] ^ data_in[4] ^ data_in[5] ^ data_in[6]
        crc_next[13] = crc[5] ^ crc[9] ^ crc[10] ^ crc[11] ^ crc[12] ^ crc[13] ^ crc[14] ^ crc[15] ^ data_in[1] ^ data_in[2] ^ data_in[3] ^ data_in[4] ^ data_in[5] ^ data_in[6] ^ data_in[7]
        crc_next[14] = crc[6] ^ crc[8] ^ crc[11] ^ crc[12] ^ crc[15] ^ data_in[0] ^ data_in[3] ^ data_in[4] ^ data_in[7]
        crc_next[15] = crc[7] ^ crc[9] ^ crc[12] ^ crc[13] ^ data_in[1] ^ data_in[4] ^ data_in[5]
        return crc_next

#    def getnewdata(data8):
#        data8 = data8 % 256
#        fieldList = []
#        for i in range(0,8):
#            if (data8 % 2):
#                fieldList.append(1)
#            else:
#                fieldList.append(0)
#            data8 = data8 >> 1
#        return(fieldList)

    def getnewdata16(data16):
        data16 = data16 % 65536
        fieldList = []
        for i in range(0,16):
            if (data16 % 2):
                fieldList.append(1)
            else:
                fieldList.append(0)
            data16 = data16 >> 1
        return(fieldList)

    def get_data_str(hex0,hex1,hex2,hex3):
# hex0 ---> bits 31:24
# hex1 ---> bits 23:16
# hex2 ---> bits 15:8
# hex3 ---> bits 7:0
        hexval = int(hex0+hex1+hex2+hex3, 2)
        dataval = "{0:#0{1}x}".format(hexval,10)
        return (dataval)

#========= start main script ============

    startData = int(start_str, 16)

    final_frame = []	# All 8-bit values in a frame prior to any offset shift
    final_frame.append([0,0,0,0,0,0,0,0])
    i = 0
    j = 0
    final_frame.append(PREAMBLE0)
    final_frame.append(PREAMBLE1)
    dataInt = startData
    dataList = []
    dataList16 = []

    while i < num_blocks:
        crc_val = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]

        while j < BLOCK_LENGTH:
            if (j%2 == 0):
                dataList16 = getnewdata16(dataInt)
                dataList = dataList16[8:16]
                if (no_inc_flag != 1):
                    dataInt = dataInt+1         ## Just increment for test data  
            else:
                dataList = dataList16[0:8]

            crc_val = crc_calc(dataList, crc_val)
            final_frame.append(dataList)
            j = j+1

        final_frame.append(crc_val[8:])
        final_frame.append(crc_val[0:8])
        j = 0
        i = i+1

# Perform any required bitshifting
    shifted_array = []

    for k in range (0, len(final_frame)):
        if k == 0:      # Do not print out the all-zero data that exists prior to the preamble
            s_list = final_frame[k]
        else:
            old_list = final_frame[k-1]
            new_list = final_frame[k]
                
            if (offset == 0):
                s_list = final_frame[k]
            elif (offset == 1):
                s_list = [new_list[1],new_list[2],new_list[3],new_list[4],new_list[5],new_list[6],new_list[7],old_list[0]]
            elif (offset == 2):
                s_list = [new_list[2],new_list[3],new_list[4],new_list[5],new_list[6],new_list[7],old_list[0],old_list[1]]
            elif (offset == 3):
                s_list = [new_list[3],new_list[4],new_list[5],new_list[6],new_list[7],old_list[0],old_list[1],old_list[2]]
            elif (offset == 4):
                s_list = [new_list[4],new_list[5],new_list[6],new_list[7],old_list[0],old_list[1],old_list[2],old_list[3]]
            elif (offset == 5):
                s_list = [new_list[5],new_list[6],new_list[7],old_list[0],old_list[1],old_list[2],old_list[3],old_list[4]]
            elif (offset == 6):
                s_list = [new_list[6],new_list[7],old_list[0],old_list[1],old_list[2],old_list[3],old_list[4],old_list[5]]
            elif (offset == 7):
                s_list = [new_list[7],old_list[0],old_list[1],old_list[2],old_list[3],old_list[4],old_list[5],old_list[6]]
#           logger.debug('old=' + str(old_list) + 'new=' + str(new_list) + 's=' + str(s_list))
                
            s_listtxt = str(s_list[7])+str(s_list[6])+str(s_list[5])+str(s_list[4])+str(s_list[3])+str(s_list[2])+str(s_list[1])+str(s_list[0])
            shifted_array.append(s_listtxt)

    address = int(addr_str, 16)  ## Base Address
    err = 0
    
    for m in range (0, len(shifted_array)/4):
        data_str = get_data_str(shifted_array[4*m+0],shifted_array[4*m+1],shifted_array[4*m+2],shifted_array[4*m+3])
#        logger.debug('data_str=' + str(data_str))
        err = err | (not write_reg(hex(address), data_str))
        address = address + 4  
    if (err):
        return(0)
    else:
        logger.debug('gen_TEST_ram_data_crc: Finished RAM' + addr_str)
        return(1)
     
#----------------------------------
def clear_TEST_buff_ram():
    "Sets the contents of the TEST data buffer to 0."
    for i in range(0,int(0x3FFF)):
        waddr = hex(TEST_BUFF_BASE_ADDR + i*4)
        if (write_reg(waddr, '0x00000000')):
            return(1)
        else:
            return(0)

#============================================================================
## DATA Functions ###########################################################

def set_DATA_reset(value):
    "Reset DATA control to default values." 
#DATA_status Register address=0x43C14000
#APP_dma_err        27
#APP_active         26
#APP_buffer_empty   25
#APP_buffer_full    24
#APP_enable         16
#DATA_accum_mode	15
#DATA_ascii_download	12
#DATA_in_select     10
#DATA_out_select    9:8
#DATA_CRC_err3      7
#DATA_CRC_err2      6
#DATA_CRC_err1      5
#DATA_CRC_err0      4
#DATA_continuous    3 ## Unused
#DATA_enable        2
#DATA_reset         0

#DATA_control Register address=0x43C14004
#DATA_accum_mode	31
#DATA_ascii_download	24
#DATA_input_disable3	23
#DATA_input_disable2	22
#DATA_input_disable1	21
#DATA_input_disable0	20
#DATA_bypass        16
#DATA_in_select     12
#DATA_out_select    9:8
#DATA_test_crc3     7
#DATA_test_crc2     6
#DATA_test_crc1     5
#DATA_test_crc0     4
#DATA_continuous    3 ## Unused
#DATA_enable        2
#DATA_reset_err     1
#DATA_reset         0
    logger.debug('set_DATA_reset =' + str(value))
    err = clear_DATA_crc_err(value)   ## Will be cleared by reset
    if (value != 0):
        addr_str = hex(DATA_BASE_ADDR + 0x00000004)
        read_modify_write(addr_str, '1', 0)             ## Set reset bit at index 0
        status = read_reg(hex(DATA_BASE_ADDR + 0))
        status = get_msg_bits(status)
        if (status[0:16] != '1000000000000000'): ## Upper 16 bits controlled by CPU1 APP
            logger.error('set_DATA_reset: DATA_reset=1, unexpected DATA_status bits[0:15] =' + status)
            return(0)
        for i in range(2,15):                        ## Check DATA registers
            addr_str = hex(DATA_BASE_ADDR + i*4)
            rdata = read_reg(addr_str)
            if (addr_str == '0x43c1400c'):   ## DATA_accum register with default field offset value
                expected = '0x02000000'
            else:
                expected = '0x00000000'
            if (rdata != expected):
                logger.error('set_DATA_reset: DATA_reset=1, address=' + addr_str + 'value=' + rdata + 'not default value.')
                return(0)
        return(1)
    else:
        addr_str = hex(DATA_BASE_ADDR + 0x00000004)
        read_modify_write(addr_str, '0', 0)    ## Clear reset bit at index 0
        status = read_reg(hex(DATA_BASE_ADDR + 0))
        status = get_msg_bits(status)
        if (status[0:16] != '0000000000000000'): ## Upper 16 bits controlled by CPU1 APP
            logger.error('set_DATA_reset: DATA_reset=0, unexpected DATA_status bits[0:15] =' + status)
            return(0)
    return(1)

#----------------------------------
def set_DATA_enable(value):
    "Set DATA enable 0=idle, 1=fill buffer memory."
#DATA_control Register address=0x43C14004
#DATA_accum_mode	31
#DATA_ascii_download	24
#DATA_input_disable3	23
#DATA_input_disable2	22
#DATA_input_disable1	21
#DATA_input_disable0	20
#DATA_bypass        16
#DATA_in_select     12
#DATA_out_select    9:8
#DATA_test_crc3     7
#DATA_test_crc2     6
#DATA_test_crc1     5
#DATA_test_crc0     4
#DATA_continuous    3 ## Unused
#DATA_enable        2
#DATA_reset_err     1
#DATA_reset         0
    logger.debug('set_DATA_enable =' + str(value))

    if (check_APP_dma_err() == 1):
        logger.warn('set_DATA_enable: DMA Error bit is set.')
        
    addr_str = hex(DATA_BASE_ADDR + 0x00000004)
    if (value != 0):
        if (read_modify_write(addr_str, '1', 2)):    ## Update enable bit at index 2
            return(1)
        return(0)
    else:
        if (read_modify_write(addr_str, '0', 2)):    ## Update enable bit at index 2
            return(1)
        return(0)

#----------------------------------
def set_DATA_test_delay(*args):
    "Sets a delay before data is dumped to ensure test completes, arguments = num_vtrig_cycles, vtrig_period in usec."
#DATA_frame Register address=0x43C14008
#DATA_preamble_length	28
#DATA_crc_length	27:16
#DATA_frame_length	15:0
    if (len(args) != 2):
        error_message = """
        Usage: set_DATA_test_delay(num_vtrig_cycles, vtrig_period)
            num_vtrig_cycles = Integer number of VTRIG cycles
            vtrig_period     = VTRIG period in usec
        """
        logger.error(error_message)
        return(0)

    vtrig_cycles = args[0]
    period = args[1]
      
    countdown_delay = 10   ## Delay in seconds
    countdown_time = 0
    
    logger.debug("set_DATA_test_delay: Arguments = %d VTRIG cycles with %7.2f usec period" %(vtrig_cycles, period))

    VTRIG_num_cycles = get_VTRIG_num_cycles()
    logger.debug('set_DATA_test_delay Check: programmed VTRIG_num_cycles=' + str(VTRIG_num_cycles))
    if (vtrig_cycles < VTRIG_num_cycles):
        logger.debug('set_DATA_test_delay: specified number of VTRIG cycles less than the programmed value, setting to the higher bvalue.')
        vtrig_cycles = VTRIG_num_cycles

    delay = 0.1 + vtrig_cycles*period*1e-6   ## period in usec, delay in sec plus 0.1 sec margin

    if (delay < 2):
        logger.debug("set_DATA_test_delay: Delay for a minimum of %-7.4f msec." % (delay*1000))
    elif(delay < 300):
        logger.debug("set_DATA_test_delay: Delay for a minimum of %-7.4f sec." % (delay))
    else:
        logger.debug("set_DATA_test_delay: Delay for a minimum of %-7.4f min." % (delay/60.0))
        
    while (countdown_time < delay):
        if (delay < countdown_delay):
            time.sleep(delay)
            break
        else:
            time.sleep(countdown_delay)
            countdown_time = countdown_time+countdown_delay
            if (delay < 200):
                logger.debug('set_DATA_test_delay: Delay time=' + str(countdown_time) + 'seconds.')
            else:
                if ((countdown_time % (countdown_delay*3)) == 0):
                    logger.debug("set_DATA_test_delay: Delay time= %-7.4f minutes." % (countdown_time/60.0))

    logger.debug('set_DATA_test_delay: Delay time finished.')
    return(delay)
    
#----------------------------------
def set_DATA_bypass(value):
    "Set DATA bypass 0=DATA Slice logic active, 1=bypass DATA Slice logic."
#DATA_control Register address=0x43C14004
#DATA_accum_mode	31
#DATA_ascii_download	24
#DATA_input_disable3	23
#DATA_input_disable2	22
#DATA_input_disable1	21
#DATA_input_disable0	20
#DATA_bypass        16
#DATA_in_select     12
#DATA_out_select    9:8
#DATA_test_crc3     7
#DATA_test_crc2     6
#DATA_test_crc1     5
#DATA_test_crc0     4
#DATA_continuous    3 ## Unused
#DATA_enable        2
#DATA_reset_err     1
#DATA_reset         0
    logger.debug('set_DATA_bypass =' + str(value))
    addr_str = hex(DATA_BASE_ADDR + 0x00000004)
    if (value != 0):
        if (read_modify_write(addr_str, '1', 16)):    ## Update bypass bit at index 11
            return(1)
        return(0)
    else:
        if (read_modify_write(addr_str, '0', 16)):    ## Update bypass bit at index 11
            return(1)
        return(0)

#----------------------------------
def read_DATA_crc_err_count(value):
    "Read CRC Error Count for Data Lane number 0 to 3."
#DATA_err_count0 Register address=0x43C14020
#DATA_err_count1 Register address=0x43C14024
#DATA_err_count2 Register address=0x43C14028
#DATA_err_count3 Register address=0x43C1402C
    if ((value < 0) | (value > 3)):
        logger.error('read_DATA_crc_err_count: Value must be one of 0,1,2 or 3.')
        return(0)
    addr_str = hex(DATA_BASE_ADDR + 0x00000020 + (0x4*value))
    count = int(read_reg(addr_str), 16)
    logger.debug('read_DATA_crc_err_count: DATA Lane' + str(value) + '=' + str(count) + ' CRC errors.')
    return(count)

#----------------------------------
def clear_DATA_crc_err(value):
    "Clear DATA CRC error count 0=CRC errors count, 1=CRC error count held to 0."
#DATA_control Register address=0x43C14004
#DATA_accum_mode	31
#DATA_ascii_download	24
#DATA_input_disable3	23
#DATA_input_disable2	22
#DATA_input_disable1	21
#DATA_input_disable0	20
#DATA_bypass        16
#DATA_in_select     12
#DATA_out_select    9:8
#DATA_test_crc3     7
#DATA_test_crc2     6
#DATA_test_crc1     5
#DATA_test_crc0     4
#DATA_continuous    3 ## Unused
#DATA_enable        2
#DATA_reset_err     1
#DATA_reset         0
    logger.debug('clear_DATA_crc_err =' + str(value))
    addr_str = hex(DATA_BASE_ADDR + 0x00000004)
    if (value != 0):
        if (read_modify_write(addr_str, '1', 1)):    ## Update reset_err bit at index 1
            return(1)
        return(0)
    else:
        if (read_modify_write(addr_str, '0', 1)):    ## Update reset_err bit at index 1
            return(1)
        return(0)
    
#---------------------------------
def set_DATA_test_crc(lane_num):
    "Set DATA CRC test bit for specified lane number."
#DATA_control Register address=0x43C14004
#DATA_accum_mode	31
#DATA_ascii_download	24
#DATA_input_disable3	23
#DATA_input_disable2	22
#DATA_input_disable1	21
#DATA_input_disable0	20
#DATA_bypass        16
#DATA_in_select     12
#DATA_out_select    9:8
#DATA_test_crc3     7
#DATA_test_crc2     6
#DATA_test_crc1     5
#DATA_test_crc0     4
#DATA_continuous    3 ## Unused
#DATA_enable        2
#DATA_reset_err     1
#DATA_reset         0
    logger.debug('set_DATA_test_crc for Lane' + str(lane_num))
    if ((int(lane_num) < 0) | (int(lane_num) > 3)):
        error_messge = """
        Usage: set_DATA_test_crc(lane_num)
            lane_num = 0 to 3.
        """
        logger.error(error_messge)
        return(0)
    addr_str = hex(DATA_BASE_ADDR + 0x00000004)     ## DATA_control Register
    if (read_modify_write(addr_str, '1', (lane_num+4))):  ## Update CRC Test bit
        return(1)
    return(0)
    
#---------------------------------
def set_DATA_ascii_download(value):
    "Specify data format for download to PC."
#DATA_control Register address=0x43C14004
#DATA_accum_mode	31
#DATA_ascii_download	24
#DATA_input_disable3	23
#DATA_input_disable2	22
#DATA_input_disable1	21
#DATA_input_disable0	20
#DATA_bypass        16
#DATA_in_select     12
#DATA_out_select    9:8
#DATA_test_crc3     7
#DATA_test_crc2     6
#DATA_test_crc1     5
#DATA_test_crc0     4
#DATA_continuous    3 ## Unused
#DATA_enable        2
#DATA_reset_err     1
#DATA_reset         0
    logger.debug('set_DATA_ascii_download =' + str(value))
    addr_str = hex(DATA_BASE_ADDR + 0x00000004)
    if (value != 0):
        if (read_modify_write(addr_str, '1', 24)):    ## Update bit at index 24
            return(1)
        return(0)
    else:
        if (read_modify_write(addr_str, '0', 24)):    ## Update bit at index 24
            return(1)
        return(0)
    
#---------------------------------
def set_DATA_accum_mode(value):
    "Specify DATA Accumulation Mode for Views."
#DATA_control Register address=0x43C14004
#DATA_accum_mode	31
#DATA_ascii_download	24
#DATA_input_disable3	23
#DATA_input_disable2	22
#DATA_input_disable1	21
#DATA_input_disable0	20
#DATA_bypass        16
#DATA_in_select     12
#DATA_out_select    9:8
#DATA_test_crc3     7
#DATA_test_crc2     6
#DATA_test_crc1     5
#DATA_test_crc0     4
#DATA_continuous    3 ## Unused
#DATA_enable        2
#DATA_reset_err     1
#DATA_reset         0
    logger.debug('set_DATA_accum_mode =' + str(value))
    addr_str = hex(DATA_BASE_ADDR + 0x00000004)         
    if (value != 0):
        if (read_modify_write(addr_str, '1', 31)):    ## Update bit at index 31
            return(1)
        return(0)
    else:
        if (read_modify_write(addr_str, '0', 31)):    ## Update bit at index 31
            return(1)
        return(0)

#---------------------------------
def clear_DATA_test_crc(lane_num):
    "Clears DATA CRC test bit for specified lane number."
#DATA_control Register address=0x43C14004
#DATA_accum_mode	31
#DATA_ascii_download	24
#DATA_input_disable3	23
#DATA_input_disable2	22
#DATA_input_disable1	21
#DATA_input_disable0	20
#DATA_bypass        16
#DATA_in_select     12
#DATA_out_select    9:8
#DATA_test_crc3     7
#DATA_test_crc2     6
#DATA_test_crc1     5
#DATA_test_crc0     4
#DATA_continuous    3 ## Unused
#DATA_enable        2
#DATA_reset_err     1
#DATA_reset         0
    logger.debug('clear_DATA_test_crc for Lane' + str(lane_num))
    if ((int(lane_num) < 0) | (int(lane_num) > 3)):
        error_message = """
        Usage: clear_DATA_test_crc(lane_num)
            lane_num = 0 to 3.
        """
        logger.error(error_message)
        return(0)
    addr_str = hex(DATA_BASE_ADDR + 0x00000004)     ## DATA_control Register
    if (read_modify_write(addr_str, '0', (lane_num+4))):  ## Update CRC Test bit
        return(1)
    return(0)

#----------------------------------
def set_DATA_out_select(bit_code):
    "Set DATA selection after DATA Slice logic as a 2-bit string."
#DATA_control Register address=0x43C14004
#DATA_accum_mode	31
#DATA_ascii_download	24
#DATA_input_disable3	23
#DATA_input_disable2	22
#DATA_input_disable1	21
#DATA_input_disable0	20
#DATA_bypass        16
#DATA_in_select     12
#DATA_out_select    9:8
#DATA_test_crc3     7
#DATA_test_crc2     6
#DATA_test_crc1     5
#DATA_test_crc0     4
#DATA_continuous    3 ## Unused
#DATA_enable        2
#DATA_reset_err     1
#DATA_reset         0

    logger.debug('set_DATA_out_select =' + str(bit_code))
    if ((int(bit_code, 2) > 3) | (int(bit_code, 2) < 0)):
        error_message = """
        Usage: set_DATA_out_select(binary string)
            00 = None selected.
            01 = Clinical A data selected (1 lane).
            10 = Clinical B data selected (4 lanes).
            11 = Test Generated Data selected (4 lanes)
        """
        logger.error(error_message)
        return(0)
    addr_str = hex(DATA_BASE_ADDR + 0x00000004)     ## DATA_control Register
    bit_code = reverse_str(bit_code)
    if (read_modify_write(addr_str, bit_code, 8)):  ## Update out select bits starting at index 8
        return(1)
    return(0)

#----------------------------------
def set_DATA_in_select(value):
    "Set DATA selection at input to DATA Slice logic 0=function data, 1=TEST RAM data."
#DATA_control Register address=0x43C14004
#DATA_accum_mode	31
#DATA_ascii_download	24
#DATA_input_disable3	23
#DATA_input_disable2	22
#DATA_input_disable1	21
#DATA_input_disable0	20
#DATA_bypass        16
#DATA_in_select     12
#DATA_out_select    9:8
#DATA_test_crc3     7
#DATA_test_crc2     6
#DATA_test_crc1     5
#DATA_test_crc0     4
#DATA_continuous    3 ## Unused
#DATA_enable        2
#DATA_reset_err     1
#DATA_reset         0
    logger.debug('set_DATA_in_select =' + str(value))
    addr_str = hex(DATA_BASE_ADDR + 0x00000004)
    if (value != 0):
        if (read_modify_write(addr_str, '1', 12)):    ## Update select bit at index 12
            return(1)
        return(0)
    else:
        if (read_modify_write(addr_str, '0', 12)):    ## Update select bit at index 12
            return(1)
        return(0)

#----------------------------------
def set_DATA_input_disable(lane_num):
    "Set DATA input disable bit for specified lane number."
#DATA_control Register address=0x43C14004
#DATA_accum_mode	31
#DATA_ascii_download	24
#DATA_input_disable3	23
#DATA_input_disable2	22
#DATA_input_disable1	21
#DATA_input_disable0	20
#DATA_bypass        16
#DATA_in_select     12
#DATA_out_select    9:8
#DATA_test_crc3     7
#DATA_test_crc2     6
#DATA_test_crc1     5
#DATA_test_crc0     4
#DATA_continuous    3 ## Unused
#DATA_enable        2
#DATA_reset_err     1
#DATA_reset         0
    logger.debug('set_DATA_input_disable for Lane' + str(lane_num))
    if ((int(lane_num) < 0) | (int(lane_num) > 3)):
        error_message = """
        Usage: set_DATA_input_disable(lane_num)
            lane_num = 0 to 3."""
        logger.error(error_message)
        return(0)
    addr_str = hex(DATA_BASE_ADDR + 0x00000004)     ## DATA_control Register
    if (read_modify_write(addr_str, '1', (lane_num+20))):  ## Update bit
        return(1)
    return(0)

#----------------------------------
def clear_DATA_input_disable(lane_num):
    "Clear DATA input disable bit for specified lane number."
#DATA_control Register address=0x43C14004
#DATA_accum_mode	31
#DATA_ascii_download	24
#DATA_input_disable3	23
#DATA_input_disable2	22
#DATA_input_disable1	21
#DATA_input_disable0	20
#DATA_bypass        16
#DATA_in_select     12
#DATA_out_select    9:8
#DATA_test_crc3     7
#DATA_test_crc2     6
#DATA_test_crc1     5
#DATA_test_crc0     4
#DATA_continuous    3 ## Unused
#DATA_enable        2
#DATA_reset_err     1
#DATA_reset         0
    logger.debug('clear_DATA_input_disable for Lane' + str(lane_num))
    if ((int(lane_num) < 0) | (int(lane_num) > 3)):
        error_message = """
        Usage: clear_DATA_input_disable(lane_num)
            lane_num = 0 to 3.
        """
        logger.error(error_message)
        return(0)
    addr_str = hex(DATA_BASE_ADDR + 0x00000004)     ## DATA_control Register
    if (read_modify_write(addr_str, '0', (lane_num+20))):  ## Update  bit
        return(1)
    return(0)

#----------------------------------
def set_DATA_frame_length(in_bytes):
    "Set DM frame length in bytes, not including preamble."
#DATA_frame Register address=0x43C14008
#DATA_preamble_length	28
#DATA_crc_length	27:16
#DATA_frame_length	15:0
    if ((in_bytes < 26) | (in_bytes > 65520)):
        logger.error('set_DATA_frame_length: Frame length must be from 16 to 65520 bytes. No value set.')
        return(0)
    bit_field = reverse_str(hexstr2bits(hex(in_bytes), 16))   ## 16 = Length of bit field
    addr_str = hex(DATA_BASE_ADDR + 0x00000008)
            
    if (read_modify_write(addr_str, bit_field, 0)):      ## Update field starting at index 0
        logger.debug('set_DATA_frame_length:' + str(in_bytes )+ 'bytes.')
#        write_reg(acc_addr_str, hex(in_bytes))
        return(1)
    else:
        return(0)

#----------------------------------
def set_DATA_accum_num(value):
    "Set number of views per Accumulated View."
#DATA_accum_num Register address=0x43C1400C
#DATA_field_offset  31:24
#Unused             23:16
#DATA_accum_num     15:0
    if (value > 65535):
        logger.warn('set_DATA_accum_num: Value ' + str(value) + ' exceeds 16-bits. Set to 65535 views.')
        logger.warn('        Number of VTRIG cycles may be changed by check_VTRIG_num_cycles() function.')
        value = 65535
    if (value < 2):
        logger.warn('set_DATA_accum_num: Value ' + str(value) + ' less than minimum of 2. Set to 2 views.')
        value = 2
    bit_field = reverse_str(hexstr2bits(hex(value), 16))   ## 16 = Length of bit field
    addr_str = hex(DATA_BASE_ADDR + 0x0000000C)
    if (read_modify_write(addr_str, bit_field, 0)):      ## Update field starting at index 0
        logger.debug('set_DATA_accum_num: ' + str(value) + ' views per Accumulated View.')
        return(1)
    else:
        return(0)

#----------------------------------
def get_DATA_accum_num():
    "Returns number of views per Accumulated View as an integer."
#DATA_accum_num Register address=0x43C1400C
#DATA_field_offset  31:24
#Unused             23:16
#DATA_accum_num     15:0
    addr_str = hex(DATA_BASE_ADDR + 0x0000000C)
    rdata = read_reg(addr_str)  ## rdata format = 0x00000000
    rdata = int(rdata[6:10], 16)
    return(rdata)

#----------------------------------
def set_DATA_field_offset(value):
    "Set offset in bytes from start of frame to first valid count field."
#-For example, with 16-bit preamble, offset is 2 bytes
#DATA_accum_num Register address=0x43C1400C
#DATA_field_offset  31:24
#Unused             23:16
#DATA_accum_num     15:0
    if ((value > 255) | (value < 0)):
        logger.error('set_DATA_field_offset: Value' + str(value) + 'not an 8-bit value')
        sys.exit(1)
    bit_field = reverse_str(hexstr2bits(hex(value), 8))   ## 8 = Length of bit field
    addr_str = hex(DATA_BASE_ADDR + 0x0000000C)
    if (read_modify_write(addr_str, bit_field, 24)):      ## Update field starting at index 24
        logger.debug('set_DATA_field_offset:' + str(value) + 'bytes from start of frame to first valid count field.')
        return(1)
    else:
        return(0)
    
#----------------------------------
def set_DATA_crc_length(in_bytes):
    "Set number of CRC fields in DM frame length in bytes."
#DATA_frame Register address=0x43C14008
#DATA_preamble_length	28
#DATA_crc_length	27:16
#DATA_frame_length	15:0
    if ((in_bytes < 0) | (in_bytes > 4095)):
        logger.error('set_DATA_frame_length: Frame length must be from 0 to 4095 bytes. No value set.')
        return(0)
    bit_field = reverse_str(hexstr2bits(hex(in_bytes), 12))   ## 12 = Length of bit field
    addr_str = hex(DATA_BASE_ADDR + 0x00000008)

    if (read_modify_write(addr_str, bit_field, 16)):      ## Update field starting at index 16
        logger.debug('set_DATA_crc_length:' + str(in_bytes) + 'bytes.')
#        write_reg(acc_addr_str, hex(in_bytes))
        return(1)
    else:
        return(0)
        
#----------------------------------
def set_DATA_preamble_length(in_words):
    "Set DM preamble length in number of 16-bit words."
#DATA_frame Register address=0x43C14008
#DATA_preamble_length	28
#DATA_crc_length	27:16
#DATA_frame_length	15:0
    logger.warn('In current version of DM Buffer preamble length fixed at 16 bits.')
    return(1)
    
#    if ((in_words <= 0) | (in_words > 2)):
#        logger.debug('set_DATA_preamble_length: Preamble length must be one or two 16-bit words. No value set.')
#        return(0)
#    bit_field = hex(in_words - 1)
#    bit_field = bit_field[2]            ## Ignore 0x prefix
#    addr_str = hex(DATA_BASE_ADDR + 0x00000008)
#    if (read_modify_write(addr_str, bit_field, 28)):      ## Update field starting at index 28
#        logger.debug('set_DATA_preamble_length:' + in_words + 'words or' + 2*in_words + 'bytes.')
#        return(1)
#    else:
#        return(0)

#----------------------------------
def set_DATA_preamble(value_str):
    "Set DM preamble value for one 16-bit word as a hex string."
#DATA_preamble0 Register address=0x43C14010
    
    if (int(value_str, 16) > 0xffff):
        logger.error('set_DATA_preamble: Input greater than 0xFFFF. No value set.')
        return(0)
    addr_str = hex(DATA_BASE_ADDR + 0x00000010)
    value_str = value_str + '0000'
    if (write_reg(addr_str, value_str)):
        logger.debug('set_DATA_preamble: Set to' + value_str)
        return(1)
    return(0)

#----------------------------------
def check_APP_ready():
    "Checks that the Application Software on CPU1 is running."
#DATA_status Register address=0x43C14000
#APP_dma_err        27
#APP_active         26
#APP_buffer_empty   25
#APP_buffer_full    24
#APP_enable         16
#DATA_accum_mode	15
#DATA_ascii_download	12
#DATA_in_select     10
#DATA_out_select    9:8
#DATA_CRC_err3      7
#DATA_CRC_err2      6
#DATA_CRC_err1      5
#DATA_CRC_err0      4
#DATA_continuous    3 ## Unused
#DATA_enable        2
#DATA_reset         0
    addr_str = hex(DATA_BASE_ADDR + 0x00000000)      ## Status address
    status = read_reg(addr_str)
    status = get_msg_bits(status)
    if (status[26] == '0'):
        logger.debug('check_APP_ready: CPU1 not running, status =' + status)
        return(0)
    if (status[27] == '1'):
        logger.debug('check_APP_ready: DMA Internal Error detected.')
        return(0)
    logger.debug('check_APP_ready: CPU1 Application Software FSM is running.')
    return(1)

#----------------------------------
def check_APP_dma_err():
    "Checks if DM Buffer DMA error status bit is set, returns 1 if true, 0 if false."
#DATA_status Register address=0x43C14000
#APP_dma_err        27
#APP_active         26
#APP_buffer_empty   25
#APP_buffer_full    24
#APP_enable         16
#DATA_accum_mode	15
#DATA_ascii_download	12
#DATA_in_select     10
#DATA_out_select    9:8
#DATA_CRC_err3      7
#DATA_CRC_err2      6
#DATA_CRC_err1      5
#DATA_CRC_err0      4
#DATA_continuous    3 ## Unused
#DATA_enable        2
#DATA_reset         0
    addr_str = hex(DATA_BASE_ADDR + 0x00000000)      ## Status address
    status = read_reg(addr_str)
    status = get_msg_bits(status)
    if (status[27] == '0'):
        return(0)
    else:
        return(1)

#----------------------------------
def check_APP_buffer_full():
    "Checks if DM Buffer full status bit is set, returns 1 if true, 0 if false."
#DATA_status Register address=0x43C14000
#APP_dma_err        27
#APP_active         26
#APP_buffer_empty   25
#APP_buffer_full    24
#APP_enable         16
#DATA_accum_mode	15
#DATA_ascii_download	12
#DATA_in_select     10
#DATA_out_select    9:8
#DATA_CRC_err3      7
#DATA_CRC_err2      6
#DATA_CRC_err1      5
#DATA_CRC_err0      4
#DATA_continuous    3 ## Unused
#DATA_enable        2
#DATA_reset         0
    addr_str = hex(DATA_BASE_ADDR + 0x00000000)      ## Status address
    status = read_reg(addr_str)
    status = get_msg_bits(status)
    if (status[24] == '0'):
        return(0)
    else:
        return(1)
    
#----------------------------------
def check_APP_buffer_empty():
    "Checks if DM Buffer empty status bit is set, returns 1 if true, 0 if false."
#DATA_status Register address=0x43C14000
#APP_dma_err        27
#APP_active         26
#APP_buffer_empty   25
#APP_buffer_full    24
#APP_enable         16
#DATA_accum_mode	15
#DATA_ascii_download	12
#DATA_in_select     10
#DATA_out_select    9:8
#DATA_CRC_err3      7
#DATA_CRC_err2      6
#DATA_CRC_err1      5
#DATA_CRC_err0      4
#DATA_continuous    3 ## Unused
#DATA_enable        2
#DATA_reset         0
    addr_str = hex(DATA_BASE_ADDR + 0x00000000)      ## Status address
    status = read_reg(addr_str)
    status = get_msg_bits(status)
    if (status[25] == '0'):
        return(0)
    else:
        return(1)

#----------------------------------
def check_DATA_ascii_download_set():
    "Checks if ASCII Download bit is set, returns 1 if true, 0 if false."
#DATA_status Register address=0x43C14000
#APP_dma_err        27
#APP_active         26
#APP_buffer_empty   25
#APP_buffer_full    24
#APP_enable         16
#DATA_accum_mode	15
#DATA_ascii_download	12
#DATA_in_select     10
#DATA_out_select    9:8
#DATA_CRC_err3      7
#DATA_CRC_err2      6
#DATA_CRC_err1      5
#DATA_CRC_err0      4
#DATA_continuous    3 ## Unused
#DATA_enable        2
#DATA_reset         0
    addr_str = hex(DATA_BASE_ADDR + 0x00000000)      ## Status address
    status = read_reg(addr_str)
    status = get_msg_bits(status)
    if (status[12] == '0'):
        return(0)
    else:
        return(1)

#----------------------------------
def check_DATA_accum_mode_set():
    "Checks if Accumulation Mode bit is set, returns 1 if true, 0 if false."
#DATA_status Register address=0x43C14000
#APP_dma_err        27
#APP_active         26
#APP_buffer_empty   25
#APP_buffer_full    24
#APP_enable         16
#DATA_accum_mode	15
#DATA_ascii_download	12
#DATA_in_select     10
#DATA_out_select    9:8
#DATA_CRC_err3      7
#DATA_CRC_err2      6
#DATA_CRC_err1      5
#DATA_CRC_err0      4
#DATA_continuous    3 ## Unused
#DATA_enable        2
#DATA_reset         0
    addr_str = hex(DATA_BASE_ADDR + 0x00000000)      ## Status address
    status = read_reg(addr_str)
    status = get_msg_bits(status)
    if (status[15] == '0'):
        return(0)
    else:
        return(1)
    
#----------------------------------
def set_APP_enable(value):
    "Set APP enable for TEST PURPOSES ONLY 0=disabled, 1=enabled."
# Enable Bit is normally controlled by CPU1 FSM
#APP_control Register address=0x43C1403C
#APP_dma_err        28
#APP_active         24
#APP_buffer_empty   16
#APP_buffer_full    8
#APP_enable         0
    logger.debug('set_APP_enable =' + str(value))
    addr_str = hex(DATA_BASE_ADDR + 0x0000003C)
    if (value != 0):
        if (read_modify_write(addr_str, '1', 0)):    ## Update enable bit at index 0
            return(1)
        return(0)
    else:
        if (read_modify_write(addr_str, '0', 0)):    ## Update enable bit at index 0
            return(1)
        return(0)

#----------------------------------
def check_APP_enable():
    "Checks if APP enable control bit is set."
# Enable Bit is normally controlled by CPU1 FSM
#APP_control Register address=0x43C1403C
#APP_dma_err        28
#APP_active         24
#APP_buffer_empty   16
#APP_buffer_full    8
#APP_enable         0
    addr_str = hex(DATA_BASE_ADDR + 0x0000003C)
    control = read_reg(addr_str)
    control = get_msg_bits(control)
    if (control[0] == '0'):
        return(0)
    else:
        return(1)

#-----------------------------------
def dump_to_file(*args):
    "Open file for binary or ASCII download, args = (filename, <1>) where optional 1 means add new line every 4 lanes."
# A download is assumed to be binary unless the function 'set_DATA_ascii_download(1)' has been
# previously executed.
# In Normal Mode, all the raw view data is stored in consecutive in full buffers except for the
# last buffer which may be partially filled.
# In Accumulation Mode, each accumulated view is stored in consecutive in full buffers except for
# the last buffer which may be partially filled.
# That is, instead of a single partially filled buffer as in Normal Mode, each accumulated view
# may have a partially filled buffer. A control flag BRK is appended to mark this.
# In Normal Mode a EOF flag marks the single partially filled buffer at the end.

    err = 0
    value = get_SYS_errors();
    if (value == '10'):  ## Error code.
        set_CPU1_FSM_disable(1)
        logger.error('dump_to_file: Terminated due to VTRIG_period too short.')
        logger.error('             VTRIG cycle started while previous data was still being received')
        sys.exit(1)

    addr_str = hex(ERR_FLAGS) ## Check to make sure DMA error has not occurred
    addr_str = addr_str.strip('L')
    value = read_reg(addr_str)    
    if (value == '0xffffffff'):  ## Error code. Python always uses lowercase
        set_CPU1_FSM_disable(1)
        logger.error('dump_to_file: Terminated due to DMA Status Error')
        logger.error('Possible cause: Incoming packet larger than specified in DMA Length register')
        sys.exit(1)

    addr_str = hex(NUM_VIEW_BYTES)
    addr_str = addr_str.strip('L')
    view_bytes = read_reg(addr_str)
    view_bytes = int(view_bytes, 16)
    logger.debug('dump_to_file: Read number of view bytes=' + str(view_bytes))

    if (check_DATA_accum_mode_set() == 1):
        accum_mode = 1
        accum_num = get_DATA_accum_num()
    else:
        accum_mode = 0

    def est_download_time(est_rate):
        "Estimate download time for ASCII file."
        addr_str = hex(TOTAL_NUM_BYTES)
        addr_str = addr_str.strip('L')
        num_bytes = int(read_reg(addr_str), 16)
        if (accum_mode):
            logger.debug('dump_to_file: Number of Accumulated View bytes to download =' + str(num_bytes))
            logger.debug('dump_to_file: Estimated download rate =' + str(est_rate) + 'KBytes/sec (Windows 7)')
        else:
            logger.debug('dump_to_file: Number of DATA bytes to download =' + str(num_bytes))
            logger.debug('dump_to_file: Estimated download rate =' + str(est_rate) + 'KBytes/sec (Windows 7)')
        return(num_bytes/(est_rate*1000.0))

    ascii_file = check_DATA_ascii_download_set()
    if (len(args) == 1):
        if (type(args[0]) is str):
            add_newline = 0
        else:
            logger.error('dump_to_file: File name argument must be a string')
            return(0)
    elif (len(args) == 2):
        if (ascii_file == 1):
            add_newline = 1
            logger.debug('dump_to_file: Add \\n after every 8 bytes (4 data lanes) in ASCII file.')
        else:
            add_newline = 0
            logger.debug('dump_to_file: Second argument ignored for binary download.')
    else:
        error_message = """
        Usage: dump_to_file(filename, <flag>)
            filename: Write data filename as a string.
            flag    : Optional flag=1 means add \n after every 4 lanes (download rate slower)
        """
        logger.error(error_message)
        sys.exit(1)

    if (ascii_file == 1):
        if (add_newline == 1):
            download_rate = EST_ASCII_NL_RATE
            logger.debug('dump_to_file: Downloading ASCII file with new line characters.')
        else:
            download_rate = EST_ASCII_RATE
            logger.debug('dump_to_file: Downloading ASCII file with NO new line characters.')
    else:
        download_rate = EST_BINARY_RATE
        logger.debug('dump_to_file: Downloading binary file.')
    logger.debug("dump_to_file: Estimated download time = %-6.1f sec" % est_download_time(download_rate))

    file_name = args[0]
    logger.debug("dump_to_file: Setting write file name to %s " % (file_name))

    if (ascii_file == 1):
        dfile = open(file_name, 'w')
    else:
        dfile = open(file_name, 'wb')
        
    globals.buffer.socket.send('D 0xA')   # Start ASCII dump (no ack message required)
    while(1):
        try:
            timeStart = time.time()
            rdata, sockAddr = globals.buffer.socket.recvfrom(RX_DGRAM_SIZE) # [rc 101517] changed to return up to RX_DGRAM_SIZE bytes
            #rdata = globals.buffer.socket.recv(RX_DGRAM_SIZE)
        except socket.timeout:
            logger.critical('SOCKET TIMEOUT CAUGHT IN DUMP_TO_FILE. SKIPPING THIS ITERATION!')
            logger.critical('Timeout duration = %2.5fs' % (time.time() - timeStart))
        #[rc 080217] changed string search to limit itself to the beginning of the short command packet
        # to avoid falsely finding the string inside actual ASIC data packet.
        #
        pkt_len = len(rdata)
        if (pkt_len > 4):
            # data packet, ignore mimics           
            EOF_found = -1 
            END_found = -1 
            ERR_found = -1 
            BRK_found = -1
        else :
            # handshake packet, check for status
            EOF_found = rdata.rfind('EOF', 0, pkt_len)
            END_found = rdata.rfind('END', 0, pkt_len)
            ERR_found = rdata.rfind('ERR', 0, pkt_len)
            BRK_found = rdata.rfind('BRK', 0, pkt_len)
        #print "pkt_len = ", pkt_len
        #print "EOF_FOUND=", EOF_found
        #print "END_FOUND=", END_found
        #print "ERR_FOUND=", ERR_found
        #print "BRK_FOUND=", BRK_found
        if ((BRK_found != -1) & (accum_mode != 1)):
            print 'dump_to_file: Received BRK signal when not in accumulation mode'
            sys.exit(1)
            
        if ((END_found == -1) & (ERR_found == -1)):
            if (BRK_found != -1):
                length = BRK_found   ## In case BRK appended to same datagram
            else:
                length = len(rdata)

            if (length < S2MM_BUFFER_LENGTH):
                rdata = rdata[0:length] 
                    
            if (add_newline == 1):   # For ASCII files only
                if ((EOF_found != -1)):
                    globals.buffer.socket.send('D 0xF')  # Signal last data received
                    dfile.close
                    break
                else:
                    wdata = ''
                    for i in range (0, int(length/8)):
                        wdata = wdata + rdata[i*8:(i*8)+8] + '\n'
                    globals.buffer.socket.send('D 0xC')  # Get next data
                    dfile.write(wdata)
            else:            
                if ((EOF_found != -1)):
                    globals.buffer.socket.send('D 0xF')  # Signal last data received
                    dfile.close
                    break
                else:
                    globals.buffer.socket.send('D 0xC')  # Get next data
                    dfile.write(rdata)
        else:
            globals.buffer.socket.send('D 0xF')   # Signal last data received
            dfile.close
            if (END_found != -1):
                logger.debug('dump_to_file: Terminated (see Serial Terminal messages). DM Buffer must be restarted.')
                sys.exit(1)
            elif (ERR_found != -1):
                logger.debug('dump_to_file: Terminated due to ERROR (see Serial Terminal messages). DM Buffer must be restarted.')
                sys.exit(1)
            else:
                logger.debug('dump_to_file: Download finished.')
            break

    if (err == 1):
        return(0)
    else:
        return(1)

#-----------------------------------
def bin2ascii(*args):
    "Convert binary data file to a text file. Arguments = (input file, output file)."
# Binary data read from memory has least significant byte first (little endian).
# Converted to big endian for ASCII text file.
    import binascii

    if (len(args) != 2):
        err = 1
    elif ((type(args[0]) is not str) or (type(args[1]) is not str)):
        err = 1
    else:
        err = 0;
    if (err):
        error_message = """
        Usage: bin2ascii(input binary file, output text file)
            input binary file = binary file name as a string
            output text file  = output ASCII text file name as a string
        """
        logger.error(error_message)
        sys.exit(1)

# b2a_qp returns a printable string for a single byte. If there is one
# element it is a printing character, if there are three elements, it is
# a control character prefixed with "=" which is stripped off.    
    def set2hexstr(wbyte_set):
        setlen = len(wbyte_set)
        if (setlen == 1):
            hexstr = hex(ord(wbyte_set))
            hexstr = (hexstr[2:].zfill(2)).upper()
        if (setlen == 3):
            hexstr = wbyte_set[1] + wbyte_set[2]
        return(hexstr)

    bfile = open(args[0], 'rb')
    dfile = open(args[1], 'w')

    logger.debug('bin2ascii: Start converting' + args[0] + 'to' + args[1])
    count = 0
    while (1):
        word32str = ''
        j = 0
        while (j<4):
            rbytebin = bfile.read(1)
            if (rbytebin == ''):    ## End of file
                break
            wbytestr = set2hexstr(binascii.b2a_qp(rbytebin))
            word32str = wbytestr + word32str  # Change to big endian
            j = j+1        
        if (rbytebin == ''):    ## End of file
            logger.debug('bin2ascii: Finished')
            break     
        dfile.write(str(word32str)+'\n')
        count = count + 4
        if ( (count & 0xFFFFF) == 0):
            logger.debug('bin2ascii: Converted' +  str(count) + 'bytes')
    bfile.close
    dfile.close
    return(1)

#-----------------------------------
def dump_buffer(rx_buffer):
    # Downloads the contents of the DM_buffer memory in binary
    # ...uses stringIO to access buffer as a file...
    # ...returns binary data in rx_buffer
    #
    # once data dump begins with "D 0xA" message to server, loop
    # while continuously receiving from socket. Each block of socket
    # data is tested for various packet lengths and parsed into data
    # or short command packets.  Loop repeats until short command
    # (EOF, ERR, END) packet detected.
    #
    # [rc 21080221] Updated packet length & remainder checks for Clinical2
    #               output data buffer sizes
    #
    # [rc 20180703] Updated Remainders for all options of N_counters
    #
    # S2MM_BUFFER_LENGTH  = (NUM_DGRAM_WORDS*4) ## =11712
    ## Constants for remainder Buffer length in various View frames
    ## Remainder definitions (must be sorted in decreasing magnitude)
    ##
    # REMAINDER_BUF_LEN1  = 9804   ## N_ctr=13  Accum= NO  Pkt_len= 44940 
    # REMAINDER_BUF_LEN2  = 9036   ## N_ctr=6   Accum= NO  Pkt_len= 20748 
    # REMAINDER_BUF_LEN3  = 7904   ## N_ctr=13  Accum= YES Pkt_len= 89888 
    # REMAINDER_BUF_LEN4  = 6944   ## N_ctr=1   Accum= YES Pkt_len= 6944  
    # REMAINDER_BUF_LEN5  = 6368   ## N_ctr=6   Accum= YES Pkt_len= 41504 
    # REMAINDER_BUF_LEN6  = 6348   ## N_ctr=12  Accum= NO  Pkt_len= 41484 
    # REMAINDER_BUF_LEN7  = 4940   ## N_ctr=12  Accum= NO  Pkt_len= 40076 
    # REMAINDER_BUF_LEN8  = 3468   ## N_ctr=1   Accum= NO  Pkt_len= 3468  
    # REMAINDER_BUF_LEN9  = 1568   ## N_ctr=7   Accum= YES Pkt_len= 48416 
    # REMAINDER_BUF_LEN10 = 992    ## N_ctr=12  Accum= YES Pkt_len= 82976 
    # REMAINDER_BUF_LEN11 = 780    ## N_ctr=7   Accum= NO  Pkt_len= 24204 
    # HS_PKT_LEN          = 4     ## handshake command packets length

    curTime = time.strftime('%Y_%m_%d__%I_%M_%S')

    # dfile = open("data_dump_%s.bin" % curTime, 'wb')
    logger.debug('Starting dump_buffer: sending D 0xA to server...')
    globals.buffer.socket.send('D 0xA')  # Start ASCII dump (no ack message required)
    rdata =''
    while (1):
        try:
            timeStart = time.time()
            rdata, sockAddr = globals.buffer.socket.recvfrom(RX_DGRAM_SIZE)

            #[rc 010918] Loop through rdata, parsing for data and commands
            #            - there is typically one packet per recv, but must
            #              handle the case of multiple packets in one recv 
            #
            pkt_len = len(rdata)
            #print "initial pkt_len = ", pkt_len

            EOF_found = -1 
            END_found = -1 
            ERR_found = -1 
            BRK_found = -1
            EOB_found = -1

            while (pkt_len > 0):
                #
                # test for longest packet first
                #
                if (pkt_len >= S2MM_BUFFER_LENGTH):
                    #
                    rx_buffer.write(rdata[0:S2MM_BUFFER_LENGTH]) # save data
                    rdata= rdata[S2MM_BUFFER_LENGTH:]            # get remainder
                    pkt_len = pkt_len - S2MM_BUFFER_LENGTH       # adjust packet length
                #
                # test for longest remainder packet
                #
                elif (pkt_len >= REMAINDER_BUF_LEN1):
                    #
                    rx_buffer.write(rdata[0:REMAINDER_BUF_LEN1])
                    rdata= rdata[REMAINDER_BUF_LEN1:]
                    pkt_len = pkt_len - REMAINDER_BUF_LEN1
                #
                # test for next longest remainder packet
                #
                elif (pkt_len >= REMAINDER_BUF_LEN2):
                    #
                    rx_buffer.write(rdata[0:REMAINDER_BUF_LEN2])
                    rdata= rdata[REMAINDER_BUF_LEN2:]
                    pkt_len = pkt_len - REMAINDER_BUF_LEN2
                #
                # test for next longest remainder packet
                #
                elif (pkt_len >= REMAINDER_BUF_LEN3):
                    #
                    rx_buffer.write(rdata[0:REMAINDER_BUF_LEN3])
                    rdata= rdata[REMAINDER_BUF_LEN3:]
                    pkt_len = pkt_len - REMAINDER_BUF_LEN3
                #
                # test for next longest remainder packet
                #
                elif (pkt_len >= REMAINDER_BUF_LEN4):
                    #
                    rx_buffer.write(rdata[0:REMAINDER_BUF_LEN4])
                    rdata= rdata[REMAINDER_BUF_LEN4:]
                    pkt_len = pkt_len - REMAINDER_BUF_LEN4
                #
                # test for next longest remainder packet
                #
                elif (pkt_len >= REMAINDER_BUF_LEN5):
                    #
                    rx_buffer.write(rdata[0:REMAINDER_BUF_LEN5])
                    rdata= rdata[REMAINDER_BUF_LEN5:]
                    pkt_len = pkt_len - REMAINDER_BUF_LEN5
                #
                # test for next longest remainder packet
                #
                elif (pkt_len >= REMAINDER_BUF_LEN6):
                    #
                    rx_buffer.write(rdata[0:REMAINDER_BUF_LEN6])
                    rdata= rdata[REMAINDER_BUF_LEN6:]
                    pkt_len = pkt_len - REMAINDER_BUF_LEN6
                #
                # test for next longest remainder packet
                #
                elif (pkt_len >= REMAINDER_BUF_LEN7):
                    #
                    rx_buffer.write(rdata[0:REMAINDER_BUF_LEN7])
                    rdata= rdata[REMAINDER_BUF_LEN7:]
                    pkt_len = pkt_len - REMAINDER_BUF_LEN7
                #
                # test for next longest remainder packet
                #
                elif (pkt_len >= REMAINDER_BUF_LEN8):
                    #
                    rx_buffer.write(rdata[0:REMAINDER_BUF_LEN8])
                    rdata= rdata[REMAINDER_BUF_LEN8:]
                    pkt_len = pkt_len - REMAINDER_BUF_LEN8
                #
                # test for next longest remainder packet
                #
                elif (pkt_len >= REMAINDER_BUF_LEN9):
                    #
                    rx_buffer.write(rdata[0:REMAINDER_BUF_LEN9])
                    rdata= rdata[REMAINDER_BUF_LEN9:]
                    pkt_len = pkt_len - REMAINDER_BUF_LEN9
                #
                # test for next longest remainder packet
                #
                elif (pkt_len >= REMAINDER_BUF_LEN10):
                    #
                    rx_buffer.write(rdata[0:REMAINDER_BUF_LEN10])
                    rdata= rdata[REMAINDER_BUF_LEN10:]
                    pkt_len = pkt_len - REMAINDER_BUF_LEN10
                #
                # test for next longest remainder packet
                #
                elif (pkt_len >= REMAINDER_BUF_LEN11):
                    #
                    rx_buffer.write(rdata[0:REMAINDER_BUF_LEN11])
                    rdata= rdata[REMAINDER_BUF_LEN11:]
                    pkt_len = pkt_len - REMAINDER_BUF_LEN11
                #
                # test for command packet
                #
                elif (pkt_len >= HS_PKT_LEN):
                    #
                    # handshake packet, check for status
                    EOF_found = rdata.rfind('EOF', 0, pkt_len)
                    END_found = rdata.rfind('END', 0, pkt_len)
                    ERR_found = rdata.rfind('ERR', 0, pkt_len)
                    EOB_found = rdata.rfind('EOB', 0, pkt_len)
                    BRK_found = rdata.rfind('BRK', 0, pkt_len)
                    rdata= rdata[HS_PKT_LEN:]
                    pkt_len = pkt_len - HS_PKT_LEN

            #
            # debug statements
            #
            #print "final pkt_len = ", pkt_len
            #if (EOF_found != -1): print "EOF_FOUND=", EOF_found
            # if (END_found != -1): print "END_FOUND=", END_found
            #
            # if (ERR_found != -1): print "ERR_FOUND=", ERR_found
            #
            # if (EOB_found != -1): print "EOB_FOUND=", EOB_found
            #
            # if (BRK_found != -1): print "BRK_FOUND=", BRK_found

            #
            # test for various handshake termination messages
            #
            if (EOF_found != -1):
                logger.debug('dump_buffer: Download finished.')
                globals.buffer.socket.send('D 0xF')  # Signal last data received
                break
            elif (END_found != -1):
                globals.buffer.socket.send('D 0xF')  # Signal last data received
                logger.error('dump_buffer: Terminated (see Serial Terminal messages). DM Buffer must be restarted.')
                sys.exit(1)
            elif (ERR_found != -1):
                globals.buffer.socket.send('D 0xF')  # Signal last data received
                logger.error('dump_buffer: Terminated due to ERROR (see Serial Terminal messages). DM Buffer must be restarted.')
                sys.exit(1)
            elif ((EOB_found != -1) | (BRK_found != -1)):
                globals.buffer.socket.send('D 0xC')  # Get next data buffer
            else:
                globals.buffer.socket.send('D 0xC')  # Get next data buffer

        except socket.timeout:
            logger.critical('SOCKET TIMEOUT CAUGHT IN DUMP_BUFFER. REQUEST RETRANSMIT OF DATA!')
            logger.critical('Timeout duration = %2.5fs' % (time.time() - timeStart))
            globals.buffer.socket.send('D 0xE')  # Send error, request to re-transmit data buffer

    return 

#-----------------------------------
def dump_buffer_old(rx_buffer):
    # Downloads the contents of the DM_buffer memory in binary
    # ...uses stringIO to access buffer as a file...
    # ...returns binary data in rx_buffer
    curTime = time.strftime('%Y_%m_%d__%I_%M_%S')

    # dfile = open("data_dump_%s.bin" % curTime, 'wb')
    logger.debug('Starting dump_buffer: sending D 0xA to server...')
    globals.buffer.socket.send('D 0xA')  # Start ASCII dump (no ack message required)
    while (1):
        try:
            timeStart = time.time()
            rdata, sockAddr = globals.buffer.socket.recvfrom(RX_DGRAM_SIZE) # [rc 101517] changed to return up to RX_DGRAM_SIZE bytes
            #rdata = globals.buffer.socket.recv(RX_DGRAM_SIZE)
            
            #[rc 080217] changed string search to limit itself to the beginning of the short command packet
            # to avoid falsely finding the string inside actual ASIC data packet.
            #
            pkt_len = len(rdata)
            if (pkt_len > 4):
                # data packet, ignore mimics           
                EOF_found = -1 
                END_found = -1 
                ERR_found = -1 
                BRK_found = -1
            else :
                # handshake packet, check for status
                EOF_found = rdata.rfind('EOF', 0, pkt_len)
                END_found = rdata.rfind('END', 0, pkt_len)
                ERR_found = rdata.rfind('ERR', 0, pkt_len)
                BRK_found = rdata.rfind('BRK', 0, pkt_len)

            #print "pkt_len = ", pkt_len
            #print "EOF_FOUND=", EOF_found
            #print "END_FOUND=", END_found
            #print "ERR_FOUND=", ERR_found
            #print "BRK_FOUND=", BRK_found

            if (BRK_found != -1):
                length = BRK_found   ## In case BRK appended to same datagram
            else:
                length = len(rdata)

            #print "len(rdata) = ", length

            if ((END_found == -1) & (ERR_found == -1)):
                if (length < S2MM_BUFFER_LENGTH):
                    rdata = rdata[0:length] 
                    
                if ((EOF_found != -1)):
                    globals.buffer.socket.send('D 0xF')  # Signal last data received
                    break
                else:
                    globals.buffer.socket.send('D 0xC')  # Get next data
                    rx_buffer.write(rdata)
                # dfile.write(rdata)
                # print('rdata = %s' % rdata)
            else:
                #print "either END or ERR found"
                #print "rdata= %s" % rdata
                #print "sending D 0xF to server"
                globals.buffer.socket.send('D 0xF')  # Signal last data received
                # dfile.close()
                if (END_found != -1):
                    logger.error('dump_buffer: Terminated (see Serial Terminal messages). DM Buffer must be restarted.')
                    sys.exit(1)   ## Added 04052017
                elif (ERR_found != -1):
                    logger.error('dump_buffer: Terminated due to ERROR (see Serial Terminal messages). DM Buffer must be restarted.')
                    sys.exit(1)   ## Added 04052017
                else:
                    logger.debug('dump_buffer: Download finished.')
                break
        except socket.timeout:
            logger.critical('SOCKET TIMEOUT CAUGHT IN DUMP_BUFFER. SKIPPING THIS ITERATION!')
            logger.critical('Timeout duration = %2.5fs' % (time.time() - timeStart))
            globals.buffer.socket.send('D 0xC')  # Get next data
            #print "sockAddr=", sockAddr

    return

#-----------------------------------

def dump_buffer_IS(rx_buffer):
    # Downloads the contents of the DM_buffer memory in binary
    # ...uses stringIO to access buffer as a file...
    # ...returns binary data in rx_buffer
    curTime = time.strftime('%Y_%m_%d__%I_%M_%S')

    # dfile = open("data_dump_%s.bin" % curTime, 'wb')
    globals.buffer.socket.send('D 0xA')  # Start ASCII dump (no ack message required)
    dupCount = 0
    while (1):
        try:
            timeStart = time.time()
            rdata = globals.buffer.socket.recv(RX_DGRAM_SIZE)
        except socket.timeout:
            logger.critical('SOCKET TIMEOUT CAUGHT IN DUMP_BUFFER. SKIPPING THIS ITERATION!')
            logger.critical('Timeout duration = %2.5fs' % (time.time() - timeStart))
            #Force to receive next data
            globals.buffer.socket.send('D 0xC')  # Get next data
            dupCount = dupCount + 1
            print "dupCount=", dupCount
            continue

        #[rc 080217] changed string search to limit itself to the beginning of the short command packet
        # to avoid falsely finding the string inside actual ASIC data packet.
        #
        pkt_len = len(rdata)
        if (pkt_len > 4):
            # data packet, ignore mimics           
            EOF_found = -1 
            END_found = -1 
            ERR_found = -1 
            BRK_found = -1
        else :
            # handshake packet, check for status
            EOF_found = rdata.rfind('EOF', 0, pkt_len)
            END_found = rdata.rfind('END', 0, pkt_len)
            ERR_found = rdata.rfind('ERR', 0, pkt_len)
            BRK_found = rdata.rfind('BRK', 0, pkt_len)

        print "pkt_len = ", pkt_len
        print "EOF_FOUND=", EOF_found
        print "END_FOUND=", END_found
        print "ERR_FOUND=", ERR_found
        print "BRK_FOUND=", BRK_found

        if (BRK_found != -1):
            length = BRK_found   ## In case BRK appended to same datagram
        else:
            length = len(rdata)

        if ((END_found == -1) & (ERR_found == -1)):
            if (length < S2MM_BUFFER_LENGTH):
                rdata = rdata[0:length] 
                    
            if ((EOF_found != -1)):
                globals.buffer.socket.send('D 0xF')  # Signal last data received
                break
            else:
                # [IT 091517: workaround for stalled data dump]
                #Skip Get next data command when force readout
                if ( dupCount > 0):
                    dupCount = dupCount - 1
                    print "dupCount afer decr=", dupCount
                else:
                    globals.buffer.socket.send('D 0xC')  # Get next data
                rx_buffer.write(rdata)
            # dfile.write(rdata)
            # print('rdata = %s' % rdata)
        else:
            globals.buffer.socket.send('D 0xF')  # Signal last data received
            # dfile.close()
            if (END_found != -1):
                logger.error('dump_buffer: Terminated (see Serial Terminal messages). DM Buffer must be restarted.')
                sys.exit(1)   ## Added 04052017
            elif (ERR_found != -1):
                logger.error('dump_buffer: Terminated due to ERROR (see Serial Terminal messages). DM Buffer must be restarted.')
                sys.exit(1)   ## Added 04052017
            else:
                logger.debug('dump_buffer: Download finished.')
            break
    return

#============================================================================


