########################################################################################################################
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
########################################################################################################################

import os
import csv
import numpy as np

import global_analysis_constants
from spectral_class import SpectralTest


class EnergyCal(SpectralTest):
    """
    Class for handling Spectral test data
    calculates derivative data: NSR, Stability %, descriptive statistics
    """
    def __init__(self, data_raw, params_raw, info_raw, current_output_dir, test_suite_dict):

        k_type = self.get_k_type(params_raw.source)
        if k_type is not None:
            # Trim off "bad" data which skews the K-edge search
            # FIXME: This needs to be handled in a better way
            data_raw = data_raw[24:-5]
            params_raw.startThresh += 24
            params_raw.endThresh -= 5

        super(EnergyCal, self).__init__(data_raw, params_raw, info_raw, current_output_dir, test_suite_dict)

        self.params.k_type = k_type

        if self.params.k_type is not None:
            self.data.k_edge_location, self.data.k_edge_ncp, self.data.k_edge_mean = self.find_k_edge_linear_fit(bin_of_interest=1)

    def get_k_type(self, source):
        """get K-edge type by matching source to K_EDGE_WINDOWS dictionary (carried over from previous procedure)"""
        for type in global_analysis_constants.K_EDGE_WINDOWS:
            if type.lower() in source.lower():
                k_type = type
                break
            else:
                k_type = None

        return k_type

    def find_k_edge_differential(self, bin_of_interest=1):
        """adapted from Elmaddin's procedure in cal_lib"""
        thresh_low, thresh_high = global_analysis_constants.K_EDGE_WINDOWS[self.params.k_type]['thLimits']
        window_low = int(thresh_low - self.params.bin_thresh_list[bin_of_interest][0])
        window_high = int(self.params.bin_thresh_list[bin_of_interest][-1] - thresh_high)

        ###################################################################
        #   order data according to asic pixel map (serpentine)
        #   with array dimension order to [pixel, threshold]
        smoothed_data = np.zeros((self.params.num_pix, self.params.num_thresh))
        for pix in range(self.params.num_pix):
            smoothed_data[pix, :] = self.data.spec_smoothed[:, bin_of_interest,
                                    self.params.pixel_map_asic[pix, 0],
                                    self.params.pixel_map_asic[pix, 1]]

        diff_data = np.zeros(np.shape(smoothed_data))
        k_edge_location = np.zeros(self.params.num_pix, dtype=int)
        for pix in range(self.params.num_pix):

            # calculate differential
            for thresh in range(self.params.num_thresh - 1):
                diff_data[pix, thresh] = abs((smoothed_data[pix, thresh + 1] - smoothed_data[pix, thresh]))

            # TODO: find out reason for this specific index window(???) - make more flexible!!
            if self.params.k_type == 'Ce':
                max_val_index = int(np.argmax(diff_data[pix, 37:47]))  # find max in window
                diff_data[pix, max_val_index + 37] = \
                    (diff_data[pix, max_val_index + 36] + diff_data[pix, max_val_index + 36]) / 2

            # find peak from max value in per pixel slices
            max_val_index = int(np.argmax(diff_data[pix, window_low:self.params.num_thresh - 1 - window_high]))
            if thresh_low <= self.params.bin_thresh_list[bin_of_interest][max_val_index + window_low] < thresh_high:
                k_edge_location[pix] = self.params.bin_thresh_list[bin_of_interest][max_val_index + window_low]

        edge_replace = np.bincount(k_edge_location).argmax()  # TODO: could be replaced with statistical mode
        k_edge_ncp_lin = np.zeros(self.params.num_pix, dtype=bool)
        k_edge_ncp = np.zeros((self.params.num_rows, self.params.num_cols), dtype=bool)
        for pix in range(self.params.num_pix):
            if k_edge_location[pix] < thresh_low:
                k_edge_location[pix] = edge_replace
                k_edge_ncp_lin[pix] = 1
                k_edge_ncp[self.params.pixel_map_asic[pix, 0], self.params.pixel_map_asic[pix, 1]] = 1

        k_edge_mean = int(round(np.nanmean(k_edge_location)))

        return k_edge_location, k_edge_ncp, k_edge_mean

    def find_k_edge_linear_fit(self, bin_of_interest=1):
        thresh_low, thresh_high = global_analysis_constants.K_EDGE_WINDOWS[self.params.k_type]['thLimits']

        ###################################################################
        #   order data according to asic pixel map (serpentine)
        #   with array dimension order to [pixel, threshold]
        smoothed_data = np.zeros((self.params.num_pix, self.params.num_thresh))
        raw_data = np.zeros((self.params.num_pix, self.params.num_thresh))
        for pix in range(self.params.num_pix):
            smoothed_data[pix, :] = self.data.spec_smoothed[:, bin_of_interest,
                                    self.params.pixel_map_asic[pix, 0],
                                    self.params.pixel_map_asic[pix, 1]]
            raw_data[pix, :] = self.data.raw_spec[:, bin_of_interest,
                               self.params.pixel_map_asic[pix, 0],
                               self.params.pixel_map_asic[pix, 1]]

        delta = 0.5 * (np.max(smoothed_data, axis=1) - np.min(smoothed_data, axis=1))
        max_thresh_idx = np.argmax(smoothed_data, axis=1)
        min_counts = np.min(smoothed_data, axis=1)

        x = np.zeros((self.params.num_pix, 2))
        y = np.zeros((self.params.num_pix, 2))

        thresh_start = np.clip(max_thresh_idx + 8, 0, self.params.num_thresh - 1)
        thresh_end = np.clip(max_thresh_idx + 15, 0, self.params.num_thresh - 1)

        x[:, 0] = self.params.thresh_start + thresh_start
        x[:, 1] = self.params.thresh_start + thresh_end
        y[:, 0] = raw_data[np.arange(self.params.num_pix), thresh_start]
        y[:, 1] = raw_data[np.arange(self.params.num_pix), thresh_end]

        k = np.zeros(self.params.num_pix)
        b = np.zeros(self.params.num_pix)

        # Calculate linear fit for each pixel
        for pix in range(self.params.num_pix):
            k[pix], b[pix] = np.polyfit(x[pix], y[pix], 1)

        k_edge_location = np.array((delta - b + min_counts) / k, dtype=int)

        k_edge_location = np.clip(k_edge_location, 0, 255)

        edge_replace = np.bincount(k_edge_location).argmax()  # TODO: could be replaced with statistical mode
        k_edge_ncp_lin = np.zeros(self.params.num_pix, dtype=bool)
        k_edge_ncp = np.zeros((self.params.num_rows, self.params.num_cols), dtype=bool)
        for pix in range(self.params.num_pix):
            if k_edge_location[pix] < thresh_low:
                k_edge_location[pix] = edge_replace
                k_edge_ncp_lin[pix] = 1
                k_edge_ncp[self.params.pixel_map_asic[pix, 0], self.params.pixel_map_asic[pix, 1]] = 1

        k_edge_mean = int(round(np.nanmean(k_edge_location)))

        return k_edge_location, k_edge_ncp, k_edge_mean

    ####################################################################################################################
    #   csv export methods
    ####################################################################################################################

    def export_k_edge_table(self):
        file_name = '{}_{}_K-edge_locations_{}.csv'.format(self.info.device_ID,
                                                           self.params.k_type,
                                                           self.info.timestamp_string)
        save_loc = os.path.join(self.info.save_location, file_name)
        csv_header = ['pixel', 'K loc']
        csv_data = zip(range(self.params.num_pix), self.data.k_edge_location)
        with open(save_loc, 'wb') as k_out_csv:
            writer = csv.writer(k_out_csv)
            writer.writerow(csv_header)
            writer.writerows(csv_data)

        return save_loc
