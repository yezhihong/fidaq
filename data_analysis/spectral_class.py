########################################################################################################################
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
########################################################################################################################

import os
import csv
import numpy as np
import matplotlib.pyplot as plt
from scipy import signal
from scipy.optimize import curve_fit

import globals

from DDC0864.cal_lib import load_energy_cal_csv

from parent_classes import *


class SpectralTest(Asic):
    """
    Class for handling Spectral test data
    calculates derivative data: NSR, Stability %, descriptive statistics
    """
    def __init__(self, data_raw, params_raw, info_raw, current_output_dir, test_suite_dict):

        super(SpectralTest, self).__init__(data_raw, params_raw, info_raw, current_output_dir, test_suite_dict)

        ###############################################################
        #   initiate stability-specific parameters
        self.data.cc_data = self.data.cc_data[:, 0:6, :, :]

        #   integrate if multiple views
        if self.data.cc_data.ndim == 5:
            self.data.cc_data = np.nansum(self.data.cc_data, axis=2)

        self.data.raw_spec = np.flip(self.data.cc_data, axis=2)

        self.params.num_thresh = np.size(self.data.cc_data, 0)  # number of thresh steps from first dimension of data array
        self.params.num_bins = np.size(self.data.cc_data, 1)  # number of bins from second dimension of data array

        self.params.thresh_sweep_list = range(self.params.thresh_start, self.params.thresh_end + 1, self.params.thresh_step)
        self.params.bin_thresh_list = self.get_bin_thresh_list()
        self.params.bin_of_interst = 1

        self.data.spec_smoothed, self.data.spec_mean, self.data.spec_median = self.make_spectra()

        if 'xray' not in self.params.source:
            # Attempt to find and load energy calibration
            self.data.energy_cal_gain = np.ones((self.params.num_rows, self.params.num_cols))
            self.data.energy_cal_offset = np.zeros((self.params.num_rows, self.params.num_cols))

            cal_filename = os.path.join(globals.ldir, self.info.site, globals.calsDir, self.info.device_ID,
                                        globals.nrgCalDir, globals.energyCalFile)

            if os.path.isfile(cal_filename):
                gain, offset = load_energy_cal_csv(cal_filename, self.info.device_ID)

                for pix in range(self.params.num_pix):
                    self.data.energy_cal_gain[self.params.pixel_map_asic[pix, 0], self.params.pixel_map_asic[pix, 1]] = \
                    gain[pix]
                    self.data.energy_cal_offset[
                        self.params.pixel_map_asic[pix, 0], self.params.pixel_map_asic[pix, 1]] = offset[pix]

            self.data.am241_peak, self.data.am241_peak_mean, self.data.am241_peak_sigma = self.calc_peak()
            self.data.am241_fwhm, self.data.am241_fwhm_mean, self.data.am241_fwhm_sigma = self.calc_fwhm()

            ###############################################################
            #   save tabular data
            self.export_energy_linearity_pixel_table()
            self.export_energy_linearity_stats_table()
            self.export_energy_resolution_pixel_table()
            self.export_energy_resolution_stats_table()

        del self.data.cc_data

    def make_spectra(self):
        #  differentiate open counter bin (bin 5)
        open_bin_diff = np.zeros((self.params.num_thresh, self.params.num_rows, self.params.num_cols))
        for thresh in range(self.params.num_thresh - 1):
            for row in range(self.params.num_rows):
                for col in range(self.params.num_cols):
                    open_bin_diff[thresh, row, col] = abs(self.data.raw_spec[thresh + 1, -1, row, col] -
                                                       self.data.raw_spec[thresh, -1, row, col])
        rough_spec = np.copy(self.data.raw_spec)
        # replace last bin with differentiated open bin
        rough_spec[:, -1, :, :] = open_bin_diff

        # remove differentiation non-linearity (DNL) spikes
        data_scrubbed = np.zeros(np.shape(rough_spec))
        spec_smoothed = np.zeros(np.shape(rough_spec))
        for cc_bin in range(self.params.num_bins):
            for row in range(self.params.num_rows):
                for col in range(self.params.num_cols):
                    data_scrubbed[:, cc_bin, row, col] = self.dnlScrub(rough_spec[:, cc_bin, row, col],
                                                                       self.params.bin_thresh_list[cc_bin],
                                                                       bin=1)
                    spec_smoothed[:, cc_bin, row, col] = signal.savgol_filter(data_scrubbed[:, cc_bin, row, col], 17, 3)

        spec_pixel_mean = np.nanmean(spec_smoothed, axis=0)
        spec_pixel_std = np.nanstd(spec_pixel_mean, axis=(1, 2))

        spec_mean = np.nanmean(spec_smoothed, axis=(2, 3))
        spec_median = np.nanmedian(spec_smoothed, axis=(2, 3))
        spec_std = np.nanmedian(spec_smoothed, axis=(2, 3))

        return spec_smoothed, spec_mean, spec_median

    def calc_peak(self, bin_of_interest=1, mean=59.54, sigma=1):
        def gauss(x, a, x0, sigma):
            return a * np.exp(-(x - x0) ** 2 / (2 * sigma ** 2))

        peak = np.zeros((self.params.num_rows, self.params.num_cols))

        for row in range(self.params.num_rows):
            for col in range(self.params.num_cols):
                spec = self.data.raw_spec[:-1, bin_of_interest, row, col]
                thresh = self.data.energy_cal_offset[row,col] + np.array(self.params.thresh_sweep_list[:-1]) * self.data.energy_cal_gain[row,col]

                savgol_spec = signal.savgol_filter(spec, 7, 3)
                smoothed_spec = signal.detrend(savgol_spec)

                popt, pcov = curve_fit(gauss, thresh, smoothed_spec, p0=[max(smoothed_spec), mean, sigma])
                peak[row, col] = popt[1]

        peak_mean = np.mean(peak)
        peak_sigma = np.std(peak)

        return peak, peak_mean, peak_sigma

    def calc_fwhm(self, bin_of_interest=1, l_window=(3,8), r_window=(3,8)):
        fwhm = np.zeros((self.params.num_rows, self.params.num_cols))

        for row in range(self.params.num_rows):
            for col in range(self.params.num_cols):
                spec = self.data.raw_spec[:, bin_of_interest, row, col]
                thresh = self.data.energy_cal_offset[row,col] + np.array(self.params.thresh_sweep_list) * self.data.energy_cal_gain[row,col]

                max_idx = np.argmax(spec)
                half_max = (spec[max_idx]) / 2

                xl = np.array([thresh[max_idx - l_window[0]], thresh[max_idx - l_window[1]]])
                yl = np.array([spec[max_idx - l_window[0]], spec[max_idx - l_window[1]]])

                xr = np.array([thresh[max_idx + r_window[0]], thresh[max_idx + r_window[1]]])
                yr = np.array([spec[max_idx + r_window[0]], spec[max_idx + r_window[1]]])

                kl, bl = np.polyfit(xl, yl, 1)
                kr, br = np.polyfit(xr, yr, 1)

                if kr != 0 and kl != 0:
                    fwhm[row, col] = (half_max - br) / kr - (half_max - bl) / kl
                else:
                    fwhm[row, col] = 0

        fwhm_mean = np.mean(fwhm)
        fwhm_sigma = np.std(fwhm)

        return fwhm, fwhm_mean, fwhm_sigma

    def get_bin_thresh_list(self):
        bin_thresh_list = np.zeros((self.params.num_bins, self.params.num_thresh))
        for cc_bin in range(self.params.num_bins):
            for thresh_ind, thresh in enumerate(self.params.thresh_sweep_list):
                bin_thresh_list[cc_bin, thresh_ind] = thresh + cc_bin

        return bin_thresh_list

    def dnlScrub(self, spectraList, thresholdList, bin=1):
        """carried over directly from Elmaddin's prodedure in cal_lib
        TODO move literals to variables outside of function"""
        # spectraList, thresholdList are a single spectra/threshold dataset
        # or a list of spectra/threshold datasets (single list not list of lists)
        # The counter value is the bin number of counter from which
        # the spectra are extracted e.g. bin = 1 for SEC1 or CC1
        dnlList = [x + bin for x in [30, 62, 94, 126, 158]]
        for i in range(1, len(spectraList) - 1):
            if thresholdList[i] in dnlList:  # removing dnl using average of two points
                if spectraList[i] > 50:
                    if all(y > 1.2 for y in [float(spectraList[i]) / spectraList[i - 1],
                                             float(spectraList[i]) / spectraList[i + 1]]):
                        spectraList[i] = (spectraList[i - 1] + spectraList[i + 1]) * 0.5
        return spectraList

    ####################################################################################################################
    #   csv export methods
    ####################################################################################################################

    def export_energy_linearity_pixel_table(self):
        table_headers = []
        table_columns = []

        ###########################################################
        #   populate pixel number
        pixel_number = list(range(self.params.num_pix))
        table_columns.append(pixel_number)
        pixel_number_header = 'pixel'
        table_headers.append(pixel_number_header)

        ###########################################################
        #   create column and header for peak
        table_columns.append(self.data.am241_peak.flatten())
        table_headers.append('Am241 Peak (keV)')

        file_name = '{}_am241_linearity_pixel_data.csv'.format(self.info.device_ID)
        save_loc = os.path.join(self.info.save_location, file_name)

        with open(save_loc, 'wb') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(table_headers)
            writer.writerows(zip(*table_columns))

    def export_energy_linearity_stats_table(self):
        table_headers = []
        table_columns = []

        table_columns.append(self.data.am241_peak_mean)
        table_headers.append('Am241 Peak mean (keV)')

        table_columns.append(self.data.am241_peak_sigma)
        table_headers.append('Am241 Peak std (keV)')

        file_name = '{}_am241_linearity_stats.csv'.format(self.info.device_ID)
        save_loc = os.path.join(self.info.save_location, file_name)

        with open(save_loc, 'wb') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(table_headers)
            writer.writerow(table_columns)

    def export_energy_resolution_pixel_table(self):
        table_headers = []
        table_columns = []

        ###########################################################
        #   populate pixel number
        pixel_number = list(range(self.params.num_pix))
        table_columns.append(pixel_number)
        pixel_number_header = 'pixel'
        table_headers.append(pixel_number_header)

        ###########################################################
        #   create column and header for fwhm
        table_columns.append(self.data.am241_fwhm.flatten())
        fwhm_header = 'Am241 FWHM (keV)'
        table_headers.append(fwhm_header)

        file_name = '{}_am241_resolution_pixel_data.csv'.format(self.info.device_ID)
        save_loc = os.path.join(self.info.save_location, file_name)

        with open(save_loc, 'wb') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(table_headers)
            writer.writerows(zip(*table_columns))

    def export_energy_resolution_stats_table(self):
        table_headers = []
        table_columns = []

        table_columns.append(self.data.am241_fwhm_mean)
        table_headers.append('Am241 FWHM mean (keV)')

        table_columns.append(self.data.am241_fwhm_sigma)
        table_headers.append('Am241 FWHM std (keV)')

        file_name = '{}_am241_resolution_stats.csv'.format(self.info.device_ID)
        save_loc = os.path.join(self.info.save_location, file_name)

        with open(save_loc, 'wb') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(table_headers)
            writer.writerow(table_columns)

    ####################################################################################################################
    #   plotting methods
    ####################################################################################################################

    def plot_spec_pixel(self, k_type=None):
        """plot individual pixel"""
        example_pixel = [11, 17]
        example_pixel_string = 'pixe1 {}-{}'.format(example_pixel[0], example_pixel[1])
        self.plot_spec(self.data.spec_smoothed[:, :, example_pixel[0], example_pixel[1]],
                       label=example_pixel_string, k_type=k_type)

    def plot_spec_mean(self, k_type=None):
        self.plot_spec(self.data.spec_mean, label='mean (all pixels)', k_type=k_type)

    def plot_spec_median(self, k_type=None):
        self.plot_spec(self.data.spec_median, label='median (all pixels)', k_type=k_type)

    def plot_spec(self, spec, label='', k_type=None):
        """
        plot spectra
        """
        fig_title = '{} {} Spectra\n{}'.format(self.info.device_ID, label, self.params.source)
        for cc_bin in range(self.params.num_bins):
            plt.plot(self.params.bin_thresh_list[cc_bin], spec[:, cc_bin])
        if k_type is not None:
            plt.axvline(self.data.k_edge_mean, color='r', linestyle='dashed', linewidth=1)
            fig_title = '{} {} Spectra\n{}\nK edge: {}keV'.format(self.info.device_ID,
                                                                  label,
                                                                  self.params.source,
                                                                  self.data.k_edge_mean)

        plt.legend(['SEC {}'.format(cc_bin) for cc_bin in range(self.params.num_bins)], fontsize=8)
        plt.title(fig_title, fontsize=8)
        file_name = '{}_{}_{}_spectra.png'.format(self.info.device_ID, self.params.source, label)
        save_loc = os.path.join(self.info.save_location, file_name)
        plt.savefig(save_loc)
        plt.close()

    def plot_per_pixel_spec(self, spec, cc_bin=1, k_type=None):
        """
        plot spectra
        """
        fig, axarr = plt.subplots(self.params.num_rows,
                                  self.params.num_cols,
                                  sharex=False,
                                  sharey=False,
                                  figsize=(12.8, 7.8), dpi=100)
        # fig = plt.figure(figsize=(12.8, 7.8), dpi=100)
        fig_title = '{} Per-Pixel Spectra - {} - SEC {}'.format(self.info.device_ID, self.params.source, cc_bin)
        plt.suptitle(fig_title, fontsize=8)
        for row in range(self.params.num_rows):
            for col in range(self.params.num_cols):
                clr = 'royalblue'
                if k_type is not None:
                    if self.data.k_edge_ncp[row, col]:
                        clr = 'r'
                # plt.subplot(self.params.num_rows, self.params.num_cols, (self.params.num_cols*row) + (col+1))
                axarr[row, col].plot(self.params.bin_thresh_list[cc_bin], spec[:, cc_bin, row, col], color=clr)
                # plt.plot(self.params.bin_thresh_list[cc_bin], spec[:, cc_bin, row, col])
                # plt.tick_params(axis='both', which='major', tick1On=False, label1On=False)
                axarr[row, col].tick_params(axis='both', which='major', tick1On=False, label1On=False)

        # plt.tight_layout(rect=[0, 0, 1, 0.95])
        file_name = '{}_{}_per_pixel_spectra.png'.format(self.info.device_ID, self.params.source)
        save_loc = os.path.join(self.info.save_location, file_name)
        plt.savefig(save_loc)
        plt.close()

    def plot_energy_linearity(self):
        if 'xray' in self.params.source:
            return

        fig_title = '{} Energy Peak - {}'.format(self.info.device_ID, self.params.source)
        plt.imshow(np.rot90(self.data.am241_peak, -1), cmap='jet')
        plt.title(fig_title)
        plt.clim(55, 65)
        plt.colorbar()
        file_name = '{}_am241_linearity_heatmap.png'.format(self.info.device_ID)
        save_loc = os.path.join(self.info.save_location, file_name)
        plt.savefig(save_loc)
        plt.close('all')

        peak = self.data.am241_peak.reshape(864, -1)

        fig_title = '{} Energy Peak - {}\nmean: {} keV    sigma: {} keV'.format(
            self.info.device_ID, self.params.source, round(self.data.am241_peak_mean, 2),
            round(self.data.am241_peak_sigma, 2))
        plt.hist(peak, bins='auto', density=False)
        plt.xlim(50, 70)
        plt.title(fig_title)
        # plt.text(63, 90, 'mean={}'.format(round(self.data.am241_linearity_mean, 2)))
        # plt.text(63, 70, 'sigma={}'.format(round(self.data.am241_linearity_sigma, 2)))
        plt.ylabel('Normalized')
        plt.xlabel('Energy, keV')
        file_name = '{}_am241_linearity.png'.format(self.info.device_ID)
        save_loc = os.path.join(self.info.save_location, file_name)
        plt.savefig(save_loc)
        plt.close('all')

    def plot_energy_resolution(self):
        if 'xray' in self.params.source:
            return

        fig_title = '{} Energy FWHM - {}'.format(self.info.device_ID, self.params.source)
        plt.imshow(np.rot90(self.data.am241_fwhm, -1), cmap='jet')
        plt.title(fig_title)
        plt.clim(5, 14)
        plt.colorbar()
        file_name = '{}_am241_resolution_heatmap.png'.format(self.info.device_ID)
        save_loc = os.path.join(self.info.save_location, file_name)
        plt.savefig(save_loc)
        plt.close('all')

        fwhm = self.data.am241_fwhm.reshape(864, -1)
        fwhm_histogram = fwhm[np.where(np.logical_and(fwhm > 6, fwhm < 16))]
        fwhm_mean = np.mean(fwhm_histogram)
        fwhm_sigma = np.std(fwhm_histogram)

        fig_title = '{} Energy FWHM - {}\nmean: {} keV    sigma: {} keV'.format(
            self.info.device_ID, self.params.source, round(fwhm_mean, 2),
            round(fwhm_sigma, 2))
        plt.hist(fwhm_histogram, bins='auto', density=False)
        plt.xlim(5, 16)
        plt.title(fig_title)
        # plt.text(63, 90, 'mean={}'.format(round(self.data.am241_linearity_mean, 2)))
        # plt.text(63, 70, 'sigma={}'.format(round(self.data.am241_linearity_sigma, 2)))
        plt.ylabel('Normalized')
        plt.xlabel('Energy, keV')
        file_name = '{}_am241_resolution.png'.format(self.info.device_ID)
        save_loc = os.path.join(self.info.save_location, file_name)
        plt.savefig(save_loc)
        plt.close('all')
