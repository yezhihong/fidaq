########################################################################################################################
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
########################################################################################################################

########################################################################################################################
#   lag / dynamic constants
LEADING_EDGE_THRESH = 0.5  # threshold of max of mean for all pixels for a given view.  Used to find shutter window
LEADING_EDGE_ADVANCE = 0.003  # number of seconds to advance past leading edge. Default 3ms
LAG_THRESH = 300  # 300 count threshold for find leading edge of individual pixels
LAG_RANGE = [-5, 5]  # min / max lag range for plotting and truncating lag data

WAFER_MAP_BINS = [4, 5]  # index of bins to be plotted in wafer maps
# NOMINAL_SHUTTER_PERIOD = 1.00  # shutter period in seconds
NOMINAL_SHUTTER_PERIOD = 1.20  # shutter period in seconds  TODO: needs to be attained from paramters (currently hard-coded)
# NOMINAL_TIME_RESOLUTION = 0.0025
# NOMINAL_TIME_RESOLUTION = 0.003
LAG_PERIOD_FIRST = 50  # time in milliseconds for first portion of shutter
LAG_PERIOD_LAST = 300  # time in milliseconds for last portion of shutter
PITCH_COMP_INTERP = [500]
PITCH_MODE = ['interp', 'manual', 'manual']

########################################################################################################################
#   uniformity constants

CU_FILTER_THICKNESS_FORCED = 1
TUBE_DISTANCE_FORCED = 30
TUBE_CURRENT_FORCED = 25.0

FLUX_CORRECTION = 0.73
DEFAULT_PIXEL_AREA = 0.09

OPEN_BIN_INTEGRATION_VAL = 30  # 30keV and above

# CURRENTS_OF_INTEREST_INDICES = [0, 4, 6, 11]
BINS_OF_INTEREST_INDICES = [0, 1, 2, 3, 4, 5]

# NCP Thresholds
THRESHOLD_OCR = [10, 15]  # % compared to mean (central pixels / edge pixels)
THRESHOLD_OCR_0mA = 5   # fixed count threshold for 0.0mA
THRESHOLD_OCR_ICR = [65.0, 85.0]  # percentage threshold of OCR/ICR

########################################################################################################################
#   1m stability constants
STABILITY_PERCENT_THRESHOLD = 1.25
STAB_NCP_TO_AMALGAMATE = {'stab_thresh_ind': [1, 2, 3]}

########################################################################################################################
#   spectral constants
K_EDGE_WINDOWS = {'Pb': {'Windows': [23, 32], 'thLimits': [97, 130]},
                  'Ce': {'Windows': [30, 0], 'thLimits': [43, 59]}}
