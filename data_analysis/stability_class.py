########################################################################################################################
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
########################################################################################################################

import os
import csv
import numpy as np
import matplotlib.pyplot as plt

import global_analysis_constants
from parent_classes import Asic


class StabilityTest(Asic):
    """
    Class for handling Uniformity test data
    calculates derivative data: NSR, Stability %, descriptive statistics
    """
    def __init__(self, data_raw, params_raw, info_raw, current_output_dir, test_suite_dict):

        super(StabilityTest, self).__init__(data_raw, params_raw, info_raw, current_output_dir, test_suite_dict)

        ### take slice of 6 Coincidence Counters if all counters (EC, CC, SEC) were collected
        if self.data.cc_data.shape[0] == 13:
            self.data.cc_data = self.data.cc_data[6:12, :, :, :]

        ###############################################################
        #   initiate stability-specific paramters
        self.params.num_bins = np.size(self.data.cc_data, 0)  # number of bins from first dimension of data array
        self.params.num_views = np.size(self.data.cc_data, 1)  # number of views from second dimension of data array

        self.data.correction_area = self.get_pixel_area()

        ###############################################################
        #
        self.data.stability, self.data.stab_ncp, self.data.stability_mean, \
            self.data.stability_std, self.data.stab_ncp_count = self.calc_stability()

        self.data.ncp_combined, self.data.ncp_combined_count = self.combine_ncps()

        self.data.raw_sum = np.nansum(self.data.cc_data, axis=(2, 3))  # .astype(float)
        # self.data.raw_sum = np.squeeze(np.nansum(np.copy(self.data.cc_data), axis=(2, 3)))  # .astype(float)
        #  self.data.cc_data[self.data.cc_data == 0] = np.nan

        self.data.stab_nsr, self.data.nsr_mean, self.data.nsr_std = self.calc_nsr()

        self.export_stab_pixel_table()
        self.export_stab_stats_table()

        del self.data.cc_data

    ####################################################################################################################
    #   derivative data methods
    ####################################################################################################################

    def get_pixel_area(self):
        """
        cacluate pixel pixel area
        """
        return self.params.pixel_area

    def calc_nsr(self):
        """
        calculates noise to signal ration and descriptive stats
        """
        num_views_per_second = np.floor(self.params.num_views / self.params.total_test_time).astype(int)
        seconds_to_trim = 0
        views_to_trim = num_views_per_second * seconds_to_trim

        stab_nsr = np.zeros((self.params.num_bins, self.params.num_rows, self.params.num_cols))
        nsr_mean = np.zeros((self.params.num_bins,))
        nsr_std = np.zeros((self.params.num_bins,))
        for cc_bin in range(self.params.num_bins):
            std = np.nanstd(np.copy(self.data.cc_data[cc_bin, views_to_trim:, :, :]), axis=0)
            mean = np.nanmean(np.copy(self.data.cc_data[cc_bin, views_to_trim:, :, :]), axis=0)
            stab_nsr[cc_bin, :, :] = std/np.sqrt(mean)
            nsr_mean[cc_bin] = np.nanmean(stab_nsr[cc_bin, :, :], (0, 1))
            nsr_std[cc_bin] = np.nanstd(stab_nsr[cc_bin, :, :], (0, 1))

        return stab_nsr, nsr_mean, nsr_std

    def calc_stability(self):
        """
        calculates NCP based on variation of counts in 60s, averaged per second
        """
        num_views_per_second = np.floor(self.params.num_views / self.params.total_test_time).astype(int)
        seconds_to_trim = 0
        views_to_trim = num_views_per_second * seconds_to_trim

        trimmed_time = int(self.params.total_test_time - seconds_to_trim)
        trimmed_data = self.data.cc_data[:, views_to_trim:, :, :]
        trimmed_views = trimmed_time * num_views_per_second

        # num_seconds =
        stab_ncp = np.zeros((self.params.num_bins, self.params.num_rows, self.params.num_cols))

        stab_1s = np.zeros((self.params.num_bins, trimmed_time, self.params.num_rows, self.params.num_cols))
        for cc_bin in range(self.params.num_bins):
            for view_1s, view in enumerate(range(0, trimmed_views, num_views_per_second)):
                for row in range(self.params.num_rows):
                    for col in range(self.params.num_cols):
                        #   calculate per second mean counts
                        stab_1s[cc_bin, view_1s, row, col] \
                            = np.nanmean(trimmed_data[cc_bin, view:view + num_views_per_second, row, col])

        stab_1s_mean = np.nanmean(stab_1s, axis=1)
        stab_1s_max = np.nanmax(stab_1s, axis=1)
        stab_1s_min = np.nanmin(stab_1s, axis=1)
        ######################################################
        #   per pixel stability
        stability = ((stab_1s_max - stab_1s_min) / stab_1s_mean) * 100
        stab_ncp[stability > global_analysis_constants.STABILITY_PERCENT_THRESHOLD] = 1
        stab_ncp[np.isnan(stability)] = 1
        stability_mean = np.nanmean(stability, axis=(1, 2))
        stability_std = np.nanstd(stability, axis=(1, 2))
        stab_ncp_count = np.count_nonzero(stab_ncp, axis=(1, 2))

        return stability, stab_ncp, stability_mean, stability_std, stab_ncp_count

    def combine_ncps(self):
        """
        combine NPCs into single array
        """

        stab_thresh_index = global_analysis_constants.STAB_NCP_TO_AMALGAMATE['stab_thresh_ind']

        ncp_combined = np.zeros((self.params.num_rows, self.params.num_cols))
        stab_ncp_combined = [np.squeeze(self.data.stab_ncp[thresh, :, :])
                             for thresh in stab_thresh_index]
        for ncp in stab_ncp_combined:
            ncp_combined = (np.logical_or(np.copy(ncp_combined), ncp)).astype(int)

        ncp_combined_count = np.count_nonzero(ncp_combined == 1)

        return ncp_combined, ncp_combined_count

    ####################################################################################################################
    #   table exporting methods
    ####################################################################################################################

    def export_stab_stats_table(self):
        """
        write stab statistical data to csv table
        """
        table_headers = []
        table_columns = []

        ###########################################################
        #   create column and header data for OCR data
        data_desc = ['NSR', 'stability %']
        stats_key = ['mean', 'std']
        nsr_stats = [self.data.nsr_mean, self.data.nsr_std]
        stability_stats = [self.data.stability_mean, self.data.stability_std]
        nsr_dict = dict(zip(stats_key, nsr_stats))
        stability_dict = dict(zip(stats_key, stability_stats))

        data_dict = dict(zip(data_desc, [nsr_dict, stability_dict]))

        for desc in data_desc:
            for cc_bin in range(self.params.num_bins):
                for stat in stats_key:
                    table_columns.append(data_dict[desc][stat][cc_bin])
                    header = '{} ({}) {}keV'.format(desc, stat, self.params.energy_kev[cc_bin])
                    table_headers.append(header)

        ###########################################################
        #   create column and header data for LAG NCP data
        for cc_bin in range(self.params.num_bins):
            table_columns.append(self.data.stab_ncp_count[cc_bin])
            header = 'NCP count {}keV'.format(self.params.energy_kev[cc_bin])
            table_headers.append(header)

        file_name = '{}_STAB_stats.csv'.format(self.info.device_ID)
        save_loc = os.path.join(self.info.save_location, file_name)

        with open(save_loc, 'wb') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(table_headers)
            writer.writerow(table_columns)

    def export_stab_pixel_table(self):
        """
        write selected stab array data to csv table
        """
        table_headers = []
        table_columns = []

        ###########################################################
        #   populate pixel number and area columns
        pixel_number = list(range(self.params.num_pix))
        table_columns.append(pixel_number)
        pixel_number_header = 'pixel'
        table_headers.append(pixel_number_header)

        pixel_area = list(self.data.correction_area.flatten())
        table_columns.append(pixel_area)
        pixel_area_header = 'pixel area (mm^2)'
        table_headers.append(pixel_area_header)

        ###########################################################
        #   create column and header data for NSR and Stability
        data_desc = ['NSR', 'stability %']
        data_members = [self.data.stab_nsr, self.data.stability]
        data_dict = dict(zip(data_desc, data_members))

        for desc in data_desc:
            for cc_bin in range(self.params.num_bins):
                table_columns.append(data_dict[desc][cc_bin, :, :].flatten())
                header = '{} ({}keV)'.format(desc, self.params.energy_kev[cc_bin])
                table_headers.append(header)

        ###########################################################
        #   create column and header data for OCR NCPs
        for cc_bin in range(self.params.num_bins):
            table_columns.append(self.data.stab_ncp[cc_bin, :].flatten())
            header = 'NCP (>{}% @ {}keV)'.format(global_analysis_constants.STABILITY_PERCENT_THRESHOLD, self.params.energy_kev[cc_bin])
            table_headers.append(header)

        file_name = '{}_STAB_pixel_data.csv'.format(self.info.device_ID)
        save_loc = os.path.join(self.info.save_location, file_name)

        with open(save_loc, 'wb') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(table_headers)
            writer.writerows(zip(*table_columns))

    ####################################################################################################################
    #   plotting methods
    ####################################################################################################################

    def plot_raw_sum(self):
        """
        plot stability line graph
        """
        title_font_size = 7
        plt.figure(figsize=(12.8, 7.8), dpi=100)
        num_bins = self.params.num_bins
        fig_title = '{} stability (sum all pixels)'.format(self.info.device_ID)
        plt.suptitle(fig_title, fontsize=9.5)
        for cc_bin in range(num_bins):
            title_string = 'count stability @ {}keV'.format(self.params.energy_kev[cc_bin])
            plt.subplot(num_bins / 3, num_bins / 2, cc_bin + 1)
            plt.tick_params(axis='both', which='major', tick1On=True, label1On=True)
            plt.plot(self.data.raw_sum[cc_bin, :])
            plt.title(title_string, fontsize=title_font_size)
        plt.tight_layout(rect=[0, 0, 1, 0.95])
        file_name = '{}_stability_RAW.png'.format(self.info.device_ID)
        save_loc = os.path.join(self.info.save_location, file_name)
        plt.savefig(save_loc)
        plt.close()

    def plot_stability(self):
        """
        plot stability heatmaps, histograms, and NCPs
        """
        title_font_size = 7
        plt.figure(figsize=(12.8, 7.8), dpi=100)
        num_bins = self.params.num_bins
        num_pix = self.params.num_pix
        num_fig_rows = 3
        fig_title = '{} Stability (max - min / mean)\nNCP ({}% deviation)'.format\
            (self.info.device_ID, global_analysis_constants.STABILITY_PERCENT_THRESHOLD)
        plt.suptitle(fig_title, fontsize=9.5)
        threshold_percent = global_analysis_constants.STABILITY_PERCENT_THRESHOLD
        threshold_percent_multiplier = 1.5

        for cc_bin in range(num_bins):
            ###################################################
            #   stability heat maps
            plot_row = 0
            im_range = [-(threshold_percent * threshold_percent_multiplier),
                        threshold_percent * threshold_percent_multiplier]
            title_string = 'stability @ {}keV'.format(self.params.energy_kev[cc_bin])
            plt.subplot(num_fig_rows, num_bins, cc_bin + 1 + (num_bins * plot_row))
            plt.tick_params(axis='both', which='major', tick1On=False, label1On=False)
            im = plt.imshow(self.data.stability[cc_bin, :, :],  cmap='jet', vmin=im_range[0], vmax=im_range[1])
            plt.title(title_string, fontsize=title_font_size)
            cbar = plt.colorbar(im, orientation='horizontal')
            cbar.ax.tick_params(labelsize=7, which='major', labelrotation=45)

            plot_row += 1

            ###################################################
            #   stability histograms
            hist_data = np.copy(self.data.stability[cc_bin, :, :]).flat[:]
            hist_data = hist_data[~np.isnan(hist_data)]

            plt.subplot(num_fig_rows, num_bins, cc_bin + 1 + (num_bins * plot_row))
            title_string = 'mean: {:.2f}%\nstd: {:.2f}%'.format(np.nanmean(self.data.stability_mean[cc_bin]),
                                                              np.nanmean(self.data.stability_std[cc_bin]))
            plt.title(title_string, fontsize=title_font_size)
            plt.hist(hist_data, bins=20, histtype='step', color='k')
            plt.tick_params(axis='both', which='major', labelsize=6)

            plot_row += 1

            #####################################################
            #   NCP maps
            title_string = 'NCP count: {}'.format(self.data.stab_ncp_count[cc_bin])
            plt.subplot(num_fig_rows, num_bins, cc_bin + 1 + (num_bins * plot_row))
            plt.tick_params(axis='both', which='major', tick1On=False, label1On=False)
            im = plt.imshow(self.data.stab_ncp[cc_bin, :, :],  cmap='gray_r', vmin=0, vmax=1)
            plt.title(title_string, fontsize=title_font_size)

        plt.tight_layout(rect=[0, 0, 1, 0.95])
        file_name = '{}_stability_PERCENT.png'.format(self.info.device_ID)
        save_loc = os.path.join(self.info.save_location, file_name)
        plt.savefig(save_loc)
        plt.close()

    def plot_stab_nsr(self):
        """
        plot noise to signal ratio heatmaps, histograms, and line graphs
        """
        title_font_size = 7
        plt.figure(figsize=(12.8, 7.8), dpi=100)
        num_bins = self.params.num_bins
        num_pix = self.params.num_pix
        num_fig_rows = 3
        fig_title = '{} stability NSR'.format(self.info.device_ID)
        plt.suptitle(fig_title, fontsize=9.5)

        for cc_bin in range(num_bins):
            ###################################################
            #   plot nsr heat maps
            plot_row = 0
            title_string = 'NSR @ {}keV'.format(self.params.energy_kev[cc_bin])
            plt.subplot(num_fig_rows, num_bins, cc_bin + 1 + (num_bins * plot_row))
            plt.tick_params(axis='both', which='major', tick1On=False, label1On=False)
            im = plt.imshow(self.data.stab_nsr[cc_bin, :, :],  cmap='jet', vmin=0.92, vmax=1.08)
            plt.title(title_string, fontsize=title_font_size)
            cbar = plt.colorbar(im, orientation='horizontal')
            cbar.ax.tick_params(labelsize=7, which='major', labelrotation=45)

            plot_row += 1

            ###################################################
            #   nsr histograms
            hist_range = [0.95, 1.05]
            hist_data = np.reshape(np.copy(self.data.stab_nsr[cc_bin, :, :]), num_pix)
            hist_data = hist_data[~np.isnan(hist_data)]


            plt.subplot(num_fig_rows, num_bins, cc_bin + 1 + (num_bins * plot_row))
            title_string = 'mean: {:.2f}\nstd: {:.2f}'.format(self.data.nsr_mean[cc_bin],
                                                              self.data.nsr_std[cc_bin])
            plt.title(title_string, fontsize=title_font_size)
            plt.hist(hist_data, range=hist_range, bins=20, histtype='step', color='k')
            plt.tick_params(axis='both', which='major', labelsize=6)

            plot_row += 1

            ####################################################
            #   line plot
            title_string = 'count stability @ {}keV'.format(self.params.energy_kev[cc_bin])
            plt.subplot(num_fig_rows, num_bins, cc_bin + 1 + (num_bins * plot_row))
            plt.tick_params(axis='both', which='major', tick1On=True, label1On=True, labelsize=6)
            plt.plot(self.data.raw_sum[cc_bin, :], color=(0.6, 0.6, 0.6))
            plt.ticklabel_format(style='sci', axis='y', scilimits=(0, 0))
            # plt.title(title_string, fontsize=title_font_size)

        plt.tight_layout(rect=[0, 0, 1, 0.95])
        file_name = '{}_stability_NSR.png'.format(self.info.device_ID)
        save_loc = os.path.join(self.info.save_location, file_name)
        plt.savefig(save_loc)
        plt.close()

    def plot_amalgamated_ncp(self):
        """
        plot NCP for stability
        """
        fig_title = '{} Combined Stability NCPs\n{}keV\nTotal NCP Count: {}'\
            .format(self.info.device_ID,
                    self.params.energy_kev[global_analysis_constants.STAB_NCP_TO_AMALGAMATE['stab_thresh_ind']],
                    self.data.ncp_combined_count)

        plt.imshow(np.rot90(self.data.ncp_combined, -1), cmap='gray_r')
        plt.title(fig_title, fontsize=8)
        file_name = '{}_combined_stability_NCP.png'.format(self.info.device_ID)
        save_loc = os.path.join(self.info.save_location, file_name)
        plt.savefig(save_loc)
        plt.close()