########################################################################################################################
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
########################################################################################################################

import sys
import json
import time
import pickle
import traceback
import easygui as gui

import globals

from global_analysis_functions import *
from global_analysis_constants import *
from dynamic_class import *
from ecal_class import *
from uniformity_class import *
from spectral_class import *
from stability_class import *


import pcct


def pcct_analysis(root_dir='', auto_load=False, summary_dict={}):
    """
    load raw data from json summary of test suite
    create test object (from Asic super class)
    """
    # load JSON summary file and loop through test types
    if auto_load:
        test_suite_dict = summary_dict
    else:
        summary_file = gui.fileopenbox(default=globals.ldir)
        root_dir, _ = os.path.split(summary_file)
        with open(summary_file) as json_in:
            test_suite_dict = json.load(json_in)

    results_dir = 'analysis results'

    dyna_dict = {'class': DynamicTest,
                 'plot': [DynamicTest.plot_dyna_ocr, DynamicTest.plot_pitch_comparison, DynamicTest.plot_shutter,
                          DynamicTest.plot_dyna_ncp],
                 'extension': '.dyna'
                 }
    unif_dict = {'class': UniformityTest,
                 'plot': [UniformityTest.plot_ocr, UniformityTest.plot_ocr_icr, UniformityTest.plot_ocr_icr_240Mcnts,
                          UniformityTest.plot_ocr_ncp, UniformityTest.plot_ocr_outliers,
                          UniformityTest.plot_amalgamated_ncp],
                 'extension': '.unif'
                 }
    stab_dict = {'class': StabilityTest,
                 'plot': [StabilityTest.plot_stability, StabilityTest.plot_amalgamated_ncp, StabilityTest.plot_raw_sum,
                          StabilityTest.plot_stab_nsr],
                 'extension': '.stab'
                 }
    spec_dict = {'class': SpectralTest,
                 'plot': [SpectralTest.plot_spec_mean, SpectralTest.plot_spec_median, SpectralTest.plot_spec_pixel,
                          SpectralTest.plot_energy_linearity, SpectralTest.plot_energy_resolution],
                 'extension': '.spec'
                 }

    test_protocols = {'DYNAMIC': dyna_dict,
                      'UNIFORMITY': unif_dict,
                      'STABILITY': stab_dict,
                      'ENERGY': spec_dict,
                      'SPECTRUM': spec_dict,
                      'ENERGY_CAL': spec_dict}

    ###################################################
    #  make one pager path
    one_pager_dir = os.path.join(root_dir, 'summary results')
    # if not os.path.exists(one_pager_dir):
    #     os.makedirs(one_pager_dir)

    test_device = TestDevice(test_suite_dict, one_pager_dir)

    t = time.time()
    for test_index, test_type in enumerate(test_suite_dict['test_type']):
        for current_file_name in test_suite_dict['output_file_list'][test_index]:

            #######################################################
            #   make results path
            current_output_dir = os.path.join(root_dir, results_dir, current_file_name)
            if not os.path.exists(current_output_dir):
                os.makedirs(current_output_dir)

            print(test_type, current_file_name)

            #######################################################
            #   load data from file
            data_raw, params_raw, info_raw = load_raw_test_data(root_dir, current_file_name)

            #######################################################
            #   run test analysis from protocols dictionary
            try:
                test = test_protocols[test_type]['class'](data_raw, params_raw, info_raw,
                                                          current_output_dir, test_suite_dict)

                #####################################################
                #   make plots
                for plot in test_protocols[test_type]['plot']:
                    plot(test)

                #####################################################
                #   save derivative results
                fname_out = test.info.device_ID
                pickle_ext = test_protocols[test_type]['extension']
                distilled_file_name = '{}_distilled{}'.format(fname_out, pickle_ext)
                fname_full = os.path.join(current_output_dir, distilled_file_name)
                with open(fname_full, 'wb') as pickle_out_handle:
                    pickle.dump(test, pickle_out_handle, protocol=pickle.HIGHEST_PROTOCOL)

            except:
                ex_type, ex, tb = sys.exc_info()
                traceback.print_exception(ex_type, ex, tb)

    total_analysis_time = time.time() - t
    print('total analysis time: {:.2f}'.format(total_analysis_time))


def energy_cal(bias, root_dir='', auto_load=False, summary_dict={}):
    """
    create energy calibration with given bias
    load raw data from json summary of test suite
    create test object (from Asic super class)
    create energy calibration files (CSV format)
    """
    # load JSON summary file and loop through test types
    print('entered in-line energy calibration')
    if auto_load:
        test_suite_dict = summary_dict
    else:
        summary_file = gui.fileopenbox(default=globals.ldir)
        root_dir, _ = os.path.split(summary_file)
        with open(summary_file) as json_in:
            test_suite_dict = json.load(json_in)

    cal_parent_dir = 'calibrations'

    t = time.time()
    spec_data = []
    for test_index, test_type in enumerate(test_suite_dict['test_type']):
        if test_type == 'ENERGY' or test_type == 'ENERGY_CAL':
            for current_file_name in test_suite_dict['output_file_list'][test_index]:

                #######################################################
                #   load data from file
                data_raw, params_raw, info_raw = load_raw_test_data(root_dir, current_file_name)

                if params_raw.hvList == bias:

                    for type in global_analysis_constants.K_EDGE_WINDOWS:
                        if type.lower() in params_raw.source.lower():
                            k_type = type
                            break
                        else:
                            k_type = None

                    if k_type is not None:
                        print(test_type, current_file_name)

                        #######################################################
                        #   make results path
                        calibration_output_dir = os.path.join(root_dir, cal_parent_dir, str(bias), 'K-edges', k_type)
                        if not os.path.exists(calibration_output_dir):
                            os.makedirs(calibration_output_dir)

                        #######################################################
                        #   run test analysis from function dictionary
                        try:
                            spec_data.append(ecal(data_raw, params_raw, info_raw, calibration_output_dir, test_suite_dict))

                        except:
                            ex_type, ex, tb = sys.exc_info()
                            traceback.print_exception(ex_type, ex, tb)

    #######################################################
    #   make combined calibration path
    calibration_output_dir = os.path.join(root_dir, cal_parent_dir, str(bias))
    if not os.path.exists(calibration_output_dir):
        os.makedirs(calibration_output_dir)

    ecal_file = calc_gains_offsets(spec_data, calibration_output_dir)

    total_analysis_time = time.time() - t
    print('total analysis time: {:.2f}'.format(total_analysis_time))

    return ecal_file

if __name__ == '__main__':
    pcct.config_logging(file_level=None)
    pcct.load_packages()

    pcct_analysis()
