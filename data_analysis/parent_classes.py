########################################################################################################################
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
########################################################################################################################

import logging
import global_analysis_constants
import numpy as np
from datetime import datetime

from DDC0864.calcFlux import calcFlux

import pcct
from pcct.module import BaseModule


logger = logging.getLogger(__name__)


########################################################################################################################
class Asic(object):
    """
    main parent class for handling all universal test data and parameters
    """

    def __init__(self, data_raw, params_raw, info_raw, root_dir, test_suite_dict):
        self.info = _TestInfo(info_raw, root_dir, test_suite_dict)
        self.params = _TestParams(params_raw, data_raw)
        self.data = _TestData(data_raw, params_raw)

        try:
            self.module = pcct.module_types()[self.info.device_manufacturer]
        except KeyError:
            logger.error('%s module not found, using default analysis parameters', self.info.device_manufacturer)
            self.module = BaseModule

        self.params.area_correction, self.params.pixel_area = self.area_correct()

    def calc_icr(self, tube_currents=[25.0]):
        """
        determine icr expected values based on pixel size
        """
        flux_correction = global_analysis_constants.FLUX_CORRECTION
        """
        TODO: change test parameters to avoid forcing tube distance and filter thickness
        ################################################################################
        """
        if self.module.FORCE_CALC_FLUX_VALS:
            tube_currents = [global_analysis_constants.TUBE_CURRENT_FORCED]
            filter_thickness = global_analysis_constants.CU_FILTER_THICKNESS_FORCED
            tube_distance = global_analysis_constants.TUBE_DISTANCE_FORCED

        else:
            filter_thickness = self.params.filter_thickness
            tube_distance = self.params.tube_distance

        tube_voltage = [self.params.tube_voltage_list][0]

        num_currs = len(tube_currents)

        icr = np.zeros((num_currs, self.params.num_rows, self.params.num_cols))

        for curr_ind, tube_current in enumerate(tube_currents):
            for row in range(self.params.num_rows):
                for col in range(self.params.num_cols):
                    icr[curr_ind, row, col] = \
                        flux_correction * calcFlux(
                            tube_voltage,
                            tube_current,
                            filter_thickness,
                            tube_distance,
                            self.params.total_test_time,  # TODO:  change this with corrected view time
                            numPix=1,
                            pixArea=self.params.pixel_area[row, col])
        icr_mean = np.nanmean(icr, axis=(1, 2))
        icr_std = np.nanstd(icr, axis=(1, 2))
        icr_sum = np.sum(icr, axis=(1, 2))
        return icr, icr_mean, icr_std, icr_sum

    def area_correct(self):
        correction, pixel_area, _ = self.module.area_correction_voxel()
        return correction, pixel_area

########################################################################################################################
class _TestInfo(object):
    """
    initiate device info
    """
    def __init__(self, info_raw, current_output_dir, test_suite_dict):
        #   load device info paramters
        self.device_ID = info_raw.sensor
        self.mm_number = info_raw.sensor[:-3]
        try:
            self.mm_asic = int(info_raw.sensor[-1])  # last character of 'sensor' TODO: make this more robust! collect from baseline
        except (IndexError, ValueError):
            logger.warning('Unable to get ASIC number from sensor ID')
            self.mm_asic = None
        self.save_location = current_output_dir
        self.device_manufacturer = test_suite_dict['device_manufacturer']
        self.timestamp_string = info_raw.timestamp_string
        self.timestamp_struct = datetime.strptime(self.timestamp_string, '%Y_%m_%d__%H_%M_%S')
        self.site = info_raw.site


########################################################################################################################
class _TestParams(object):
    """
    initiate test and analysis parameters
    """
    def __init__(self, params_raw, data_raw):
        # provided by collection parameters
        self.test_type = params_raw.test_type
        try:
            if hasattr(params_raw, 'photon_source'):
                self.source = params_raw.photon_source
            else:
                self.source = params_raw.source
        except AttributeError:
            self.source = None
        self.energy_kev = params_raw.thresholds
        self.xray_stab_time = params_raw.xRayStabTime
        self.tube_current_list = params_raw.tubeCurrentList
        self.tube_voltage_list = params_raw.tubeVoltageList
        try:
            self.filter_type = params_raw.filter_type
        except AttributeError:
            self.filter_type = None
        try:
            if hasattr(params_raw, 'filter_thickness'):
                self.filter_thickness = params_raw.filter_thickness
            elif hasattr(params_raw, 'cuTube'):
                self.filter_thickness = params_raw.cuTube
        except AttributeError:
            self.filter_thickness = None
        self.max_view_period = params_raw.max_view_period
        self.tube_distance = params_raw.distance
        self.thresh_step = params_raw.threshStep
        self.thresh_start = params_raw.startThresh
        self.thresh_end = params_raw.endThresh
        self.hv_stab_time = params_raw.hvStabTime
        self.total_test_time = params_raw.totTime
        # self.view_period = global_analysis_constants.NOMINAL_TIME_RESOLUTION
        self.view_period = params_raw.timeResolution
        self.hv_list = params_raw.hvList

        #######################################################################
        #   pixel geometry
        self.num_rows = np.size(data_raw, axis=-2)  # number of rows from second last dimension of data array
        self.num_cols = np.size(data_raw, axis=-1)  # number of columns from last dimension of data array
        self.num_pix = self.num_rows * self.num_cols
        self.pixel_map_asic = self.make_pix_map_asic()  # create map of pixel indices to ASIC pixels (serpentine)

    def make_pix_map_asic(self):
        # create index map for 24x36 array
        pix_map = np.indices((self.num_rows, self.num_cols))

        #   flip every other column to match TI ASIC pixel mapping
        for row in range(1, self.num_rows, 2):
            pix_map[1, row, :] = np.flip(pix_map[1, row, :])

        pix_map = np.reshape(pix_map, (2, 864))
        pix_map = np.transpose(pix_map, (1, 0))

        return pix_map


########################################################################################################################
class _TestData(object):
    """
    initiate raw data and flip
    """
    def __init__(self, data_raw, params_raw):
        # flip data along row dimension to correct orientation for anode viewing
        if params_raw.test_type == 'UNIFORMITY':
            if data_raw.ndim == 5:
                self.cc_data = np.flip(data_raw, axis=3).astype(float)
            elif data_raw.ndim == 4:
                self.cc_data = np.flip(data_raw, axis=2).astype(float)
            else:
                print('unexpected UNIFORMITY data structure')
        else:
            self.cc_data = np.flip(data_raw, axis=2).astype(float)


########################################################################################################################
class TestDevice(object):
    """
    object for collecting derivative test results and plotting the 'one-pager' report
    TODO:  This is incomplete / not currently in use
    """
    def __init__(self, test_suite_dict, one_pager_dir):
        self.dyna = []
        self.unif = []
        self.stab = []
        self.one_pager_dir = one_pager_dir
        self.device = test_suite_dict['device']
        self.device_manufacturer = test_suite_dict['device_manufacturer']

    def append_dyna(self, dyna):
        self.dyna.append(dyna)

    def append_unif(self, unif):
        self.unif.append(unif)

    def append_stab(self, stab):
        self.stab.append(stab)

    def amalgamate_ncps(self):
        pass
