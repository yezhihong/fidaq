########################################################################################################################
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
########################################################################################################################

import os
import csv
import numpy as np
import matplotlib.pyplot as plt

import global_analysis_constants
from parent_classes import *


class UniformityTest(Asic):
    """
    Class for handling Uniformity test data
    calculates derivative data: OCR, OCR/ICR, descriptive statistics
    """

    def __init__(self, data_raw, params_raw, info_raw, current_output_dir, test_suite_dict):

        super(UniformityTest, self).__init__(data_raw, params_raw, info_raw, current_output_dir, test_suite_dict)

        ### take slice of 6 Coincidence Counters if all counters (EC, CC, SEC) were collected
        if self.data.cc_data.shape[1] == 13:
            if self.data.cc_data.ndim == 5:
                self.data.cc_data = self.data.cc_data[:, 6:12, :, :, :]
            elif self.data.cc_data.ndim == 4:
                self.data.cc_data = self.data.cc_data[:, 6:12, :, :]

        ##############################################################
        #   initiate uniformity-specific paramters
        self.params.num_currs = np.size(self.data.cc_data, axis=0)    # number of currents from first dimension of data array
        self.params.num_bins = np.size(self.data.cc_data, axis=1)     # number of bins from second dimension of data array

        ##############################################################
        # currents used for plotting and NCP calcs
        # self.params.currents_of_interest_indices = global_analysis_constants.CURRENTS_OF_INTEREST_INDICES
        self.params.currents_of_interest_indices = self.module.CURRENTS_OF_INTEREST_IND
        self.params.currents_of_interest = [self.params.tube_current_list[i]
                                            for i in self.params.currents_of_interest_indices]
        self.params.bins_of_interest_indices = global_analysis_constants.BINS_OF_INTEREST_INDICES

        ###############################################################
        # OCR Raw
        if self.data.cc_data.ndim == 5:
            self.data.ocr_raw = np.nansum(self.data.cc_data, axis=2)  # reduce 0.25s views to 1.0s summed
        else:
            self.data.ocr_raw = self.data.cc_data
        self.data.ocr_raw_open = self.integrate_ocr()  # make open bins from raw
        self.data.ocr_raw_sum_cc = self.make_ocr_raw_sum_cc()  # append integrated OCR (30keV and above) to ocr_raw
        self.data.ocr_raw_mean, self.data.ocr_raw_std, self.data.ocr_raw_max, self.data.ocr_raw_min, \
            self.data.ocr_raw_total_counts, _ = \
            self.calc_ocr_stats(self.data.ocr_raw_sum_cc)

        ################################################################
        #   OCR Corrected
        self.data.ocr_corrected = self.correct_ocr()   # geometric correction of OCR
        self.data.ocr_cleaned, self.data.ocr_outliers, self.data.ocr_outlier_count, \
            self.data.ocr_Q_fence_low, self.data.ocr_Q_fence_high \
            = self.remove_outliers(self.data.ocr_corrected)
        self.data.ocr_mean, self.data.ocr_std, self.data.ocr_max, self.data.ocr_min, \
            self.data.ocr_total_counts, self.data.ocr_uniformity = \
            self.calc_ocr_stats(self.data.ocr_cleaned)

        #################################################################
        #   OCR/ICR
        self.data.icr, self.data.icr_mean, self.data.icr_std, self.data.icr_sum = \
            self.calc_icr(tube_currents=self.params.currents_of_interest)
        self.data.ocr_icr_comp, self.data.ocr_icr_mean, self.data.ocr_icr_std = \
            self.calc_ocr_icr_comp()

        ###############################################################
        #   dead time
        # self.data.dead_time, self.data.dead_time_mean, self.data.dead_time_std = \
        #     self.calc_dead_time()  # calculate dead time from OCR/ICR

        ###############################################################
        #   NCPs
        self.data.ocr_ncp, self.data.ocr_percent_of_mean, self.data.ocr_ncp_count = self.calc_ocr_ncp()
        self.data.ocr_icr_ncp, self.data.ocr_icr_ncp_count = self.calc_ocr_icr_ncp()
        self.data.ncp_combined, self.data.ncp_combined_count = self.combine_ncps()

        ###############################################################
        #   save tabular data
        self.export_ocr_pixel_table()
        self.export_ocr_stats_table()
        self.export_icr_pixel_table()
        self.export_icr_stats_table()

        del self.data.cc_data

    ####################################################################################################################
    #   derivative data methods
    ####################################################################################################################

    def correct_ocr(self):
        """
        load geometric data correction for given product type and adjust values accordingly
        """
        ocr_corrected = np.zeros(np.shape(self.data.ocr_raw_sum_cc))
        for curr_ind, current in enumerate(self.params.tube_current_list):
            for cc_bin in range(np.size(self.data.ocr_raw_sum_cc, axis=1)):
                ocr_corrected[curr_ind, cc_bin, :, :] = \
                    np.round(np.copy(self.data.ocr_raw_sum_cc[curr_ind, cc_bin, :, :]) * self.params.area_correction)

        return ocr_corrected

    def calc_ocr_icr_comp(self):
        """
        calculate ratio (percentage) of OCR to ICR
        includes all closed bins individually, as well as sum of raw counts (integrated bins) above 30keV
        """
        num_currs = len(self.params.currents_of_interest_indices)
        ocr_icr_comp = np.zeros((num_currs, self.params.num_bins + 1, self.params.num_rows, self.params.num_cols))
        for curr_ind, curr in enumerate(self.params.currents_of_interest_indices):
            for cc_bin in range(self.params.num_bins + 1):
                for row in range(self.params.num_rows):
                    for col in range(self.params.num_cols):
                        ocr_icr_comp[curr_ind, cc_bin, row, col] = self.data.ocr_raw_sum_cc[curr, cc_bin, row, col] / \
                                                                   self.data.icr[-1, row, col] * 100

        ocr_icr_mean = np.nanmean(ocr_icr_comp, axis=(2, 3))
        ocr_icr_std = np.nanstd(ocr_icr_comp, axis=(2, 3))

        return ocr_icr_comp, ocr_icr_mean, ocr_icr_std

    def integrate_ocr(self):
        """
        integrate ocr into open bin configuration by summing bins upward
        """
        ocr_raw_open = np.zeros(np.shape(self.data.ocr_raw))
        for cc_bin in range(self.params.num_bins):
            ocr_raw_open[:, cc_bin, :, :] = np.nansum(np.copy(self.data.ocr_raw[:, cc_bin:, :, :]), axis=1)

        return ocr_raw_open

    def make_ocr_raw_sum_cc(self):
        """
        create N-D array with all closed bins, plus additional open bin (integrated 30keV and above)
        """
        integration_bin = global_analysis_constants.OPEN_BIN_INTEGRATION_VAL
        if integration_bin in self.params.energy_kev:
            integration_index = list(self.params.energy_kev).index(integration_bin)
        else:
            integration_index = self.module.OPEN_BIN_INTEGRATION_IND

        ocr_raw_sum_cc = np.zeros((self.params.num_currs,
                                   self.params.num_bins + 1,
                                   self.params.num_rows,
                                   self.params.num_cols))
        ocr_raw_sum_cc[:, :-1, :, :] = self.data.ocr_raw
        ocr_raw_sum_cc[:, -1, :, :] = self.data.ocr_raw_open[:, integration_index, :, :]  # integrated 30keV and above

        return ocr_raw_sum_cc

    def calc_dead_time(self):
        """
        calculate dead time according to:  (ICR - OCR) / ICR * OCR
        using 25mA SUMCC bin
        """
        # dead_time = np.zeros(np.shape(self.data.ocr_raw_sum_cc[-1, -1, :, :]))
        dead_time = ((self.data.icr[-1, :, :] - self.data.ocr_raw_sum_cc[-1, -1, :, :]) /
                     (self.data.icr[-1, :, :] * self.data.ocr_raw_sum_cc[-1, -1, :, :]))

        dead_time_mean = np.nanmean(dead_time)
        dead_time_std = np.nanstd(dead_time)

        return dead_time, dead_time_mean, dead_time_std

    def remove_outliers(self, ocr_data):
        """
        use inter-quartile analysis to determine and remove statistical outlier pixels per tube current
        """
        Q1, Q2, Q3 = np.quantile(ocr_data, (0.25, 0.50, 0.75), axis=(2, 3))
        IQR = Q3 - Q1
        fence_low = Q1 - (1.5 * IQR)
        fence_low[fence_low < 0] = 0
        fence_high = Q3 + (1.5 * IQR)

        ocr_clean = np.copy(ocr_data)
        ocr_outliers = np.zeros(np.shape(ocr_data))
        for curr, _ in enumerate(self.params.tube_current_list):
            for cc_bin in range(self.params.num_bins + 1):
                for row in range(self.params.num_rows):
                    for col in range(self.params.num_cols):
                        if (ocr_data[curr, cc_bin, row, col] < fence_low[curr, cc_bin]
                                or ocr_data[curr, cc_bin, row, col] > fence_high[curr, cc_bin]):
                            ocr_clean[curr, cc_bin, row, col] = np.NaN
                            ocr_outliers[curr, cc_bin, row, col] = 1

        ocr_outlier_count = np.count_nonzero(ocr_outliers, axis=(2, 3))
        return ocr_clean, ocr_outliers, ocr_outlier_count, fence_low, fence_high

    def calc_ocr_stats(self, ocr_data):
        """
        claculate descriptive stats and uniformity (std/mean) for OCR data
        """
        mean = np.nanmean(ocr_data, axis=(2, 3))
        std = np.nanstd(ocr_data, axis=(2, 3))
        max_ = np.nanmax(ocr_data, axis=(2, 3))
        min_ = np.nanmin(ocr_data, axis=(2, 3))
        tot_cnts = np.nansum(ocr_data, axis=(2, 3))
        uniformity = np.zeros(np.shape(ocr_data[:, :, 0, 0]))
        for curr, _ in enumerate(self.params.tube_current_list):
            for cc_bin in range(self.params.num_bins + 1):
                uniformity[curr, cc_bin] = std[curr, cc_bin] / mean[curr, cc_bin]

        return mean, std, max_, min_, tot_cnts, uniformity

    def calc_ocr_ncp(self):
        """
        calculate Non-Conforming Pixels for area corrected OCR based on percentage of mean OCR
        """
        ocr_range = global_analysis_constants.THRESHOLD_OCR
        fixed_0ma_thresh = global_analysis_constants.THRESHOLD_OCR_0mA
        num_currs = len(self.params.currents_of_interest_indices)
        ocr_ncp = np.zeros((num_currs,
                            self.params.num_bins,
                            self.params.num_rows, self.params.num_cols))
        ocr_percent_of_mean = np.zeros((num_currs,
                                        self.params.num_bins,
                                        self.params.num_rows, self.params.num_cols))
        for curr_ind, curr in enumerate(self.params.currents_of_interest_indices):
            for cc_bin in range(self.params.num_bins):
                for row in range(self.params.num_rows):
                    for col in range(self.params.num_cols):
                        if curr_ind > 0:
                            ocr_percent_of_mean[curr_ind, cc_bin, row, col] = \
                                (self.data.ocr_corrected[curr, cc_bin, row, col] /
                                 self.data.ocr_mean[curr, cc_bin] * 100)
                            if 0 < row < self.params.num_rows - 1 and 0 < col < self.params.num_cols - 1:
                                range_ind = 0  # range index for bulk pixels
                            else:
                                range_ind = 1  # range index for edge pixels
                            if (abs(100 - ocr_percent_of_mean[curr_ind, cc_bin, row, col]) > ocr_range[range_ind]
                                    or np.isnan(ocr_percent_of_mean[curr_ind, cc_bin, row, col])):
                                ocr_ncp[curr_ind, cc_bin, row, col] = 1
                        elif curr_ind == 0:
                            if self.data.ocr_corrected[curr, cc_bin, row, col] > fixed_0ma_thresh:
                                ocr_ncp[curr_ind, cc_bin, row, col] = 1
        ocr_ncp_count = np.count_nonzero(ocr_ncp == 1, axis=(2, 3))

        return ocr_ncp, ocr_percent_of_mean, ocr_ncp_count

    def calc_ocr_icr_ncp(self):
        """
        calculate Non-Conforming Pixels for OCR/ICR @ 25.0mA based on set threshold (e.g. 65-85%)
        """
        ocr_icr_range = global_analysis_constants.THRESHOLD_OCR_ICR
        ocr_icr_ncp = np.zeros((self.params.num_rows, self.params.num_cols))

        ####################################################
        #   determine NCPs
        ocr_icr_ncp[self.data.ocr_icr_comp[-1, -1, :, :] < ocr_icr_range[0]] = 1  # below minimum threshold
        ocr_icr_ncp[self.data.ocr_icr_comp[-1, -1, :, :] > ocr_icr_range[1]] = 1  # above maximum threshold
        ocr_icr_ncp[np.isnan(self.data.ocr_icr_comp[-1, -1, :, :])] = 1  # NaN value

        ocr_icr_ncp_count = np.count_nonzero(ocr_icr_ncp == 1)  # count per bin/current

        return ocr_icr_ncp, ocr_icr_ncp_count

    def combine_ncps(self):
        """
        combine NPCs into single array
        """
        # ocr_thresh_keV = global_analysis_constants.OCR_ICR_NCP_TO_AMALGAMATE['ocr_thresh_keV']
        # ocr_current_mA = global_analysis_constants.OCR_ICR_NCP_TO_AMALGAMATE['ocr_current_mA']

        # ocr_thresh_index = [np.where(self.params.energy_kev ==
        #                              ocr_thresh_keV[i])[0][0] for i in range(len(ocr_thresh_keV))]
        # ocr_current_index = [np.where(np.array(self.params.currents_of_interest) ==
        #                               ocr_current_mA[i])[0][0] for i in range(len(ocr_current_mA))]

        ocr_thresh_index = self.module.OCR_ICR_NCP_TO_AMALGAMATE_THRESH_IND
        ocr_current_index = self.module.OCR_ICR_NCP_TO_AMALGAMATE_CURRENT_IND

        ncp_combined = np.zeros((self.params.num_rows, self.params.num_cols))
        ocr_ncp_combined = [np.squeeze(self.data.ocr_ncp[curr, thresh, :, :])
                            for curr in ocr_current_index
                            for thresh in ocr_thresh_index]
        for ncp in ocr_ncp_combined:
            ncp_combined = (np.logical_or(np.copy(ncp_combined), ncp)).astype(int)

        ncp_combined = (np.logical_or(ncp_combined, self.data.ocr_icr_ncp)).astype(int)

        ncp_combined_count = np.count_nonzero(ncp_combined == 1)

        return ncp_combined, ncp_combined_count

    ####################################################################################################################
    #   table exports
    ####################################################################################################################

    def export_ocr_stats_table(self):
        """
        write OCR statistical data to csv table
        """
        table_headers = []
        table_columns = []
        keV_strings = [keV for keV in self.params.energy_kev]
        keV_strings.append('{}-{}'.format(self.params.energy_kev[1], self.params.energy_kev[-1]))

        ###########################################################
        #   create column and header for uniformity metric
        for curr, curr_ind in zip(self.params.currents_of_interest, self.params.currents_of_interest_indices):
            for cc_bin in range(np.size(self.data.ocr_uniformity, axis=1)):
                table_columns.append(self.data.ocr_uniformity[curr_ind, cc_bin])
                header = 'OCR uniformtiy ({:.1f}mA @ {}keV)'.format(curr, keV_strings[cc_bin])
                table_headers.append(header)

        ###########################################################
        #   create column and header data for OCR data
        data_desc = ['raw', 'corrected']
        stats_key = ['mean', 'std', 'max', 'min', 'total counts']
        raw_stats = [self.data.ocr_raw_mean, self.data.ocr_raw_std, self.data.ocr_raw_max, self.data.ocr_raw_min, self.data.ocr_raw_total_counts]
        corrected_stats = [self.data.ocr_mean, self.data.ocr_std, self.data.ocr_max, self.data.ocr_min, self.data.ocr_total_counts]
        raw_dict = dict(zip(stats_key, raw_stats))
        corrected_dict = dict(zip(stats_key, corrected_stats))
        data_dict = dict(zip(data_desc, [raw_dict, corrected_dict]))

        for desc in data_dict:
            for curr, curr_ind in zip(self.params.currents_of_interest, self.params.currents_of_interest_indices):
                for cc_bin in range(np.size(self.data.ocr_raw_sum_cc, axis=1)):
                    for stat in stats_key:
                        table_columns.append(data_dict[desc][stat][curr_ind, cc_bin])
                        header = 'OCR {} ({} {:.1f}mA @ {}keV)'.format(stat, desc, curr, keV_strings[cc_bin])
                        table_headers.append(header)

        ###########################################################
        #   create column and header data for OCR NCPs
        ncp_desc = ['NCP', 'Outlier']
        ncp_members = [self.data.ocr_ncp_count, self.data.ocr_outlier_count]
        ncp_dict = dict(zip(ncp_desc, ncp_members))

        for desc in ncp_desc:
            for curr_ind, curr in enumerate(self.params.currents_of_interest):
                for cc_bin in range(np.size(ncp_dict[desc], axis=1)):
                    table_columns.append(ncp_dict[desc][curr_ind, cc_bin])
                    header = '{} Count {:.1f}mA {}keV'.format(desc, curr, keV_strings[cc_bin])
                    table_headers.append(header)

        file_name = '{}_OCR_stats.csv'.format(self.info.device_ID)
        save_loc = os.path.join(self.info.save_location, file_name)

        with open(save_loc, 'wb') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(table_headers)
            writer.writerow(table_columns)

    def export_ocr_pixel_table(self):
        """
        write selected OCR array data to csv table
        """
        table_headers = []
        table_columns = []
        keV_strings = [keV for keV in self.params.energy_kev]
        keV_strings.append('{}-{}'.format(self.params.energy_kev[1], self.params.energy_kev[-1]))

        ###########################################################
        #   populate pixel number and area columns
        pixel_number = list(range(self.params.num_pix))
        table_columns.append(pixel_number)
        pixel_number_header = 'pixel'
        table_headers.append(pixel_number_header)

        pixel_area = list(self.params.pixel_area.flatten())
        table_columns.append(pixel_area)
        pixel_area_header = 'pixel area (mm^2)'
        table_headers.append(pixel_area_header)

        ###########################################################
        #   create column and header data for OCR raw and corrected
        data_desc = ['raw', 'corrected']
        data_members = [self.data.ocr_raw_sum_cc, self.data.ocr_corrected]
        data_dict = dict(zip(data_desc, data_members))

        for desc in data_dict:
            for curr, curr_ind in zip(self.params.currents_of_interest, self.params.currents_of_interest_indices):
                for cc_bin in range(np.size(data_dict[desc], axis=1)):
                    table_columns.append(data_dict[desc][curr_ind, cc_bin, :, :].flatten())
                    header = 'OCR ({}) {:.1f}mA {}keV'.format(desc, curr, keV_strings[cc_bin])
                    table_headers.append(header)

        ###########################################################
        #   create column and header data for OCR NCPs
        ncp_desc = ['% of mean', 'NCP', 'Outliers']
        ncp_members = [self.data.ocr_percent_of_mean, self.data.ocr_ncp, self.data.ocr_outliers]
        ncp_dict = dict(zip(ncp_desc, ncp_members))

        for desc in ncp_desc:
            for curr_ind, curr in enumerate(self.params.currents_of_interest):
                for cc_bin in range(np.size(ncp_dict[desc], axis=1)):
                    table_columns.append(ncp_dict[desc][curr_ind, cc_bin, :, :].flatten())
                    header = 'OCR ({}) {:.1f}mA {}keV'.format(desc, curr, keV_strings[cc_bin])
                    table_headers.append(header)

        file_name = '{}_OCR_pixel_data.csv'.format(self.info.device_ID)
        save_loc = os.path.join(self.info.save_location, file_name)

        with open(save_loc, 'wb') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(table_headers)
            writer.writerows(zip(*table_columns))

    def export_icr_pixel_table(self):
        """
        write selected OCR/ICR array data to csv table
        """
        table_headers = []
        table_columns = []
        keV_strings = [keV for keV in self.params.energy_kev]
        keV_strings.append('{}-{}'.format(self.params.energy_kev[1], self.params.energy_kev[-1]))

        ###########################################################
        #   populate pixel number and area columns
        pixel_number = list(range(self.params.num_pix))
        table_columns.append(pixel_number)
        pixel_number_header = 'pixel'
        table_headers.append(pixel_number_header)

        pixel_area = list(self.params.pixel_area.flatten())
        table_columns.append(pixel_area)
        pixel_area_header = 'pixel area (mm^2)'
        table_headers.append(pixel_area_header)

        ###########################################################
        #   create column and header for ICR
        table_columns.append(self.data.icr[-1, :, :].flatten())
        icr_header = 'ICR @ {}mA'.format(self.params.currents_of_interest[-1])
        table_headers.append(icr_header)

        ###########################################################
        #   create column and header data for OCR/ICR
        for cc_bin in range(np.size(self.data.ocr_icr_comp, axis=1)):
            table_columns.append(self.data.ocr_icr_comp[-1, cc_bin, :, :].flatten())
            header = '% ICR ({:.1f}mA @ {}keV)'.format(self.params.currents_of_interest[-1], keV_strings[cc_bin])
            table_headers.append(header)

        ###########################################################
        #   create column and header data for OCR/ICR NCPs
        table_columns.append(self.data.ocr_icr_ncp.flatten())
        ncp_header = 'NCP ({:.1f}mA @ {}keV)'.format(self.params.currents_of_interest[-1], keV_strings[-1])
        table_headers.append(ncp_header)

        file_name = '{}_ICR_pixel_data.csv'.format(self.info.device_ID)
        save_loc = os.path.join(self.info.save_location, file_name)

        with open(save_loc, 'wb') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(table_headers)
            writer.writerows(zip(*table_columns))

    def export_icr_stats_table(self):
        """
        write OCR statistical data to csv table
        """
        table_headers = []
        table_columns = []
        keV_strings = [keV for keV in self.params.energy_kev]
        keV_strings.append('{}-{}'.format(self.params.energy_kev[1], self.params.energy_kev[-1]))

        ###########################################################
        #   create column and header data ICR stats
        table_columns.append(np.nanmean(self.params.pixel_area))
        icr_mean_header = 'mean pixel area (mm^2)'
        table_headers.append(icr_mean_header)

        table_columns.append(self.data.icr_mean[-1])
        icr_mean_header = 'ICR mean @ {}mA'.format(self.params.currents_of_interest[-1])
        table_headers.append(icr_mean_header)

        table_columns.append(self.data.icr_std[-1])
        icr_std_header = 'ICR std @ {}mA'.format(self.params.currents_of_interest[-1])
        table_headers.append(icr_std_header)

        ###########################################################
        #   create column and header data for OCR/ICR stats
        data_desc = ['mean', 'std']
        icr_stats = [self.data.ocr_icr_mean, self.data.ocr_icr_std]
        data_dict = dict(zip(data_desc, icr_stats))

        for cc_bin in range(np.size(self.data.ocr_icr_mean, axis=1)):
            for desc in data_desc:
                table_columns.append(data_dict[desc][-1, cc_bin])
                header = 'ICR  % {} ({:.1f}mA @ {}keV)'.format(desc,
                                                             self.params.currents_of_interest[-1],
                                                             keV_strings[cc_bin])
                table_headers.append(header)

        ###########################################################
        #   create column and header data for OCR/ICR NCP data
        table_columns.append(self.data.ocr_icr_ncp_count)
        header = 'ICR NCP count ({:.1f}mA @ {}keV)'.format(self.params.currents_of_interest[-1], keV_strings[-1])
        table_headers.append(header)

        file_name = '{}_ICR_stats.csv'.format(self.info.device_ID)
        save_loc = os.path.join(self.info.save_location, file_name)

        with open(save_loc, 'wb') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(table_headers)
            writer.writerow(table_columns)

    ####################################################################################################################
    #   plotting methods
    ####################################################################################################################

    def plot_ocr(self):
        """
        plot OCR raw counts and descriptive stats for bins of interest
        """
        title_font_size = 6
        current_indices = self.params.currents_of_interest_indices  # (0, 4, 6, 11) currents 0.0, 1.0, 3.0, and 25.0mA
        num_currs = len(current_indices)
        num_bins = self.params.num_bins
        # num_currs = self.params.num_currs

        fig, axarr = plt.subplots(num_bins, num_currs * 2, sharex=False, sharey='col', figsize=(12.8, 7.8), dpi=100)

        for row in range(num_bins):
            for col in range(num_currs):
                axarr[(num_bins - 1) - row, col*2].set(xlabel='{}mA'.format(self.params.tube_current_list[current_indices[col]]),
                                                       ylabel='{}keV'.format(self.params.energy_kev[row]))
                axarr[(num_bins - 1) - row, col*2].label_outer()
                axarr[(num_bins - 1) - row, col*2].tick_params(axis='both', which='major',
                                                               tick1On=False, label1On=False)

        fig_title = '{} uniformity OCR (Area Corrected)'.format(self.info.device_ID)
        plt.suptitle(fig_title, fontsize=9.5)
        for cc_bin in range(num_bins):
            row = 1
            # for curr in range(num_currs):
            for col, curr in enumerate(current_indices):
                ###################################################
                #   heat maps
                color_scale = (np.nanmin(self.data.ocr_Q_fence_low[curr, :-1]), np.nanmax(self.data.ocr_Q_fence_high[curr, :-1]))
                im_title = 'unif: {:.2f}, cnts: {:.2e}'.format(self.data.ocr_uniformity[curr, cc_bin],
                                                               self.data.ocr_total_counts[curr, cc_bin])

                axarr[(num_bins - 1) - cc_bin, col*2].imshow(self.data.ocr_corrected[curr, cc_bin, :, :], cmap='jet',
                                                             vmin=color_scale[0], vmax=color_scale[1])
                axarr[(num_bins - 1) - cc_bin, col * 2].set_title(im_title, fontsize=title_font_size)

                ###################################################
                #   histograms
                hist_title = 'mn: {:.2e}, std: {:.2e}'.format(self.data.ocr_mean[curr, cc_bin],
                                                              self.data.ocr_std[curr, cc_bin])
                hist_range = [self.data.ocr_mean[curr, cc_bin] - (self.data.ocr_std[curr, cc_bin]*3),
                              self.data.ocr_mean[curr, cc_bin] + (self.data.ocr_std[curr, cc_bin]*3)]

                axarr[(num_bins - 1) - cc_bin, (col*2) + 1].hist(np.copy(self.data.ocr_corrected[curr, cc_bin, :, :]).flatten(),
                                                                 range=hist_range, bins=20,
                                                                 histtype='stepfilled', color='k')

                axarr[(num_bins - 1) - cc_bin, (col*2) + 1].set_title(hist_title, fontsize=title_font_size)
                axarr[(num_bins - 1) - cc_bin, (col * 2) + 1].tick_params(axis='both', which='major', tick1On=True,
                                                                          label1On=True, labelsize=6)
                axarr[(num_bins - 1) - cc_bin, (col * 2) + 1].ticklabel_format(style='sci', axis='x', scilimits=(0, 0))
                row += 1

        plt.tight_layout(rect=[0, 0, 1, 0.95])
        file_name = '{}_uniformity_OCR.png'.format(self.info.device_ID)
        save_loc = os.path.join(self.info.save_location, file_name)
        plt.savefig(save_loc)
        plt.close()

    def plot_ocr_icr(self):
        """
        plot OCR/ICR percentages for 25mA and raw counts across all bins
        """

        title_font_size = 9
        # plt.figure(figsize=(12.8, 7.8), dpi=100)
        num_currs = self.params.num_currs
        num_bins = self.params.num_bins + 1
        num_pix = self.params.num_pix

        fig, axarr = plt.subplots(2, num_bins, sharex='row', sharey='row', figsize=(12.8, 7.8), dpi=100)
        for ind, ax in enumerate(axarr.flat):
            if ind % 2:
                ax.set(ylabel='OCR per pixel')
            ax.set(xlabel="tube current (mA)")
            ax.label_outer()

        fig_title = '{} uniformity OCR/ICR'.format(self.info.device_ID)
        fig.suptitle(fig_title, fontsize=9.5)

        # color_scale = (0, np.nanstd(self.data.ocr_icr_comp[:]*4))
        ocr_reshape = np.reshape(np.copy(self.data.ocr_raw_sum_cc), (num_currs, num_bins, num_pix))

        for cc_bin in range(num_bins):
            #########################################################
            #   plot open bin OCR/ICR heat maps
            if cc_bin == num_bins - 1:
                bin_text = 'Sum CC'
                color_scale = [0, 100]
            else:
                bin_text = '{}keV'.format(self.params.energy_kev[cc_bin])
                color_scale = [0, 25]

            title_string = '{}\nmean: {:.1f}%\nstd: {:.1f}%'.format(bin_text,
                                                                    np.nanmean(self.data.ocr_icr_comp[-1, cc_bin, :, :]),
                                                                    np.nanstd(self.data.ocr_icr_comp[-1, cc_bin, :, :]))

            axarr[0, cc_bin].tick_params(axis='both', which='major', tick1On=False, label1On=False)
            im = axarr[0, cc_bin].imshow(self.data.ocr_icr_comp[-1, cc_bin, :, :], cmap='jet',
                                         vmin=color_scale[0], vmax=color_scale[1])
            axarr[0, cc_bin].set_title(title_string, fontsize=title_font_size)
            cbar = fig.colorbar(im, ax=axarr[0, cc_bin], orientation='horizontal')
            cbar.set_label(label='OCR/ICR (%) @ 25mA', size=7)
            cbar.ax.tick_params(labelsize=7)

            #########################################################
            #   plot open bin OCR
            axarr[1, cc_bin].plot(self.params.tube_current_list, ocr_reshape[:, cc_bin, :], color=(0.8, 0.8, 0.8))
            axarr[1, cc_bin].plot(self.params.tube_current_list, self.data.ocr_raw_mean[:, cc_bin], color='r')

        plt.tight_layout(rect=[0, 0, 1, 0.95])
        file_name = '{}_ICR.png'.format(self.info.device_ID)
        save_loc = os.path.join(self.info.save_location, file_name)
        plt.savefig(save_loc)
        plt.close()

    def plot_dead_time(self):
        title_string = '{} Dead Time (ns)'.format(self.info.device_ID)
        title_font_size = 12
        dt_range = (0.6e-8, 2.6e-8)
        fig = plt.figure()
        ####################################################
        #   plot heatmap of dead time
        plt.subplot(1, 2, 1)
        im = plt.imshow(np.rot90(self.data.dead_time, -1), cmap='jet', vmin=dt_range[0], vmax=dt_range[1])
        plt.tick_params(axis='both', which='major', tick1On=False, label1On=False)

        fig.suptitle(title_string, fontsize=title_font_size)
        cbar = plt.colorbar(im, orientation='horizontal')
        cbar.ax.tick_params(labelsize=7, which='major')

        ####################################################
        #   plot histogram and dead time descriptive stats
        plt.subplot(1, 2, 2)
        plt.title('mean: {:.2e}\nstd: {:.2e}'.format(self.data.dead_time_mean, self.data.dead_time_std), fontsize=8)
        plt.hist(self.data.dead_time.flat[:], range=dt_range, bins=20, histtype='step', color='k')
        plt.axvline(self.data.dead_time_mean, color='r', linestyle='dashed', linewidth=1)

        plt.tight_layout(rect=[0, 0, 1, 0.95])
        file_name = '{}_calculated_dead_time.png'.format(self.info.device_ID)
        save_loc = os.path.join(self.info.save_location, file_name)
        plt.savefig(save_loc)
        plt.close()

    def plot_ocr_icr_240Mcnts(self):
        """
        plot OCR/ICR at 25mA (240Mcnts) with NCPs, histogram and descriptive stats
        """

        color_scale = [0, 100]
        fig = plt.figure()
        title_font_size = 12
        title_string = '{} OCR/ICR @ 240Mcnts (25mA >30keV)'.format(self.info.device_ID)
        fig.suptitle(title_string, fontsize=title_font_size)

        ########################################################
        #   plot heat map
        ax1 = plt.subplot(1, 3, 1)
        im = plt.imshow(np.rot90(self.data.ocr_icr_comp[-1, -1, :, :], -1), cmap='jet', vmin=color_scale[0], vmax=color_scale[1])
        plt.tick_params(axis='both', which='major', tick1On=False, label1On=False)
        heat_map_title = 'OCR/ICR'.format(self.params)
        plt.title(heat_map_title, fontsize=7)
        cbar = plt.colorbar(im, orientation='horizontal')
        cbar.set_label(label='OCR/ICR (%) @ 25mA', size=7)
        cbar.ax.tick_params(labelsize=7, which='major')

        ########################################################
        #   plot NCP map
        ax2 = plt.subplot(1, 3, 2, sharex=ax1, sharey=ax1)
        ncp_title = '{} OCR/ICR NCP\n>30keV @ 25mA ({}%-{}%)\nCount: {}'.format(self.info.device_ID,
                                                                     global_analysis_constants.THRESHOLD_OCR_ICR[0],
                                                                     global_analysis_constants.THRESHOLD_OCR_ICR[1],
                                                                     self.data.ocr_icr_ncp_count)
        plt.imshow(np.rot90(self.data.ocr_icr_ncp, -1), cmap='gray_r')
        # plt.tick_params(axis='both', which='major', tick1On=False, label1On=False)
        plt.title(ncp_title, fontsize=7)

        ########################################################
        #   plot histogram
        hist_range = [50, 90]
        plt.subplot(1, 3, 3)
        plt.hist(self.data.ocr_icr_comp[-1, -1, :, :].flat[:], range=hist_range, bins=40, histtype='step', color='k')
        plt.axvline(self.data.ocr_icr_mean[-1, -1], color='r', linestyle='dashed', linewidth=1)
        hist_title = 'mean: {:.1f}%\nstd: {:.1f}%'.format(self.data.ocr_icr_mean[-1, -1], self.data.ocr_icr_std[-1, -1])
        plt.title(hist_title, fontsize=7)

        plt.tight_layout(rect=[0, 0, 1, 0.95])
        file_name = '{}_ICR_SUM-CC.png'.format(self.info.device_ID)
        save_loc = os.path.join(self.info.save_location, file_name)
        plt.savefig(save_loc)
        plt.close()

    def plot_ocr_ncp(self):
        """
        plot NCP for OCR
        """
        title_font_size = 6
        current_indices = self.params.currents_of_interest_indices  # (0, 4, 6, 11) currents 0.0, 1.0, 3.0, and 25.0mA
        num_currs = len(current_indices)
        num_bins = self.params.num_bins
        ncp_range = global_analysis_constants.THRESHOLD_OCR

        fig, axarr = plt.subplots(num_bins, num_currs, sharex=False, sharey='col', figsize=(12.8, 7.8), dpi=100)
        for row in range(num_bins):
            for col in range(num_currs):
                axarr[(num_bins - 1) - row, col].set(xlabel='{}mA'.format(self.params.tube_current_list[current_indices[col]]),
                                                     ylabel='{}keV'.format(self.params.energy_kev[row]))
                axarr[(num_bins - 1) - row, col].label_outer()
                axarr[(num_bins - 1) - row, col].tick_params(axis='both', which='major',
                                                             tick1On=False, label1On=False)

        fig_title = '{} OCR NCP (area corrected)\n>{}% mean (interior) >{}% mean (edge)'.format(self.info.device_ID,
                                                                                               ncp_range[0],
                                                                                               ncp_range[1])
        plt.suptitle(fig_title, fontsize=9.5)
        for cc_bin in range(num_bins):
            for col, curr in enumerate(current_indices):
                ###################################################
                #   binary maps
                color_scale = (0, 1)
                title_string = 'NCP count: {}'.format(self.data.ocr_ncp_count[col, cc_bin])

                axarr[(num_bins - 1) - cc_bin, col].imshow(self.data.ocr_ncp[col, cc_bin, :, :], cmap='gray_r',
                                                           vmin=color_scale[0], vmax=color_scale[1])
                axarr[(num_bins - 1) - cc_bin, col].set_title(title_string, fontsize=title_font_size)

        plt.tight_layout(rect=[0, 0, 1, 0.95])
        file_name = '{}_OCR_NCP.png'.format(self.info.device_ID)
        save_loc = os.path.join(self.info.save_location, file_name)
        plt.savefig(save_loc)
        plt.close()


    def plot_ocr_outliers(self):
        """
        plot outliers for OCR
        """
        title_font_size = 6
        current_indices = self.params.currents_of_interest_indices  # (0, 4, 6, 11) currents 0.0, 1.0, 3.0, and 25.0mA
        num_currs = len(current_indices)
        num_bins = self.params.num_bins
        ncp_range = global_analysis_constants.THRESHOLD_OCR

        fig, axarr = plt.subplots(num_bins, num_currs, sharex=False, sharey='col', figsize=(12.8, 7.8), dpi=100)
        for row in range(num_bins):
            for col in range(num_currs):
                axarr[(num_bins - 1) - row, col].set(xlabel='{}mA'.format(self.params.tube_current_list[current_indices[col]]),
                                                     ylabel='{}keV'.format(self.params.energy_kev[row]))
                axarr[(num_bins - 1) - row, col].label_outer()
                axarr[(num_bins - 1) - row, col].tick_params(axis='both', which='major',
                                                             tick1On=False, label1On=False)

        fig_title = '{} OCR Outliers (area corrected)\n< Q1 - 1.5*IQR > Q3 + 1.5*IQR'.format(self.info.device_ID)
        plt.suptitle(fig_title, fontsize=9.5)
        for cc_bin in range(num_bins):
            for col, curr in enumerate(current_indices):
                ###################################################
                #   binary maps
                color_scale = (0, 1)
                title_string = 'Outlier count: {}'.format(self.data.ocr_outlier_count[col, cc_bin])

                axarr[(num_bins - 1) - cc_bin, col].imshow(self.data.ocr_outliers[col, cc_bin, :, :], cmap='gray_r',
                                                           vmin=color_scale[0], vmax=color_scale[1])
                axarr[(num_bins - 1) - cc_bin, col].set_title(title_string, fontsize=title_font_size)

        plt.tight_layout(rect=[0, 0, 1, 0.95])
        file_name = '{}_OCR_Outliers.png'.format(self.info.device_ID)
        save_loc = os.path.join(self.info.save_location, file_name)
        plt.savefig(save_loc)
        plt.close()

    def plot_ocr_icr_ncp(self):
        """
        plot NCP for OCR/ICR
        """
        fig_title = '{} OCR/ICR NCP\n>30keV @ 25mA ({}%-{}%)\nCount: {}'.format(self.info.device_ID,
                                                                     global_analysis_constants.THRESHOLD_OCR_ICR[0],
                                                                     global_analysis_constants.THRESHOLD_OCR_ICR[1],
                                                                     self.data.ocr_icr_ncp_count)
        plt.imshow(np.rot90(self.data.ocr_icr_ncp, -1), cmap='gray_r')
        plt.title(fig_title)
        file_name = '{}_OCR_ICR_NCP.png'.format(self.info.device_ID)
        save_loc = os.path.join(self.info.save_location, file_name)
        plt.savefig(save_loc)
        plt.close()

    def plot_amalgamated_ncp(self):
        """
        plot NCP for OCR/ICR
        """
        fig_title = '{} Combined Uniformity NCPs\n{}keV {}mA\nTotal NCP Count: {}'\
            .format(self.info.device_ID,
                    self.params.energy_kev[self.params.bins_of_interest_indices][self.module.OCR_ICR_NCP_TO_AMALGAMATE_THRESH_IND],
                    self.params.tube_current_list[self.params.currents_of_interest_indices][self.module.OCR_ICR_NCP_TO_AMALGAMATE_CURRENT_IND],
                    # global_analysis_constants.OCR_ICR_NCP_TO_AMALGAMATE['ocr_thresh_keV'],
                    # global_analysis_constants.OCR_ICR_NCP_TO_AMALGAMATE['ocr_current_mA'],
                    self.data.ncp_combined_count)

        plt.imshow(np.rot90(self.data.ncp_combined, -1), cmap='gray_r')
        plt.title(fig_title, fontsize=8)
        file_name = '{}_combined_uniformity_NCP.png'.format(self.info.device_ID)
        save_loc = os.path.join(self.info.save_location, file_name)
        plt.savefig(save_loc)
        plt.close()