########################################################################################################################
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
########################################################################################################################

import os
import csv
from scipy import io as sio

import globals

from ecal_class import *
from spectral_class import *


########################################################################################################################
#   global data handling functions
def load_raw_test_data(root_dir, current_file_name):
    """
    load raw test data from *.mat file
    descramble if necessary (RevA sensor screener)
    """
    full_file = os.path.join(root_dir, current_file_name)

    mat = sio.loadmat(full_file, struct_as_record=False, squeeze_me=True)
    cc_struct = mat['cc_struct']
    params_raw = cc_struct.params
    info_raw = cc_struct.info
    info_raw.timestamp_string = current_file_name[-20:]  # parse timestamp from trailing 20 characters of path string
    info_raw.timestamp_struct = datetime.strptime(info_raw.timestamp_string, '%Y_%m_%d__%H_%M_%S')

    data_raw = cc_struct.data.cc_data

    return data_raw, params_raw, info_raw


########################################################################################################################
#   instantiate spectral object and calculate values
def ecal(data_raw, params_raw, info_raw, current_output_dir, test_suite_dict):

    ecal_data = EnergyCal(data_raw, params_raw, info_raw, current_output_dir, test_suite_dict)

    ecal_data.export_k_edge_table()

    ################################################################
    #   plot spectra
    ecal_data.plot_spec_pixel(k_type=ecal_data.params.k_type)
    ecal_data.plot_spec_mean(k_type=ecal_data.params.k_type)
    ecal_data.plot_spec_median(k_type=ecal_data.params.k_type)

    return ecal_data


def calc_gains_offsets(spec_data, calibration_output_dir):

    mm_number = spec_data[0].info.mm_number
    timestamp_string = spec_data[0].info.timestamp_string
    num_pixels = spec_data[0].params.num_pix
    num_asics = 2
    num_tests = 2

    #  collect K-edge location data per test type
    Pb_edges = np.empty((2, num_pixels), dtype=int)
    Ce_edges = np.empty((2, num_pixels), dtype=int)
    for i in range(len(spec_data)):
        if spec_data[i].params.k_type == 'Pb':
            Pb_edges[spec_data[i].info.mm_asic, :] = spec_data[i].data.k_edge_location
        if spec_data[i].params.k_type == 'Ce':
            Ce_edges[spec_data[i].info.mm_asic, :] = spec_data[i].data.k_edge_location

    Pb_k_edge_keV = globals.pb_Edge
    Ce_k_edge_keV = globals.ce_Edge

    #  calculate gains and offsets
    gain_data = np.zeros((num_asics, num_pixels))
    offset_data = np.zeros((num_asics, num_pixels))
    for asic in range(num_asics):
        for pix in range(num_pixels):
            gain_data[asic, pix] = (Pb_k_edge_keV - Ce_k_edge_keV) / (Pb_edges[asic, pix] - Ce_edges[asic, pix])
            offset_data[asic, pix] = Pb_k_edge_keV - gain_data[asic, pix] * Pb_edges[asic, pix]

    cal_header = ['pixel', 'gain0', 'offset0', 'gain1', 'offset1']
    pixel_list = list(range(num_pixels))
    cal_data = zip(pixel_list, gain_data[0, :], offset_data[0, :], gain_data[1, :], offset_data[1, :])

    file_name = '{}_Energy_Cal_{}.csv'.format(mm_number, timestamp_string)
    cal_file = os.path.join(calibration_output_dir, file_name)
    with open(cal_file, 'wb') as cal_csv:
        writer = csv.writer(cal_csv)
        writer.writerow(cal_header)
        writer.writerows(cal_data)

    return cal_file
