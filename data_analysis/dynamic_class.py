########################################################################################################################
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
########################################################################################################################

import os
import csv
import numpy as np
from itertools import product
from matplotlib import pyplot as plt
from skimage.transform import rescale

import global_analysis_constants
from parent_classes import *


class DynamicTest(Asic):
    """
    Class for handling Dynamic Response test data
    calculates derivative data: Lag and OCR maps, pixel pitch comparisons, and descriptive statistics
    """
    def __init__(self, data_raw, params_raw, info_raw, current_output_dir, test_suite_dict):

        super(DynamicTest, self).__init__(data_raw, params_raw, info_raw, current_output_dir, test_suite_dict)

        ### take slice of 6 Coincidence Counters if all counters (EC, CC, SEC) were collected
        if self.data.cc_data.shape[0] == 13:
            self.data.cc_data = self.data.cc_data[6:12, :, :, :]

        #######################################################################
        #   initiate lag-specific paramters
        self.params.leading_edge_thresh = global_analysis_constants.LEADING_EDGE_THRESH
        self.params.leading_edge_advance = int(global_analysis_constants.LEADING_EDGE_ADVANCE / self.params.view_period)
        try:
            self.params.shutter_period = params_raw.shutter_on
        except AttributeError:
            self.params.shutter_period = global_analysis_constants.NOMINAL_SHUTTER_PERIOD

        self.params.views_per_shutter = int(self.params.shutter_period / self.params.view_period)
        self.params.lag_thresh = global_analysis_constants.LAG_THRESH
        self.params.lag_period_first = global_analysis_constants.LAG_PERIOD_FIRST
        self.params.lag_period_last = global_analysis_constants.LAG_PERIOD_LAST
        self.params.lag_views_first = int(self.params.lag_period_first /
                                          self.params.shutter_period / 1000 / self.params.view_period)
        self.params.lag_views_last = int(self.params.lag_period_last /
                                         self.params.shutter_period / 1000 / self.params.view_period)
        self.params.lag_range = global_analysis_constants.LAG_RANGE
        self.params.ncp_range = self.module.NCP_RANGE
        self.params.num_bins = np.size(self.data.cc_data, 0)  # number of bins from first dimension of data array
        self.params.num_views = np.size(self.data.cc_data, 1)  # number of views from second dimension of data array

        self.data.correction_area, self.params.nominal_pixel_pitch = self.get_pixel_pitch()
        self.params.pitch_comp = [global_analysis_constants.PITCH_COMP_INTERP[0], self.params.nominal_pixel_pitch * 2, self.params.nominal_pixel_pitch * 3]
        self.params.pitch_mode = global_analysis_constants.PITCH_MODE

        ########################################################################
        #
        cc_reshape = np.reshape(self.data.cc_data, (self.params.num_bins, self.params.num_views,
                                                    self.params.num_pix), order='F')
        cc_accum = self.make_open_bins(cc_reshape)

        ########################################################################
        #   initiate lag-specific data manipulations
        self.data.mean_sum_cc = np.nanmean(cc_accum[0, :, :], axis=1)
        self.data.cc_accum_window = self.make_shutter_window(cc_accum)
        #   calculate lag/ocr results
        self.data.ocr_mean, self.data.ocr_median, self.data.ocr_std, self.data.ocr_map = \
            self.calc_ocr()
        self.data.dyna_raw, self.data.dyna_map, self.data.dyna_mean, self.data.dyna_std, \
            self.data.dyna_raw_mean, self.data.dyna_raw_std = \
            self.calc_dyna_map()
        self.data.dyna_ncp = self.calc_ncp()
        self.data.dyna_ncp_count = np.count_nonzero(self.data.dyna_ncp == 1, axis=(1, 2))
        self.data.dyna_clipped, self.data.dyna_clipped_mean, self.data.dyna_clipped_std, self.data.dyna_clipped_ncp = \
            self.clip_dyna(dyna_raw=self.data.dyna_raw)
        self.data.pitch_comp, self.data.pitch_comp_mean, self.data.pitch_comp_std = \
            self.calc_pixel_pitch_comp(np.copy(self.data.dyna_map))

        self.export_dyna_pixel_table()
        self.export_dyna_stats_table()

        # delete heavy volume attributes after calculating derivative data
        del self.data.cc_data
        # del self.data.dyna_raw
        del self.data.cc_accum_window

    ####################################################################################################################
    #   derivative data methods
    ####################################################################################################################

    def get_pixel_pitch(self):
        """
        cacluate the nominal (average) pixel pitch using square root of mean pixel area
        """
        pixel_area = self.params.pixel_area
        nominal_pitch = int(np.sqrt(np.nanmean(pixel_area)) * 1000)

        return pixel_area, nominal_pitch

    def make_open_bins(self, cc_reshape):
        """
        create open bins by summing bins recursively
        """
        cc_accum = np.zeros(np.shape(cc_reshape))  # instantiate zero array
        for cc_bin in range(self.params.num_bins):
            cc_accum[cc_bin, :, :] = np.sum(cc_reshape[cc_bin:, :, :], axis=0)
        return cc_accum

    def make_shutter_window(self, cc_accum):
        """
        find leading edge and view window of first shutter opening
        """
        num_bins = self.params.num_bins

        cc_window_all_means = np.array([[i for i in np.squeeze(np.floor(np.nanmean(cc_accum[j, :, :], axis=1)))]
                                        for j in range(num_bins)])
        cc_window_max_mean = [np.nanmax(cc_window_all_means[cc_bin][:])
                              for cc_bin in range(num_bins)]
        cc_window_threshold = [(cc_window_max_mean[cc_bin] * self.params.leading_edge_thresh)
                               for cc_bin in range(num_bins)]
        #   find first value above threshold in sumcc
        cc_window_start = np.where(cc_window_all_means[0, :] > cc_window_threshold[0])[0][0]
        #   find first drop below window threshold
        cc_window_end = np.where(cc_window_all_means[0, cc_window_start:] < cc_window_threshold[0])[0][0]
        if cc_window_end < self.params.views_per_shutter:
            print('shutter window less than nominal: {} views'.format(cc_window_end))
        elif cc_window_end > self.params.views_per_shutter:
            print('shutter window greater than nominal: {} views'.format(cc_window_end))
            print('trimming shutter window to: {} views'.format(self.params.views_per_shutter))
            cc_window_end = self.params.views_per_shutter
        cc_accum_window = np.copy(cc_accum[:, cc_window_start:cc_window_start + cc_window_end, :])  # use first bin start (equivalent to "CC SUM")

        return cc_accum_window

    def calc_ocr(self):
        """
        calculate OCR by summing all views for each pixel per bin
        """
        ocr = np.zeros((self.params.num_bins, self.params.num_pix))
        for cc_bin in range(self.params.num_bins):
            ocr[cc_bin, :] = np.nansum(self.data.cc_accum_window[cc_bin, :, :], axis=0)
        ocr_mean = np.nanmean(ocr, axis=1)
        ocr_median = np.nanmedian(ocr, axis=1)
        ocr_std = np.nanstd(ocr, axis=1)
        ocr_map = np.reshape(ocr, (self.params.num_bins, self.params.num_rows, self.params.num_cols), order='F')

        return ocr_mean, ocr_median, ocr_std, ocr_map

    def calc_dyna_map(self):
        """
        calculate lag per pixel
        """
        dyna_raw = np.zeros((self.params.num_bins, self.params.num_pix))
        for cc_bin in range(self.params.num_bins):
            for pix in range(self.params.num_pix):
                lag_vec = self.data.cc_accum_window[cc_bin, :, pix]
                try:
                    dyna_raw[cc_bin, pix] = self.dyna_metrics(lag_vec, self.params.lag_thresh,
                                                              self.params.lag_views_first, self.params.lag_views_last)
                except:
                    dyna_raw[cc_bin, pix] = np.NaN

        dyna_map = np.reshape(np.copy(dyna_raw),
                              (self.params.num_bins,
                               self.params.num_rows,
                               self.params.num_cols),
                              order='F')
        dyna_map = self.dyna_replace_bds(dyna_map)

        dyna_mean = np.nanmean(dyna_map, axis=(1, 2))
        dyna_std = np.nanstd(dyna_map, axis=(1, 2))
        dyna_raw_mean = np.nanmean(dyna_raw, axis=1)
        dyna_raw_std = np.nanstd(dyna_raw, axis=1)

        return dyna_raw, dyna_map, dyna_mean, dyna_std, dyna_raw_mean, dyna_raw_std

    def calc_ncp(self):
        """calculate NCPs for dynamic response"""
        dyna_ncp = np.zeros((self.params.num_bins, self.params.num_pix))
        for cc_bin in range(self.params.num_bins):
            for pix in range(self.params.num_pix):
                if self.data.dyna_raw[cc_bin, pix] < self.params.ncp_range[0] or self.data.dyna_raw[cc_bin, pix] > self.params.ncp_range[1]:
                    dyna_ncp[cc_bin, pix] = 1
        # reshape back into 2D map arrays
        dyna_ncp = np.reshape(dyna_ncp, (self.params.num_bins, self.params.num_rows, self.params.num_cols))

        return dyna_ncp

    def clip_dyna(self, dyna_raw):
        """
        clip dyna_raw to dyna_range (as opposed to averaging neighbours)
        """
        dyna_clipped = np.zeros((self.params.num_bins, self.params.num_pix))
        dyna_clipped_ncp = np.zeros((self.params.num_bins, self.params.num_pix))
        for cc_bin in range(self.params.num_bins):
            for pix in range(self.params.num_pix):
                if dyna_raw[cc_bin, pix] < self.params.lag_range[0]:
                    dyna_clipped[cc_bin, pix] = self.params.lag_range[0]
                    dyna_clipped_ncp[cc_bin, pix] = 1
                elif dyna_raw[cc_bin, pix] > self.params.lag_range[1]:
                    dyna_clipped[cc_bin, pix] = self.params.lag_range[1]
                    dyna_clipped_ncp[cc_bin, pix] = 1
                else:
                    dyna_clipped[cc_bin, pix] = dyna_raw[cc_bin, pix]

        dyna_clipped_mean = np.nanmean(dyna_clipped, axis=1)
        dyna_clipped_std = np.nanstd(dyna_clipped, axis=1)

        return dyna_clipped, dyna_clipped_mean, dyna_clipped_std, dyna_clipped_ncp

    def dyna_metrics(self, dyna_vec, thresh, views_first_50ms, views_last_300ms):
        """
        calculates dynamic response per pixel
        dyna_vec: vector array of all views of single shutter exposure (e.g. 400) for a given pixel
        thresh: fixed count threshold for sensing leading edge of exposure (e.g. 300 counts)
        views_first_50s: number of views in the first 50ms
        views_last_300ms: number of views in the last 300ms
        """
        try:
            # find first count above threshold and advance designated number of views
            ind_start = np.where(dyna_vec > thresh)[0][0] + self.params.leading_edge_advance
            dyna_vec = dyna_vec[ind_start:]  # clip lag_vec to starting index
            dyna_per_pixel = 100 * (1 - (np.mean(dyna_vec[:views_first_50ms]) /
                                        np.mean(dyna_vec[(len(dyna_vec) - views_last_300ms):])))
        except:
            dyna_per_pixel = np.NaN
            # ind_start = np.NaN
        return dyna_per_pixel

    def dyna_replace_bds(self, dyna_map):
        """
        replace out of range and bad pixels with average of valid neighbours
        """
        lag_range = self.params.lag_range
        # filter matrix of relative index coordinates to 9 neighbouring pixels
        filt_mat = list(product([-1, 0, 1], repeat=2))
        num_rows = self.params.num_rows
        num_cols = self.params.num_cols
        num_bins = self.params.num_bins
        tile = np.copy(dyna_map)
        tile_replace = np.zeros([num_bins,  num_rows, num_cols])
        replace_mask = np.zeros([num_bins,  num_rows, num_cols])

        for cc_bin in range(num_bins):
            for row in range(num_rows):
                for col in range(num_cols):
                    collector = 0
                    num_coll = 0

                    # mask positions of bad values (out of range, or NaN)
                    if tile[cc_bin, row, col] <= lag_range[0] or tile[cc_bin, row, col] >= lag_range[1] \
                            or np.isnan(tile[cc_bin, row, col]):
                        replace_mask[cc_bin, row, col] = 1
                    else:
                        replace_mask[cc_bin, row, col] = 0

                    # use 3x3 filter matrix "filt_mat" to collect valid neighbouring values within lag_range
                    for filt in range(len(filt_mat)):
                        coll_pos = [row + filt_mat[filt][0], col + filt_mat[filt][1]]
                        if replace_mask[cc_bin, row, col] == 1 \
                                and 0 <= coll_pos[0] < num_rows and 0 <= coll_pos[1] < num_cols \
                                and lag_range[0] <= tile[cc_bin, coll_pos[0], coll_pos[1]] <= lag_range[1]:
                            collector += tile[cc_bin, coll_pos[0], coll_pos[1]]
                            num_coll += 1

                    # assign average of valid neighbours to temporary matrix
                    if replace_mask[cc_bin, row, col] == 1:
                        if collector != 0 and num_coll != 0:
                            tile_replace[cc_bin, row, col] = collector / num_coll
                        else:
                            tile_replace[cc_bin, row, col] = np.NaN

        # wholesale replacement of masked bad values with average
        np.putmask(tile, replace_mask, tile_replace)

        return tile

    def calc_pixel_pitch_comp(self, dyna_map):
        """
        create interpolated pitch comparisons
        """
        num_comps = len(self.params.pitch_comp)
        pitch_comp = [None] * num_comps
        pitch_comp_mean = [None] * num_comps
        pitch_comp_std = [None] * num_comps

        for comp in range(num_comps):
            ratio = float(self.params.nominal_pixel_pitch) / self.params.pitch_comp[comp]
            new_rows = int(np.round(ratio * self.params.num_rows))
            new_cols = int(np.round(ratio * self.params.num_cols))
            dyna_resamp = np.zeros((self.params.num_bins, new_rows, new_cols))

            if self.params.pitch_mode[comp] == 'interp':
                for cc_bin in range(self.params.num_bins):
                    dyna_resamp[cc_bin, :, :] = rescale(np.copy(dyna_map[cc_bin, :, :]), ratio,
                                                        multichannel=False, anti_aliasing=False, mode='constant')
            elif self.params.pitch_mode[comp] == 'manual':
                for cc_bin in range(self.params.num_bins):
                    dyna_resamp[cc_bin, :, :] = self.manual_pitch_calc(np.copy(dyna_map[cc_bin, :, :]),
                                                                       resample_pixels=int(1 / ratio))

            pitch_comp[comp] = dyna_resamp
            pitch_comp_mean[comp] = np.nanmean(dyna_resamp, axis=(1, 2))
            pitch_comp_std[comp] = np.nanstd(dyna_resamp, axis=(1, 2))

        return pitch_comp, pitch_comp_mean, pitch_comp_std

    def manual_pitch_calc(self, tile, resample_pixels):
        """
        manual resampling of pixel size by averaging integer pixel groups (e.g. 2x2, 3x3)
        """
        rows_orig = self.params.num_rows
        cols_orig = self.params.num_cols
        rows_new = int(rows_orig / resample_pixels)
        cols_new = int(cols_orig / resample_pixels)
        tile_resample = np.zeros([rows_new, cols_new])
        n = resample_pixels
        for row in range(rows_new):
            for col in range(cols_new):
                tile_resample[row, col] = np.nanmean(tile[((row+1) * n) - n: ((row+1) * n),
                                                     ((col+1) * n) - n: ((col+1) * n)])
        return tile_resample

    ####################################################################################################################
    #   table exporting methods
    ####################################################################################################################

    def export_dyna_stats_table(self):
        """
        write Dynamic Response statistical data to csv table
        """
        table_headers = []
        table_columns = []

        ###########################################################
        #   create column and header data for LAG data
        data_desc = ['raw', 'averaged', 'clipped']
        stats_key = ['mean', 'std']
        raw_stats = [self.data.dyna_raw_mean, self.data.dyna_raw_std]
        averaged_stats = [self.data.dyna_mean, self.data.dyna_std]
        clipped_stats = [self.data.dyna_clipped_mean, self.data.dyna_clipped_std]
        raw_dict = dict(zip(stats_key, raw_stats))
        averaged_dict = dict(zip(stats_key, averaged_stats))
        clipped_dict = dict(zip(stats_key, clipped_stats))
        data_dict = dict(zip(data_desc, [raw_dict, averaged_dict, clipped_dict]))

        for desc in data_desc:
            for cc_bin in range(self.params.num_bins):
                for stat in stats_key:
                    table_columns.append(data_dict[desc][stat][cc_bin])
                    header = 'dyna {} ({}) {}keV'.format(stat, desc, self.params.energy_kev[cc_bin])
                    table_headers.append(header)

        ###########################################################
        #   create column and header data for LAG NCP data
        for cc_bin in range(self.params.num_bins):
            table_columns.append(self.data.dyna_ncp_count[cc_bin])
            header = 'NCP count {}keV'.format(self.params.energy_kev[cc_bin])
            table_headers.append(header)

        file_name = '{}_DYNA_stats.csv'.format(self.info.device_ID)
        save_loc = os.path.join(self.info.save_location, file_name)

        with open(save_loc, 'wb') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(table_headers)
            writer.writerow(table_columns)

    def export_dyna_pixel_table(self):
        """
        write selected Dynamic Response array data to csv table
        """
        table_headers = []
        table_columns = []

        ###########################################################
        #   populate pixel number and area columns
        pixel_number = list(range(self.params.num_pix))
        table_columns.append(pixel_number)
        pixel_number_header = 'pixel'
        table_headers.append(pixel_number_header)

        pixel_area = list(self.data.correction_area.flatten())
        table_columns.append(pixel_area)
        pixel_area_header = 'pixel area (mm^2)'
        table_headers.append(pixel_area_header)

        ###########################################################
        #   create column and header data for LAG raw and corrected
        dyna_averaged_flat = np.reshape(np.copy(self.data.dyna_map),
                                        (self.params.num_bins,
                                        self.params.num_pix),
                                        order='F')

        data_desc = ['raw', 'averaged', 'clipped']
        data_members = [self.data.dyna_raw, dyna_averaged_flat, self.data.dyna_clipped]
        data_dict = dict(zip(data_desc, data_members))

        for desc in data_dict:
            for cc_bin in range(self.params.num_bins):
                table_columns.append(data_dict[desc][cc_bin, :])
                header = 'dyna ({}) {}keV'.format(desc, self.params.energy_kev[cc_bin])
                table_headers.append(header)

        ###########################################################
        #   create column and header data for LAG NCPs
        dyna_ncp_flat = np.reshape(np.copy(self.data.dyna_ncp),
                                   (self.params.num_bins,
                                    self.params.num_pix),
                                   order='F')

        ncp_desc = ['averaged', 'clipped']
        ncp_members = [dyna_ncp_flat, self.data.dyna_clipped_ncp]
        ncp_dict = dict(zip(ncp_desc, ncp_members))

        for desc in ncp_desc:
            for cc_bin in range(np.size(ncp_dict[desc], axis=0)):
                table_columns.append(ncp_dict[desc][cc_bin, :])
                header = 'NCP ({}) {}keV'.format(desc, self.params.energy_kev[cc_bin])
                table_headers.append(header)

        file_name = '{}_DYNA_pixel_data.csv'.format(self.info.device_ID)
        save_loc = os.path.join(self.info.save_location, file_name)

        with open(save_loc, 'wb') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(table_headers)
            writer.writerows(zip(*table_columns))

    ####################################################################################################################
    #   plotting methods
    ####################################################################################################################

    def plot_dyna_ocr(self):
        """
        plotting function for dynamic response and OCR
        """
        num_bins = self.params.num_bins
        plt.figure(figsize=(12.8, 7.8), dpi=100)
        fig_title = 'Dynamic Response / OCR  -  {}'.format(self.info.device_ID)
        plt.suptitle(fig_title, fontsize=9.5)

        for cc_bin in range(num_bins):
            tile_map = np.rot90(self.data.dyna_map[cc_bin, :, :], -1)
            hist_data = np.copy(self.data.dyna_clipped[cc_bin])
            hist_data = hist_data[~np.isnan(hist_data)]
            ocr_map = np.rot90(self.data.ocr_map[cc_bin, :, :], -1)
            plot_row = 0
            title_font_size = 7

            ###################################################
            #   Lag map
            plt.subplot(3, num_bins, cc_bin + 1 + (num_bins*plot_row))
            plt.imshow(tile_map, cmap='jet', vmin=-5, vmax=5)
            dyna_title_str = '> {}keV \n mean: {:.2f} \n std: {:.2f}'.format(self.params.energy_kev[cc_bin],
                                                                                 self.data.dyna_mean[cc_bin],
                                                                                 self.data.dyna_std[cc_bin])
            plt.colorbar()
            plt.title(dyna_title_str, fontsize=title_font_size)
            plt.tick_params(axis='both', which='major', tick1On=False, label1On=False)

            plot_row += 1

            ###################################################
            #   Lag histogram
            plt.subplot(3, num_bins, cc_bin + 1 + (num_bins*plot_row))
            plt.hist(hist_data, bins=40, range=[-5, 5], histtype='stepfilled', color='k')
            plt.axvline(0, color='r', linestyle='dashed', linewidth=1)
            dyna_title_str = 'range limited \n mean: {:.2f} \n std: {:.2f}'.format(self.data.dyna_clipped_mean[cc_bin],
                                                                                   self.data.dyna_clipped_std[cc_bin])
            plt.title(dyna_title_str, fontsize=title_font_size)
            plt.tick_params(axis='both', which='major', labelsize=6)

            plot_row += 1

            ###################################################
            #   OCR map
            plt.subplot(3, num_bins, cc_bin + 1 + (num_bins*plot_row))
            ocr_title_str = 'OCR \n median: {:.2e}'.format(self.data.ocr_median[cc_bin])
            plt.title(ocr_title_str, fontsize=title_font_size)
            plt.imshow(ocr_map, cmap='jet',
                       vmin=self.module.OCR_COLOR_RANGE[0],
                       vmax=self.module.OCR_COLOR_RANGE[1])
            plt.colorbar(format='%.0e')
            plt.tick_params(axis='both', which='major', tick1On=False, label1On=False)

        plt.tight_layout(rect=[0, 0, 1, 0.95])
        file_name = '{}_DYNA_OCR.png'.format(self.info.device_ID)
        save_loc = os.path.join(self.info.save_location, file_name)
        plt.savefig(save_loc)
        plt.close()

    def plot_pitch_comparison(self):
        """
        plot pixel pitch comparisons
        """
        title_font_size = 7
        plt.figure(figsize=(12.8, 7.8), dpi=100)
        fig_title = '{} - pixel pitch comparison'.format(self.info.device_ID)
        plt.suptitle(fig_title, fontsize=9.5)
        for cc_bin in range(self.params.num_bins):

            for comp in range(len(self.params.pitch_comp)):
                current_pitch_map = np.rot90(self.data.pitch_comp[comp][cc_bin, :, :], -1)
                plt.subplot(3, self.params.num_bins, cc_bin + 1 + (self.params.num_bins * comp))
                plt.imshow(current_pitch_map, cmap='jet',
                           vmin=global_analysis_constants.LAG_RANGE[0],
                           vmax=global_analysis_constants.LAG_RANGE[1])
                title_string = '{}$\mu$ pixel pitch \n mean: {:.2f} \n std: {:.2f}'.format(
                    self.params.pitch_comp[comp],
                    self.data.pitch_comp_mean[comp][cc_bin],
                    self.data.pitch_comp_std[comp][cc_bin])
                plt.colorbar()
                plt.title(title_string, fontsize=title_font_size)
                plt.tick_params(axis='both', which='major', tick1On=False, label1On=False)
        plt.tight_layout(rect=[0, 0, 1, 0.95])
        file_name = '{}_PITCH_COMP.png'.format(self.info.device_ID)
        save_loc = os.path.join(self.info.save_location, file_name)
        plt.savefig(save_loc)
        plt.close()

    def plot_dyna_ncp(self):
        """
        plot NCP for raw lag (lag values outside of bracketed range OR NaN)
        """
        title_font_size = 7
        plt.figure(figsize=(12.8, 7.8), dpi=100)
        fig_title = '{} - Dynamic Response NCPs (out of range {})'.format(self.info.device_ID, self.module.NCP_RANGE)
        plt.suptitle(fig_title, fontsize=9.5)
        for cc_bin in range(self.params.num_bins):
            current_ncp_map = np.rot90(self.data.dyna_ncp[cc_bin, :, :], -1)
            plt.subplot(2, self.params.num_bins / 2, cc_bin + 1)
            plt.imshow(current_ncp_map, cmap="gray_r")
            title_string = '{}keV\nNCP count: {}'.format(self.params.energy_kev[cc_bin],
                                                         self.data.dyna_ncp_count[cc_bin])
            plt.title(title_string, fontsize=title_font_size)
            plt.tick_params(axis='both', which='major', tick1On=False, label1On=False)
        plt.tight_layout(rect=[0, 0, 1, 0.95])
        file_name = '{}_DYNA_NCP.png'.format(self.info.device_ID)
        save_loc = os.path.join(self.info.save_location, file_name)
        plt.savefig(save_loc)
        plt.close()

    def plot_shutter(self):
        """
        plot all views for mean of all pixels shutter openeing/closing chunks
        """
        fig_title = '{} - Shutter Test\n(total mean all pixels, all bins)'.format(self.info.device_ID)
        title_font_size = 7
        plt.figure()
        plt.title(fig_title, fontsize=title_font_size)
        plt.plot(self.data.mean_sum_cc)
        file_name = '{}_shutter_window.png'.format(self.info.device_ID)
        save_loc = os.path.join(self.info.save_location, file_name)
        plt.savefig(save_loc)
        plt.close()
