########################################################################################################################
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
########################################################################################################################

import logging
import numpy as np
from time import time, sleep

from pcct.detector import KintexDmAdapter
from pcct.module import BaseModule

# Legacy test code
from DDC0864.dmFunctions import writeReg, readReg, is_set
from dmBuffer import DM


logger = logging.getLogger(__name__)


HVRegDict = {'HV0': {'LOAD_BIT': 5, 'DONE_BIT': 4, 'DAC_REG': '0x00017'},
             'HV1': {'LOAD_BIT': 2, 'DONE_BIT': 1, 'DAC_REG': '0x00018'}}


class CanonModule(BaseModule):
    MODULE_NAME = 'CMED'
    @staticmethod
    def area_correction():
        """
        set CMED metalized PIXEL size dimensions and calculate area and normalisation factors
        """
        cmed_x = np.zeros((24, 36), dtype=float)
        cmed_y = np.zeros((24, 36), dtype=float)

        #######################################
        #  Set all column dimensions
        cmed_x[:, 0] = 0.207  # first column
        cmed_x[:, 1:34] = 0.272  # bulk columns
        cmed_x[:, 34] = 0.257  # second last columns
        cmed_x[:, 35] = 0.172  # last columns

        #######################################
        #  Set all row dimensions
        cmed_y[(0, 23), :] = 0.285
        cmed_y[1:23, :] = 0.294

        #######################################
        #  Set fiducial pixel
        cmed_y[0, 0] = 0.207

        #######################################
        # create pixel area array
        cmed_pixel_area = cmed_x * cmed_y

        cmed_pixel_shape = np.dstack((cmed_x, cmed_y))

        #######################################
        # create area correction ratio
        ocr_correction = cmed_pixel_area[10, 16] / cmed_pixel_area  # (largest pixel / all other pixels)

        return ocr_correction, cmed_pixel_area, cmed_pixel_shape

    @staticmethod
    def area_correction_voxel():
        """
        set CMED VOXEL size dimensions and calculate area and normalisation factors
        """
        cmed_x = np.zeros((24, 36), dtype=float)
        cmed_y = np.zeros((24, 36), dtype=float)

        #######################################
        #  Set all column dimensions
        cmed_x[:, 0] = 0.292  # first column
        cmed_x[:, 1:34] = 0.342  # bulk columns
        cmed_x[:, 34] = 0.327  # second last columns
        cmed_x[:, 35] = 0.257  # last columns

        #######################################
        #  Set all row dimensions
        cmed_y[(0, 23), :] = 0.370
        cmed_y[1:23, :] = 0.364

        #######################################
        # create pixel area array
        cmed_pixel_area = cmed_x * cmed_y

        cmed_pixel_shape = np.dstack((cmed_x, cmed_y))

        #######################################
        # create area correction ratio
        ocr_correction = cmed_pixel_area[10, 16] / cmed_pixel_area  # (largest pixel / all other pixels)

        return ocr_correction, cmed_pixel_area, cmed_pixel_shape


class CanonDetector(KintexDmAdapter):
    CUSTOMER = 'Canon'

    MODULE_TYPE = CanonModule.MODULE_NAME
    N_MODULES = 8

    HV_STABILIZATION_TIME = 1.5

    BLUE_BOX_TEST_DISTANCE = 30
    BLUE_BOX_TEST_SLOTS = [3]
    YELLOW_BOX_TEST_SLOTS = [7]
    YELLOW_BOX_ACTIVE_ASICS = [15]
    YELLOW_BOX_PASSIVE_ASICS = [14]

    def setup(self):
        self.enable_adc_vref()
        self.enableHVMon(True)
        self.setHvState('off')

    def closeHv(self):
        pass

    def setHvVoltage(self, voltage):
        if voltage <= 1200:
            hv0Mon, hv1Mon = self.readHVMon()
            self.enableHVMon(True)
            logger.info('Setting HV Supply Voltage to {}V'.format(-voltage))
            self.setHV(voltage)
            self.restoreHVMon(hv0Mon, hv1Mon)
        else:
            logger.error('Invalid HV Voltage or voltage out of range' % voltage)

    def setHvState(self, state='off'):
        if state == 'on':
            self.enableHV(True)
            logger.info('Waiting {}s for HV to stabilize'.format(self.HV_STABILIZATION_TIME))
            sleep(self.HV_STABILIZATION_TIME)
        elif state == 'off':
            self.enableHV(False)
        else:
            raise ValueError('Invalid HV state: {}'.format(state))

    def readHvVoltage(self):
            hv1, hv2 = self.readHV()
            logger.info('HV1 Voltage: {}, HV2 Voltage: {}'.format(hv1, hv2))

    def enableHV(self, enable):
        if enable:
            writeReg('0x00016', 7, 7, '0x0001', 0, 1)  # Enables both HV Channels (Register address will change)
        else:
            writeReg('0x00016', 7, 7, '0x0000', 0, 1)

    def setHV(self, voltage, writeTimeOut=1):
        # voltage is in volts

        minVoltage = 0
        maxVoltage = 1270

        if not minVoltage <= voltage <= maxVoltage:
            ValueError('Voltage out of range. Please enter a voltage between {} and {}'.format(minVoltage, maxVoltage))

        HVDACSetting = int(round(float(voltage) / maxVoltage * (2 ** 12 - 1)))

        for hv in HVRegDict:
            writeReg(HVRegDict[hv]['DAC_REG'], 11, 0, hex(HVDACSetting), 0, 1)
            writeReg('0x00016', HVRegDict[hv]['LOAD_BIT'], HVRegDict[hv]['LOAD_BIT'], '0x0000', 0, 1)
            writeReg('0x00016', HVRegDict[hv]['LOAD_BIT'], HVRegDict[hv]['LOAD_BIT'], '0x0001', 0, 1)
            currentTime = time()
            while not is_set(int(DM.SPI_read('0x00016'), 0), HVRegDict[hv]['DONE_BIT']) and (time() - currentTime < writeTimeOut):
                pass

            if not is_set(int(DM.SPI_read('0x00016'), 0), HVRegDict[hv]['DONE_BIT']):
                raise RuntimeError('Write to {} DAC Timed Out'.format(hv))

            writeReg('0x00016', HVRegDict[hv]['LOAD_BIT'], HVRegDict[hv]['LOAD_BIT'], '0x0000', 0, 1)  # Must be dropped low to free SPI bus

    def readHV(self):
        HV0Voltage = int(round(float.fromhex(readReg(HVRegDict['HV0']['DAC_REG'], 0, 11, 0)) * 1270 / 4095))
        HV1Voltage = int(round(float.fromhex(readReg(HVRegDict['HV1']['DAC_REG'], 0, 11, 0)) * 1270 / 4095))
        return HV0Voltage, HV1Voltage

    def setHVPDown(self, setting, HVDACNum):
        if HVDACNum not in (0, 1):
            raise ValueError('Invalid HV DAC Number: {}'.format(HVDACNum))

        PDownReg = '0x00017' if HVDACNum == 0 else '0x00018'
        writeReg(PDownReg, 13, 12, hex(setting), 0, 1)

    def readHVPDown(self, HVDACNum):
        if HVDACNum not in (0, 1):
            raise ValueError('Invalid HV DAC Number: {}'.format(HVDACNum))

        PDownReg = '0x00017' if HVDACNum == 0 else '0x00018'
        return int(readReg(PDownReg, 0, 13, 12), 0)

    def enableHVMon(self, enable):
        if enable:
            writeReg('0x00016', 6, 6, '0x0001', 0, 1) # Enables both HV Channels (Register address will change)
            writeReg('0x00016', 3, 3, '0x0001', 0, 1)
        else:
            writeReg('0x00016', 6, 6, '0x0000', 0, 1)
            writeReg('0x00016', 3, 3, '0x0000', 0, 1)

    def readHVMon(self):
        hv0Mon = readReg('0x00016', 0, 3, 3)
        hv1Mon = readReg('0x00016', 0, 6, 6)
        return hv0Mon, hv1Mon

    def restoreHVMon(self, hv0Mon, hv1Mon):
        writeReg('0x00016', 3, 3, hv0Mon, 0, 1)
        writeReg('0x00016', 6, 6, hv1Mon, 0, 1)


PCCT_DETECTORS = {'Canon': CanonDetector}
PCCT_MODULES = {CanonModule.MODULE_NAME: CanonModule}
