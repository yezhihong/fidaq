import globals
## Created by Zhihong Ye 07/18/2019 to read in values from input files
## Need to define the global directionary, textModeSettings, in globals.py
## Make sure the keys in the input files match the definition of the directionary
def Load_Input(kFileName):
    with open(kFileName) as file:
        for line in file:
            if not line.startswith("#"):
                key, val = line.strip().split(':',1)
                if key=='ModuleIDs':
                    globals.textModeSettings[key] = val.split()
                    globals.textModeSettings[key] = [w.replace('NULL','') for w in globals.textModeSettings[key]]
                elif key=='IPPort':
                    globals.textModeSettings[key] = int(val.strip())
                else:
                    globals.textModeSettings[key] = val.strip()