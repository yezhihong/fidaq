########################################################################################################################
# (c) Copyright Redlen Technologies Inc. All rights reserved.
#
# This file is subject to the terms and conditions defined in the Joint Development Agreement as identified in the
# accompanying README file.
########################################################################################################################

import os
from collections import OrderedDict

# Temporary global class instances
buffer = None
detector = None

# directory strings ###############################################################
scratchDir = r'docs\csvData\multiFileAppend'        # scratch directory
calsDir = r'docs\deviceSpecificCals'                # calibration files directory
tdir = r'\\server1\X'                               # remote server director
ldir = r'C:\CTData'                                 # local directory
logdir = r'C:\CTData\Logs'                          # log files directory
regDumps = r'C:\CTData\regDumps'                    # register dumps
csvDir = r'docs\csvData'                            # csv data directory
docDir = r'docs'                                    # docs data directory
miscDir = r'C:\Temp\testMiscChain\docs\miscFiles'   # temporary for electrical repeatability investigation
energyCalFile = r'energyCal.csv'
testResultsFolder = 'DMTestResults'
chainToScratch = r'csvData\multiFileAppend'
nrgCalDir = 'energy'
DATA_OUTPUT_DIR = r'matData'
nrgDir = r'energy'
set18File = 'set18_chain_prog_vals_B.csv'
elecCalVisFolder = 'Visualizations'
nrgCalVisFolder = 'Visualizations'
elecCalTestVisFolder = r'{}\analysis results\ELECTRICAL'
nrgCalTestVisFolder = r'{}\analysis results\ENERGY'
ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
DUMP_DIR = r'C:\CTData\Dumps'

# version string ##################################################################
versionString = 'Functional Test V1.3.0 June 3, 2019'

# switches ########################################################################
miscVerify = True       # verify misc files on/off
threshVerify = False    # verify thresh files on/off
modeX = True            # modeX on/off - Not used anymore. Mode X on by default
storeRawCalData = True  # store Raw data on/off
qtrigPostCal = False
addCapPostCal = False
quantizerPostCal = False
saveA2DataFiles = True
check_isotope = True
# Skipping the register verification until we come to an agreement on the differences between our setup and the TI
# reference file
DO_GLOBAL_REGISTER_VERIFICATION = False
run_analysis = False
USE_DUMMY_FILTER_WHEEL = True
USE_DUMMY_HV = False
USE_DUMMY_LABJACK = True
USE_DUMMY_XRAY = True

# k-edge calibrations #####################################################################
pbKedgeName = 'xray-Pb-K-edge'
ceKedgeName = 'xray-CeO2-K-edge'
cuKedgeName = 'xray-1mmCu-K-edge'

# asic params #####################################################################
numRows = 24
numCols = 36
numPixels = 864
pixelPitch = 300e-6
VER_ID = 5

# check limits for deadtime calibration ###########################################
dt_FE_Min = 200     # lower limit for deadtime FE cal check
dt_FE_Max = 226     # upper limit for deadtime FE cal check
dt_BE_Min = 24      # lower limit for deadtime BE cal check
dt_BE_Max = 70      # upper limit for deadtime BE cal check
dt_BE2_Min = 5      # lower limit for deadtime BE2 cal check
dt_BE2_Max = 12     # upper limit for deadtime BE2 cal check

# chain register defaults #########################################################
startChain = 0
endChain = 11

# active leakage modes ############################################################
lkgActHigh = 'active2_high'
lkgActLow = 'active2_low'
lkgActNom = 'active2_nom'
lkgDisabled = 'dis_lkg'
lkgPassive = 'passive'

# k-edge values ###################################################################
pb_Edge = 88.004    # lead k-edge value in kev
ce_Edge = 40.445    # cerium k-edge value in kev

# global register read exclusion list #############################################
regExclList = [0x40, 0x41, 0x43]

# asic defaults ###################################################################
crystal_gain = 34.7e-18     # aC/kev
cdump = 24e-15              # 24fF dump capacitor

# qtrig gain dictionary ###########################################################
qtrigGainDict =    {'stdgain': {'a': {'gain': 2.0},
                                'b': {'gain': 2.4}},
                    'midgain': {'a': {'gain': 1.7},
                                'b': {'gain': 2.0}},
                    'higain':  {'a': {'gain': 1.4},
                                'b': {'gain': 1.7}}
                    }

# Test Dialog Dictionaries ###################################################################
testTypeDict = OrderedDict([('DM','dm'),
                            ('MM', 'mm'),
                            ('Sensor', 'sensor'),
                            ])

testSiteList = ['PTank_1', 'RedlenB', 'RedlenC', 'RedlenD', 'Other']
visualFolder = 'analysis results'

server = tdir
mfgDirectory = 'TEST LOG'

productDirDict = {testTypeDict['DM']: 'DETECTOR MODULE',
                  testTypeDict['MM']: 'MINI MODULE',
                  testTypeDict['Sensor']: 'SENSOR MODULE',}


dirStruct = {testTypeDict['DM']: {'perDevice': {'traveler': 'Traveler',
                                                'measurements': 'Measurement Data',
                                                'layout': 'Layout'},

                                  'perTest': {'calibrations': 'Calibrations',
                                              'rawData': 'Raw Test Data',
                                              'visualizations': 'Visualizations'}
                                  },

             testTypeDict['MM']: {'perDevice': {'traveler': 'Traveler',},

                                  'perTest': {'calibrations': 'Calibrations',
                                              'rawData': 'Raw Test Data',
                                              'visualizations': 'Visualizations'}
                                  },

             testTypeDict['Sensor']: {'perDevice': {'traveler': 'Traveler', },

                                      'perTest': {#'calibrations': 'Calibrations',
                                                  'rawData': 'Raw Test Data',
                                                  'visualizations': 'Visualizations'}
                                      }}

# Use this to enable the custom test suite GUI
enableTestSuiteGui = False

# Use these to enable dev mode so that test data isn't written to production
# As a bonus, no GUI is needed in dev mode
# However, there is no data validation so incorrect values will crash the program
enableDevMode = False
devModeSettings = {'DMType':'',                     # From pcct.detector_types()
                   'DMID': '',                      # The DM Serial Number. Ignored in MM and Sensor Testing
                   'TestType': testTypeDict['MM'],  # From global.testTypeDict. Mostly changes how test data is saved
                   'ModuleIDs':['',                 # Serial numbers of the MMs being tested. Top = Slot 0, Bottom = Slot 7
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                ''],
                   'CZTIDs': ['', '',  # Serial numbers of the MMs being tested. Top = Slot 0, Bottom = Slot 7
                              '', '',
                              '', '',
                              '', '',
                              '', '',
                              '', '',
                              '', '',
                              '', '',],
                   'Gain':'midgain',                # From DDC0864.baseline.gainDict. At the moment most tests are done at midgain
                   'TestSite': 'RedlenD',           # From globals.testSiteList
                   'ExperimentList': ['Energy Calibration'     # From DDC0864.baseline.experimentDict. Tests will be carried out in the order they are entered here
                                      ],
                   'Reason': 'New Ecal Algorithm',              # Enter a reason for the test. This reason will be added to the name of the folder where the test data is stored. Ignored if blank (not implemented)
                   'ActiveAsicNums': [],                        # Manually change ActiveAsicNums, ignored if left empty
                   'PassiveAsicNums': [],                       # Manually change PassiveAsicNums, ignored if left empty
                   'Directory': 'C:\\DEV TESTS\\'  # Edit this to change where Dev mode tests are saved
                   }

enableTextMode = True
textModeSettings = {'DMType':'',                     # From pcct.detector_types()
                   'DMID': '',                      # The DM Serial Number. Ignored in MM and Sensor Testing
                   'TestType': testTypeDict['MM'],  # From global.testTypeDict. Mostly changes how test data is saved
                   'ModuleIDs':['',                 # Serial numbers of the MMs being tested. Top = Slot 0, Bottom = Slot 7
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                ''],
                   'CZTIDs': ['', '',  # Serial numbers of the MMs being tested. Top = Slot 0, Bottom = Slot 7
                              '', '',
                              '', '',
                              '', '',
                              '', '',
                              '', '',
                              '', '',
                              '', '',],
                   'Gain':'midgain',                # From DDC0864.baseline.gainDict. At the moment most tests are done at midgain
                   'TestSite': 'RedlenD',           # From globals.testSiteList
                   'ExperimentList': ['Energy Calibration'     # From DDC0864.baseline.experimentDict. Tests will be carried out in the order they are entered here
                                      ],
                   'Purpose': '',                   # Enter a purpose for the test. This purpose will be added to cc_struct.info
                   'Reason': '',
                   'IPAddress': '192.168.1.25',                 # Manually change IP Address for this DMB
                   'IPPort': 4,                                 # Manually change IP Port for this DMB
                   'ActiveAsicNums': [],                        # Manually change ActiveAsicNums, ignored if left empty
                   'PassiveAsicNums': [],                       # Manually change PassiveAsicNums, ignored if left empty
                   'Directory': '', # Edit this to change where Text mode tests are saved
                   'SuiteName': '',  # Edit this to change the test suite name
                   'DataDir': ''
                   }


# Factory Tests ###################################################################
nrgCal = 'energycalibration'
unifTest = 'testitemid22_23_24_28crp_uniformity_background-ecal'
stsTest = 'testitemid30shorttermstability-ecal'
dynrespTest = 'testitemid30adynamicresponse-ecal'

# mutable values ##################################################################
sensorId = ''

# Essential registers dictionary (rev 5 asic only) ################################
essentialReg = {'essential1': {'addr': '0x66', 'msb': 15, 'lsb':15, 'val': '0x0001'},
                'essential2': {'addr': '0x61', 'msb': 15, 'lsb':15, 'val': '0x0001'},
                'essential3': {'addr': '0x89', 'msb': 4, 'lsb':4, 'val': '0x0001'},
                'essential4': {'addr': '0x13', 'msb': 15, 'lsb':15, 'val': '0x0001'},
                'essential5': {'addr': '0x4c', 'msb': 10, 'lsb': 9, 'val': '0x0001'},
                'essential6': {'addr': '0x4c', 'msb': 14, 'lsb':12, 'val': '0x0001'},
                'essential7': {'addr': '0x63', 'msb': 7, 'lsb':7, 'val': '0x0001'},
                'essential8': {'addr': '0x5f', 'msb': 9, 'lsb':8, 'val': '0x0003'},
                'essential9': {'addr': '0x5f', 'msb': 11, 'lsb':10, 'val': '0x0003'},
                'essential10': {'addr': '0x71', 'msb': 12, 'lsb':12, 'val': '0x0001'},
                'essential11': {'addr': '0x13', 'msb': 15, 'lsb':15, 'val': '0x0001'},
                'essential12': {'addr': '0x85', 'msb': 7, 'lsb':5, 'val': '0x0006'},
                'essential13': {'addr': '0x77', 'msb': 2, 'lsb':2, 'val': '0x0001'},
                'essential14': {'addr': '0x13', 'msb': 15, 'lsb':15, 'val': '0x0001'},
                'essential15': {'addr': '0x68', 'msb': 8, 'lsb':8, 'val': '0x0001'},
                }
essentialRegList_rev5 = ['essential1','essential2','essential3','essential4','essential5','essential6','essential7',
                         'essential8','essential9','essential10','essential12','essential13','essential15',]

# global registers ################################################################
globalReg = {'calibControl': {'addr': '0x33', 'bitDefs':{'adc_offset_calib_mode': [15, 15],
                                                         'deadtime_calib_mode': [12, 12],
                                                         'leakage_comp_mode': [9, 7],
                                                         'leakage_calib_mode': [4, 4],
                                                         'qdump_clk_sel': [1, 1],
                                                         'qdump_enable': [0, 0]}}}


# in progress #####################################################################
acsDict = { 'w' : '0x0000',
            '2w': '0x0001',
            '3w': '0x0002',
            '4w': '0x0003',
            '5w': '0x0004',
            '6w': '0x0005',
            '7w': '0x0006',
            '8w': '0x0007'}


