# Instruction to run redlen software for Windows (not tried on Linux yet but I think the software should also work under Linux).

## Preparation of required software
  * Install Python2.7 for Windoes 
    (Our team found out that directly installing python package could be better than installing anaconda or other similar package-management tools.)
  * Install Python Virtual-Environment under redlend_sw/ folder
     1) In python command windows, install virtual-environment tool by running: 
	            ```pip install virtualenv```
	 2) Run: 
           ```virtualenv.exe -r /PATH-TO/redlend_sw/env/``` 
	    It should create a folder named "env" which pretty much copy over all needed python libraries
	 3) Activate the virtual-environment by running	
	           ```./env/Script/activate.bat"``` 
	    from a windows command-window. (If using Powershell in Administrator mode, firstly run Set-ExecutionPolicy -ExecutionPolicy Unrestricted):
     4) Install required libraries (may also have to manually install other packages depending on how Python2.7 is installed)
	 	            ```pip install -r requirements.txt```
	 5) Install LabJack Driver (not install could throw out error messages but won't affect the excution of the code) for Windows 
	    from: https://labjack.com/support/software/examples/ud/labjackpython
		
## Prepare the files needed for one DMB:
     1) For each DMB, RedLen will provide a zip file named like "Dilivery_Report".
   	    The folder contains four subfolders for four individual ASICs, including their own electrical and energy calibration files.
		Unzip the files,copy these four subfolders (e.g., M20188-A0, M20188-A1, M20995-A0 and M20995-A1) 
	    into the folder: C:\CTData\${SITE}\docs\deviceSpecificCals\	
    2) Use the configuration example file to create a dedicated input file for the DMB. Please read the comments inside the file for how to make change. 
	3) Edit the file "globals.py", by setting the correct flags:
	     run_analysis = False      %%We analyze the data elsewhere so always False
		 USE_DUMMY_HV = False      %%False 
         USE_DUMMY_FILTER_WHEEL = True %%True for RedBox, False for FAT when using the Filters
		 USE_DUMMY_LABJACK = True  %%Don't know what it does so set it to "True"
         USE_DUMMY_XRAY = True     %%True if not use the FAT system that can control the X-Ray
	
## Then you should be able to excute the software by running under the command-window:
               ```python main.py config_example.txt```	    
        Also can edit and then directly click the excutable file "RUN" to start the software

	
